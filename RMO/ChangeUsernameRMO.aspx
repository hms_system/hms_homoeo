﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RMO/RMOMaster.master" AutoEventWireup="true" CodeFile="ChangeUsernameRMO.aspx.cs" Inherits="RMO_ChangeUsernameRMO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form runat="server">   
     <table>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style35">&nbsp;</td>
                <td class="auto-style29">&nbsp;</td>
                <td class="auto-style15">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="center">CHANGE USERNAME</td>
                <td class="auto-style21">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="label_small">Enter New UserName:</td>
                <td>
                    <asp:TextBox ID="txt_newusername" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_newusername" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td rowspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style30"></td>
                <td class="auto-style30"></td>
                <td class="label_small">Enter Password:</td>
                <td>
                    <asp:TextBox ID="txt_password" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style43">
                    <asp:Label ID="lbl_title" runat="server" CssClass="label_small"></asp:Label>
                </td>
                <td class="auto-style45">&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style40"><strong>
                    <asp:Button ID="btn_back" runat="server" OnClick="btn_back_Click" Text="<<BACK" CssClass="buttonyellow"  />
                    </strong></td>
                <td ><strong>
                    <asp:Button ID="btn_changeusername" runat="server" OnClick="btn_changeusername_Click" Text="CHANGE USERNAME" CssClass="buttonyellow"  />
                    </strong></td>
                <td></td>
            </tr>
        </table>
    </form>
</asp:Content>

