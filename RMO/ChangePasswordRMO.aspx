﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RMO/RMOMaster.master" AutoEventWireup="true" CodeFile="ChangePasswordRMO.aspx.cs" Inherits="RMO_ChangePasswordRMO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<title></title>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <table>
            <tr>
                <td class="auto-style21">
                    
                </td>
                <td class="auto-style21">
                    &nbsp;</td>
                <td class="auto-style21">
                    &nbsp;</td>
                <td class="center" colspan="2">
                    CHANGE YOUR PASSWORD</td>
                <td class="auto-style22">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style32">&nbsp;</td>
                <td class="auto-style32">&nbsp;&nbsp;&nbsp; &nbsp;</td>
                <td class="auto-style32">&nbsp;</td>
                <td class="auto-style37">
                   
                    <asp:Label ID="lbl_title" runat="server" CssClass="label_small"></asp:Label>
                </td>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style32">&nbsp;</td>
                <td class="auto-style32">&nbsp;</td>
                <td class="auto-style32">&nbsp;</td>
                <td class="label_small">
                    Enter Old Password:</td>
                <td ><asp:TextBox ID="txt_old" runat="server" TextMode="Password" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:Label ID="lbl_old" runat="server" CssClass="label_small"></asp:Label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style18">&nbsp;</td>
                <td class="auto-style18">&nbsp;</td>
                <td class="auto-style18">&nbsp;</td>
                <td class="label_small">Enter New Password:</td>
                <td class="auto-style30">
                    <asp:TextBox ID="txt_new" runat="server" TextMode="Password" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_new" ErrorMessage="*Please enter valid password" ForeColor="Red" ValidationExpression="[a-zA-Z0-9 ]{8,}" ValidationGroup="a"></asp:RegularExpressionValidator>

                </td>
            </tr>
            <tr>
                <td class="auto-style18">&nbsp;</td>
                <td class="auto-style18">&nbsp;</td>
                <td class="auto-style18">&nbsp;</td>
                <td class="label_small">Retype New Password:</td>
                <td class="auto-style30">
                    <asp:TextBox ID="txt_retype" runat="server" TextMode="Password" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:Label ID="lbl_retype" runat="server" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style32">&nbsp;</td>
                <td class="auto-style32"></td>
                <td class="auto-style32"></td>
                <td class="auto-style27"></td>
                <td class="auto-style30"></td>
                <td></td>
            </tr>
            <tr>
                <td class="auto-style32">
                    &nbsp;</td>
                <td class="auto-style32">
                    &nbsp;</td>
                <td class="auto-style32">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btn_back" runat="server" OnClick="btn_back_Click" Text="<<BACK"  CssClass="buttonyellow" />
                </td>
                <td>
                    <asp:Button ID="btn_chg" runat="server" OnClick="btn_chg_Click" Text="CHANGE PASSWORD" CssClass="buttonyellow" ValidationGroup="a" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
           
        </table>

    <div>
    
    </div>
        </form>
</asp:Content>

