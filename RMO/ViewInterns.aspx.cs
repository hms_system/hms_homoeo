﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class RMO_ViewInterns : System.Web.UI.Page
{
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
    
            btn_back1.Visible = false;
            GridView_interns.Visible = false;
            c.getcon();
            gridbind();
            c.con.Close();
        }



    }
   
    private void gridbind()
    {
        
       // string status = "Pending";
        SqlCommand cmd_intrn = new SqlCommand("select * from Interns_Details  where in_status='Pending'", c.con);
        DataTable dt_intrn = new DataTable();
        SqlDataAdapter da_intrn = new SqlDataAdapter(cmd_intrn);
        da_intrn.Fill(dt_intrn);


        if (dt_intrn.Rows.Count > 0)
        {
            MultiView1.ActiveViewIndex = 0;
            GridView_interns.DataSource = dt_intrn;
            GridView_interns.DataBind();
        }
        else
        {
            MultiView1.ActiveViewIndex = 1;
        }




    }


    protected void GridView_interns_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        c.getcon();
        if (e.CommandName == "SelectButton2")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_interns.Rows[index];

            SqlCommand cmd_update = new SqlCommand("update Interns_Details set in_status='Approved' where intern_id='" + row.Cells[0].Text + "'", c.con);
            cmd_update.ExecuteNonQuery();
            gridbind();
      


        }
        if (e.CommandName == "SelectButton3")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_interns.Rows[index];

            SqlCommand cmd_delete = new SqlCommand("delete  from Interns_Details where intern_id='" + row.Cells[0].Text + "'", c.con);
            cmd_delete.ExecuteNonQuery();
            gridbind();
        }
        if (e.CommandName == "SelectButton1")
        {
            MultiView1.ActiveViewIndex = 2;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_interns.Rows[index];
            SqlCommand cmd_bind = new SqlCommand("select * from Interns_Details where intern_id='" + row.Cells[0].Text + "' ", c.con);
           DataTable dt_bind = new DataTable();
        SqlDataAdapter da_bind = new SqlDataAdapter(cmd_bind);
        da_bind.Fill(dt_bind);
        if (dt_bind.Rows.Count > 0)
        {
            DataRow row_bind = dt_bind.Rows[dt_bind.Rows.Count - 1];

            String name = Convert.ToString(row_bind[1]);
            String address = Convert.ToString(row_bind[2]);
            
            DateTime dob = Convert.ToDateTime(row_bind[4]);
            String in_dob = dob.ToShortDateString();

            String email = Convert.ToString(row_bind[10]);
            String gen = Convert.ToString(row_bind[5]);
            
            DateTime jdate = Convert.ToDateTime(row_bind[8]);
            String in_jdate = jdate.ToShortDateString();

            String instaddr= Convert.ToString(row_bind[3]);
            String period= Convert.ToString(row_bind[9]);
            String nationality = Convert.ToString(row_bind[6]);
            String contact= Convert.ToString(row_bind[11]);
            String guardian= Convert.ToString(row_bind[7]);
           // String img = Convert.ToString(row_bind[13]);
           
            
            Label12.Text = name;
            Label13.Text=in_dob;
            Label14.Text =gen;
            Label15.Text=instaddr;
            Label17.Text=address;
            Label18.Text=guardian;
            Label19.Text=nationality;
            Label20.Text=in_jdate;
            Label21.Text=period;
            Label22.Text=email;
            Label23.Text=contact;
           // Image3.ImageUrl= "~/Uploads/" + img;
            
            }
           
        }
        c.con.Close();
    }
    protected void btn_aprvd_Click(object sender, EventArgs e)
    {
     
        btn_back1.Visible = true;
        GridView_interns.Visible = true;


    }
    protected void btn_back1_Click(object sender, EventArgs e)
    {
        Response.Redirect("RMOHome.aspx");
    }

    protected void btn_back_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    
    }
}