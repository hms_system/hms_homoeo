﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class RMO_RMOInternSchedule : System.Web.UI.Page
{
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            c.getcon();
            SqlCommand cmd_dateout = new SqlCommand("delete from Intern_Schedule where dateout<Convert(datetime,'" + DateTime.Today + "',103)", c.con);
            cmd_dateout.ExecuteNonQuery();

            bindintern();
            bindtimeslot();
            gridbind();
            c.con.Close();

        }
    }
    public void bindintern()
    {

        string str_in_name = "select * from Interns_details where intern_id not in (select internid from Intern_Schedule where datein  in (Convert(datetime,'" + DateTime.Today + "',103)) )";
        SqlCommand cmd_in_name = new SqlCommand(str_in_name, c.con);
        SqlDataAdapter da_in_name = new SqlDataAdapter(cmd_in_name);
        DataTable dt_in_name = new DataTable();
        da_in_name.Fill(dt_in_name);
        if (dt_in_name.Rows.Count > 0)
        {
            ddl_intern_name.DataSource = dt_in_name;
            ddl_intern_name.DataValueField = "intern_id";
            ddl_intern_name.DataTextField = "intern_name";
            ddl_intern_name.DataBind();
            ddl_intern_name.Items.Insert(0, "SELECT");
        }

    }
    public void bindtimeslot()
    {
        string str_time = "select * from Time_SlotMaster";
        SqlCommand cmd_time = new SqlCommand(str_time, c.con);
        SqlDataAdapter da_time = new SqlDataAdapter(cmd_time);
        DataTable dt_time = new DataTable();
        da_time.Fill(dt_time);
        if (dt_time.Rows.Count > 0)
        {
            ddl_timeslot.DataSource = dt_time;
            ddl_timeslot.DataValueField = "time_id";
            ddl_timeslot.DataTextField = "timeslot";
            ddl_timeslot.DataBind();
            ddl_timeslot.Items.Insert(0, "SELECT");
        }

    }
    public void gridbind()
    {

        string str_intern_schedule = "select s.intern_schedule_id, i.intern_id,i.intern_name ,t.timeslot,s.datein,s.dateout from Interns_Details i inner join Intern_Schedule s on i.intern_id=s.internid inner join Time_SlotMaster t on t.time_id=s.time_id ";
        SqlCommand cmd_intern_schedule = new SqlCommand(str_intern_schedule, c.con);
        DataTable dt_intern_schedule = new DataTable();
        SqlDataAdapter da_intern_schedule = new SqlDataAdapter(cmd_intern_schedule);
        da_intern_schedule.Fill(dt_intern_schedule);
        if (dt_intern_schedule.Rows.Count > 0)
        {
            lbl_no.Visible = false;
            GridView1.Visible = true;

            GridView1.DataSource = dt_intern_schedule;
            GridView1.DataBind();
        }
        else
        {
            lbl_no.Visible = true;
            GridView1.Visible = false;
        }

    }

    protected void btn_add_doc_schedule_Click(object sender, EventArgs e)
    {
        c.getcon();
        DateTime dateout = Convert.ToDateTime(txt_date.Text).AddDays(7);

        string intern_schedule = "insert into Intern_Schedule values(Convert(datetime,'" + dateout.Date + "',103),Convert(datetime,'" + DateTime.Now + "',103),'" + ddl_timeslot.SelectedItem.Value + "','" + ddl_intern_name.SelectedItem.Value + "')";
        SqlCommand cmd_intern_schedule = new SqlCommand(intern_schedule, c.con);
        cmd_intern_schedule.ExecuteNonQuery();

        gridbind();
        bindintern();
        bindtimeslot();
        c.con.Close();
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        c.getcon();
        if (e.CommandName == "SelectButton2")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];

            // int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0].ToString());
            string str_dr_del = "delete from Intern_Schedule where intern_schedule_id='" + row.Cells[0].Text + "'";

            SqlCommand cmd_dr_del = new SqlCommand(str_dr_del, c.con);
            cmd_dr_del.ExecuteNonQuery();
            GridView1.EditIndex = -1;

            gridbind();
            bindintern();
            bindtimeslot();
            c.con.Close();
        }
    }
}