﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class RMO_ChangeUsernameRMO : System.Web.UI.Page
{
    Connection c= new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;

    }
    protected void btn_changeusername_Click(object sender, EventArgs e)
    {
c.getcon();

        string name = Session["id"].ToString();

        string str_username = "select * from Login where username= '" + txt_newusername.Text + "' ";
        SqlCommand cmd_username = new SqlCommand(str_username, c.con);
        SqlDataAdapter da_username = new SqlDataAdapter(cmd_username);
        DataTable dt_username = new DataTable();
        da_username.Fill(dt_username);

        int k = cmd_username.ExecuteNonQuery();

        if (dt_username.Rows.Count > 0)
        {
            Response.Write("<script>alert('Username Already Exists');</script>");
        }
        else
        {
            string str_usr = "select * from Login where empid= '" + name + "' and password='" + txt_password.Text + "'";
            SqlCommand cmd_usr = new SqlCommand(str_usr, c.con);
            SqlDataAdapter da_usr = new SqlDataAdapter(cmd_usr);
            DataTable dt_usr = new DataTable();
            da_usr.Fill(dt_usr);

            int ks = cmd_usr.ExecuteNonQuery();

            if (dt_usr.Rows.Count > 0)
            {
                String update_usr = "update Login set username='" + txt_newusername.Text + "' where empid='" + name + "'";
                SqlCommand cmd_update_usr = new SqlCommand(update_usr, c.con);
                cmd_update_usr.ExecuteNonQuery();
                Response.Write("<script>alert('Username Changed Successfully');</script>");

                string select_usr = "select * from Login where username= '" + txt_newusername.Text + "' ";
                SqlCommand cmd_select_usr = new SqlCommand(select_usr, c.con);
                SqlDataAdapter da_select_usr = new SqlDataAdapter(cmd_select_usr);
                DataTable dt_select_usr = new DataTable();
                da_select_usr.Fill(dt_select_usr);

                int k1 = cmd_select_usr.ExecuteNonQuery();

                if (dt_select_usr.Rows.Count > 0)
                {
                    DataRow row11 = dt_select_usr.Rows[dt_select_usr.Rows.Count - 1];
                    string user = Convert.ToString(row11[1]);
                    lbl_title.Text = "New Username - " + user;
                    txt_newusername.Text = " ";
                    Session["id"] = user;
                }
            }

            else
                Response.Write("<script>alert('Incorrect Password');</script>");
        }
        txt_password.Text = " ";
        txt_newusername.Text = " ";

        c.con.Close();

    }
    protected void btn_back_Click(object sender, EventArgs e)
    {
        Response.Redirect("RMOHome.aspx");
    }
}
