﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RMO/RMOMaster.master" AutoEventWireup="true" CodeFile="HeadNurseSchedule.aspx.cs" Inherits="RMO_HeadNurseSchedule" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<form runat="server">
    <table align="center">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="7">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="lbl_doc_name" runat="server" Text="Name:" CssClass="label_small"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddl_Hnurse_name" runat="server" AutoPostBack="True" CssClass="search_categories" style="left: 4px; top: 0px">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_oproom_no" runat="server" Text="Time Slot:" CssClass="label_small"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddl_timeslot" runat="server" AutoPostBack="True"  CssClass="search_categories" style="left: -4px; top: 0px">
                </asp:DropDownList>
               
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Date" CssClass="label_small"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_date" runat="server"  CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <cc1:CalendarExtender ID="txt_date_CalendarExtender" runat="server" Format="dd/MM/yyyy" Enabled="True" TargetControlID="txt_date">
                </cc1:CalendarExtender>
            </td>
            <td>&nbsp;
                </td>


            <td>
               
                <asp:Button ID="btn_add_doc_schedule" runat="server" OnClick="btn_add_doc_schedule_Click" Text="ADD" CssClass="buttonyellow" />
            
            </td>
        </tr>
        <tr>
            <td class="auto-style1"></td>
            <td class="auto-style1">
                </td>
            <td colspan="7" class="auto-style1">
                </td>
            <td class="auto-style1"></td>
            <td class="auto-style1"></td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style1">
                &nbsp;</td>
            <td>
             <asp:Label ID="lbl_no" runat="server" CssClass="label_small" ForeColor="Red" Text="ADD NEW SCHEDULE!!!" Visible="False" Width="300px"></asp:Label></td>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style1">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1"></td>
            <td class="auto-style1"></td>
            <td colspan="7" class="auto-style1">
                <asp:GridView ID="GridView1" runat="server" CellPadding="2"  AutoGenerateColumns="False"  ForeColor="Black" GridLines="None" DataKeyNames="hnurse_schedule_id" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" >
                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    <Columns>
                        <asp:BoundField DataField="hnurse_schedule_id" HeaderText="NO" />
                        <asp:BoundField DataField="empname" HeaderText="Name" />
                        <asp:BoundField DataField="timeslot" HeaderText="Time Slot" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/RMO/images/delete.png" OnClick="ImageButton2_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/delete.png" />
                    </EmptyDataTemplate>
                    <FooterStyle BackColor="Tan" />
                    <HeaderStyle BackColor="Tan" Font-Bold="True" />
                    <PagerStyle ForeColor="DarkSlateBlue" HorizontalAlign="Center" BackColor="PaleGoldenrod" />
                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                    <SortedAscendingCellStyle BackColor="#FAFAE7" />
                    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                    <SortedDescendingCellStyle BackColor="#E1DB9C" />
                    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                </asp:GridView>
            </td>
            <td class="auto-style1"></td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="7">
                &nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
        </tr>
        
      
    </table>
    </form>
</asp:Content>

