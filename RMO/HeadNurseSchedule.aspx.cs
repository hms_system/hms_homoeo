﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class RMO_HeadNurseSchedule : System.Web.UI.Page
{
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            c.getcon();
            SqlCommand cmd_dateout = new SqlCommand("delete from HeadNurse_Schedule where dateout<Convert(datetime,'" + DateTime.Today + "',103)", c.con);
            cmd_dateout.ExecuteNonQuery();

            bindnurse();
            bindtimeslot();
            gridbind();
            c.con.Close();
        }
    }
    public void bindnurse()
    {
        
        string str_nurse_name = "select * from Employee_details where empid not in (select hnurse_empid from HeadNurse_Schedule where datein  in (Convert(datetime,'" + DateTime.Today + "',103)) )and typeid='2'";
        SqlCommand cmd_nurse_name = new SqlCommand(str_nurse_name, c.con);
        SqlDataAdapter da_nurse_name = new SqlDataAdapter(cmd_nurse_name);
        DataTable dt_nurse_name = new DataTable();
        da_nurse_name.Fill(dt_nurse_name);
        if (dt_nurse_name.Rows.Count > 0)
        {

            ddl_Hnurse_name.DataSource = dt_nurse_name;
            ddl_Hnurse_name.DataValueField = "empid";
            ddl_Hnurse_name.DataTextField = "empname";
            ddl_Hnurse_name.DataBind();
            ddl_Hnurse_name.Items.Insert(0, "SELECT");
        }
        
    }
    public void bindtimeslot()
    {
     
        string str_time = "select * from Time_SlotMaster";
        SqlCommand cmd_time = new SqlCommand(str_time, c.con);
        SqlDataAdapter da_time = new SqlDataAdapter(cmd_time);
        DataTable dt_time = new DataTable();
        da_time.Fill(dt_time);
        if (dt_time.Rows.Count > 0)
        {
            ddl_timeslot.DataSource = dt_time;
            ddl_timeslot.DataValueField = "time_id";
            ddl_timeslot.DataTextField = "timeslot";
            ddl_timeslot.DataBind();
            ddl_timeslot.Items.Insert(0, "SELECT");
        }
     
    }
    public void gridbind()
    {

       


        String str_nurse_schedule = "select hn.hnurse_schedule_id, e.empid,e.empname ,t.timeslot from Employee_Details e inner join HeadNurse_Schedule hn on e.empid=hn.hnurse_empid inner join Time_SlotMaster t on t.time_id=hn.time_id where typeid='2'";
       // hn.datein,hn.dateout 
        SqlCommand cmd_nurse_schedule = new SqlCommand(str_nurse_schedule, c.con);
        DataTable dt_nurse_schedule = new DataTable();
        SqlDataAdapter da_nurse_schedule = new SqlDataAdapter(cmd_nurse_schedule);
        da_nurse_schedule.Fill(dt_nurse_schedule);
        if (dt_nurse_schedule.Rows.Count > 0)
        {
            //DataRow row1 = dt_nurse_schedule.Rows[dt_nurse_schedule.Rows.Count - 1];

            //DateTime dtt = Convert.ToDateTime(row1[5]);
            //String dtin = dtt.ToShortDateString();
           
            lbl_no.Visible = false;
            GridView1.Visible = true;
            GridView1.DataSource = dt_nurse_schedule;
            GridView1.DataBind();
        }
        else
        {
            lbl_no.Visible = true;
            GridView1.Visible = false;
        }
     

    }

    protected void btn_add_doc_schedule_Click(object sender, EventArgs e)
    {
        c.getcon();
        
        DateTime dateout = Convert.ToDateTime(txt_date.Text).AddDays(7);
       
        string HNurse_schedule = "insert into HeadNurse_Schedule values(Convert(datetime,'" +  dateout + "',103),Convert(datetime,'" + txt_date.Text + "',103),'" + ddl_timeslot.SelectedItem.Value + "','" + ddl_Hnurse_name.SelectedItem.Value + "')";
        SqlCommand cmd_HNurse_schedule = new SqlCommand(HNurse_schedule, c.con);
        cmd_HNurse_schedule.ExecuteNonQuery();
        
        gridbind();
        bindnurse();
        bindtimeslot();
        c.con.Close();
    }
  

    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        c.getcon();
        ImageButton im = (ImageButton)sender;
        GridViewRow grv = im.NamingContainer as GridViewRow;
        int id = Convert.ToInt32(GridView1.DataKeys[grv.RowIndex].Values[0].ToString());

        string str_dr_del = "delete from HeadNurse_Schedule where hnurse_schedule_id='" + id + "'";

        SqlCommand cmd_dr_del = new SqlCommand(str_dr_del, c.con);
        cmd_dr_del.ExecuteNonQuery();
        GridView1.EditIndex = -1;
        gridbind();
        bindnurse();
        bindtimeslot();
        c.con.Close();
       
    }
}