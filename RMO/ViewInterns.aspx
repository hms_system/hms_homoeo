﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RMO/RMOMaster.master" AutoEventWireup="true" CodeFile="ViewInterns.aspx.cs" Inherits="RMO_ViewInterns" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title></title>
    
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
    <table>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            </tr>
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                         
                        <table>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <asp:Button ID="btn_aprvd" runat="server" CssClass="buttonyellow" Text="Approve Interns" OnClick="btn_aprvd_Click" />
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <asp:GridView ID="GridView_interns" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" OnRowCommand="GridView_interns_RowCommand" Width="817px">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                        <Columns>
                                            <asp:BoundField DataField="intern_id" HeaderText="Intern Id" />
                                            <asp:BoundField DataField="intern_name" HeaderText="Name" />
                                            <asp:BoundField DataField="in_address" HeaderText="Address" />
                                            <asp:BoundField DataField="in_institute" HeaderText="Institute Name" />
                                            <asp:BoundField DataField="in_joindate" HeaderText="Join Date" />
                                            <asp:TemplateField>  
                            <ItemTemplate>  
                                <asp:Button ID="btn_view" runat="server" Width="90" Text="View Profile" CommandName="SelectButton1"  CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />  
                            </ItemTemplate> 
                             
                        </asp:TemplateField>  
                         <asp:TemplateField>  
                            <ItemTemplate>  
                                <asp:Button ID="btn_approve" runat="server" Width="80" Text="Approve" CommandName="SelectButton2"  CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />  
                            </ItemTemplate> 
                             
                        </asp:TemplateField>  
                       
                                             <asp:TemplateField>  
                            <ItemTemplate>  
                                <asp:Button ID="btn_delete" runat="server" Width="60" Text="Delete" CommandName="SelectButton3"  CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />  
                            </ItemTemplate> 
                             
                        </asp:TemplateField>  
                       
                                        </Columns>
                                        <EditRowStyle BackColor="#999999" />
                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                    </asp:GridView>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>&nbsp;</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:Button ID="btn_back1" runat="server" CssClass="buttonyellow" OnClick="btn_back1_Click" Text="<<BACK" />
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>

                    
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table class="auto-style2">
                            <tr>
                                <td>
                                    <asp:Label ID="lbl_no_msg" runat="server" CssClass="label_small" ForeColor="Red" Text="No New Records!!!!!....." Width="200px"></asp:Label>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <table>
            <tr>
                <td class="auto-style3"></td>
                <td class="auto-style3"></td>
                <td class="auto-style3">
                    </td>
                <td>
                    <%--<asp:Image ID="Image3" runat="server" Height="100px" Width="120px" />--%>
                </td>
                <td class="auto-style3"></td>
            </tr>
           
            <tr>
                <td></td>
                <td> 
                    <asp:Label ID="Label1" runat="server" CssClass="label_small" Text="Name"></asp:Label>
                </td>
                <td>
                   
                        <asp:Label ID="Label12" runat="server" CssClass="label_black"></asp:Label>
                   
                        </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label2" runat="server" CssClass="label_small" Text="DOB"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label13" runat="server" CssClass="label_black"></asp:Label>
                </td>
            <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label3" runat="server" CssClass="label" Text="Gender"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label14" runat="server" CssClass="label_black"></asp:Label>
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label4" runat="server" CssClass="label_small" Text="Name &amp; Address of Institute"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label15" runat="server" CssClass="label_black"></asp:Label>
                </td>
                <td>                    
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                   </td>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label5" runat="server" CssClass="label_small" Text="Address"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label17" runat="server" CssClass="label_black"></asp:Label>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="Label6" runat="server" CssClass="label_small" Text="Name of Gaurdian"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label18" runat="server" CssClass="label_black"></asp:Label>
                </td>
                <td>

                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label7" runat="server" CssClass="label_small" Text="Nationality"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label19" runat="server" CssClass="label_black"></asp:Label>
                </td>
                <td>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="Label8" runat="server" CssClass="label_small" Text="Date of Joining"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label20" runat="server" CssClass="label_black"></asp:Label>
                </td>
                <td>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label9" runat="server" CssClass="label_small" Text="Period of Internship"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label21" runat="server" CssClass="label_black"></asp:Label>
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label10" runat="server" CssClass="label_small" Text="Email Id"></asp:Label>
                </td>
                <td>
                    
                    <asp:Label ID="Label22" runat="server" CssClass="label_black"></asp:Label>
                    
                </td>
                <td>
                   </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="Label11" runat="server" CssClass="label_small" Text="Contact No"></asp:Label>
                </td>
                <td>
                                  <asp:Label ID="Label23" runat="server" CssClass="label_black"></asp:Label>
                                  </td>
                <td> 
                    
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td>
                   
                    <asp:Button ID="btn_back" runat="server" Text="BACK" OnClick="btn_back_Click"  CssClass="buttonyellow" />
                     </td>
                <td>
                    
                </td>
                <td>
                    
                </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="left">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="left">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
                    </asp:View>
                </asp:MultiView>
                </table>
        </form>
</asp:Content>

