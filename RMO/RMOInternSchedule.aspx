﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RMO/RMOMaster.master" AutoEventWireup="true" CodeFile="RMOInternSchedule.aspx.cs" Inherits="RMO_RMOInternSchedule" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <center>
    <table>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_doc_name" runat="server" Text="Name:" CssClass="label_small" ></asp:Label>
            </td>
            <td >
                <asp:DropDownList ID="ddl_intern_name" runat="server" AutoPostBack="True"  CssClass="search_categories" >
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_oproom_no" runat="server" Text="Time Slot:" CssClass="label_small"></asp:Label>
                <asp:DropDownList ID="ddl_timeslot" runat="server" AutoPostBack="True" CssClass="search_categories">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td >
                &nbsp;</td>
            <td ">
                <strong>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label1" runat="server" Text="Date" style="color: #000099" CssClass="label_small"></asp:Label>
                </strong>
            </td>
            <td class="auto-style21">
                <asp:TextBox ID="txt_date" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                <cc1:CalendarExtender ID="txt_date_CalendarExtender" runat="server"  Format="dd/MM/yyyy" Enabled="True" TargetControlID="txt_date">
                </cc1:CalendarExtender>
            </td>
            <td class="auto-style21">
                &nbsp;</td>
            <td>
                
                <asp:Button ID="btn_add_doc_schedule" runat="server" OnClick="btn_add_doc_schedule_Click" Text="ADD" CssClass="buttonyellow" />
                
            </td>
            <td class="auto-style21">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style39" style="width: 246px; height: 32px;">&nbsp;</td>
            <td class="auto-style39" style="width: 246px; height: 32px;"></td>
            <td class="auto-style24" style="height: 32px">
                </td>
            <td style="width: 179px; height: 32px;">
                &nbsp;</td>
            <td style="width: 179px; height: 32px;" colspan="2">
                <asp:Label ID="lbl_no" runat="server" CssClass="label_small" ForeColor="Red" Text="ADD NEW SCHEDULE!!!" Visible="False" Width="300px"></asp:Label></td>
            <td class="auto-style43" style="width: 186px; height: 32px;"></td>
            <td class="auto-style27" style="height: 32px"><span style="color: #000099">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></td>
            <td class="auto-style27" style="height: 32px"></td>
            <td class="auto-style27" style="height: 32px"></td>
            <td class="auto-style27" style="height: 32px"></td>
            <td class="auto-style27" style="height: 32px"></td>
        </tr>
        <tr>
            <td class="auto-style38" style="width: 246px">&nbsp;</td>
            <td class="auto-style38" style="width: 246px">&nbsp;</td>
            <td class="auto-style37">&nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="3">
                <asp:GridView ID="GridView1" runat="server" CellPadding="2" Width="420px" AutoGenerateColumns="False"  ForeColor="Black" GridLines="None" Height="74px" DataKeyNames="intern_schedule_id" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" OnRowCommand="GridView1_RowCommand">
                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    <Columns>
                        <asp:BoundField DataField="intern_id" HeaderText="NO" />
                        <asp:BoundField DataField="intern_name" HeaderText="Name" />
                        <asp:BoundField DataField="timeslot" HeaderText="Time Slot" />
                        <asp:TemplateField>  
                            <ItemTemplate>  
                                <asp:Button ID="btn_delete" runat="server" Width="60" Text="Delete" CommandName="SelectButton1"  CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />  
                            </ItemTemplate> 
                             
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="Tan" />
                    <HeaderStyle BackColor="Tan" Font-Bold="True" />
                    <PagerStyle ForeColor="DarkSlateBlue" HorizontalAlign="Center" BackColor="PaleGoldenrod" />
                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                    <SortedAscendingCellStyle BackColor="#FAFAE7" />
                    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                    <SortedDescendingCellStyle BackColor="#E1DB9C" />
                    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                </asp:GridView>
            </td>
            <td>&nbsp;</td>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style38" style="width: 246px">&nbsp;</td>
            <td class="auto-style38" style="width: 246px">&nbsp;</td>
            <td class="auto-style37">&nbsp;</td>
            <td style="width: 179px">&nbsp;</td>
            <td style="width: 179px" colspan="2">&nbsp;</td>
            <td class="auto-style42" style="width: 186px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
        </center>
   </form>
</asp:Content>

