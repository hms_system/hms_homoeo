﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using System.IO;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
public partial class RMO_PrintInternList : System.Web.UI.Page
{
    Connection c = new Connection();
    DataTable tb = new DataTable();
    DataRow dr;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            c.getcon();
            createtable();
            c.con.Close();
        }

    }
    private void showPdf(string strS)
    {
        Response.ClearContent();
        Response.ClearHeaders();
        Response.ContentType = "application/pdf";
        Response.AddHeader("Content-Dispostion", "attachment;filename=" + strS);
        Response.TransmitFile(strS);
        Response.End();
        Response.Flush();
        Response.Clear();
    }

    public void createtable()
    {
        int i;
        tb.Columns.Add("Intern ID", typeof(string));
        tb.Columns.Add("Name", typeof(string));
        tb.Columns.Add("Institute", typeof(string));
        tb.Columns.Add("Internship Period", typeof(string));

        SqlCommand cmd_table = new SqlCommand("select * from Interns_Details", c.con);
        SqlDataAdapter da_table = new SqlDataAdapter(cmd_table);
        DataTable dt_table = new DataTable();
        da_table.Fill(dt_table);
        int count = dt_table.Rows.Count;
        if (dt_table.Rows.Count > 0)
        {
            for (i = count; i > 0; i--)
            {

                DataRow row_table = dt_table.Rows[dt_table.Rows.Count - i];
                dr = tb.NewRow();
                dr["Intern ID"] = Convert.ToString(row_table[0]);
                dr["Name"] = Convert.ToString(row_table[1]);
                dr["Institute"] = Convert.ToString(row_table[3]);
                dr["Internship Period"] = Convert.ToString(row_table[9]);

                tb.Rows.Add(dr);
                Gv1.DataSource = tb;
                Gv1.DataBind();
                ViewState["table1"] = tb;







            }

            //protected void Addnewrow(object sender, EventArgs e)
            //{
            //    tb =(DataTable) ViewState["table1"];
            //    dr = tb.NewRow();
            //    dr["Prod_NO"] = txtb1.Text;
            //    dr["Prod_Name"] = txtb2.Text;
            //    dr["Order_Date"] = txtb3.Text;
            //    dr["Quantity"] = txtb4.Text;
            //    tb.Rows.Add(dr);
            //    Gv1.DataSource = tb;
            //    Gv1.DataBind();
            //    txtb1.Text = "";
            //    txtb2.Text = "";
            //    txtb3.Text = "";
            //    txtb4.Text = "";
            //}
        }
    }
    protected void lbl_print_Click(object sender, EventArgs e)
    {

        //int i;
        ////tb.Columns.Add("Intern ID", typeof(string));
        ////tb.Columns.Add("Name", typeof(string));
        ////tb.Columns.Add("Institute", typeof(string));
        ////tb.Columns.Add("Internship Period", typeof(string));

        //SqlCommand cmd_table = new SqlCommand("select * from Interns_Details", c.con);
        //SqlDataAdapter da_table = new SqlDataAdapter(cmd_table);
        //DataTable dt_table = new DataTable();
        //da_table.Fill(dt_table);
        //int count = dt_table.Rows.Count;
        //if (dt_table.Rows.Count > 0)
        //{
        //    for (i = count; i > 0; i--)
        //    {

        //        DataRow row_table = dt_table.Rows[dt_table.Rows.Count - i];






                ///////////////////////

                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                Gv1.AllowPaging = false;
                Gv1.DataBind();
                Gv1.RenderControl(hw);
                Gv1.HeaderRow.Style.Add("width", "15%");
                Gv1.HeaderRow.Style.Add("font-size", "10px");
                Gv1.Style.Add("text-decoration", "none");
                Gv1.Style.Add("font-family", "Arial, Helvetica, sans-serif;");
                Gv1.Style.Add("font-size", "8px");
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
       
    
}


                /////////////////////////

                //dr = tb.NewRow();
                //dr["Intern ID"] = Convert.ToString(row_table[0]);
                //dr["Name"] = Convert.ToString(row_table[1]);
                //dr["Institute"] = Convert.ToString(row_table[3]);
                //dr["Internship Period"] = Convert.ToString(row_table[9]);

                //tb.Rows.Add(dr);
                //Gv1.DataSource = tb;
                //Gv1.DataBind();
                //ViewState["table1"] = tb;








     


              
        ////////////////////////////////////////////////////


        //    //pdf
        //    //pdf generation

        //    //Create a Document object
        //    var document = new Document(PageSize.A4, 50, 50, 25, 25);

        //    // Create a new PdfWriter object, specifying the output stream
        //    var output = new MemoryStream();
        //    //var writer = PdfWriter.GetInstance(document, output);
        //    var writer = PdfWriter.GetInstance(document, new FileStream(HttpContext.Current.Server.MapPath("~/TestPrintCard.pdf"), FileMode.Create));
        //    writer.PageEvent = new ITextEvents();

        //    //Font set
        //    BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        //    Font times = new Font(bfTimes, 14, Font.BOLD);


        //    //Font set
        //    BaseFont bfTimesText = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        //    Font timesText = new Font(bfTimes, 12, Font.NORMAL);

        //    BaseFont bfTimes1 = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        //    Font times1 = new Font(bfTimes1, 14, Font.BOLD);


        //    // Open the Document for writing
        //    document.Open();





        //    var paragph2 = new Paragraph();
        //    paragph2.Alignment = Element.ALIGN_LEFT;
        //    paragph2.SpacingAfter = 15f;

        //    Phrase ph1 = new Phrase(tb, timesText);
        //    paragph2.Add(ph1);

        //    document.Add(paragph2);
            




        //    document.Close();

        //    string path = Server.MapPath("~/PrintList.pdf");

        //    showPdf(path);


        //    Response.Redirect(Server.MapPath("~/PrintList.pdf"));

        //}
       
   
   