﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Nurse_IPAdmission_list : System.Web.UI.Page
{
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            c.getcon();
            bindgrid();
            c.con.Close();
        }

    }
    public void bindgrid()
    {
        //pid = Convert.ToInt32(Request.QueryString["PID"].ToString());

        string str_ip_patient = "select * from patient_details p inner join IPAdmission_details ip on p.cardno=ip.patient_id where ip.date_of_admission=Convert(datetime,'" + DateTime.Now.Date + "',105) ";

        SqlCommand cmd_ip_patient = new SqlCommand(str_ip_patient, c.con);
        DataTable dt_ip_patient = new DataTable();
        SqlDataAdapter da_ip_patient = new SqlDataAdapter(cmd_ip_patient);
        da_ip_patient.Fill(dt_ip_patient);
        if (dt_ip_patient.Rows.Count > 0)
        {

            GridView1.DataSource = dt_ip_patient;
            GridView1.DataBind();
        }



    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "SelectButton")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];
            Response.Redirect("View_ip_patient.aspx?IPID=" + row.Cells[0].Text);
        }
   
    }
}