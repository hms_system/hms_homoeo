﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Nurse/NurseMasterPage.master" AutoEventWireup="true" CodeFile="IP_PatientList.aspx.cs" Inherits="Nurse_IP_PatientList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
    <title></title>
     <link href="../layout/styles/layout.css" rel="stylesheet" />

    
    <style type="text/css">
        .auto-style1 {
            text-align: right;
        }
        .auto-style2 {
            text-align: left;
        }
        .auto-style3 {
            font-size: medium;
            color: #0000CC;
            text-align: center;
        }
        .auto-style4 {
            font-size: medium;
            color: #0000CC;
            text-align: center;
            width: 67px;
        }
        .auto-style5 {
            width: 67px;
        }
        .auto-style6 {
            height: 18px;
        }
        .auto-style7 {
            width: 67px;
            height: 18px;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style3" colspan="2">
                    <h2><strong>&nbsp;IN-PATIENT LIST&nbsp;</strong></h2>
                </td>
                <td class="auto-style4">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCommand="GridView1_RowCommand" Width="606px" >
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                             <asp:BoundField DataField="admission_id" HeaderText="Admission ID" />

                            <asp:BoundField DataField="cardno" HeaderText="Card No" />
                            <asp:BoundField DataField="patient_name" HeaderText="Patient Name" />
                             <asp:TemplateField>  
                            <ItemTemplate>  
                                <asp:Button ID="btn_select" runat="server" Width="60" Text="SELECT" CommandName="SelectButton"  CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />  
                            </ItemTemplate> 
                             
                        </asp:TemplateField> 
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                </td>
                <td class="auto-style5">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style6"></td>
                <td class="auto-style6"></td>
                <td class="auto-style6"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>

</asp:Content>
