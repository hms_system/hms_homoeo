﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class AllocatePage : System.Web.UI.Page
{
    Connection c = new Connection();
    int bid = 0;

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_srch_Click(object sender, EventArgs e)
    {
        c.getcon();
        SqlCommand cmd111 = new SqlCommand("select * from Patient_details where cardno= '" + txt_cardno.Text + "' ", c.con);
        SqlDataAdapter sda111 = new SqlDataAdapter(cmd111);
        DataTable dt111 = new DataTable();
        sda111.Fill(dt111);

        int k = cmd111.ExecuteNonQuery();

        if (dt111.Rows.Count > 0)
        {
            DataRow row11 = dt111.Rows[dt111.Rows.Count - 1];
            String pname = Convert.ToString(row11[1]);
            DateTime dob = Convert.ToDateTime(row11[2]);
            String gender = Convert.ToString(row11[3]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
            lbl_age.Text = Convert.ToString(age) + " years";
            lbl_gen.Text = gender;
            lbl_name.Text = pname;

        }
        c.con.Close();
    }
    protected void btn_alloc_Click(object sender, EventArgs e)
    {
        c.getcon();
        bid = Convert.ToInt32(Request.QueryString["Bedid"].ToString());

        String s = "insert into Bed_allocation values('" + txt_cardno.Text + "','" + bid + "',Convert(datetime,'" + DateTime.Now + "',103),' ')";
        SqlCommand cmds = new SqlCommand(s, c.con);
        cmds.ExecuteNonQuery();
        c.con.Close();
        Response.Redirect("IP_Admission.aspx");
    }
}