﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;



public partial class Nurse_View_observation : System.Web.UI.Page
{
    Connection c = new Connection();

    int oid = 0;
    int nid = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        oid = Convert.ToInt32(Request.QueryString["OID"].ToString());
        nid = Convert.ToInt32(Session["id"].ToString()); 

        if (!IsPostBack)
        {
            MultiView1.ActiveViewIndex = 0;
            c.getcon();
           
            
            string view_obv_patient = "select p.cardno, p.patient_name,p.house_name,p.street,p.contact_no,ob.remarks_dr from Observation_details ob inner join  Patient_details p on ob.obv_patientid=p.cardno where ob.obv_id='" + oid + "'";
            SqlCommand cmd_view_obv_patient = new SqlCommand(view_obv_patient, c.con);
            SqlDataAdapter da_view_obv_patient = new SqlDataAdapter(cmd_view_obv_patient);
            DataTable dt_view_obv_patient = new DataTable();
            da_view_obv_patient.Fill(dt_view_obv_patient);
            if (dt_view_obv_patient.Rows.Count > 0)
            {
                DataRow row1 = dt_view_obv_patient.Rows[dt_view_obv_patient.Rows.Count - 1];

                int id = Convert.ToInt32(row1[0]);

                lbl_view_card_no.Text = Convert.ToString(id);
                string name = Convert.ToString(row1[1]);
                lbl_view_patient_name.Text = Convert.ToString(name);
                string hname = Convert.ToString(row1[2]);
                lbl_housename.Text = Convert.ToString(hname);
                string place = Convert.ToString(row1[3]);
                lbl_place.Text = Convert.ToString(place);
                
                string contact = Convert.ToString(row1[4]);
                lbl_view_contact_no.Text = Convert.ToString(contact);
                string remarks = Convert.ToString(row1[5]);
                lbl_instr_view.Text = Convert.ToString(remarks);
                
            }

            gridbind();
            string str_test = "select * from Add_Attributes ";
            SqlCommand cmd_test = new SqlCommand(str_test, c.con);
            SqlDataAdapter da_test = new SqlDataAdapter(cmd_test);
            DataTable dt_test = new DataTable();
            da_test.Fill(dt_test);
            if (dt_test.Rows.Count > 0)
            {
               ddl_parameter.DataSource = dt_test;
               ddl_parameter.DataValueField = "investigation_id";
               ddl_parameter.DataTextField = "investigation_attribute";
               ddl_parameter.DataBind();
               ddl_parameter.Items.Insert(0, "SELECT");
            }
        }
        c.con.Close();
    }
    protected void lbtn_vital_parameter_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }
    protected void btn_back_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    protected void btn_next_vital_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
         c.getcon();
        int id = Convert.ToInt32(Session["id"]);
        nid = Convert.ToInt32(Session["id"].ToString()); 

        string str_obv_parameter = "insert into Investigation_details values(Convert(datetime,'" + DateTime.Now + "',103),'" + ddl_parameter.SelectedItem.Value + "','" + txt_value.Text + "','','" + id + "')";
        SqlCommand cmd_obv_parameter = new SqlCommand(str_obv_parameter, c.con);
        cmd_obv_parameter.ExecuteNonQuery();


        //string testid=Convert.ToString(ddl_test_attributes.SelectedItem.Value);
        string str_disp_parameter = "select * from Investigation_details  inner join Add_Attributes on Investigation_details.investigation_id=Add_Attributes.investigation_id where Investigation_details.patient_id='" + id + "' and Add_Attributes.investigation_id='" +ddl_parameter.SelectedItem.Value + "' order by Investigation_details.date";
        SqlCommand cmd_disp_parameter = new SqlCommand(str_disp_parameter, c.con);
        DataTable dt_disp_parameter = new DataTable();
        SqlDataAdapter da_disp_parameter = new SqlDataAdapter(cmd_disp_parameter);
        da_disp_parameter.Fill(dt_disp_parameter);
        if (dt_disp_parameter.Rows.Count > 0)
        {
            GridView1.DataSource = dt_disp_parameter;
            GridView1.DataBind();
        }
        c.con.Close();
        btn_back.Visible = true;

    }
   public void gridbind()
    {
       // c.getcon();
        string str_disp_med = "select * from  Prescription p inner join   Observation_details ob on p.patient_id=ob.obv_patientid where ob.obv_patientid='" + lbl_view_card_no.Text + "' and p.prescription_date=Convert(datetime,'" + DateTime.Now.Date + "',105) ";
        SqlCommand cmd_disp_med = new SqlCommand(str_disp_med, c.con);
        SqlDataAdapter da_disp_med = new SqlDataAdapter(cmd_disp_med);
        DataTable dt_disp_med = new DataTable();
        da_disp_med.Fill(dt_disp_med);
        if (dt_disp_med.Rows.Count > 0)
        {
            GridView2.DataSource = dt_disp_med;
            GridView2.DataBind();
        }
       
    }
   
    protected void lbtn_med_manage_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }
    protected void btn_back_actiontaken_Click(object sender, EventArgs e)
    {
        nid = Convert.ToInt32(Session["id"].ToString());
        oid = Convert.ToInt32(Request.QueryString["OID"].ToString());

        c.getcon();
            string str_update_obv = "update Observation_details set n_emp_id='" + nid + "',actions_taken='" + rbtn_action_taken.SelectedItem.Text + "' where obv_id='" + oid + "'";
            SqlCommand cmd_update_obv = new SqlCommand(str_update_obv, c.con);
            cmd_update_obv.ExecuteNonQuery();

            MultiView1.ActiveViewIndex = 0;
       
    }
    protected void lbtn_actions_taken_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 3;
    }
    protected void btn_back_medicine_management_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    protected void btn_next_med_man_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 3;
    }
    protected void btn_nurse_add_Click(object sender, EventArgs e)
    {
        c.getcon();
        //int id = Convert.ToInt32(Session["id"]);
        string str_med = "insert into Medicine_log values('" + txt_medicin_name.Text + "','" + txt_dosage.Text + "','" + nid+ "',Convert(datetime,'" + DateTime.Now + "',103),'"+oid+"')";
        SqlCommand cmd_me = new SqlCommand(str_med, c.con);
        cmd_me.ExecuteNonQuery();
        string str_disp_med = "select medicin_name,dose,time from Medicine_log where patient_id='"+oid+"' order by medicin_id desc ";
        SqlCommand cmd_disp_med = new SqlCommand(str_disp_med, c.con);
        DataTable dt_disp_med = new DataTable();
        SqlDataAdapter da_disp_parameter = new SqlDataAdapter(cmd_disp_med);
        da_disp_parameter.Fill(dt_disp_med);
        if (dt_disp_med.Rows.Count > 0)
        {
            GridView3.DataSource = dt_disp_med;
            GridView3.DataBind();
        }
        c.con.Close();
        //btn_back.Visible = true;

    }
    
}