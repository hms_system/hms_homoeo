﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Nurse/NurseMasterPage.master" AutoEventWireup="true" CodeFile="NurseHome.aspx.cs" Inherits="Nurse_NurseHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
     <link href="../layout/styles/layout.css" rel="stylesheet" />

    
    <style type="text/css">
        .auto-style1 {
            text-align: right;
        }
        .auto-style2 {
            text-align: left;
        }
        .auto-style4 {
            text-align: center;
            width: 237px;
        }
        .auto-style5 {
            width: 237px;
        }
        .auto-style6 {
            text-align: left;
            width: 237px;
            height: 16px;
        }
        .auto-style7 {
            height: 48px;
        }
        .auto-style9 {
            text-align: center;
            height: 16px;
        }
        .auto-style10 {
            height: 16px;
            width: 504px;
        }
        .auto-style11 {
            color: #0099FF;
        }
        .auto-style12 {
            color: #0066FF;
        }
        .auto-style15 {
            height: 48px;
            width: 104px;
        }
        .auto-style16 {
            text-align: center;
            width: 230px;
            height: 48px;
        }
        .auto-style17 {
            width: 504px;
        }
        .auto-style18 {
            text-align: center;
            width: 504px;
        }
        .auto-style19 {
            text-align: center;
            width: 237px;
            height: 48px;
        }
    </style>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 100%">
        <tr>
            <td>
                <asp:LinkButton ID="lbtn_logout" runat="server" OnClick="lbtn_logout_Click">LOGOUT</asp:LinkButton>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

