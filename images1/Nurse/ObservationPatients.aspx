﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Nurse/NurseMasterPage.master" AutoEventWireup="true" CodeFile="ObservationPatients.aspx.cs" Inherits="Nurse_ObservationPatients" %>

<script runat="server">  
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
    <title></title>
     <link href="../layout/styles/layout.css" rel="stylesheet" />

    
    <style type="text/css">
        .auto-style1 {
            text-align: right;
        }
        .auto-style2 {
            text-align: left;
        }
        .auto-style7 {
            color: #0000CC;
        }
        .auto-style8 {
            text-align: center;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr class="auto-style7">
                <td>&nbsp;</td>
                <td class="auto-style8"><strong>PATIENTS IN OBSERVATION</strong></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                <td class="auto-style2">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCommand="GridView1_RowCommand" >
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                             <asp:BoundField DataField="obv_id" HeaderText="Observation ID" />

                            <asp:BoundField DataField="cardno" HeaderText="Card No" />
                            <asp:BoundField DataField="patient_name" HeaderText="Patient Name" />
                             <asp:TemplateField>  
                            <ItemTemplate>  
                                <asp:Button ID="btn_select" runat="server" Width="60" Text="SELECT" CommandName="SelectButton"  CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />  
                            </ItemTemplate> 
                             
                        </asp:TemplateField> 
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                </td>
                <td class="auto-style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</asp:Content>

