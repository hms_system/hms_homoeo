﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class IP_Admission : System.Web.UI.Page
{
    Connection c = new Connection();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindFemale();
            BindMale();
            BindPay();

        }
    }
    public void BindFemale()
    {
        c.getcon();
        SqlCommand cmd_ward = new SqlCommand("select b.bed_id,w.ward_id from Bed_master b inner join Ward_Master w on w.ward_id=b.ward_id where w.ward_name='Female' ", c.con);
        SqlDataAdapter sda_ward = new SqlDataAdapter(cmd_ward);
        DataTable dt_ward = new DataTable();
        sda_ward.Fill(dt_ward);


        int k_ward = cmd_ward.ExecuteNonQuery();

        if (dt_ward.Rows.Count > 0)
        {
            int wid = Convert.ToInt32(dt_ward.Rows[0][1]);
            int occcount=0;
            for (int j = 0; j < dt_ward.Rows.Count; j++)
           {
            int id = Convert.ToInt32(dt_ward.Rows[j][0]);
        SqlCommand cmd_pat = new SqlCommand("select * from Bed_allocation where bed_id='"+id+"' and timeout =' ' ", c.con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            occcount += 1;
            lbl_occ_fem.Text = occcount.ToString()+ " OCCUPIED";
            lbl_unocc_fem.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
            lbl_unocc_mal.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
            lbl_unocc_pay.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
            lbl_occ_fem.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");
            lbl_occ_mal.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");
            lbl_occ_pay.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");

            }
        }

            SqlCommand cmd_bed = new SqlCommand("select count(*) from Bed_master where ward_id='" + wid + "' ", c.con);
            SqlDataAdapter sda_bed = new SqlDataAdapter(cmd_ward);
            DataTable dt_bed = new DataTable();
            sda_bed.Fill(dt_bed);


            int k_bed = cmd_ward.ExecuteNonQuery();
            int unocccount = 0;
            if (dt_bed.Rows.Count > 0)
            {
                for (int j = 0; j < dt_bed.Rows.Count; j++)
                {
                    unocccount += 1;
                    
                }
                unocccount = unocccount - occcount;
                lbl_unocc_fem.Text = unocccount.ToString() + " UNOCCUPIED";

            }
        }
        c.con.Close();
    }
    public void BindMale()
    {
        c.getcon();
        SqlCommand cmd_ward = new SqlCommand("select b.bed_id,w.ward_id from Bed_master b inner join Ward_Master w on w.ward_id=b.ward_id where w.ward_name='Male' ", c.con);
        SqlDataAdapter sda_ward = new SqlDataAdapter(cmd_ward);
        DataTable dt_ward = new DataTable();
        sda_ward.Fill(dt_ward);


        int k_ward = cmd_ward.ExecuteNonQuery();

        if (dt_ward.Rows.Count > 0)
        {
            int wid = Convert.ToInt32(dt_ward.Rows[0][1]);
            int occcount = 0;
            for (int j = 0; j < dt_ward.Rows.Count; j++)
            {
                int id = Convert.ToInt32(dt_ward.Rows[j][0]);
                SqlCommand cmd_pat = new SqlCommand("select * from Bed_allocation where bed_id='" + id + "' and timeout =' ' ", c.con);
                SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
                DataTable dt_pat = new DataTable();
                sda_pat.Fill(dt_pat);


                int k_pat = cmd_pat.ExecuteNonQuery();

                if (dt_pat.Rows.Count > 0)
                {
                    occcount += 1;
                    lbl_occ_mal.Text = occcount.ToString() + " OCCUPIED";
                    lbl_unocc_fem.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                    lbl_unocc_mal.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                    lbl_unocc_pay.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                    lbl_occ_fem.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");
                    lbl_occ_mal.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");
                    lbl_occ_pay.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");

                }
            }

            SqlCommand cmd_bed = new SqlCommand("select count(*) from Bed_master where ward_id='" + wid + "' ", c.con);
            SqlDataAdapter sda_bed = new SqlDataAdapter(cmd_ward);
            DataTable dt_bed = new DataTable();
            sda_bed.Fill(dt_bed);


            int k_bed = cmd_ward.ExecuteNonQuery();
            int unocccount = 0;
            if (dt_bed.Rows.Count > 0)
            {
                for (int j = 0; j < dt_bed.Rows.Count; j++)
                {
                    unocccount += 1;

                }
                unocccount = unocccount - occcount;
                lbl_unocc_mal.Text = unocccount.ToString() + " UNOCCUPIED";

            }
        }
        c.con.Close();
    }
    public void BindPay()
    {
        c.getcon();
        SqlCommand cmd_ward = new SqlCommand("select b.bed_id,w.ward_id from Bed_master b inner join Ward_Master w on w.ward_id=b.ward_id where w.ward_name='Pay' ", c.con);
        SqlDataAdapter sda_ward = new SqlDataAdapter(cmd_ward);
        DataTable dt_ward = new DataTable();
        sda_ward.Fill(dt_ward);


        int k_ward = cmd_ward.ExecuteNonQuery();

        if (dt_ward.Rows.Count > 0)
        {
            int wid = Convert.ToInt32(dt_ward.Rows[0][1]);
            int occcount = 0;
            for (int j = 0; j < dt_ward.Rows.Count; j++)
            {
                int id = Convert.ToInt32(dt_ward.Rows[j][0]);
                SqlCommand cmd_pat = new SqlCommand("select * from Bed_allocation where bed_id='" + id + "' and timeout =' ' ", c.con);
                SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
                DataTable dt_pat = new DataTable();
                sda_pat.Fill(dt_pat);


                int k_pat = cmd_pat.ExecuteNonQuery();

                if (dt_pat.Rows.Count > 0)
                {
                    occcount += 1;
                    lbl_occ_pay.Text = occcount.ToString() + " OCCUPIED";
                    lbl_unocc_fem.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                    lbl_unocc_mal.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                    lbl_unocc_pay.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                    lbl_occ_fem.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");
                    lbl_occ_mal.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");
                    lbl_occ_pay.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");

                }
            }

            SqlCommand cmd_bed = new SqlCommand("select count(*) from Bed_master where ward_id='" + wid + "' ", c.con);
            SqlDataAdapter sda_bed = new SqlDataAdapter(cmd_ward);
            DataTable dt_bed = new DataTable();
            sda_bed.Fill(dt_bed);


            int k_bed = cmd_ward.ExecuteNonQuery();
            int unocccount = 0;
            if (dt_bed.Rows.Count > 0)
            {
                for (int j = 0; j < dt_bed.Rows.Count; j++)
                {
                    unocccount += 1;

                }
                unocccount = unocccount - occcount;
                lbl_unocc_pay.Text = unocccount.ToString() + " UNOCCUPIED";

            }
        }
        c.con.Close();
    }
 
    protected void Link_Female_Click(object sender, EventArgs e)
    {
        c.getcon();
        SqlCommand cmd_ward = new SqlCommand("select * from Ward_Master where ward_name='Female' ", c.con);
        SqlDataAdapter sda_ward = new SqlDataAdapter(cmd_ward);
        DataTable dt_ward = new DataTable();
        sda_ward.Fill(dt_ward);
        int WardId=0;

        int k_ward = cmd_ward.ExecuteNonQuery();

        if (dt_ward.Rows.Count > 0)
        {
            WardId = Convert.ToInt32(dt_ward.Rows[0][0]);
            Response.Redirect("WardHome.aspx?WardId=" + WardId);
        }
        c.con.Close();
    }
    protected void Link_Male_Click(object sender, EventArgs e)
    {
        c.getcon();
        SqlCommand cmd_ward = new SqlCommand("select * from Ward_Master where ward_name='Male' ", c.con);
        SqlDataAdapter sda_ward = new SqlDataAdapter(cmd_ward);
        DataTable dt_ward = new DataTable();
        sda_ward.Fill(dt_ward);
        int WardId = 0;

        int k_ward = cmd_ward.ExecuteNonQuery();

        if (dt_ward.Rows.Count > 0)
        {
            WardId = Convert.ToInt32(dt_ward.Rows[0][0]);
            Response.Redirect("WardHome.aspx?WardId=" + WardId);
        }
        c.con.Close();
    }
    protected void Link_Pay_Click(object sender, EventArgs e)
    {
        c.getcon();
        SqlCommand cmd_ward = new SqlCommand("select * from Ward_Master where ward_name='Pay' ", c.con);
        SqlDataAdapter sda_ward = new SqlDataAdapter(cmd_ward);
        DataTable dt_ward = new DataTable();
        sda_ward.Fill(dt_ward);
        int WardId = 0;

        int k_ward = cmd_ward.ExecuteNonQuery();

        if (dt_ward.Rows.Count > 0)
        {
            WardId = Convert.ToInt32(dt_ward.Rows[0][0]);
            Response.Redirect("WardHome.aspx?WardId=" + WardId);
        }
        c.con.Close();
    }
}