﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Nurse_ViewPatient : System.Web.UI.Page
{
    Connection c = new Connection();
    int ipid = 0;
    int nid = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;

        ipid = Convert.ToInt32(Request.QueryString["IPID"].ToString());
        nid = Convert.ToInt32(Session["id"].ToString());

        if (!IsPostBack)
        {
            MultiView1.ActiveViewIndex = 0;
            c.getcon();

            btn_routineback.Visible = false;
            btn_back.Visible = false;

            string str_vtest = "select * from Add_Attributes ";
            SqlCommand cmd_vtest = new SqlCommand(str_vtest, c.con);
            SqlDataAdapter da_vtest = new SqlDataAdapter(cmd_vtest);
            DataTable dt_vtest = new DataTable();
            da_vtest.Fill(dt_vtest);
            if (dt_vtest.Rows.Count > 0)
            {
                ddl_test_attributes.DataSource = dt_vtest;
                ddl_test_attributes.DataValueField = "investigation_id";
                ddl_test_attributes.DataTextField = "investigation_attribute";
                ddl_test_attributes.DataBind();
                ddl_test_attributes.Items.Insert(0, "SELECT");
            }



            string view_ip_patient = "select ip.admission_id, p.cardno, p.patient_name,p.dob,p.gender,p.house_name,p.street from IPAdmission_details ip inner join  Patient_details p on ip.patient_id=p.cardno where ip.admission_id='" + ipid + "'";
            SqlCommand cmd_view_ip_patient = new SqlCommand(view_ip_patient, c.con);
            SqlDataAdapter da_view_ip_patient = new SqlDataAdapter(cmd_view_ip_patient);
            DataTable dt_view_ip_patient = new DataTable();
            da_view_ip_patient.Fill(dt_view_ip_patient);
            if (dt_view_ip_patient.Rows.Count > 0)
            {
                DataRow row1 = dt_view_ip_patient.Rows[dt_view_ip_patient.Rows.Count - 1];

                int id = Convert.ToInt32(row1[0]);
                lbl_ip_admsn_no.Text = Convert.ToString(id);
                int cardno = Convert.ToInt32(row1[1]);
                lbl_cardno.Text = Convert.ToString(row1[1]);

                string name = Convert.ToString(row1[2]);
                lbl_ip_patient_name.Text = Convert.ToString(name);
                //string dob=Convert.ToString(row1[3]);
                //int age=((DateTime.Now.Year-Convert.
                string gender = Convert.ToString(row1[4]);
                lbl_gender.Text = Convert.ToString(gender);
                string hname = Convert.ToString(row1[5]);
                lbl_hname.Text = Convert.ToString(hname);
                string place = Convert.ToString(row1[6]);
                lbl_place.Text = Convert.ToString(place);


            }
            gridbind();
            c.con.Close();
        }
    }
    public void gridbind()
    {
        // c.getcon();
        string str_select1 = "select patient_id from IPAdmission_details where admission_id='" + ipid + "'";
        SqlCommand cmd_select1 = new SqlCommand(str_select1, c.con);
        SqlDataAdapter da_select1 = new SqlDataAdapter(cmd_select1);
        DataTable dt_select1 = new DataTable();
        da_select1.Fill(dt_select1);
        if (dt_select1.Rows.Count > 0)
        {
            DataRow row1 = dt_select1.Rows[dt_select1.Rows.Count - 1];

            int pid = Convert.ToInt32(row1[0]);

            string str_ip_disp_med = "select * from  Prescription p inner join   IPAdmission_details ip on p.patient_id=ip.patient_id where ip.patient_id='" + pid + "' and p.prescription_date=Convert(datetime,'" + DateTime.Now.Date + "',105) ";
            SqlCommand cmd_ip_disp_med = new SqlCommand(str_ip_disp_med, c.con);
            SqlDataAdapter da_ip_disp_med = new SqlDataAdapter(cmd_ip_disp_med);
            DataTable dt_ip_disp_med = new DataTable();
            da_ip_disp_med.Fill(dt_ip_disp_med);
            if (dt_ip_disp_med.Rows.Count > 0)
            {
                GridView2.DataSource = dt_ip_disp_med;
                GridView2.DataBind();
            }
        }

    }

    protected void lbtn_ip_vital_parameters_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }
    protected void lbtn_routine_checkup_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 3;
    }
    protected void lbtn_med_management_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }
    protected void btn_nurse_add_Click(object sender, EventArgs e)
    {
        c.getcon();
        //int id = Convert.ToInt32(Session["id"]);
        string str_ip_med = "insert into Medicine_log values('" + txt_ip_medicin_name.Text + "','" + txt_ip_dosage.Text + "','" + nid + "',Convert(datetime,'" + DateTime.Now + "',103),'" + ipid + "')";
        SqlCommand cmd_ip_med = new SqlCommand(str_ip_med, c.con);
        cmd_ip_med.ExecuteNonQuery();

        string str_ip_disp_medicine = "select medicin_name,dose,time from Medicine_log where patient_id='" + ipid + "' order by medicin_id desc ";
        SqlCommand cmd_disp_medicine = new SqlCommand(str_ip_disp_medicine, c.con);
        DataTable dt_disp_medicine = new DataTable();
        SqlDataAdapter da_disp_parameter = new SqlDataAdapter(cmd_disp_medicine);
        da_disp_parameter.Fill(dt_disp_medicine);
        if (dt_disp_medicine.Rows.Count > 0)
        {
            GridView3.DataSource = dt_disp_medicine;
            GridView3.DataBind();
        }
        c.con.Close();
        //btn_back.Visible = true;


    }
    protected void btn_medback_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    protected void btn_vitaladd_Click(object sender, EventArgs e)
    {
        c.getcon();
        ipid = Convert.ToInt32(Request.QueryString["IPID"].ToString());
        int id = Convert.ToInt32(Session["id"]);
        string str_invest = "insert into Investigation_details values(Convert(datetime,'" + DateTime.Now + "',103),'" + ddl_test_attributes.SelectedItem.Value + "','" + txt_test_values.Text + "','','" + ipid + "')";
        SqlCommand cmd_invest = new SqlCommand(str_invest, c.con);
        cmd_invest.ExecuteNonQuery();


        //string testid=Convert.ToString(ddl_test_attributes.SelectedItem.Value);
        string str_test = "select * from Investigation_details  inner join Add_Attributes on Investigation_details.investigation_id=Add_Attributes.investigation_id where Investigation_details.patient_id='" + ipid + "' and Add_Attributes.investigation_id='" + ddl_test_attributes.SelectedItem.Value + "' order by Investigation_details.date";
        SqlCommand cmd_test = new SqlCommand(str_test, c.con);
        DataTable dt_test = new DataTable();
        SqlDataAdapter da_test = new SqlDataAdapter(cmd_test);
        da_test.Fill(dt_test);
        if (dt_test.Rows.Count > 0)
        {

            GridView1.DataSource = dt_test;
            GridView1.DataBind();
        }

        c.con.Close();
        btn_back.Visible = true;

    }
    protected void btn_routinetest_Click(object sender, EventArgs e)
    {
        c.getcon();
        ipid = Convert.ToInt32(Request.QueryString["IPID"].ToString());
        int id = Convert.ToInt32(Session["id"]);
        string str_invest = "insert into Routine_checkup values('" + txt_routinetest.Text + "','" + txt_routinevalue.Text + "','" + ipid + "',Convert(datetime,'" + DateTime.Now + "',103))";
        SqlCommand cmd_invest = new SqlCommand(str_invest, c.con);
        cmd_invest.ExecuteNonQuery();


        //string testid=Convert.ToString(ddl_test_attributes.SelectedItem.Value);
        string str_rtest = "select * from Routine_checkup  where ipid='" + ipid + "' order by routinedate";
        SqlCommand cmd_rtest = new SqlCommand(str_rtest, c.con);
        DataTable dt_rtest = new DataTable();
        SqlDataAdapter da_test = new SqlDataAdapter(cmd_rtest);
        da_test.Fill(dt_rtest);
        if (dt_rtest.Rows.Count > 0)
        {

            GridView4.DataSource = dt_rtest;

            GridView4.DataBind();
        }

        c.con.Close();
        btn_routineback.Visible = true;

    }
    protected void btn_back_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    protected void btn_routineback_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
}