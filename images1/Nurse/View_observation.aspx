﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="View_observation.aspx.cs" Inherits="Nurse_View_observation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style3 {
            height: 42px;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style4 {
            text-align: center;
            height: 23px;
        }
        .auto-style5 {
            text-align: center;
        }
        .auto-style6 {
            height: 26px;
        }
        .auto-style7 {
            text-align: right;
            width: 109px;
        }
        .auto-style8 {
            height: 28px;
            text-align: center;
            width: 114px;
        }
        .auto-style11 {
            height: 26px;
            width: 488px;
        }
        .auto-style12 {
            height: 28px;
            width: 488px;
        }
        .auto-style13 {
            height: 28px;
        }
        .auto-style14 {
            height: 26px;
            width: 114px;
        }
        .auto-style15 {
            height: 26px;
            width: 488px;
            text-align: center;
        }
        .auto-style16 {
            width: 104%;
            height: 238px;
        }
        .auto-style17 {
            text-align: center;
            height: 46px;
            width: 109px;
        }
        .auto-style18 {
            height: 46px;
        }
        .auto-style19 {
            text-align: center;
            width: 109px;
        }
        .auto-style20 {
            width: 109px;
        }
        .auto-style21 {
            width: 109px;
            height: 23px;
        }
        .auto-style22 {
            text-align: left;
            height: 23px;
        }
        .auto-style23 {
            text-align: center;
            height: 42px;
        }
        .auto-style24 {
            height: 28px;
            width: 114px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:MultiView ID="MultiView1" runat="server">
            <table class="auto-style1">
                <tr>
                    <td>
                        <asp:View ID="View1" runat="server">
                            <table class="auto-style16">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl_card_no" runat="server" Text="Card No."></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_view_card_no" runat="server"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl_patient_name" runat="server" Text="Patient Name"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_view_patient_name" runat="server"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        <asp:Label ID="lbl_address" runat="server" Text="Address"></asp:Label>
                                    </td>
                                    <td class="auto-style3">
                                        <asp:Label ID="lbl_housename" runat="server"></asp:Label>
                                        <br />
                                        <asp:Label ID="lbl_place" runat="server"></asp:Label>
                                    </td>
                                    <td class="auto-style3"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style2">
                                        <asp:Label ID="lbl_contact_no" runat="server" Text="Contact No."></asp:Label>
                                    </td>
                                    <td class="auto-style2">
                                        <asp:Label ID="lbl_view_contact_no" runat="server"></asp:Label>
                                    </td>
                                    <td class="auto-style2"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style2">
                                        <asp:Label ID="lbl_instruction" runat="server" Text="Instructions from Medical Officer"></asp:Label>
                                    </td>
                                    <td class="auto-style2">
                                        <asp:Label ID="lbl_instr_view" runat="server" Text="Label"></asp:Label>
                                    </td>
                                    <td class="auto-style2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lbtn_vital_parameter" runat="server" OnClick="lbtn_vital_parameter_Click">Vital Parameters</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtn_med_manage" runat="server" OnClick="lbtn_med_manage_Click">Medicines &amp; Management</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtn_actions_taken" runat="server" OnClick="lbtn_actions_taken_Click">Actions Taken</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:View ID="View2" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td class="auto-style4" colspan="4"><strong>VITAL PARAMETERS</strong></td>
                                </tr>
                                <tr>
                                    <td class="auto-style17">
                                        <asp:Label ID="lbl_parameter" runat="server" Text="Parameter"></asp:Label>
                                    </td>
                                    <td colspan="2" class="auto-style18">
                                        <asp:DropDownList ID="ddl_parameter" runat="server" AutoPostBack="True" Height="33px" Width="108px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="auto-style18"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style19">
                                        <asp:Label ID="lbl_value" runat="server" Text="Value"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txt_value" runat="server" Height="23px" Width="98px"></asp:TextBox>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btn_add" runat="server" OnClick="btn_add_Click" Text="Add" Width="90px" Height="35px" />
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style20">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style20">&nbsp;</td>
                                    <td class="auto-style5" colspan="2">
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:BoundField DataField="investigation_attribute" HeaderText="Parameters" />
                                                <asp:BoundField DataField="test_value" HeaderText="Value" />
                                                <asp:BoundField DataField="date" HeaderText="Date &amp; Time" />
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style21">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Label ID="lbl_nremarks" runat="server" Text="Remarks"></asp:Label>
                                    </td>
                                    <td class="auto-style22" colspan="2">
                                        <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                    <td class="auto-style2"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style7">
                                        <asp:Button ID="btn_back" runat="server" Text="&lt;&lt; Back" OnClick="btn_back_Click" />
                                    </td>
                                    <td colspan="2">&nbsp;&nbsp;</td>
                                    <td>
                                        <asp:Button ID="btn_next_vital" runat="server" OnClick="btn_next_vital_Click" Text="Next&gt;&gt;" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:View ID="View3" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td class="auto-style5" colspan="4"><strong>MEDICINES AND MANAGEMENT</strong></td>
                                </tr>
                                <tr>
                                    <td class="auto-style23" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="auto-style5">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style23" colspan="4"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style8">
                                        &nbsp;</td>
                                    <td class="auto-style12">
                                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="prescription_id" ForeColor="#333333" GridLines="None" >
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:BoundField DataField="drug_name" HeaderText="Name of Medicine" />
                                                <asp:BoundField DataField="dose" HeaderText="Dossage" />
                                                <asp:BoundField DataField="frequency" HeaderText="Frequency" />
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>
                                    </td>
                                    <td class="auto-style13">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style8">&nbsp;</td>
                                    <td class="auto-style12">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style24">
                                        <asp:Label ID="lbl_medicin_name" runat="server" Text="Name of Medicine"></asp:Label>
                                    </td>
                                    <td class="auto-style12">
                                        <asp:TextBox ID="txt_medicin_name" runat="server"></asp:TextBox>
                                    </td>
                                    <td class="auto-style13"></td>
                                    <td class="auto-style13"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style8">
                                        <asp:Label ID="lbl_dosage" runat="server" Text="Dosage"></asp:Label>
                                    </td>
                                    <td class="auto-style12">
                                        <asp:TextBox ID="txt_dosage" runat="server"></asp:TextBox>
                                    </td>
                                    <td class="auto-style13">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style8">
                                        &nbsp;</td>
                                    <td class="auto-style12">
                                        &nbsp;</td>
                                    <td class="auto-style13">
                                        <asp:Button ID="btn_nurse_add" runat="server" OnClick="btn_nurse_add_Click" Text="Add" Width="75px" />
                                    </td>
                                    <td class="auto-style13"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style8">&nbsp;</td>
                                    <td class="auto-style12">
                                        <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:BoundField DataField="medicin_name" HeaderText="Name of Medicine" />
                                                <asp:BoundField DataField="dose" HeaderText="Dose" />
                                                <asp:BoundField DataField="time" HeaderText="Time" />
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>
                                    </td>
                                    <td class="auto-style13">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style8">&nbsp;</td>
                                    <td class="auto-style12">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style14">&nbsp;</td>
                                    <td class="auto-style15">
                                        &nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style14">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btn_back_medicine_management" runat="server" Text="&lt;&lt; Back" OnClick="btn_back_medicine_management_Click" />
                                    </td>
                                    <td class="auto-style11"></td>
                                    <td class="auto-style6">
                                        <asp:Button ID="btn_next_med_man" runat="server" Text="Next &gt;&gt;" OnClick="btn_next_med_man_Click" />
                                    </td>
                                    <td class="auto-style6"></td>
                                </tr>
                            </table>
                        </asp:View>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:View ID="View4" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Actions Taken</td>
                                    <td>
                                        <asp:RadioButtonList ID="rbtn_action_taken" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="0">Improved &amp; Discharged</asp:ListItem>
                                            <asp:ListItem Value="1">Improving &amp; Shifted to IP</asp:ListItem>
                                            <asp:ListItem Value="2">Worst ,Discharged &amp; Directed to HIgher</asp:ListItem>
                                            <asp:ListItem Value="3">Expired</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btn_back_actiontaken" runat="server" Text="&lt;&lt; Back" OnClick="btn_back_actiontaken_Click" />
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </asp:MultiView>
    
    </div>
    </form>
</body>
</html>
