﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IP_Admission.aspx.cs" Inherits="IP_Admission" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {}
        .auto-style5 {
            font-size: large;
            color: #333399;
            text-align: left;
        }
        .auto-style8 {
        }
        .auto-style9 {
            width: 200px;
        }
        .auto-style10 {
            text-align: center;
            color: #0000CC;
            font-size: x-large;
        }
        .auto-style11 {
            font-size: large;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table class="auto-style1">
        <tr>
            <td class="auto-style10" colspan="3"><strong>IP WARDS</strong></td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="3">
                <asp:LinkButton ID="Link_Female" runat="server" CssClass="auto-style11" OnClick="Link_Female_Click">Female Ward</asp:LinkButton>
            </td>
            <td class="auto-style2">
                <asp:Label ID="lbl_occ_fem" runat="server"></asp:Label>
            </td>
            <td class="auto-style2">
                <asp:Label ID="lbl_unocc_fem" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style5" colspan="3">
                <asp:LinkButton ID="Link_Male" runat="server" OnClick="Link_Male_Click">Male Ward</asp:LinkButton>
            </td>
            <td class="auto-style2">
                <asp:Label ID="lbl_occ_mal" runat="server"></asp:Label>
            </td>
            <td class="auto-style2">
                <asp:Label ID="lbl_unocc_mal" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style8" colspan="3">
                <asp:LinkButton ID="Link_Pay" runat="server" CssClass="auto-style11" OnClick="Link_Pay_Click">Pay ward</asp:LinkButton>
            </td>
            <td class="auto-style2">
                <asp:Label ID="lbl_occ_pay" runat="server"></asp:Label>
            </td>
            <td class="auto-style2">
                <asp:Label ID="lbl_unocc_pay" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
           
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
        </tr>
        </table>
    
        
    
    </form>
</body>
</html>
