﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AllocatePage.aspx.cs" Inherits="AllocatePage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Enter CardNo</td>
                <td>
                    <asp:TextBox ID="txt_cardno" runat="server"></asp:TextBox>
                    <asp:Button ID="btn_srch" runat="server" OnClick="btn_srch_Click" Text="Search" />
                </td>
            </tr>
            <tr>
                <td>Patient Name</td>
                <td>
                    <asp:Label ID="lbl_name" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Age</td>
                <td>
                    <asp:Label ID="lbl_age" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>
                    <asp:Label ID="lbl_gen" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_alloc" runat="server" OnClick="btn_alloc_Click" Text="Allocate Bed" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
