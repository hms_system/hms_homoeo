﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Nurse/NurseMasterPage.master" AutoEventWireup="true" CodeFile="ViewPatient.aspx.cs" Inherits="Nurse_ViewPatient" %>

<script runat="server">  
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
    <title></title>
     <link href="../layout/styles/layout.css" rel="stylesheet" />

    
    <style type="text/css">
        .auto-style1 {
            text-align: right;
        }
        .auto-style3 {
            font-size: medium;
        }
        .auto-style4 {
            text-align: center;
            width: 298px;
        }
        .auto-style9 {
            width: 103px;
        }
        .auto-style10 {
            width: 108px;
        }
        .auto-style11 {
            width: 109px;
        }
        .auto-style12 {
            width: 110px;
        }
        .auto-style19 {
            text-align: center;
        }
        .auto-style20 {
            text-align: right;
            height: 21px;
            width: 808px;
        }
        .auto-style26 {
            width: 144px;
            height: 27px;
        }
        .auto-style27 {
            width: 176px;
            height: 27px;
        }
        .auto-style28 {
            width: 190px;
            height: 27px;
            text-align: left;
            font-size: small;
            color: #0033CC;
        }
        .auto-style29 {
            width: 208px;
            height: 27px;
        text-align: left;
    }
        .auto-style30 {
            width: 259px;
            height: 27px;
        }
        .auto-style31 {
            width: 218px;
            height: 27px;
        }
        .auto-style33 {
            width: 144px;
            height: 29px;
        }
        .auto-style34 {
            width: 176px;
            height: 29px;
        }
        .auto-style35 {
            width: 190px;
            text-align: left;
            height: 29px;
            color: #0033CC;
        }
        .auto-style36 {
            width: 208px;
            height: 29px;
        text-align: left;
    }
        .auto-style37 {
            width: 162px;
            height: 20px;
        }
        .auto-style38 {
            width: 103px;
            height: 20px;
        }
        .auto-style41 {
            width: 162px;
            height: 33px;
        }
        .auto-style42 {
            width: 103px;
            height: 33px;
        }
        .auto-style43 {
            width: 162px;
            height: 35px;
        }
        .auto-style44 {
            width: 103px;
            height: 35px;
        }
        .auto-style45 {
            width: 259px;
            height: 29px;
        }
        .auto-style46 {
            width: 218px;
            height: 29px;
        }
        .auto-style47 {
            width: 144px;
            height: 31px;
        }
        .auto-style48 {
            width: 176px;
            height: 31px;
        }
        .auto-style49 {
            width: 190px;
            text-align: left;
            height: 31px;
            font-size: small;
            color: #0033CC;
        }
        .auto-style50 {
            width: 208px;
            height: 31px;
        text-align: left;
    }
        .auto-style51 {
            width: 259px;
            height: 31px;
        }
        .auto-style52 {
            width: 218px;
            height: 31px;
        }
        .auto-style60 {
        width: 107px;
        height: 23px;
    }
    .auto-style61 {
        width: 116px;
        height: 23px;
            font-size: small;
            color: #0033CC;
        }
    .auto-style63 {
        width: 133px;
        height: 23px;
    }
    .auto-style64 {
            width: 162px;
            height: 23px;
            font-size: small;
            color: #0033CC;
        }
    .auto-style65 {
            height: 23px;
            font-size: small;
            color: #0033CC;
        }
    .auto-style66 {
        width: 107px;
        height: 28px;
            color: #000099;
        }
    .auto-style67 {
            width: 116px;
            height: 28px;
            color: #000099;
        }
    .auto-style68 {
        width: 144px;
        height: 40px;
    }
    .auto-style69 {
        width: 176px;
        height: 40px;
    }
    .auto-style70 {
        width: 190px;
        text-align: left;
        height: 40px;
            font-size: small;
            color: #0033CC;
        }
    .auto-style71 {
        width: 208px;
        height: 40px;
        text-align: left;
    }
    .auto-style72 {
        width: 259px;
        height: 40px;
    }
    .auto-style73 {
        height: 40px;
            width: 218px;
        }
    .auto-style80 {
        width: 176px;
        text-align: left;
    }
    .auto-style81 {
        text-align: right;
        height: 129px;
        width: 1133px;
    }
    .auto-style83 {
        width: 132px;
            text-align: center;
        }
    .auto-style84 {
        width: 131px;
    }
    .auto-style85 {
        width: 130px;
    }
    .auto-style86 {
        width: 126px;
    }
    .auto-style87 {
        width: 125px;
            color: blue;
            font-size: small;
        }
        .auto-style98 {
            width: 218px;
            height: 16px;
        }
        .auto-style106 {
            width: 218px;
        }
        .auto-style107 {
            height: 36px;
            width: 218px;
        }
        .auto-style114 {
            width: 144px;
            height: 36px;
        }
        .auto-style115 {
            width: 144px;
        }
        .auto-style116 {
            width: 176px;
            height: 36px;
        }
        .auto-style117 {
            width: 176px;
        }
        .auto-style118 {
            width: 259px;
            height: 36px;
        }
        .auto-style119 {
            width: 259px;
        }
        .auto-style120 {
            color: #0000CC;
        }
        .auto-style122 {
            font-size: small;
        }
        .auto-style125 {
            width: 190px;
        }
        .auto-style126 {
            color: #0033CC;
        }
        .auto-style134 {
            font-size: small;
            color: #0033CC;
        }
        .auto-style140 {
            text-align: right;
            height: 129px;
            width: 1129px;
        }
        .auto-style141 {
            text-align: right;
        }
        .auto-style142 {
            height: 23px;
            text-align: center;
            font-size: small;
            color: #0033CC;
        }
        .auto-style143 {
            width: 133px;
            height: 23px;
            font-size: small;
        }
        .auto-style144 {
            width: 107px;
            height: 23px;
            text-align: center;
        }
        .auto-style146 {
            width: 162px;
            height: 20px;
            color: blue;
            font-size: small;
        }
        .auto-style147 {
            color: blue;
        }
        .auto-style148 {
            width: 130px;
            color: blue;
            font-size: small;
        }
        .auto-style149 {
            text-align: center;
            color: blue;
            font-size: small;
        }
        .auto-style150 {
            width: 130px;
            font-size: small;
        }
        .auto-style151 {
            font-size: medium;
            text-align: center;
        }
        .auto-style152 {
            color: blue;
            font-size: small;
        }
        .auto-style153 {
            text-align: right;
            height: 129px;
            width: 1131px;
        }
        .auto-style155 {
            text-align: right;
            height: 129px;
            width: 1127px;
        }
        .auto-style161 {
            text-align: center;
            color: #000099;
            font-size: small;
        }
        .auto-style163 {
            width: 126px;
            color: blue;
            font-size: small;
        }
        .auto-style166 {
            width: 190px;
            text-align: left;
        }
        .auto-style167 {
            width: 190px;
            text-align: left;
            height: 36px;
            font-size: small;
            color: #0033CC;
        }
        .auto-style168 {
            width: 208px;
            text-align: left;
            height: 36px;
        }
        .auto-style169 {
            width: 208px;
            text-align: left;
        }
        .auto-style170 {
            width: 208px;
        }
    </style>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="Form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server">
                        <table class="auto-style141" style="width: 1153px">
                            <tr>
                                <td class="auto-style12">
                                    <asp:View ID="View1" runat="server">
                                        <table class="auto-style81">
                                            <tr>
                                                <td class="auto-style20" colspan="6">
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style115">
                                                    &nbsp;</td>
                                                <td class="auto-style19" colspan="4"><strong><span class="auto-style120">PATIENT DETAILS</span><br /> </strong></td>
                                                <td class="auto-style106">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style26"></td>
                                                <td class="auto-style27"></td>
                                                <td class="auto-style28"><strong>Admission No</strong></td>
                                                <td class="auto-style29">
                                                    <asp:Label ID="lbl_ip_admsn_no" runat="server"></asp:Label>
                                                </td>
                                                <td class="auto-style30"></td>
                                                <td class="auto-style31"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style33"></td>
                                                <td class="auto-style34">
                                                    </td>
                                                <td class="auto-style35">
                                                    <strong>
                                                    <asp:Label ID="lbl_card_no" runat="server" CssClass="auto-style122" Text="Card Number"></asp:Label>
                                                    </strong>
                                                </td>
                                                <td class="auto-style36">
                                                    <asp:Label ID="lbl_cardno" runat="server"></asp:Label>
                                                </td>
                                                <td class="auto-style45"></td>
                                                <td class="auto-style46">
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style47">
                                                    </td>
                                                <td class="auto-style48"></td>
                                                <td class="auto-style49">
                                                    <strong>Name</strong></td>
                                                <td class="auto-style50">
                                                    <asp:Label ID="lbl_ip_patient_name" runat="server"></asp:Label>
                                                </td>
                                                <td class="auto-style51">
                                                    </td>
                                                <td class="auto-style52"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style114"></td>
                                                <td class="auto-style116"></td>
                                                <td class="auto-style167">
                                                    <strong>Age</strong></td>
                                                <td class="auto-style168">
                                                    <asp:Label ID="lbl_age" runat="server"></asp:Label>
                                                </td>
                                                <td class="auto-style118"></td>
                                                <td class="auto-style107"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style114">
                                                    </td>
                                                <td class="auto-style116"></td>
                                                <td class="auto-style167">
                                                    <strong>Gender</strong></td>
                                                <td class="auto-style168">
                                                    <asp:Label ID="lbl_gender" runat="server"></asp:Label>
                                                </td>
                                                <td class="auto-style118">
                                                    </td>
                                                <td class="auto-style107"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style68"></td>
                                                <td class="auto-style69"></td>
                                                <td class="auto-style70">
                                                    <strong>Address<br /> </strong>
                                                    </td>
                                                <td class="auto-style71">
                                                    <asp:Label ID="lbl_hname" runat="server"></asp:Label>
                                                    <br />
                                                    <asp:Label ID="lbl_place" runat="server"></asp:Label>
                                                </td>
                                                <td class="auto-style72"></td>
                                                <td class="auto-style73"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style115">
                                                    &nbsp;</td>
                                                <td class="auto-style80">
                                                    <strong>
                                                    <asp:LinkButton ID="lbtn_ip_vital_parameters" runat="server" CssClass="auto-style66" OnClick="lbtn_ip_vital_parameters_Click">Vital Parameters</asp:LinkButton>
                                                    </strong>
                                                </td>
                                                <td class="auto-style166">
                                                    <strong>
                                                    <asp:LinkButton ID="lbtn_routine_checkup" runat="server" CssClass="auto-style67" OnClick="lbtn_routine_checkup_Click">Routine Checkup</asp:LinkButton>
                                                    </strong>
                                                </td>
                                                <td class="auto-style169">
                                                    <strong>
                                                    <asp:LinkButton ID="lbtn_med_management" runat="server" CssClass="auto-style67" OnClick="lbtn_med_management_Click">Medicines &amp; Management</asp:LinkButton>
                                                    </strong>
                                                </td>
                                                <td class="auto-style119">
                                                    &nbsp;</td>
                                                <td class="auto-style98">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style115">&nbsp;</td>
                                                <td class="auto-style117">&nbsp;</td>
                                                <td class="auto-style125">&nbsp;</td>
                                                <td class="auto-style170">&nbsp;</td>
                                                <td class="auto-style119">&nbsp;</td>
                                                <td class="auto-style106">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style12">
                                    <asp:View ID="View2" runat="server">
                                        <table class="auto-style140">
                                            <tr>
                                                <td class="auto-style142" colspan="3"><strong>MEDICINES&nbsp; &amp;&nbsp; MANAGEMENT</strong></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style61">&nbsp;</td>
                                                <td class="auto-style144"><strong>
                                                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4" CssClass="auto-style134" DataKeyNames="prescription_id" ForeColor="#333333" GridLines="None" Height="153px" Width="366px">
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <Columns>
                                                            <asp:BoundField DataField="drug_name" HeaderText="Name of Medicine" />
                                                            <asp:BoundField DataField="dose" HeaderText="Dossage" />
                                                            <asp:BoundField DataField="frequency" HeaderText="Frequency" />
                                                        </Columns>
                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                    </strong></td>
                                                <td class="auto-style60">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style143"><span class="auto-style126"></span></td>
                                                <td class="auto-style60"></span></td>
                                                <td class="auto-style60">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style64">
                                                    <strong>
                                                    <asp:Label ID="lbl_ip_medicin_name" runat="server" Text="Name of Medicine"></asp:Label>
                                                    </strong>
                                                </td>
                                                <td class="auto-style60">
                                                    <strong>
                                                    <asp:TextBox ID="txt_ip_medicin_name" runat="server" CssClass="auto-style134" Height="16px" Width="195px"></asp:TextBox>
                                                    </strong>
                                                </td>
                                                <td class="auto-style60">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txt_ip_medicin_name" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="c"></asp:RequiredFieldValidator>
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style64">
                                                    <strong>
                                                    <asp:Label ID="lbl_ip_dosage" runat="server" Text="Dosage"></asp:Label>
                                                    </strong>
                                                </td>
                                                <td class="auto-style60">
                                                     <strong>
                                                     <asp:TextBox ID="txt_ip_dosage" runat="server" CssClass="auto-style134" Height="16px" Width="195px"></asp:TextBox>
                                                     <span class="auto-style134">&nbsp;&nbsp;&nbsp; </span>
                                                     <asp:Button ID="btn_ip_nurse_add" runat="server" CssClass="auto-style134" Height="20px" OnClick="btn_nurse_add_Click" Text="Add" Width="50px" ValidationGroup="c" />
                                                     </strong>
                                                </td>
                                                <td class="auto-style60">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txt_ip_dosage" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="c"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style143"><span class="auto-style126"></span></td>
                                                <td class="auto-style60"></span></td>
                                                <td class="auto-style60">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style65">&nbsp;</td>
                                                <td class="auto-style83">
                                                    <strong>
                                                    <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="4" CssClass="auto-style134" ForeColor="#333333" GridLines="None" Height="124px" Width="382px">
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <Columns>
                                                            <asp:BoundField DataField="medicin_name" HeaderText="Name of Medicine" />
                                                            <asp:BoundField DataField="dose" HeaderText="Dose" />
                                                            <asp:BoundField DataField="time" HeaderText="Time" />
                                                        </Columns>
                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                    </strong>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style63"></td>
                                                <td class="auto-style60"></td>
                                                <td class="auto-style60"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style65">&nbsp;</td>
                                                <td>
                                                    <strong>
                                                    <asp:Button ID="btn_medback" runat="server" CssClass="auto-style134" OnClick="btn_medback_Click" Text="&lt;&lt;BACK" Width="115px" />
                                                    </strong>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                                <td class="auto-style10">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style12">
                                    <asp:View ID="View3" runat="server">
                                       
                                        <table class="auto-style153">
                                            <tr>
                                                <td class="auto-style149" colspan="5"><strong>VITAL PARAMETERS</strong></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style84">
                                                    <strong>
                                                    <asp:Label ID="lbl_test" runat="server" CssClass="auto-style146" Text="Name of Test"></asp:Label>
                                                    </strong>
                                                </td>
                                                <td class="auto-style9">
                                                    <strong>
                                                    <asp:DropDownList ID="ddl_test_attributes" runat="server" AutoPostBack="True" CssClass="auto-style146">
                                                    </asp:DropDownList>
                                                    </strong>
                                                </td>
                                                <td class="auto-style9">
                                                    <strong>
                                                    <asp:Label ID="lbl_value" runat="server" CssClass="auto-style146" Text="Value"></asp:Label>
                                                    </strong>
                                                </td>
                                                <td class="auto-style38">
                                                    <strong>
                                                    <asp:TextBox ID="txt_test_values" runat="server" CssClass="auto-style146"></asp:TextBox>
                                                    </strong>
                                                </td>
                                                <td class="auto-style9">
                                                    <strong>
                                                    <asp:Button ID="btn_vitaladd" runat="server" CssClass="auto-style152" Height="35px" OnClick="btn_vitaladd_Click" Text="ADD" Width="132px" />
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style148"><span class="auto-style37"></span></td>
                                                <td colspan="3"><strong></strong></td>
                                                <td><strong></strong></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style85"></td>
                                                <td colspan="3"><span class="auto-style147"></span></td>
                                                <td></span></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style148"><strong></span></strong></td>
                                                <td colspan="3" class="auto-style151">
                                                    <strong>
                                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" CssClass="auto-style146" DataKeyNames="invs_detail_id" ForeColor="#333333" GridLines="None" Height="134px" Width="423px">
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <Columns>
                                                            <asp:BoundField DataField="investigation_attribute" HeaderText="Name of Test" />
                                                            <asp:BoundField DataField="test_value" HeaderText="Value" />
                                                            <asp:BoundField DataField="date" HeaderText="Date" />
                                                        </Columns>
                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                    </strong>
                                                </td>
                                                <td><span class="auto-style37"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style150"><span class="auto-style147"></span></td>
                                                <td colspan="3"><strong></span></strong></span></td>
                                                <td>
                                                    <strong>
                                                    <asp:Button ID="btn_back" runat="server" CssClass="auto-style146" Height="36px" OnClick="btn_back_Click" Text="&lt;&lt;BACK" Width="88px" />
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr class="auto-style146">
                                                <td class="auto-style85"><strong></strong></td>
                                                <td colspan="3"><strong></strong></td>
                                                <td><strong></strong></td>
                                            </tr>
                                        </table>
                                    
                                    </asp:View>
                                </td>
                                <td class="auto-style10">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="auto-style41"></td>
                            </tr>
                            <tr>
                                <td class="auto-style12">
                                    <asp:View ID="View4" runat="server">
                                        
                                        <table class="auto-style155">
                                            <tr>
                                                <td class="auto-style86"></td>
                                                <td colspan="3" class="auto-style161"><strong>ROUTINE CHECKUP</strong></td>
                                                <td></span></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style87">
                                                    <strong>
                                                    <asp:Label ID="lbl_routinetest" runat="server" CssClass="auto-style37" Text="Name of Test"></asp:Label>
                                                    </strong>
                                                </td>
                                                <td class="auto-style44">
                                                    <strong>
                                                    <asp:TextBox ID="txt_routinetest" runat="server" CssClass="auto-style152"></asp:TextBox>
                                                    </strong>
                                                </td>
                                                <td class="auto-style43">
                                                    <strong>
                                                    <asp:Label ID="lbl_routinevalue" runat="server" CssClass="auto-style146" Text="Value"></asp:Label>
                                                    </strong>
                                                </td>
                                                <td class="auto-style42">
                                                    <strong>
                                                    <asp:TextBox ID="txt_routinevalue" runat="server" CssClass="auto-style146"></asp:TextBox>
                                                    </strong>
                                                </td>
                                                <td class="auto-style4">
                                                    <strong>
                                                    <asp:Button ID="btn_routinetest" runat="server" CssClass="auto-style152" Height="23px" OnClick="btn_routinetest_Click" Text="ADD" Width="120px" />
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style163"><span class="auto-style37"></span></td>
                                                <td colspan="3">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style86"></td>
                                                <td colspan="3"><span class="auto-style122"></span></td>
                                                <td></span></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style163"><strong></span></strong></td>
                                                <td colspan="3" class="auto-style3">
                                                    <strong>
                                                    <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" CellPadding="4" CssClass="auto-style152" DataKeyNames="routineid" ForeColor="#333333" GridLines="None" Height="180px" Width="423px">
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <Columns>
                                                            <asp:BoundField DataField="routinename" HeaderText="Name of Test" />
                                                            <asp:BoundField DataField="routinevalue" HeaderText="Value" />
                                                            <asp:BoundField DataField="routinedate" HeaderText="Date" />
                                                        </Columns>
                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                    </strong>
                                                </td>
                                                <td><span class="auto-style37"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style86"></td>
                                                <td colspan="3" class="auto-style152"><strong></span></strong></td>
                                                <td>
                                                    <strong>
                                                    <asp:Button ID="btn_routineback" runat="server" CssClass="auto-style146" Height="36px" OnClick="btn_routineback_Click" Text="BACK" Width="88px" />
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr class="auto-style37">
                                                <td class="auto-style86"></td>
                                                <td colspan="3"><span class="auto-style122"></span></td>
                                                <td></span></td>
                                            </tr>
                                        </table>
                                    
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style12">&nbsp;</td>
                            </tr>
                        </table>
                    </asp:MultiView>
                </td>
                <td>&nbsp;</td>
            </tr>
            </table>
    
    </div>
  </form> 

</asp:Content>


