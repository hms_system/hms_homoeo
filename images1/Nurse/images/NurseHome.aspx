﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Nurse/NurseMasterPage.master" AutoEventWireup="true" CodeFile="NurseHome.aspx.cs" Inherits="Nurse_images_NurseHome" %>
<script runat="server">  
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
    <title></title>
     <link href="../layout/styles/layout.css" rel="stylesheet" />

    
    <style type="text/css">
        .auto-style1 {
            text-align: right;
        }
        .auto-style2 {
            text-align: left;
        }
        .auto-style4 {
            text-align: center;
            width: 237px;
        }
        .auto-style5 {
            width: 237px;
        }
        .auto-style6 {
            text-align: left;
            width: 237px;
            height: 16px;
        }
        .auto-style7 {
            height: 48px;
        }
        .auto-style9 {
            text-align: center;
            height: 16px;
        }
        .auto-style10 {
            height: 16px;
            width: 504px;
        }
        .auto-style11 {
            color: #0099FF;
        }
        .auto-style12 {
            color: #0066FF;
        }
        .auto-style15 {
            height: 48px;
            width: 104px;
        }
        .auto-style16 {
            text-align: center;
            width: 230px;
            height: 48px;
        }
        .auto-style17 {
            width: 504px;
        }
        .auto-style18 {
            text-align: center;
            width: 504px;
        }
        .auto-style19 {
            text-align: center;
            width: 237px;
            height: 48px;
        }
    </style>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
   <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>&nbsp;</td>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style17">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style7">
                    </td>
                <td class="auto-style19">
                    <asp:ImageButton ID="imgbtn_obv" runat="server" Height="90px" ImageUrl="~/Nurse/images/obs1.jpg" OnClick="imgbtn_obv_Click" Width="90px" />
                    <strong>
                    <br />
                    </strong>
                    <asp:Label ID="Label1" runat="server" CssClass="auto-style11" Text="PATIENTS IN OBSERVATION"></asp:Label>
                </td>
                <td class="auto-style18">
                    <asp:ImageButton ID="imgbtn_ip" runat="server" Height="90px" ImageUrl="~/Nurse/images/ip.jpeg" OnClick="imgbtn_ip_Click" Width="90px" />
                    <br />
                    <asp:Label ID="Label2" runat="server" CssClass="auto-style12" Text="IP PATIENTS"></asp:Label>
                </td>
                <td class="auto-style16">;<asp:ImageButton ID="imgbtn_logout" runat="server" Height="90px" ImageUrl="~/Nurse/images/logout3.png" OnClick="imgbtn_logout_Click" Width="90px" />
                    <br />
                    <asp:Label ID="Label3" runat="server" CssClass="auto-style12" Text="LOGOUT"></asp:Label>
                </td>
                <td class="auto-style15">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style7">
                    &nbsp;</td>
                <td class="auto-style19">
                    &nbsp;</td>
                <td class="auto-style18">
                    &nbsp;</td>
                <td class="auto-style16">&nbsp;</td>
                <td class="auto-style15">&nbsp;</td>
            </tr>
            
        </table>
    
    </div>
 
</form>
</asp:Content>






