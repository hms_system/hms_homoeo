﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Nurse_images_NurseHome : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void imgbtn_obv_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("ObservationPatients.aspx");
    }
    protected void imgbtn_ip_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("IPPatients.aspx");
    }
    protected void imgbtn_logout_Click(object sender, ImageClickEventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("~/Login.aspx");
    }
}