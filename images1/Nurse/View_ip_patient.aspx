﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Nurse/NurseMaster.master" AutoEventWireup="true" CodeFile="View_ip_patient.aspx.cs" Inherits="Nurse_View_ip_patient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <title></title>
     <link rel="stylesheet" type="text/css" href="reset.css" />
    <link rel="stylesheet" type="text/css" href="style.css" /> 
   
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style9 {
            height: 23px;
        }
        .auto-style10 {
            width: 9px;
        }
        .auto-style11 {
            width: 232px;
            height: 23px;
        }
        .auto-style12 {
            width: 743px;
        }
        .auto-style13 {
            text-align: center;
            }
        .auto-style14 {
            text-decoration: underline;
        }
        .auto-style15 {
            width: 345px;
            height: 16px;
            margin-left: 80px;
            text-align: left;
        }
        .auto-style16 {
            width: 150px;
        }
        .auto-style17 {
            width: 150px;
            height: 23px;
            text-align: justify;
        }
        .auto-style18 {
            width: 150px;
            text-align: justify;
            height: 32px;
        }
        .auto-style19 {
            width: 150px;
            height: 23px;
        }
        .auto-style24 {
            height: 23px;
            text-align: center;
        }
        .auto-style25 {
            width: 345px;
            height: 23px;
            text-align: center;
        }
        .auto-style26 {
            width: 150px;
            height: 23px;
            text-align: center;
        }
        .auto-style30 {
            width: 169px;
            height: 16px;
            margin-left: 80px;
        }
        .auto-style31 {
            width: 169px;
            height: 23px;
            text-align: center;
        }
        .auto-style34 {
            width: 146px;
            height: 16px;
            margin-left: 80px;
            text-align: left;
        }
        .auto-style35 {
            width: 146px;
            height: 23px;
            text-align: center;
        }
        .auto-style36 {
            width: 169px;
            height: 23px;
        }
        .auto-style37 {
            width: 169px;
        }
        .auto-style38 {
            height: 23px;
            text-align: center;
            text-decoration: underline;
        }
        .auto-style39 {
            height: 23px;
            width: 34px;
        }
        .auto-style40 {
            width: 743px;
            height: 23px;
        }
        .auto-style41 {
            width: 9px;
            height: 23px;
        }
        .auto-style42 {
            text-align: center;
            text-decoration: underline;
        }
        .auto-style45 {
            width: 345px;
        }
        .auto-style47 {
            width: 146px;
        }
        .auto-style53 {
            width: 146px;
            text-align: left;
            height: 32px;
        }
        .auto-style54 {
            width: 345px;
            text-align: left;
            height: 32px;
        }
        .auto-style55 {
            width: 146px;
            height: 23px;
            text-align: left;
        }
        .auto-style56 {
            width: 345px;
            height: 23px;
            text-align: left;
        }
        .auto-style57 {
            width: 333px;
            height: 23px;
        }
        .auto-style58 {
            width: 333px;
        }
        .auto-style59 {
            width: 333px;
            height: 23px;
            text-align: center;
        }
        .auto-style60 {
            height: 16px;
        }
        .auto-style61 {
            height: 16px;
            text-align: center;
            width: 336px;
        }
        .auto-style63 {
            height: 16px;
            width: 336px;
        }
        .auto-style64 {
            height: 16px;
            text-align: right;
            width: 336px;
        }
        .auto-style65 {
            width: 336px;
        }
        .auto-style66 {
            color: #333399;
        }
        .auto-style67 {
            color: #000066;
        }
        .auto-style68 {
            width: 333px;
            height: 16px;
        }
        .auto-style69 {
            width: 150px;
            height: 16px;
        }
        .auto-style70 {
            width: 345px;
            height: 16px;
        }
        .auto-style71 {
            width: 146px;
            height: 16px;
        }
        .auto-style72 {
            width: 169px;
            height: 16px;
        }
        .auto-style73 {
            width: 150px;
            height: 16px;
            text-align: justify;
        }
        .auto-style74 {
            width: 333px;
            height: 30px;
        }
        .auto-style75 {
            width: 150px;
            height: 30px;
            text-align: justify;
        }
        .auto-style76 {
            width: 345px;
            height: 30px;
            text-align: left;
        }
        .auto-style77 {
            width: 146px;
            height: 30px;
            text-align: left;
        }
        .auto-style78 {
            width: 169px;
            height: 30px;
        }
        .auto-style79 {
            height: 30px;
        }
        .auto-style80 {
            width: 333px;
            height: 32px;
        }
        .auto-style81 {
            width: 169px;
            height: 32px;
        }
        .auto-style82 {
            height: 32px;
        }
        .auto-style83 {
            text-align: justify;
        }
        .auto-style84 {
            height: 23px;
            width: 94px;
            text-align: left;
        }
        .auto-style85 {
            height: 23px;
            text-align: center;
            text-decoration: underline;
            width: 94px;
        }
        .auto-style86 {
            height: 23px;
            text-align: center;
            text-decoration: underline;
            width: 115px;
        }
        .auto-style87 {
            height: 23px;
            width: 115px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <form runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server">
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style12">
                                    <asp:View ID="View1" runat="server">
                                        <table class="auto-style1">
                                            <tr>
                                                <td class="auto-style13" colspan="6">
                                                    <h3 class="auto-style14"><strong>PATIENT DETAILS</strong></h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style57">
                                                    <asp:LinkButton ID="lbtn_ip_vital_parameters" runat="server" CssClass="auto-style66" OnClick="lbtn_ip_vital_parameters_Click">Vital Parameters</asp:LinkButton>
                                                </td>
                                                <td class="auto-style17"></td>
                                                <td class="auto-style56">
                                                    <strong>Admission No.</strong></td>
                                                <td class="auto-style55">
                                                    <asp:Label ID="lbl_ip_admsn_no" runat="server"></asp:Label>
                                                </td>
                                                <td class="auto-style36">
                                                    </td>
                                                <td class="auto-style9"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style57"></td>
                                                <td class="auto-style17">
                                                    &nbsp;</td>
                                                <td class="auto-style56">
                                                    <strong>
                                                    <asp:Label ID="lbl_card_no" runat="server" Text="Card Number"></asp:Label>
                                                    </strong>
                                                </td>
                                                <td class="auto-style55">
                                                    <asp:Label ID="lbl_cardno" runat="server"></asp:Label>
                                                </td>
                                                <td class="auto-style36"></td>
                                                <td class="auto-style9">
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style57">
                                                    <asp:LinkButton ID="lbtn_routine_checkup" runat="server" CssClass="auto-style67" OnClick="lbtn_routine_checkup_Click">Routine Checkup</asp:LinkButton>
                                                </td>
                                                <td class="auto-style17">&nbsp;</td>
                                                <td class="auto-style56">
                                                    <strong>Name</strong></td>
                                                <td class="auto-style55">
                                                    <asp:Label ID="lbl_ip_patient_name" runat="server"></asp:Label>
                                                </td>
                                                <td class="auto-style36">
                                                    &nbsp;</td>
                                                <td class="auto-style9">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style68"></td>
                                                <td class="auto-style73"></td>
                                                <td class="auto-style15">
                                                    <strong>Age</strong></td>
                                                <td class="auto-style34">
                                                    <asp:Label ID="lbl_age" runat="server"></asp:Label>
                                                </td>
                                                <td class="auto-style30"></td>
                                                <td class="auto-style60"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style74">
                                                    <asp:LinkButton ID="lbtn_ward_details" runat="server" OnClick="lbtn_ward_details_Click" CssClass="auto-style67">Ward Details</asp:LinkButton>
                                                </td>
                                                <td class="auto-style75">&nbsp;</td>
                                                <td class="auto-style76">
                                                    <strong>Gender</strong></td>
                                                <td class="auto-style77">
                                                    <asp:Label ID="lbl_gender" runat="server"></asp:Label>
                                                </td>
                                                <td class="auto-style78">
                                                    &nbsp;</td>
                                                <td class="auto-style79">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style80"></td>
                                                <td class="auto-style18"></td>
                                                <td class="auto-style54">
                                                    <strong>Address</strong><br />
                                                    </td>
                                                <td class="auto-style53">
                                                    <asp:Label ID="lbl_hname" runat="server"></asp:Label>
                                                    <br />
                                                    <asp:Label ID="lbl_place" runat="server"></asp:Label>
                                                </td>
                                                <td class="auto-style81"></td>
                                                <td class="auto-style82"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style57">
                                                    <asp:LinkButton ID="lbtn_bystander" runat="server" OnClick="lbtn_bystander_Click" CssClass="auto-style67" >ByStander Details</asp:LinkButton>
                                                </td>
                                                <td class="auto-style17">&nbsp;</td>
                                                <td class="auto-style56">
                                                    <strong>Contact No.</strong></td>
                                                <td class="auto-style55">
                                                    <asp:Label ID="lbl_contact" runat="server"></asp:Label>
                                                </td>
                                                <td class="auto-style36">
                                                    &nbsp;</td>
                                                <td class="auto-style9">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style57"></td>
                                                <td class="auto-style17">&nbsp;</td>
                                                <td class="auto-style56">
                                                    &nbsp;<strong>Annual Income</strong></td>
                                                <td class="auto-style55">
                                                    <asp:TextBox ID="txt_annualincome" runat="server" Width="79px"></asp:TextBox>
                                                    <asp:Button ID="btn_add_income" runat="server" Height="22px" OnClick="btn_add_income_Click" Text="Add" Width="51px" />
                                                </td>
                                                <td class="auto-style36">&nbsp;</td>
                                                <td class="auto-style9">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style57">
                                                    <asp:LinkButton ID="lbtn_med_management" runat="server" OnClick="lbtn_med_management_Click" CssClass="auto-style67">Medicines &amp; Management</asp:LinkButton>
                                                </td>
                                                <td class="auto-style19"></td>
                                                <td class="auto-style56"></td>
                                                <td class="auto-style55">&nbsp;</td>
                                                <td class="auto-style36">
                                                    &nbsp;</td>
                                                <td class="auto-style9">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style59">&nbsp;</td>
                                                <td class="auto-style26">&nbsp;</td>
                                                <td class="auto-style25">&nbsp;</td>
                                                <td class="auto-style35">&nbsp;</td>
                                                <td class="auto-style31">&nbsp;</td>
                                                <td class="auto-style24">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style68">
                                                    <asp:LinkButton ID="lbtn_dischrge_summary" runat="server" CssClass="auto-style67" OnClick="lbtn_dischrge_summary_Click">Discharge Summary</asp:LinkButton>
                                                </td>
                                                <td class="auto-style69"></td>
                                                <td class="auto-style70"></td>
                                                <td class="auto-style71"></td>
                                                <td class="auto-style72">
                                                    </td>
                                                <td class="auto-style60"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style58">&nbsp;</td>
                                                <td class="auto-style16">&nbsp;</td>
                                                <td class="auto-style45">&nbsp;</td>
                                                <td class="auto-style47">&nbsp;</td>
                                                <td class="auto-style37">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                                <td class="auto-style10">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style12">
                                    <asp:View ID="View2" runat="server">
                                        <table class="auto-style1">
                                            <tr>
                                                <td class="auto-style38" colspan="6"><strong>BYSTANDER INFORMATIONS</strong></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td colspan="4">
                                                    &nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbl_bystander_name" runat="server" Text="Name"></asp:Label>
                                                </td>
                                                <td colspan="4">
                                                    <asp:TextBox ID="txt_bystander_name" runat="server" Height="36px" Width="220px"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="4">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbl_bystander_address" runat="server" Text="Address"></asp:Label>
                                                </td>
                                                <td colspan="4">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="2">
                                                    <asp:Label ID="lbl_bystander_hname" runat="server" Text="House Name"></asp:Label>
                                                </td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="txt_bystander_hname" runat="server" Width="248px" Height="30px"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="2">
                                                    <asp:Label ID="lbl_bystander_place" runat="server" Text="Street/Place"></asp:Label>
                                                </td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="txt_bystander_place" runat="server" Height="39px" Width="243px"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="2">
                                                    <asp:Label ID="lbl_district" runat="server" Text="District"></asp:Label>
                                                </td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="txt_bystander_district" runat="server" Height="33px" Width="244px"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="2">
                                                    <asp:Label ID="lbl_state" runat="server" Text="State"></asp:Label>
                                                </td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="txt_bystander_state" runat="server" Height="35px" Width="243px"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="2">
                                                    <asp:Label ID="lbl_pin" runat="server" Text="Pin Code"></asp:Label>
                                                </td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="txt_bystander_pin" runat="server" Height="18px" Width="243px"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style9">
                                                    &nbsp;</td>
                                                <td class="auto-style9" colspan="2">
                                                    <asp:Label ID="lbl_contact_no" runat="server" Text="Contact No"></asp:Label>
                                                </td>
                                                <td class="auto-style9" colspan="2">
                                                    <asp:TextBox ID="txt_bystander_contact" runat="server" Height="22px" Width="244px"></asp:TextBox>
                                                </td>
                                                <td class="auto-style9"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style9"></td>
                                                <td class="auto-style9" colspan="2"></td>
                                                <td class="auto-style9" colspan="2"></td>
                                                <td class="auto-style9"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style9">
                                                    <asp:Label ID="lbl_relation" runat="server" Text="Relation"></asp:Label>
                                                </td>
                                                <td class="auto-style11">
                                                    <asp:DropDownList ID="ddl_relation" runat="server" AutoPostBack="True" Height="32px" Width="179px" OnSelectedIndexChanged="ddl_relation_SelectedIndexChanged">
                                                        <asp:ListItem Value="0">Family </asp:ListItem>
                                                        <asp:ListItem Value="1">Third Party</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="auto-style39" colspan="2">
                                                    <asp:Label ID="lbl_specify" runat="server" Text="Specify"></asp:Label>
                                                </td>
                                                <td class="auto-style9">
                                                    <asp:TextBox ID="txt_specify" runat="server" Width="215px" Height="27px"></asp:TextBox>
                                                </td>
                                                <td class="auto-style9"></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="4">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style9">
                                                    <asp:Button ID="btn_back1" runat="server" Height="29px" Text="&lt;&lt;BACK" Width="117px" OnClick="btn_back1_Click" />
                                                </td>
                                                <td class="auto-style24" colspan="4">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btn_add" runat="server" Text="ADD" Width="143px" Height="27px" OnClick="btn_add_Click" />
                                                </td>
                                                <td class="auto-style9">
                                                    <asp:Button ID="btn_next1" runat="server" Height="30px" Text="NEXT&gt;&gt;" Width="106px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="4">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                                <td class="auto-style10">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style12">
                                    <asp:View ID="View3" runat="server">
                                        <table class="auto-style1">
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style38" colspan="3"><strong>WARD INFORMATION</strong></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbl_ward_type" runat="server" Text="Type of Ward"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rbl_ward" runat="server" AutoPostBack="True" Height="43px" RepeatDirection="Horizontal" Width="362px">
                                                        <asp:ListItem Value="0">Male Ward</asp:ListItem>
                                                        <asp:ListItem Value="1">Female Ward</asp:ListItem>
                                                        <asp:ListItem Value="2">Pay Ward</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Bed No.</td>
                                                <td>
                                                    <asp:DropDownList ID="ddl_bedno" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbl_diet_type" runat="server" Text="Type of diet"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rbn_diet_type" runat="server" AutoPostBack="True">
                                                        <asp:ListItem>Half Milk Diet</asp:ListItem>
                                                        <asp:ListItem>Half Diet</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                                <td class="auto-style10">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style12">
                                    &nbsp;</td>
                                <td class="auto-style10">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style12">
                                    <asp:View ID="View4" runat="server">
                                        <table class="auto-style1">
                                            <tr>
                                                <td class="auto-style42" colspan="3"><strong>MEDICINES &amp; MANAGEMENT</strong></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style63"></td>
                                                <td class="auto-style60"></td>
                                                <td class="auto-style60"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style61">
                                                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="prescription_id" ForeColor="#333333" GridLines="None" Height="153px" Width="280px" >
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:BoundField DataField="drug_name" HeaderText="Name of Medicine" />
                                                <asp:BoundField DataField="dose" HeaderText="Dossage" />
                                                <asp:BoundField DataField="frequency" HeaderText="Frequency" />
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>    
                                                </td>
                                                <td class="auto-style60">&nbsp;</td>
                                                <td class="auto-style60">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style63">&nbsp;</td>
                                                <td class="auto-style60">&nbsp;</td>
                                                <td class="auto-style60">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style64">
                                                    <asp:Label ID="lbl_ip_medicin_name" runat="server" Text="Name of Medicine"></asp:Label>
                                                </td>
                                                <td class="auto-style60">
                                                    <asp:TextBox ID="txt_ip_medicin_name" runat="server" Height="16px" Width="195px"></asp:TextBox>
                                                </td>
                                                <td class="auto-style60">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style64">
                                                    <asp:Label ID="lbl_ip_dosage" runat="server" Text="Dosage"></asp:Label>
                                                </td>
                                                <td class="auto-style60">
                                                     <asp:TextBox ID="txt_ip_dosage" runat="server" Height="16px" Width="195px"></asp:TextBox>
                                                     &nbsp;&nbsp;&nbsp;
                                                     <asp:Button ID="btn_ip_nurse_add" runat="server" Height="20px" OnClick="btn_nurse_add_Click" Text="Add" Width="50px" />
                                                </td>
                                                <td class="auto-style60">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style63">&nbsp;</td>
                                                <td class="auto-style60">&nbsp;</td>
                                                <td class="auto-style60">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style65">&nbsp;</td>
                                                <td class="auto-style83">
                                                    <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Height="124px" Width="303px">
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <Columns>
                                                            <asp:BoundField DataField="medicin_name" HeaderText="Name of Medicine" />
                                                            <asp:BoundField DataField="dose" HeaderText="Dose" />
                                                            <asp:BoundField DataField="time" HeaderText="Time" />
                                                        </Columns>
                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style63"></td>
                                                <td class="auto-style60"></td>
                                                <td class="auto-style60"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style65">&nbsp;</td>
                                                <td>
                                                    <asp:Button ID="btn_medback" runat="server" OnClick="btn_medback_Click" Text="&lt;&lt;BACK" Width="115px" />
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                                <td class="auto-style10">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style12">
                                    <asp:View ID="View5" runat="server">
                                       
                                        <table class="auto-style1">
                                            <tr>
                                                <td class="auto-style85">&nbsp;</td>
                                                <td class="auto-style13" colspan="3">VITAL PARAMETERS</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style85">&nbsp;</td>
                                                <td colspan="3">&nbsp;</td>
                                                <td></span></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style84">
                                                    <asp:Label ID="lbl_test" runat="server" Text="Name of Test" CssClass="auto-style37"></asp:Label>
                                                </td>
                                                <td class="auto-style9">
                                                    <asp:DropDownList ID="ddl_test_attributes" runat="server" AutoPostBack="True" CssClass="auto-style37" >
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="auto-style9">
                                                    <asp:Label ID="lbl_value" runat="server" Text="Value" CssClass="auto-style37"></asp:Label>
                                                </td>
                                                <td class="auto-style38">
                                                    <asp:TextBox ID="txt_test_values" runat="server" CssClass="auto-style37"></asp:TextBox>
                                                </td>
                                                <td class="auto-style9">
                                                    <asp:Button ID="btn_vitaladd" runat="server" OnClick="btn_vitaladd_Click" Text="ADD" CssClass="auto-style37" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style85"><span class="auto-style37"></span></td>
                                                <td colspan="3">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style85"></td>
                                                <td colspan="3">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style85"></span></td>
                                                <td colspan="3" class="auto-style3">
                                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" DataKeyNames="invs_detail_id" Height="134px" Width="423px" CssClass="auto-style37" >
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <Columns>
                                                            <asp:BoundField DataField="investigation_attribute" HeaderText="Name of Test" />
                                                            <asp:BoundField DataField="test_value" HeaderText="Value" />
                                                            <asp:BoundField DataField="date" HeaderText="Date" />
                                                        </Columns>
                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                                <td><span class="auto-style37"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style85">&nbsp;</td>
                                                <td colspan="3"></span></td>
                                                <td>
                                                    <asp:Button ID="btn_back" runat="server" OnClick="btn_back_Click" Text="&lt;&lt;BACK" Height="36px" Width="88px" CssClass="auto-style37" />
                                                </td>
                                            </tr>
                                            <tr class="auto-style37">
                                                <td class="auto-style85">&nbsp;</td>
                                                <td colspan="3">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    
                                    </asp:View>
                                </td>
                                <td class="auto-style10">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style40">
                                    <asp:View ID="View6" runat="server">
                                        <table class="auto-style1">
                                            <tr>
                                                <td class="auto-style42" colspan="4"><strong>DISCHARGE SUMMARY</strong></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style60"></td>
                                                <td class="auto-style60"></td>
                                                <td class="auto-style60"></td>
                                                <td class="auto-style60"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style60"></td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="lbl_dispatname" runat="server" Text="Name of Patient"></asp:Label>
                                                </td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="lbl_dis_dispname" runat="server"></asp:Label>
                                                    <br />
                                                </td>
                                                <td class="auto-style60"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style60">&nbsp;</td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="lbl_disage" runat="server" Text="Age"></asp:Label>
                                                </td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="lbl_dis_dispage" runat="server"></asp:Label>
                                                    <br />
                                                </td>
                                                <td class="auto-style60">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style60"></td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="lbl_disgender" runat="server" Text="Gender"></asp:Label>
                                                </td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="lbl_dis_dispgender" runat="server"></asp:Label>
                                                    <br />
                                                </td>
                                                <td class="auto-style60"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style60">&nbsp;</td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="lbl_dis_address" runat="server" Text="Address"></asp:Label>
                                                </td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="Label13" runat="server" Text="Label"></asp:Label>
                                                    <br />
                                                </td>
                                                <td class="auto-style60">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style60"></td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="Label5" runat="server" Text="Date of Admission"></asp:Label>
                                                </td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="Label14" runat="server" Text="Label"></asp:Label>
                                                    <br />
                                                </td>
                                                <td class="auto-style60"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style60">&nbsp;</td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="Label6" runat="server" Text="Date of Discharge"></asp:Label>
                                                </td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="Label15" runat="server" Text="Label"></asp:Label>
                                                    <br />
                                                </td>
                                                <td class="auto-style60">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style60">&nbsp;</td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="Label7" runat="server" Text="Provisional Diagonosis"></asp:Label>
                                                </td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="Label16" runat="server" Text="Label"></asp:Label>
                                                    <br />
                                                </td>
                                                <td class="auto-style60">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style60">&nbsp;</td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="Label8" runat="server" Text="Medicines"></asp:Label>
                                                </td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="Label17" runat="server" Text="Label"></asp:Label>
                                                    <br />
                                                </td>
                                                <td class="auto-style60">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style60"></td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="Label9" runat="server" Text="Advice"></asp:Label>
                                                </td>
                                                <td class="auto-style60">
                                                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="auto-style60"></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style60">&nbsp;</td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="Label10" runat="server" Text="Reason For Discharge"></asp:Label>
                                                </td>
                                                <td class="auto-style60">
                                                    <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                    //improved,worst,discharge at request,discharge against advice,ubscounting</td>
                                                <td class="auto-style60">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style60">&nbsp;</td>
                                                <td class="auto-style60">
                                                    <asp:Label ID="Label11" runat="server" Text="Signature"></asp:Label>
                                                </td>
                                                <td class="auto-style60">&nbsp;</td>
                                                <td class="auto-style60">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <asp:CheckBox ID="CheckBox1" runat="server" Text="Certificate Issued" />
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="2">
                                                    <asp:Button ID="btn_discharge" runat="server" OnClick="btn_discharge_Click" Text="&lt;&lt;BACK" />
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                                <td class="auto-style41"></td>
                            </tr>
                            <tr>
                                <td class="auto-style12">
                                    <asp:View ID="View7" runat="server">
                                        
                                        <table class="auto-style1">
                                            <tr>
                                                <td class="auto-style86"></td>
                                                <td class="auto-style13" colspan="3">ROUTINE CHECKUP</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style86"></td>
                                                <td colspan="3">&nbsp;</td>
                                                <td></span></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style87">
                                                    <asp:Label ID="lbl_routinetest" runat="server" Text="Name of Test" CssClass="auto-style37"></asp:Label>
                                                </td>
                                                <td class="auto-style44">
                                                    <asp:TextBox ID="txt_routinetest" runat="server"></asp:TextBox>
                                                </td>
                                                <td class="auto-style43">
                                                    <asp:Label ID="lbl_routinevalue" runat="server" Text="Value" CssClass="auto-style37"></asp:Label>
                                                </td>
                                                <td class="auto-style42">
                                                    <asp:TextBox ID="txt_routinevalue" runat="server" CssClass="auto-style37"></asp:TextBox>
                                                </td>
                                                <td class="auto-style4">
                                                    <asp:Button ID="btn_routinetest" runat="server" OnClick="btn_routinetest_Click" Text="ADD" Height="23px" Width="120px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style86"><span class="auto-style37"></span></td>
                                                <td colspan="3">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style86"></td>
                                                <td colspan="3">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style86"></span></td>
                                                <td colspan="3" class="auto-style3">
                                                    <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" DataKeyNames="routineid" Height="180px" Width="423px" >
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <Columns>
                                                            <asp:BoundField DataField="routinename" HeaderText="Name of Test" />
                                                            <asp:BoundField DataField="routinevalue" HeaderText="Value" />
                                                            <asp:BoundField DataField="routinedate" HeaderText="Date" />
                                                        </Columns>
                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </td>
                                                <td><span class="auto-style37"></span></td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style86"></td>
                                                <td colspan="3"></span></td>
                                                <td>
                                                    <asp:Button ID="btn_routineback" runat="server" OnClick="btn_back_Click" Text="&lt;&lt;BACK" Height="36px" Width="88px" CssClass="auto-style37" />
                                                </td>
                                            </tr>
                                            <tr class="auto-style37">
                                                <td class="auto-style86"></td>
                                                <td colspan="3">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    
                                    </asp:View>
                                </td>
                                <td class="auto-style10">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style12">&nbsp;</td>
                                <td class="auto-style10">&nbsp;</td>
                            </tr>
                        </table>
                    </asp:MultiView>
                </td>
                <td>&nbsp;</td>
            </tr>
            </table>
    
    </div>
  </form> 

</asp:Content>

