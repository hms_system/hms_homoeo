﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


//functions used to insert the data of interns to the database

public partial class InternRegistrationForm : System.Web.UI.Page
{
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
        if (!IsPostBack)
        {

            lbl_perioderror.Visible = false;
        }

    }



    protected void btn_back_Click(object sender, EventArgs e)
    {
        Response.Redirect("Login.aspx");
    }
    protected void btn_clear_Click(object sender, EventArgs e)
    {
        txt_name.Text = "";
        txt_address.Text = "";
        txt_contact.Text = "";
        txt_dob.Text = "";
        txt_email.Text = "";
        txt_gaurdian.Text = "";
        txt_institute.Text = "";
        txt_joiningdate.Text = "";
        txt_nationality.Text = "";
    }


    protected void btn_next_Click(object sender, EventArgs e)
    {
        if (ddl_period.SelectedIndex > 0)
        {
            lbl_perioderror.Visible = false;
            c.getcon();

            string str_intern = "insert into Interns_Details values('" + txt_name.Text + "','" + txt_address.Text + "','" + txt_institute.Text + "','" + txt_dob.Text + "','" + rbl_gender.SelectedItem.Text + "','" + txt_nationality.Text + "','" + txt_gaurdian.Text + "','" + txt_joiningdate.Text + "','" + ddl_period.SelectedItem.Text + "','" + txt_email.Text + "','" + txt_contact.Text + "','Pending','')select @@Identity";
            SqlCommand cmd_intern = new SqlCommand(str_intern, c.con);
            object obj = new object();
            obj = cmd_intern.ExecuteScalar();
            Response.Write("<script>alert('Registration Completed Successfully');</script>");
            //cmd_intern.ExecuteNonQuery();
            //String str_sel = "";






            Response.Redirect("Intern_Reg.aspx?INID=" + obj);

            c.con.Close();

        }
    }
}