﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Nurse_N_WardHome : System.Web.UI.Page
{
    Connection c = new Connection();
    int wid = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            wid = Convert.ToInt32(Request.QueryString["WardId"].ToString());
            Session["wd"] = wid;

        }
        Bind();
    }
    public void Bind()
    {
        c.getcon();
        //int wid = Convert.ToInt32(Request.QueryString["WardId"].ToString());
        wid = Convert.ToInt32(Session["wd"]);
        SqlCommand cmd_bed = new SqlCommand("select * from Bed_master where ward_id='" + wid + "' ", c.con);
        SqlDataAdapter sda_bed = new SqlDataAdapter(cmd_bed);
        DataTable dt_bed = new DataTable();
        sda_bed.Fill(dt_bed);


        int k_bed = cmd_bed.ExecuteNonQuery();

        if (dt_bed.Rows.Count > 0)
        {
            int bid = 0;
            ImageButton[] i = new ImageButton[dt_bed.Rows.Count];
            Label[] l = new Label[dt_bed.Rows.Count];
            for (int j = 0; j < dt_bed.Rows.Count; j++)
            {
                String bname = Convert.ToString(dt_bed.Rows[j][2]);
                bid = Convert.ToInt32(dt_bed.Rows[j][0]);
                i[j] = new ImageButton();
                i[j].AlternateText = bid.ToString();
                l[j] = new Label();
                i[j].Click += (s, args) =>
                {
                    this.GotoPage(s, args);
                };
                i[j].ID = "image_" + j;
                l[j].ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");

                l[j].Text = "Bed " + bname;
                i[j].Height = 200;
                i[j].Width = 200;
                i[j].ImageUrl = "~/HeadNurse/images/bed.png";
                if (j % 2 == 0)
                {
                    Form.Controls.Add(new LiteralControl("<br /><br /><br /><br /><br />"));

                }
                Form.Controls.Add(i[j]);
                Form.Controls.Add(l[j]);
                Form.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
            }
            for (int j = 0; j < dt_bed.Rows.Count; j++)
            {
                bid = Convert.ToInt32(dt_bed.Rows[j][0]);
                i[j] = new ImageButton();
                i[j].AlternateText = bid.ToString();
                i[j].Click += (s, args) =>
                {
                    this.GotoPage(s, args);
                };
            }
            SqlCommand cmd_pat = new SqlCommand("select p.cardno,p.patient_name,p.dob,p.gender,m.bed_no from Patient_details p inner join Bed_allocation c on p.cardno=c.patient_id inner join Bed_master m on m.bed_id=c.bed_id where ward_id='" + wid + "' and timeout =' ' ", c.con);
            SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
            DataTable dt_pat = new DataTable();
            sda_pat.Fill(dt_pat);


            int k_pat = cmd_pat.ExecuteNonQuery();

            if (dt_pat.Rows.Count > 0)
            {
                for (int j = 0; j < dt_pat.Rows.Count; j++)
                {

                    String name = Convert.ToString(dt_pat.Rows[j][1]);
                    String pid = Convert.ToString(dt_pat.Rows[j][0]);
                    int id = Convert.ToInt32(dt_pat.Rows[j][4]);
                    DateTime dob = Convert.ToDateTime(dt_pat.Rows[j][2]);
                    String gender = Convert.ToString(dt_pat.Rows[j][3]);
                    int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
                    i[id - 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");
                    l[id - 1].ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");
                    l[id - 1].Text = l[id - 1].Text + "- (Patient Id- " + pid + " Age- " + age + " Gender- " + gender + " )";
                }
            }

        }


        Form.Controls.Add(new LiteralControl("<br /><br /><br /><br />"));

        Page.Controls.Add(new LiteralControl("<br />"));
        Page.Controls.Add(new LiteralControl("<br />"));

        c.con.Close();
    }
    public void GotoPage(object sender, ImageClickEventArgs e)
    {

        ImageButton clickedbutton = sender as ImageButton;
        c.getcon();
        SqlCommand cmd_pat = new SqlCommand("select * from Bed_allocation where bed_id='" + clickedbutton.AlternateText + "' and timeout =' ' ", c.con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            Response.Redirect("N_AllocatedPage.aspx?Bedid=" + clickedbutton.AlternateText);
        }
        //else

        //    Response.Redirect("AllocatePage.aspx?Bedid=" + clickedbutton.AlternateText);

        c.con.Close();


        // ImageButton clickedbutton = sender as ImageButton;

        //Response.Redirect("AllocatePage.aspx?Bedid=" + clickedbutton.AlternateText);



    }
}