﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Nurse/NurseMaster.master" AutoEventWireup="true" CodeFile="N_AllocatedPage.aspx.cs" Inherits="Nurse_N_AllocatedPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <form id="form1" runat="server">
        <table >
            <tr>
                <td class="auto-style1" >&nbsp;</td>
                <td class="auto-style1" ></td>
                <td class="auto-style1" ></td>
            </tr>
            <tr>
                <td class="label_small">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                <td class="label_small">Patient Id:</td>
                <td class="auto-style2">
                    <asp:Label ID="lbl_patno" runat="server" CssClass="label_black"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label_small">&nbsp;</td>
                <td class="label_small">Patient Name:</td>
                <td>
                    <asp:Label ID="lbl_pname" runat="server" CssClass="label_black"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label_small">&nbsp;</td>
                <td class="label_small">Gender:</td>
                <td class="auto-style4">
                    <asp:Label ID="lbl_gen" runat="server" CssClass="label_black"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label_small">&nbsp;</td>
                <td class="label_small">Age:</td>
                <td>
                    <asp:Label ID="lbl_age" runat="server" CssClass="label_black"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label_small">&nbsp;</td>
                <td class="label_small">Treatment Status:</td>
                <td>
                    <asp:Label ID="lbl_status" runat="server" CssClass="label_black"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label_small">&nbsp;</td>
                <td class="label_small">&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="label_small">&nbsp;</td>
                <td colspan="2" >
         <asp:Button ID="link_observ" runat="server" OnClick="link_observ_Click" Text="Observations" CssClass="btn-info" />
        <asp:Button ID="link_vitals" runat="server" OnClick="link_vit_Click" Text="Vitals" CssClass="btn-info" />
     
                </td>
            </tr>
            <tr>
                <td> &nbsp;</td>
                <td colspan="2"> 
                    <%--<div class="drop">
<ul class="drop_menu">

     <li>
         <asp:LinkButton ID="link_observ" runat="server" OnClick="link_observ_Click">Observations</asp:LinkButton>
     </li>
     <li>
     </li>
    <li>
        <asp:LinkButton ID="link_vitals" runat="server" OnClick="link_vit_Click">Vitals</asp:LinkButton>
     

     </li>
    <li>
        <asp:LinkButton ID="link_dis" runat="server" OnClick="link_dis_Click">Discharge</asp:LinkButton>
     
     </li>
</ul>
</div>--%>
</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td colspan="2">
                    <asp:MultiView ID="mulview_headnurse" runat="server">
                        <table class="auto-style1">
                            <tr>
                                <td>
                                    <asp:View ID="view_observations" runat="server">
                                        <table class="auto-style1">
                                            <tr>
                                                <td></td>
                                                <td class="auto-style1"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" CssClass="label_small" Text="Particulars:"></asp:Label>
                                                </td>
                                                <td >
                                                    <asp:TextBox ID="txt_obspart" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" CssClass="label_small" Text="Observation:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txt_observ" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label3" runat="server" CssClass="label_small" Text="Remarks:"></asp:Label>
                                                </td>
                                                <td >
                                                    <asp:TextBox ID="txt_rem" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td class="auto-style3">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style2">
                                                    &nbsp;</td>
                                                <td class="auto-style2">
                                                    <asp:Button ID="btn_add" runat="server" cssclass="buttonyellow" OnClick="btn_add_Click" Text="Add" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style2">&nbsp;</td>
                                                <td class="auto-style2">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:View ID="view_vitals" runat="server">
                                        <table>
                                             <tr>
            <td>&nbsp;</td>
                                                 <td>
                                                     <asp:Label ID="Label4" runat="server" CssClass="label_small" Text="Particulars"></asp:Label>
                                                 </td>
            <td>
                <asp:DropDownList ID="ddl_vitals" runat="server">
                </asp:DropDownList>
                                                 </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style2">
                <asp:Label ID="Label5" runat="server" CssClass="label_small" Text="Value"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_value" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
            <td class="auto-style3"></td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="auto-style1">&nbsp;</td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2"></td>
            <td class="auto-style2"></td>
            <td class="auto-style2"></td>
            <td class="auto-style2"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btn_addvit" runat="server" CssClass="buttonyellow" OnClick="btn_addvit_Click" Text="Add" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" Visible="False" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">
                &nbsp;</td>
            <td colspan="3">
                <asp:GridView ID="GridView_Vitals" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" OnPageIndexChanging="GridView_Vitals_PageIndexChanging" PageSize="50" Width="1000px">
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                    <Columns>
                        <asp:BoundField DataField="investigation_attribute" HeaderStyle-HorizontalAlign="Center" HeaderText="Particulars" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="test_value" HeaderStyle-HorizontalAlign="Center" HeaderText="Value" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                &nbsp;</td>
            <td class="auto-style2" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">
                &nbsp;</td>
            <td class="auto-style2" colspan="3">&nbsp;</td>
        </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                        </table>
                    </asp:MultiView>
                </td>
            </tr>
            </table>
    </form>

</asp:Content>

