﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Nurse_N_IPAdmissionlist : System.Web.UI.Page
{
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MultiView1.ActiveViewIndex = 0;
            c.getcon();
            bindgrid();
            bindgrid1();
            c.con.Close();
        }

    }
    public void bindgrid()
    {
        //pid = Convert.ToInt32(Request.QueryString["PID"].ToString());

        string str_ip_patient = "select * from patient_details p inner join IPAdmission_details ip on p.cardno=ip.patient_id where ip.date_of_admission=Convert(datetime,'" + DateTime.Now.Date + "',105) and ip.status=@ipstatus  ";

        SqlCommand cmd_ip_patient = new SqlCommand(str_ip_patient, c.con);
        cmd_ip_patient.Parameters.AddWithValue("@ipstatus", "Treatment Pending");
        DataTable dt_ip_patient = new DataTable();
        SqlDataAdapter da_ip_patient = new SqlDataAdapter(cmd_ip_patient);
        da_ip_patient.Fill(dt_ip_patient);
        if (dt_ip_patient.Rows.Count > 0)
        {

            GridView1.DataSource = dt_ip_patient;
            GridView1.DataBind();
        }



    }
    public void bindgrid1()
    {
        //pid = Convert.ToInt32(Request.QueryString["PID"].ToString());

        string ip_patient = "select * from patient_details p inner join IPAdmission_details ip on p.cardno=ip.patient_id where ip.status='Treatment Pending' ";

        SqlCommand cmd_patient = new SqlCommand(ip_patient, c.con);
        DataTable dt_patient = new DataTable();
        SqlDataAdapter da_patient = new SqlDataAdapter(cmd_patient);
        da_patient.Fill(dt_patient);
        if (dt_patient.Rows.Count > 0)
        {

            GridView2.DataSource = dt_patient;
            GridView2.DataBind();
        }



    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }
    //protected void imgbtndiscrgesumry_Click(object sender, ImageClickEventArgs e)
    //{
    //    Response.Redirect("PrintDischargeSummary.aspx");
    //}
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        Response.Redirect("N_IPAdmission.aspx");
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "SelectButton")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];
            Response.Redirect("N_ViewIPpatient.aspx?IPID=" + row.Cells[0].Text);
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        // Response.Redirect("TodayIPList.aspx");
        MultiView1.ActiveViewIndex = 1;
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }
    protected void Button3_Click(object sender, EventArgs e)
    {

    }
}