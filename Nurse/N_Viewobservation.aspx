﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Nurse/NurseMaster.master" AutoEventWireup="true" CodeFile="N_Viewobservation.aspx.cs" Inherits="Nurse_N_Viewobservation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <form id="form1" runat="server">
    
    
        <asp:MultiView ID="MultiView1" runat="server">
            
                        <asp:View ID="View1" runat="server">
                            <table>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbl_card_no" runat="server" CssClass="label_small" Text="Card No."></asp:Label>
                                        </td>
                                    <td>
                                        <asp:Label ID="lbl_view_card_no" runat="server" CssClass="label_black"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbl_patient_name" runat="server" CssClass="label_small" Text="Patient Name"></asp:Label>
                                       </td>
                                    <td>
                                        <asp:Label ID="lbl_view_patient_name" runat="server" CssClass="label_black"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbl_address" runat="server" CssClass="label_small" Text="Address"></asp:Label>
                                        </td>
                                    <td>
                                        <asp:Label ID="lbl_housename" runat="server" CssClass="label_black"></asp:Label>
                                        <br />
                                        <asp:Label ID="lbl_place" runat="server" CssClass="label_black"></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_contact_no" runat="server" CssClass="label_small" Text="Contact No."></asp:Label>
                                       </td>
                                    <td>
                                        <asp:Label ID="lbl_view_contact_no" runat="server" CssClass="label_black"></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_instruction" runat="server" CssClass="label_small" Text="Instructions from Medical Officer"></asp:Label>
                                        </td>
                                    <td>
                                        <asp:Label ID="lbl_instr_view" runat="server" CssClass="label_black"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><%--<asp:LinkButton ID="lbtn_vital_parameter" runat="server" OnClick="lbtn_vital_parameter_Click">Vital Parameters</asp:LinkButton>--%>&nbsp;&nbsp;
                                        <asp:Button ID="lbtn_vital_parameter" runat="server" CssClass="btn-info" OnClick="lbtn_vital_parameter_Click" Text="Vital Parameters" />
                                    </td>
                                    <td><%--<asp:LinkButton ID="lbtn_med_manage" runat="server" OnClick="lbtn_med_manage_Click">Medicines & Management</asp:LinkButton>--%>
                                        <asp:Button ID="lbtn_med_manage" runat="server" CssClass="btn-info" OnClick="lbtn_med_manage_Click" Text="Medicines &amp; Management" />
                                    </td>
                                    <td><%--<asp:LinkButton ID="lbtn_actions_taken" runat="server" OnClick="lbtn_actions_taken_Click">Actions Taken</asp:LinkButton>--%>
                                        <asp:Button ID="lbtn_actions_taken" runat="server" CssClass="btn-info" OnClick="lbtn_actions_taken_Click" Text="Actions Taken" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:View ID="View2" runat="server">
                            <table>
                                <tr>
                                    <td></td>
                                    <td class="center" colspan="3">VITAL PARAMETERS</td>
                                </tr>
                                <tr>
                                    <td class="auto-style1">
                                        </td>
                                    <td class="auto-style1">
                                        <asp:Label ID="lbl_parameter" runat="server" Text="Parameter" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style1">
                                        <asp:DropDownList ID="ddl_parameter" runat="server" AutoPostBack="True" Height="33px" Width="108px" CssClass="search_categories">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="auto-style1"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style1">&nbsp;</td>
                                    <td class="auto-style1">&nbsp;</td>
                                    <td class="auto-style1">&nbsp;</td>
                                    <td class="auto-style1">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style46">
                                    </td>
                                    <td class="auto-style49">
                                        <asp:Label ID="lbl_value" runat="server" Text="Value" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style48">
                                        <asp:TextBox ID="txt_value" runat="server" Height="33px" Width="98px"></asp:TextBox>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btn_add" runat="server" OnClick="btn_add_Click" Text="Add" CssClass="buttonyellow" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td class="auto-style48"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style46">&nbsp;</td>
                                    <td class="auto-style49">&nbsp;</td>
                                    <td class="auto-style48">&nbsp;</td>
                                    <td class="auto-style48">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Width="361px">
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:BoundField DataField="investigation_attribute" HeaderText="Parameters" />
                                                <asp:BoundField DataField="test_value" HeaderText="Value" />
                                                <asp:BoundField DataField="date" HeaderText="Date &amp; Time" />
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>
                                    </td>
                                    <td class="auto-style22">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style43">&nbsp;</td>
                                    <td class="auto-style36">&nbsp;</td>
                                    <td class="auto-style59">&nbsp;</td>
                                    <td class="auto-style22">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style58">
                                    </td>
                                    <td class="auto-style35">
                                        <asp:Label ID="lbl_nremarks" runat="server" Text="Remarks" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style55">
                                        <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" CssClass="text-left"></asp:TextBox>
                                    </td>
                                    <td class="auto-style56"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style58">&nbsp;</td>
                                    <td class="auto-style35">&nbsp;</td>
                                    <td class="auto-style55">&nbsp;</td>
                                    <td class="auto-style56">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style42">
                                        &nbsp;</td>
                                   <td class="auto-style7">
                                        <asp:Button ID="btn_back" runat="server" OnClick="btn_back_Click" Text="Back" CssClass="buttonyellow" />
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:View ID="View3" runat="server">
                            <table class="auto-style20">
                                <tr>
                                    <td class="auto-style23" colspan="4"><strong>MEDICINES&nbsp; &amp;&nbsp; MANAGEMENT</strong></td>
                                </tr>
                                <tr>
                                    <td class="auto-style73">
                                        &nbsp;</td>
                                    <td class="auto-style60">
                                        &nbsp;</td>
                                    <td class="auto-style69"><strong>
                                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4" CssClass="auto-style72" DataKeyNames="prescription_id" ForeColor="#333333" GridLines="None" Width="333px">
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:BoundField DataField="drug_name" HeaderText="Name of Medicine" />
                                                <asp:BoundField DataField="dose" HeaderText="Dossage" />
                                                <asp:BoundField DataField="frequency" HeaderText="Frequency" />
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>
                                        </strong></td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style73">&nbsp;</td>
                                    <td class="auto-style12">&nbsp;</td>
                                    <td class="auto-style66">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style65">
                                        &nbsp;</td>
                                    <td class="auto-style12">
                                        <strong>
                                        <asp:Label ID="lbl_medicin_name" runat="server" CssClass="label_small" Text="Name of Medicine"></asp:Label>
                                        </strong>
                                    </td>
                                    <td class="auto-style67"><strong>
                                        <asp:TextBox ID="txt_medicin_name" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </strong></td>
                                    <td class="auto-style13"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style65">&nbsp;</td>
                                    <td class="auto-style12">&nbsp;</td>
                                    <td class="auto-style67">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style73">
                                        &nbsp;</td>
                                    <td class="auto-style12">
                                        <strong>
                                        <asp:Label ID="lbl_dosage" runat="server" Text="Dosage" CssClass="label_small"></asp:Label>
                                        </strong>
                                    </td>
                                    <td class="auto-style67"><strong>
                                        <asp:TextBox ID="txt_dosage" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </strong></td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style73">&nbsp;</td>
                                    <td class="auto-style12">&nbsp;</td>
                                    <td class="auto-style67">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style73">
                                        &nbsp;</td>
                                    <td class="auto-style12">
                                        &nbsp;</td>
                                    <td class="auto-style66">
                                        <strong>
                                        <asp:Button ID="btn_nurse_add" runat="server" CssClass="buttonyellow" OnClick="btn_nurse_add_Click" Text="Add" />
                                        </strong>
                                    </td>
                                    <td class="auto-style13"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style73">&nbsp;</td>
                                    <td class="auto-style12">&nbsp;</td>
                                    <td class="auto-style66">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style73">&nbsp;</td>
                                    <td class="auto-style12">
                                        &nbsp;</td>
                                    <td class="auto-style69"><strong>
                                        <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="4" CssClass="auto-style72" ForeColor="#333333" GridLines="None" Width="328px">
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:BoundField DataField="medicin_name" HeaderText="Name of Medicine" />
                                                <asp:BoundField DataField="dose" HeaderText="Dose" />
                                                <asp:BoundField DataField="time" HeaderText="Time" />
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>
                                        </strong></td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style73">&nbsp;</td>
                                    <td class="auto-style12">&nbsp;</td>
                                    <td class="auto-style66">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style70"></td>
                                    <td class="auto-style15">
                                        <strong>
                                        <asp:Button ID="btn_back_medicine_management" runat="server" CssClass="buttonyellow" OnClick="btn_back_medicine_management_Click" Text="Back" />
                                        </strong></td>
                                    <td class="auto-style61">&nbsp;</td>
                                    <td class="auto-style71">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style64">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    <td class="auto-style11"></td>
                                    <td class="auto-style68">
                                        &nbsp;</td>
                                    <td class="auto-style6"></td>
                                </tr>
                            </table>
                        </asp:View>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:View ID="View4" runat="server">
                            <table class="auto-style21">
                                <tr>
                                    <td class="auto-style78" colspan="2"><strong>Actions Taken</strong></td>
                                    <td class="auto-style77"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style74"></td>
                                    <td class="auto-style79">
                                        <asp:RadioButtonList ID="rbtn_action_taken" runat="server" AutoPostBack="True" CssClass="auto-style82">
                                            <asp:ListItem Value="0" Selected="True">Improved &amp; Discharged</asp:ListItem>
                                            <asp:ListItem Value="1">Improving &amp; Shifted to IP</asp:ListItem>
                                            <asp:ListItem Value="2">Worst ,Discharged &amp; Directed to HIgher</asp:ListItem>
                                            <asp:ListItem Value="3">Expired</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td class="auto-style80"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style76">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style76">
                                        <asp:Button ID="btn_back_actiontaken" runat="server" Text="Back" OnClick="btn_back_actiontaken_Click" CssClass="buttonyellow" />
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style76">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                    
          </asp:MultiView>
    
   
    </form>
</asp:Content>

