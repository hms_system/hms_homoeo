﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Nurse/NurseMaster.master" AutoEventWireup="true" CodeFile="ChangeUsername_nurse.aspx.cs" Inherits="Nurse_ChangeUsername_nurse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .auto-style1 {
        height: 32px;
    }
    .auto-style2 {
        color: #142fe0;
        font-weight: bold;
        display: block;
        width: 150px;
        float: left;
        text-align: left;
        height: 32px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <form id="Form1" runat="server">
     
    <table class="auto-style1">
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style35">&nbsp;</td>
                <td class="auto-style29">&nbsp;</td>
                <td class="auto-style15">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="center" colspan="2"><strong>CHANGE USERNAME</strong></td>
                <td class="auto-style21">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="label_small"><strong>Enter New UserName:</strong></td>
                <td class="auto-style28">
                    <asp:TextBox ID="txt_newusername" runat="server" Height="22px" Width="174px" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_newusername" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>

                    <br />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_newusername" ErrorMessage="*Please enter valid name" ForeColor="Red" ValidationExpression="[a-zA-Z]{1,}" ValidationGroup="a" Width="200px"></asp:RegularExpressionValidator>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                </td>
                <td rowspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1"></td>
                <td class="auto-style1"></td>
                <td class="auto-style2"><strong>Enter Password:</strong></td>
                <td class="auto-style1">
                    <asp:TextBox ID="txt_password" runat="server" Height="22px" Width="176px" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td class="auto-style1"></td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style43">
                    <strong>
                    <asp:Label ID="lbl_title" runat="server" CssClass="label_small"></asp:Label>
                    </strong>
                </td>
                <td class="auto-style45">&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td ><strong>
                    <asp:Button ID="btn_back" runat="server" OnClick="btn_back_Click" Text="BACK" CssClass="buttonyellow" />
                    </strong></td>
                <td ><strong>
                    <asp:Button ID="btn_changeusername" runat="server" OnClick="btn_changeusername_Click" Text="CHANGE USERNAME" CssClass="buttonyellow" ValidationGroup="a" />
                    </strong></td>
                <td></td>
            </tr>
        </table>
        </form>
</asp:Content>

