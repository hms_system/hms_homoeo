﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Nurse/NurseMaster.master" AutoEventWireup="true" CodeFile="N_ViewIPpatient.aspx.cs" Inherits="Nurse_N_ViewIPpatient" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <form id="form1" runat="server">
    <center>
    <table>
        <tr>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="3"></td>
            <td>&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
            <td>
                <asp:Label ID="Label11" runat="server" CssClass="label_small" Text="Admission No."></asp:Label>
            </td>
            <td colspan="3">
                <asp:Label ID="lbl_ip_admsn_no" runat="server" CssClass="label_small"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td class="auto-style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="3"></td>
            <td>&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="Label12" runat="server" CssClass="label_small" Text="Card No."></asp:Label>
            </td>
            <td colspan="3">
                <asp:Label ID="lbl_cardno" runat="server" CssClass="label_small"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td class="auto-style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="3"></td>
            <td>&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td></td>
            <td>
                <asp:Label ID="Label13" runat="server" CssClass="label_small" Text="Name"></asp:Label>
            </td>
            <td colspan="3">
                <asp:Label ID="lbl_ip_patient_name" runat="server" CssClass="label_small"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td class="auto-style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="3"></td>
            <td>&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td></td>
            <td>
                <asp:Label ID="Label14" runat="server" CssClass="label_small" Text="Age"></asp:Label>
            </td>
            <td colspan="3">
                <asp:Label ID="lbl_age" runat="server" CssClass="label_small"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td class="auto-style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="3"></td>
            <td>&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td></td>
            <td>
                <asp:Label ID="Label15" runat="server" CssClass="label_small" Text="Gender"></asp:Label>
            </td>
            <td colspan="3">
                <asp:Label ID="lbl_gender" runat="server" CssClass="label_small"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td class="auto-style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="3"></td>
            <td>&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td ></td>
            <td >
                <asp:Label ID="Label16" runat="server" CssClass="label_small" Text="Address"></asp:Label>
            </td>
            <td  colspan="3">
                <asp:Label ID="lbl_hname" runat="server" CssClass="label_small"></asp:Label>
                <br />
                <asp:Label ID="lbl_place" runat="server" CssClass="label_small"></asp:Label>
            </td>
            <td >
                &nbsp;</td>
            <td class="auto-style3">
            </td>
            <td >
                &nbsp;</td>
            <td >
                &nbsp;</td>
            <td >
                <br />
            </td>
            <td >
                &nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="3"></td>
            <td>&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="Label17" runat="server" CssClass="label_small" Text="Contact No."></asp:Label>
            </td>
            <td colspan="3">
                <asp:Label ID="lbl_contact" runat="server" CssClass="label_small"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td class="auto-style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="3"></td>
            <td>&nbsp;</td>
            <td class="auto-style2">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="Label18" runat="server" CssClass="label_small" Text="Annual Income"></asp:Label>
            </td>
            <td class="text-left">
                <asp:TextBox ID="txt_annualincome" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                <asp:Button ID="btn_add_income" runat="server" CssClass="buttonyellow" OnClick="btn_add_income_Click" Text="ADD" ValidationGroup="a" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
               </td>
            <td>
               </td>
            <td>
               </td>
            <td>
               </td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="text-left">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>

        <tr>
            <td colspan="3">
               <asp:Button ID="Button5" runat="server" Text="Medicines And Management" OnClick="Button5_Click" CssClass="btn-info"></asp:Button>
                <%--<asp:Button ID="Button4" runat="server" Text="Bystander Details" OnClick="Button4_Click" CssClass="btn-info"></asp:Button>--%>
                <asp:Button ID="Button3" runat="server" Text="Routine Checkup" OnClick="Button3_Click" CssClass="btn-info"></asp:Button>
                <asp:Button ID="Button2" runat="server" Text="Ward Details" OnClick="Button2_Click" CssClass="btn-info"></asp:Button>
                <asp:Button ID="Button1" runat="server" Text="Vital Parameters" OnClick="Button1_Click" CssClass="btn-info"></asp:Button>
                
               <%--<asp:Button ID="Button13" runat="server" Text="Button"></asp:Button>--%>
            </td>
            
        </tr>

       <%-- <tr>
            <td></td>
            <td colspan="4">
                <div class="drop">
                    <ul class="drop_menu">

                        <li>
                            <asp:LinkButton ID="Button5" runat="server" OnClick="Button5_Click">Medicines And Management</asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="Button4" runat="server" OnClick="Button4_Click">Bystander Details</asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="Button3" runat="server" OnClick="Button3_Click">Routine Checkup</asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="Button2" runat="server" OnClick="Button2_Click">Ward Details</asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="Button6" runat="server" OnClick="Button6_Click">Discharge Summary</asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="Button1" runat="server" OnClick="Button1_Click">Vital Parameters</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </td>
            <td>
               </td>
        </tr>--%>

        <tr>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="3">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>
               </td>
            <td>
               </td>
            <td>
               </td>
            <td>
               </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>&nbsp;</td>
            <td colspan="3">
                <asp:MultiView ID="MultiView1" runat="server">

                    <asp:View ID="View1" runat="server">
                             
                       
                            <table>
                            <tr>
                                <td></td>
                                <td>
                                    </td>
                                <td></td>
                            </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Image ID="Image3" runat="server" Height="200px" ImageUrl="~/HeadNurse/images/img-13.jpeg" Width="1000px" />
                                    </td>
                                    <td></td>
                                </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                       

                    </asp:View>
            </td>
        </tr>
        <tr>
            <td>
                <asp:View ID="View2" runat="server">
                    <table>
                        <tr>
                            <td class="center" colspan="6">BYSTANDER INFORMATION</td>
                        </tr>
                        <tr>
                            <td  colspan="6">&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="lbl_bystander_name" runat="server" CssClass="label_small" Text="Name"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_bystander_name" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>

                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="lbl_bystander_address" runat="server" CssClass="label_small" Text="Address"></asp:Label>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_bystander_hname" runat="server" CssClass="label_small" Text="House Name"></asp:Label>

                            </td>
                            <td>

                                <asp:TextBox ID="txt_bystander_hname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>

                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_bystander_place" runat="server" CssClass="label_small" Text="Street/Place"></asp:Label>

                            </td>
                            <td>
                                <asp:TextBox ID="txt_bystander_place" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>

                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_district" runat="server" CssClass="label_small" Text="District"></asp:Label>

                            </td>
                            <td>
                                <asp:TextBox ID="txt_bystander_district" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_state" runat="server" CssClass="label_small" Text="State"></asp:Label>

                            </td>
                            <td>

                                <asp:TextBox ID="txt_bystander_state" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>

                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_pin" runat="server" CssClass="label_small" Text="Pin Code"></asp:Label>

                            </td>
                            <td>

                                <asp:TextBox ID="txt_bystander_pin" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>

                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_contact_no" runat="server" CssClass="label_small" Text="Contact No"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_bystander_contact" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                <br />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="lbl_relation" runat="server" CssClass="label_small" Text="Relation"></asp:Label>
                            </td>
                            <td>

                                <asp:DropDownList ID="ddl_relation" runat="server" AutoPostBack="True" CssClass="search_categories" OnSelectedIndexChanged="ddl_relation_SelectedIndexChanged" style="height: 38px">
                                    <asp:ListItem>Select</asp:ListItem>
                                    <asp:ListItem Value="0">Family </asp:ListItem>
                                    <asp:ListItem Value="1">Third Party</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>

                                <asp:Label ID="lbl_specify" runat="server" CssClass="label_small" Text="Specify"></asp:Label>

                            </td>
                            <td>
                                <asp:TextBox ID="txt_specify" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="btn_back1" runat="server" CssClass="buttonyellow" OnClick="btn_back1_Click" Text="BACK" />
                            </td>
                            <td>
                                <asp:Button ID="btn_add" runat="server" CssClass="buttonyellow" OnClick="btn_add_Click" Text="ADD" ValidationGroup="a" />
                            </td>
                            <td>
                                <asp:Button ID="btn_next1" runat="server" CssClass="buttonyellow" Text="NEXT" OnClick="btn_next1_Click" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </asp:View>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:View ID="View3" runat="server">
                    <table>
                        <tr>
                            <td class="center" colspan="6">MEDICINES AND MANAGEMENT</td>
                        </tr>
                        <tr>
                            <td  colspan="6">&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                
                                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4"  width="400px" DataKeyNames="prescription_id" GridLines="None" ForeColor="Black">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField DataField="drug_name" HeaderText="Name of Medicine" />
                                        <asp:BoundField DataField="dose" HeaderText="Dossage" />
                                        <asp:BoundField DataField="frequency" HeaderText="Frequency" />
                                    </Columns>
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>
                               
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="lbl_ip_medicin_name" runat="server" CssClass="label_small" Text="Name of Medicine"></asp:Label>
                            </td>
                            <td>

                                <asp:TextBox ID="txt_ip_medicin_name" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>

                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="lbl_ip_dosage" runat="server" CssClass="label_small" Text="Dosage"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_ip_dosage" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btn_ip_nurse_add0" runat="server" CssClass="buttonyellow" OnClick="btn_nurse_add_Click" Text="Add" ValidationGroup="c" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <center>
                                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" ForeColor="Black">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField DataField="medicin_name" HeaderText="Name of Medicine" />
                                        <asp:BoundField DataField="dose" HeaderText="Dose" />
                                        <asp:BoundField DataField="time" HeaderText="Time" />
                                    </Columns>
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>
                                </center>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <asp:Button ID="btn_medback" runat="server" CssClass="buttonyellow" OnClick="btn_medback_Click" Text="BACK" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </asp:View>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:View ID="View4" runat="server">

                    <table>
                        <tr>
                            <td class="center" colspan="6">VITAL PARAMETERS</td>
                        </tr>
                        <tr>
                            <td  colspan="6">&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="lbl_test" runat="server" CssClass="label_small" Text="Name of Test"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddl_test_attributes" runat="server" AutoPostBack="True" CssClass="search_categories">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Label ID="lbl_value" runat="server" CssClass="label_small" Text="Value"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_test_values" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btn_vitaladd" runat="server" CssClass="buttonyellow" OnClick="btn_vitaladd_Click" Text="ADD" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" width="400px" CellPadding="4" DataKeyNames="invs_detail_id" GridLines="None" ForeColor="Black">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField DataField="investigation_attribute" HeaderText="Name of Test" />
                                        <asp:BoundField DataField="test_value" HeaderText="Value" />
                                        <asp:BoundField DataField="date" HeaderText="Date" />
                                    </Columns>
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>

                                <asp:Button ID="btn_back" runat="server" CssClass="buttonyellow" OnClick="btn_back_Click" Text="<<BACK" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>

                </asp:View>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:View ID="View5" runat="server">
                    <table>
                        <tr>
                            <td class="center" colspan="4">DISCHARGE SUMMARY</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="lbl_dis_cardno" runat="server" CssClass="label_small" Text="Card No."></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_cardno" runat="server" CssClass="label_black"></asp:Label>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="lbl_dispatname" runat="server" CssClass="label_small" Text="Name of Patient"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_pname" runat="server" CssClass="label_black"></asp:Label>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_disage" runat="server" Text="Age" CssClass="label_small"></asp:Label>

                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_age" runat="server" CssClass="label_black"></asp:Label>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_disgender" runat="server" Text="Gender" CssClass="label_small"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_gender" runat="server" CssClass="label_black"></asp:Label>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_dis_address" runat="server" Text="Address" CssClass="label_small"></asp:Label>

                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_addr" runat="server" CssClass="label_black"></asp:Label>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Label ID="Label5" runat="server" Text="Date of Admission" CssClass="label_small"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_admsn_date" runat="server" CssClass="label_black"></asp:Label>
                                <br />

                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <%--<cc1:MaskedEditExtender ID="txt_date_of_dischrge_MaskedEditExtender" runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txt_date_of_dischrge" UserDateFormat="DayMonthYear" InputDirection="RightToLeft" ClearTextOnInvalid="True">
                                                    </cc1:MaskedEditExtender>--%>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Label ID="Label7" runat="server" Text="Provisional Diagonosis" CssClass="label_small"></asp:Label>

                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_diagnosis" runat="server" CssClass="label_black"></asp:Label>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="Label8" runat="server" Text="Medicines" CssClass="label_small"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_medcines" runat="server" CssClass="label_black"></asp:Label>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td ></td>
                            <td >

                                <asp:Label ID="Label9" runat="server" CssClass="label_small" Text="Advice"></asp:Label>

                            </td>
                            <td >
                                <asp:DropDownList ID="ddl_dis_advice" runat="server" AutoPostBack="True" CssClass="search_categories">
                                    <asp:ListItem Value="0">SELECT</asp:ListItem>
                                    <asp:ListItem Value="1">abc</asp:ListItem>
                                    <asp:ListItem Value="2">xyz</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="Label10" runat="server" CssClass="label_small" Text="Reason For Discharge"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddl_dischrge_reason" runat="server" AutoPostBack="True" CssClass="search_categories">
                                    <asp:ListItem>select</asp:ListItem>
                                    <asp:ListItem>Improved</asp:ListItem>
                                    <asp:ListItem>Worst</asp:ListItem>
                                    <asp:ListItem>Discharge at request</asp:ListItem>
                                    <asp:ListItem>Discharge against advice</asp:ListItem>
                                    <asp:ListItem>Abscond</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_dis_dr" runat="server" Text="Name of Discharging Doctor" CssClass="label_small"></asp:Label>

                            </td>
                            <td>
                                <asp:TextBox ID="txt_dis_dr" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="Label6" runat="server" CssClass="label_small" Text="Date of Discharge"></asp:Label>
                            </td>
                            <td>

                                <asp:TextBox ID="txt_date_of_dischrge" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                <cc1:CalendarExtender ID="txt_date_of_dischrge_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_date_of_dischrge"></cc1:CalendarExtender>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="btn_discharge" runat="server" OnClick="btn_discharge_Click" Text="BACK" CssClass="buttonyellow" />
                            </td>
                            <td>
                                <%--<asp:Button ID="btn_dischrgesummary_print" runat="server" CssClass="buttonyellow" OnClick="btn_dischrgesummary_print_Click" Text="PRINT" />--%>
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:View ID="View6" runat="server">

                    <table>
                        <tr>
                            <td >&nbsp;</td>
                            <td  colspan="7">ROUTINE CHECKUP</td>
                        </tr>
                        <tr>
                            <td >&nbsp;</td>
                            <td  colspan="7">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td>
                                <asp:Label ID="lbl_routinetest" runat="server" CssClass="label_small" Text="Name of Test"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_routinetest" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                            </td>
                            <td></td>
                            <td>
                                <asp:Label ID="lbl_routinevalue" runat="server" CssClass="label_small" Text="Value"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_routinevalue" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btn_routinetest2" runat="server" CssClass="buttonyellow" OnClick="btn_routinetest_Click" Text="ADD" />
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td>

                                <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" width="400px" CellPadding="4" DataKeyNames="routineid" GridLines="None" ForeColor="Black">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField DataField="routinename" HeaderText="Name of Test" />
                                        <asp:BoundField DataField="routinevalue" HeaderText="Value" />
                                        <asp:BoundField DataField="routinedate" HeaderText="Date" />
                                    </Columns>
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>

                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <asp:Button ID="btn_routineback2" runat="server" CssClass="buttonyellow" OnClick="btn_back_Click" Text="<<BACK" />
                            </td>
                        </tr>
                    </table>

                </asp:View>


                </asp:MultiView>
            </td>
            <td>
                &nbsp;</td>
            <td class="auto-style2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </center>
    </form>
</asp:Content>

