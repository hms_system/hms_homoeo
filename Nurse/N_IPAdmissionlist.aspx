﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Nurse/NurseMaster.master" AutoEventWireup="true" CodeFile="N_IPAdmissionlist.aspx.cs" Inherits="Nurse_N_IPAdmissionlist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="Form1" runat="server"> 
    <div>
        <table>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server">

                        <table>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <asp:View ID="View1" runat="server">
                                        <table>
                                            <tr>
                                                <td class="center" colspan="4">IN-PATIENTS</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td colspan="2">
                                                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Today'List" CssClass="btn-info" />
                                                    <asp:Button ID="Button2" runat="server" CssClass="btn-info" OnClick="Button2_Click" Text="Admitted Patient List" />
                                                </td>
                                                <td>
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                    <br />
                                    <asp:View ID="View2" runat="server">
                                        
                                            <table>
                                                <tr>
                                                    <td></td>
                                                    <td class="center">TODAY'S IN-PATIENT LIST</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="center">
                                                        <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Button" />
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCommand="GridView1_RowCommand">
                                                            <AlternatingRowStyle BackColor="White" />
                                                            <Columns>
                                                                <asp:BoundField DataField="admission_id" HeaderText="Admission ID" />

                                                                <asp:BoundField DataField="cardno" HeaderText="Card No" />
                                                                <asp:BoundField DataField="patient_name" HeaderText="Patient Name" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Button ID="btn_select" runat="server" Width="60" Text="SELECT" CommandName="SelectButton" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EditRowStyle BackColor="#2461BF" />
                                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                            <RowStyle BackColor="#EFF3FB" />
                                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                        </asp:GridView>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>

                                            </table>
                                        
                                    </asp:View>
                                    <br />
                                    <asp:View ID="View3" runat="server">
                                                                                <table>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td class="center">CURRENT IN-PATIENTS LIST</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                        <td class="center">&nbsp;</td>
                                                                                        <td>&nbsp;</td>
                                                                                    </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical" >
                                                        <AlternatingRowStyle BackColor="#DCDCDC" />
                                                        <Columns>
                                                            <asp:BoundField DataField="admission_id" HeaderText="Admission No." />
                                                            <asp:BoundField DataField="patient_id" HeaderText="Card No." />
                                                            <asp:BoundField DataField="patient_name" HeaderText="Patient Name" />
                                                        </Columns>
                                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                        <SortedDescendingHeaderStyle BackColor="#000065" />
                                                    </asp:GridView>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td class="btn-info">
                                                    <%--<asp:LinkButton ID="LinkButton4" runat="server" CssClass="linkbtn" OnClick="LinkButton4_Click">Next</asp:LinkButton>--%>
                                                    <asp:Button ID="LinkButton4" runat="server" Text="Next" OnClick="LinkButton4_Click" CssClass="buttonyellow" />
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </asp:View>
                                </td>
                                <td>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </asp:MultiView>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>

    </div>
</form>
</asp:Content>

