﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


//functions used to login different users to their corressponding sessions

public partial class Login: System.Web.UI.Page
{
    Connection c = new Connection();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            lbl_error.Visible = false;
        }

    }
    protected void btn_login_Click(object sender, EventArgs e)
    {
        c.getcon();
        string str_login = "select * from login where username='" + txt_uname.Text + "' and password='" + txt_pwd.Text + "'";
        SqlCommand cmd_login = new SqlCommand(str_login, c.con);
        SqlDataAdapter da_login = new SqlDataAdapter(cmd_login);
        DataTable dt_login = new DataTable();
        da_login.Fill(dt_login);

        if (dt_login.Rows.Count > 0)
        {
            DataRow row1 = dt_login.Rows[dt_login.Rows.Count - 1];
            Session["id"] = row1[4];
            Session["uid"] = row1[1];
            int type = Convert.ToInt32(row1["usertype"]);
            if (type == 1)
                Response.Redirect("Doctor/Default.aspx");
            else if (type == 7)
               Response.Redirect("Nurse/NurseHome.aspx");
            else if (type == 4)
                Response.Redirect("Registration/RegHome.aspx");
            else if (type == 2)
                Response.Redirect("HeadNurse/HeadHome.aspx");
            else if (type == 5)
                Response.Redirect("Admin/Superend.aspx");
            else if (type == 6)
                Response.Redirect("RMO/RMOHome.aspx");

            else if (type == 8)
                Response.Redirect("Stock/StockHome.aspx");


        }
        else
        {
            lbl_error.Visible = true;
            lbl_error.Text = "The username or password you've entered is incorrect";
            lbl_error.ForeColor = System.Drawing.Color.Red;

        }

        
    }
}