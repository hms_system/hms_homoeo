﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title></title>

    <link rel="shortcut icon" type="image/icon" href="images/favicon.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link id="switcher" href="css/themes/default-theme.css" rel="stylesheet">
    <link href="css/slick.css" rel="stylesheet">
    <link rel='stylesheet prefetch' href='css/photoswipe.css'>
    <link rel='stylesheet prefetch' href='css/default-skin.css'>
    <link href="style.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Habibi' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Cinzel+Decorative:900' rel='stylesheet' type='text/css'>

    <style type="text/css">
       
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">
        <center>
                <table class="table-hover">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>
                    </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="lbl_uname" runat="server" Text="User Name" CssClass="label_small" ></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_uname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="lbl_pwd" runat="server" Text="Password" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_pwd" runat="server" TextMode="Password" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
             
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                   <asp:Button ID="btn_login" runat="server" Text="Log In" OnClick="btn_login_Click" CssClass="buttonyellow"/>
                </td>
              </tr>
            <tr>
                <td></td>
                <td></td>
                <td>                    
                    <asp:Label ID="lbl_error" runat="server" ForeColor="Red"></asp:Label>
                  </td>
            </tr>
        </table>
</center>
    </form>
</asp:Content>

