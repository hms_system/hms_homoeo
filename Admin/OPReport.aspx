﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="OPReport.aspx.cs" Inherits="Admin_OPReport" %>
<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <form id="form1" runat="server">
<center>
    <table>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="Label1" runat="server" CssClass="label_small" Text="Search By"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddl_search" runat="server" AutoPostBack="True" CssClass="auto-style1" OnSelectedIndexChanged="ddl_search_SelectedIndexChanged">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="1">Today</asp:ListItem>
                    <asp:ListItem Value="2">Month</asp:ListItem>
                    <asp:ListItem Value="2">Year</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btn_search" runat="server" OnClick="btn_search_Click" Text="OK" CssClass="buttonyellow" />
            </td>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
       <%-- <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_stdate" runat="server" CssClass="label_small" Text="StartDate"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_stdate" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                <cc1:CalendarExtender ID="txt_stdate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_stdate">
                </cc1:CalendarExtender>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_endate" runat="server" CssClass="label_small" Text="EndDate"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_endate" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                <cc1:CalendarExtender ID="txt_endate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_endate">
                </cc1:CalendarExtender>
            </td>
            <td>
                <asp:Button ID="btn_week" runat="server" CssClass="btn" OnClick="btn_week_Click" Text="OK" />
            </td>
            <td>
                &nbsp;</td>
        </tr>--%>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_month" runat="server" CssClass="label_small" Text="Month" Enabled="False"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddl_month" runat="server" CssClass="search_categories" AutoPostBack="True"  >
                    <asp:ListItem Value="0">SELECT</asp:ListItem>
                    <asp:ListItem Value="1">JAN</asp:ListItem>
                    <asp:ListItem Value="2">FEB</asp:ListItem>
                    <asp:ListItem Value="3">MARCH</asp:ListItem>
                    <asp:ListItem Value="4">APRIL</asp:ListItem>
                    <asp:ListItem Value="5">MAY</asp:ListItem>
                    <asp:ListItem Value="6">JUNE</asp:ListItem>
                    <asp:ListItem Value="7">JULY</asp:ListItem>
                    <asp:ListItem Value="8">AUG</asp:ListItem>
                    <asp:ListItem Value="9">SEPT</asp:ListItem>
                    <asp:ListItem Value="10">0CT</asp:ListItem>
                    <asp:ListItem Value="11">NOV</asp:ListItem>
                    <asp:ListItem Value="12">DEC</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_year" runat="server" CssClass="label_small" Text="Year" Enabled="False"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_year" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btn_month" runat="server" OnClick="btn_month_Click" Text="SEARCH" CssClass="buttonyellow" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_s_year" runat="server" CssClass="label_small" Text="Year" Enabled="False"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_search_year" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td></td>
            <td></td>
            <td>
                <asp:Button ID="btn_year" runat="server" OnClick="btn_year_Click" Text="SEARCH" CssClass="buttonyellow" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td rowspan="8"></td>
            <td  rowspan="8">&nbsp;</td>
            <td  rowspan="8">
                <asp:Chart ID="Chart1" runat="server">
                    <Series>
                        <asp:Series Name="Series1">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </td>
            <td></td>
            <td>
                <asp:Label ID="Label2" runat="server" CssClass="label_small" Text="Male"></asp:Label>
            </td>
            <td colspan="2">
                <asp:Label ID="lbl_male" runat="server" CssClass="label_black"></asp:Label>
            </td>
            <td rowspan="8"></td>
            <td rowspan="8"></td>
            <td rowspan="8">
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="Label5" runat="server" CssClass="label_small" Text="Female"></asp:Label>
            </td>
            <td colspan="2">
                <asp:Label ID="lbl_female" runat="server" CssClass="label_black"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="Label7" runat="server" CssClass="label_small" Text="Children"></asp:Label>
            </td>
            <td colspan="2">
                <asp:Label ID="lbl_children" runat="server" CssClass="label_black"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="Label9" runat="server" CssClass="label_small" Text="Total"></asp:Label>
            </td>
            <td colspan="2">
                <asp:Label ID="lbl_total" runat="server" CssClass="label_black"></asp:Label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </center>
    </form>
</asp:Content>

