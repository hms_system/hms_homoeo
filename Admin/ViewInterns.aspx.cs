﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_ViewInterns : System.Web.UI.Page
{
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

           
            gridviewintern();
            
        }

    }
    public void gridviewintern()
    {
        c.getcon();
        string str_internview = "select * from Interns_Details where in_status='Approved'";

        SqlCommand cmd_internview = new SqlCommand(str_internview, c.con);
        SqlDataAdapter da_internview = new SqlDataAdapter(cmd_internview);
        DataTable dt_internview = new DataTable();
        da_internview.Fill(dt_internview);
        if (dt_internview.Rows.Count > 0)
        {
            GridView1.DataSource = dt_internview;
            GridView1.DataBind();
        }

        c.con.Close();

    }
    
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        gridviewintern();
    }
}
