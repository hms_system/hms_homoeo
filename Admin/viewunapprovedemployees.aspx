﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="viewunapprovedemployees.aspx.cs" Inherits="Admin_viewunapprovedemployees" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <form id="Form1" runat="server">

     <center> 
    
        <asp:MultiView ID="MultiView1" runat="server" >
            <asp:View ID="View1" runat="server"><table><tr><td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1"></td>
                    <td class="auto-style1"></td>
                    <td class="auto-style1"></td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:GridView ID="gridview_emp" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" OnRowCommand="gridview_emp_RowCommand" Width="500px">
                            <AlternatingRowStyle BackColor="PaleGoldenrod" />
                            <Columns>
                                <asp:BoundField DataField="empid" HeaderText="Employee Id" />
                                <asp:BoundField DataField="empname" HeaderText="Employee Name" />
                                <asp:BoundField DataField="designation" HeaderText="Designation " />
                                <%--<asp:ButtonField CommandName="ApproveButton" ItemStyle-Width="30" Text="Approve">
                                <ItemStyle Width="30px" />
                                </asp:ButtonField>
                                <asp:ButtonField CommandName="DeleteButton" ItemStyle-Width="30" Text="Delete">
                                <ItemStyle Width="30px" />
                                </asp:ButtonField>--%>
                                
                         <asp:TemplateField>
                             <ItemTemplate><asp:Button ID="btn_approve" runat="server" Width="80" Text="Approve" CommandName="SelectButton2"  CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" /></ItemTemplate>
                        </asp:TemplateField>  
                         <asp:TemplateField>
                              <ItemTemplate><asp:Button ID="btn_delete" runat="server" Width="60" Text="Delete" CommandName="SelectButton3"  CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" /></ItemTemplate>
                         </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="Tan" />
                            <HeaderStyle BackColor="Tan" Font-Bold="True" />
                            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                            <SortedAscendingCellStyle BackColor="#FAFAE7" />
                            <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                            <SortedDescendingCellStyle BackColor="#E1DB9C" />
                            <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                        </asp:GridView>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                </table></asp:View>
            <asp:View ID="View2" runat="server">
                <table>
                    <tr>
                        <td></td>

                    </tr>
                    <tr>
                        <td></td>

                    </tr>
                    <tr>
                        <td></td>

                    </tr>
                    <tr>
                        <td>  <h1><font color="red">SORRY! No Records Found!!!</font></h1></td>

                    </tr>
                    <tr>
                        <td></td>

                    </tr>
                    <tr>
                        <td></td>

                    </tr>
                </table>
             
            </asp:View>
        </asp:MultiView>  
           
            </center>  
</form>
</asp:Content>

