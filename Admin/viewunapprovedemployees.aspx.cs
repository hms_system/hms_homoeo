﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_viewunapprovedemployees : System.Web.UI.Page
{
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            BindGridView();
        } 

    }
    private void BindGridView()
    {
        c.getcon();
        string status = "Pending";
        SqlCommand cmd = new SqlCommand("select * from Employee_details e inner join Designation d on d.desig_id=e.desig_id where status='" + status + "'", c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            MultiView1.ActiveViewIndex = 0;
            gridview_emp.DataSource = dt;
            gridview_emp.DataBind();
        }
        else
        {
            MultiView1.ActiveViewIndex = 1;
        }

    }
    protected void gridview_emp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "SelectButton2")
    {
        int index = Convert.ToInt32(e.CommandArgument);
        GridViewRow row = gridview_emp.Rows[index];
        Response.Redirect("Approveuser.aspx?EmpNo=" + row.Cells[0].Text);
    }
        if (e.CommandName == "SelectButton2")
    {
        int index = Convert.ToInt32(e.CommandArgument);
        GridViewRow row = gridview_emp.Rows[index];
        Response.Redirect("Deleteuser.aspx?EmpNo=" + row.Cells[0].Text);
    }
}

}
