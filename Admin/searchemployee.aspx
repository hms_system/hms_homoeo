﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="searchemployee.aspx.cs" Inherits="Admin_searchemployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <form runat="server">

        <center>
                    
        <table>
            
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td> &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="label_small">Employee Id</td>
                <td>
                  
                    <asp:TextBox ID="txt_empid" runat="server"  CssClass="twitterStyleTextbox"></asp:TextBox>
                   
                </td>
                <td>
                    <asp:Button ID="btn_search" runat="server" Text="Search" OnClick="btn_search_Click" CssClass="buttonyellow" />
                   
                   </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                  
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>    
               <td>&nbsp;</td>    
               <td>&nbsp;</td>    
               <td class="label_small">Employee Type Id</td>    
               <td>    
                 
                   <asp:TextBox ID="txt_emptypeid" runat="server" ReadOnly="true" Enabled="False"  CssClass="twitterStyleTextbox"/>    
                  
               </td>    
               <td></td>    
           </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                  
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
           
           <tr>    
               <td>&nbsp;</td>    
               <td>&nbsp;</td>    
               <td class="label_small">Employee Name</td>    
               <td>   
                   <asp:TextBox ID="txt_empname" runat="server" ReadOnly="true" Enabled="False"  CssClass="twitterStyleTextbox"/>    
                    
               </td>    
               <td>    
                  </td>    
           </tr> 
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                  
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
             
           <tr>    
               <td>&nbsp;</td>    
               <td>&nbsp;</td>    
               <td class="label_small">Date of Birth</td>    
               <td>    
                 
                   <asp:TextBox ID="txt_dob" runat="server" ReadOnly="true" Enabled="False"  CssClass="twitterStyleTextbox"/>

               </td>    
               <td>    
                  </td>    
           </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                  
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
              
           <tr>    
               <td></td>    
               <td></td>    
               <td class="label_small">Gender</td>    
               <td>    
                  <asp:TextBox ID="txt_gen" runat="server" ReadOnly="true" Enabled="False" CssClass="twitterStyleTextbox"/> </td>    
               <td>    
               </td>    
           </tr> 
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                  
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
             
           <tr>    
               <td>&nbsp;</td>    
               <td>&nbsp;</td>    
               <td class="label_small">Joining date</td>    
               <td>    
                       
                   <asp:TextBox ID="txt_joiningdate" runat="server" ReadOnly="true" Enabled="False"  CssClass="twitterStyleTextbox"/>  </td>    
               <td>    
                   </td>    
           </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                  
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
              
           <tr>    
               <td>&nbsp;</td>    
               <td>&nbsp;</td>    
               <td class="label_small">Contact 1</td>    
               <td>    
                      
                   <asp:TextBox ID="txt_con1" runat="server"  ReadOnly="true" Enabled="False"  CssClass="twitterStyleTextbox"/>    
                    
               </td>    
               <td>    
                  </td>    
           </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                  
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
           
           <tr>    
               <td>&nbsp;</td>    
               <td>&nbsp;</td>    
               <td class="label_small">Email Address</td>    
               <td>    
                     
                   <asp:TextBox ID="txt_email" runat="server"  ReadOnly="true" Enabled="False"  CssClass="twitterStyleTextbox"/>    </td>    
               <td>    
                   </td>    
           </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                  
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
           <tr>    
               <td>&nbsp;</td>    
               <td>&nbsp;</td>    
               <td class="label_small">Designation</td>    
               <td>    
         
                   <asp:TextBox ID="txt_desigid" runat="server"  ReadOnly="true" Enabled="False" CssClass="twitterStyleTextbox"/>    
                       </td>    
               <td>    
                  </td>    
           </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                  
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btn_cancel" runat="server" Text="Cancel" OnClick="btn_cancel_Click" CssClass="buttonyellow" />
                </td>
                <td>
                    <asp:Button ID="btn_delete" runat="server" Text="Delete" OnClick="btn_delete_Click" CssClass="buttonyellow"/>
                    </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td></td>
                <td>
                   </td>
                <td></td>
            </tr>
            
        </table>
            </center>


        </form>
        
</asp:Content>

