﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing.Design;
using System.Net.Mail;
using System.Data;
using System.Data.SqlClient;


public partial class Admin_adduserpage : System.Web.UI.Page
{
    Connection c = new Connection();

    protected void Page_Load(object sender, EventArgs e)
    {
        CreateRandomPassword();
        if (!IsPostBack)
        {

            int empno = 0;
            ViewState["empno"] = Convert.ToInt32(Request.QueryString["EmpNo"].ToString());

            BindTextBoxvalues();
            
        }
    }

    //function to bind the informations of employees  from database to the textboxs 
    

    private void BindTextBoxvalues()
    {

        c.getcon();
        SqlCommand cmd = new SqlCommand("select e.empid, e.empname,e.email, j.job,d.designation from Employee_details e inner join Job_type j on e.typeid=j.typeid  inner join Designation d on e.desig_id=d.desig_id where e.empid='" + Convert.ToInt32(ViewState["empno"]) + "'", c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        txt_id.Text = dt.Rows[0][0].ToString();
        txt_name.Text = dt.Rows[0][1].ToString();
        txt_email.Text = dt.Rows[0][2].ToString();
        txt_type.Text = dt.Rows[0][3].ToString();
        txt_desig.Text = dt.Rows[0][4].ToString();

    }

    //function to generate username and password randomly
    private void CreateRandomPassword()
    {
        String allowedChars = "";

        allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";

        allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";

        allowedChars += "1,2,3,4,5,6,7,8,9,0,!,@,#,$,%,&,?";

        char[] sep = { ',' };
        
        String[] arr = allowedChars.Split(sep);

        String passwordString = "";

        String temp = "";

        Random rand = new Random();

        for (int i = 0; i < 10; i++)
        {

            temp = arr[rand.Next(0, arr.Length)];

            passwordString += temp;

        }

        lbl_pass.Text= Convert.ToString(passwordString);
        lbl_user.Text = Convert.ToString(passwordString);

}
    //function to send username and password as mail

    public void sendmail()
    {
      String p = txt_email.Text.Trim();
        MailMessage msg = new MailMessage();
        msg.From = new MailAddress("govthomoeoktm2016@gmail.com");
        msg.To.Add(new MailAddress(p));
        msg.Subject = "Account Approved";
        msg.Body = String.Format("<html><head></head><body><b>Hi,</b></br><p>Your Account has been approved..Your username is <b> '"+lbl_user.Text+"'</b> and your password is <b>'" + lbl_pass.Text+ "'</p></b></body></html>");
        msg.IsBodyHtml = true;
        SmtpClient smtp = new SmtpClient();
        smtp.EnableSsl = true;
        smtp.Send(msg);

        // ScriptManager.RegisterStartupScript(Page, this.GetType(), "write", "alert('Registered successfully')", true);

    }


    protected void btn_submit_Click(object sender, EventArgs e)
{
            c.getcon();

            String status = "Approved";
            SqlCommand cmd = new SqlCommand("update Employee_details set status='" + status + "' where empid='" + Convert.ToInt32(ViewState["empno"]) + "'", c.con);

            int result = cmd.ExecuteNonQuery();

            if (result == 1)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowSuccess", "javascript:alert('Employee Approved Successfully');", true);
               Response.Write("Employee Approved");
            }
            String s = "insert into Login values('" + lbl_user.Text + "','" + lbl_pass.Text + "','" + txt_type.Text + "','" + txt_id.Text + "')";
            SqlCommand cmd1 = new SqlCommand(s, c.con);
            cmd1.ExecuteNonQuery();
            sendmail();
            c.con.Close();
            Response.Write("<script>alert('User Created Successfully');</script>");
         
}
}
    