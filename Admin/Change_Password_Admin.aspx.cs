﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Admin_Change_Password_Admin : System.Web.UI.Page
{
    Connection c = new Connection();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;

    }
    protected void btn_back_Click(object sender, EventArgs e)
    {
        Response.Redirect("Superend.aspx");

    }
    protected void btn_chg_Click(object sender, EventArgs e)
    {
        c.getcon();
        string name = Session["id"].ToString();
        string str_user = "select * from Login where empid= '" + name + "' ";
        SqlCommand cmd_user = new SqlCommand(str_user, c.con);
        SqlDataAdapter da_user = new SqlDataAdapter(cmd_user);
        DataTable dt_user = new DataTable();
        da_user.Fill(dt_user);

        int k = cmd_user.ExecuteNonQuery();

        if (dt_user.Rows.Count > 0)
        {
            DataRow row11 = dt_user.Rows[dt_user.Rows.Count - 1];
            string pass = Convert.ToString(row11[2]);
            if (pass == txt_old.Text)
            {


                if (txt_new.Text == txt_retype.Text)
                {


                    String str_newuser = "update Login set password='" + txt_retype.Text + "' where empid='" + name + "'";
                    SqlCommand cmd_newuser = new SqlCommand(str_newuser, c.con);
                    cmd_newuser.ExecuteNonQuery();
                    Response.Write("<script>alert('Password Changed Successfully');</script>");
                    lbl_retype.Text = " ";
                    lbl_old.Text = " ";
                    c.con.Close();
                }
                else
                {
                    lbl_retype.Text = "Retyped password not equal to new password";
                }
            }
            else
            {
                lbl_old.Text = "Password Incorrect";
            }
            txt_new.Text = " ";
            txt_old.Text = " ";
            txt_retype.Text = " ";
            lbl_old.Text = " ";
            lbl_retype.Text = " ";
        }

    }
}