﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

//search employees by there employee id

public partial class Admin_searchemployee : System.Web.UI.Page
{

    Connection c = new Connection();
 
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Superend.aspx");
    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        Response.Redirect("deleteuser.aspx?EmpNo=" + txt_empid.Text);

    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        c.getcon();
        SqlCommand cmd_emp = new SqlCommand("select * from Employee_details where empid='" + txt_empid.Text + "'", c.con);
        DataTable dt_emp = new DataTable();
        SqlDataAdapter da_emp = new SqlDataAdapter(cmd_emp);
        da_emp.Fill(dt_emp);
        if (dt_emp.Rows.Count > 0)
        {
            DataRow row_emp = dt_emp.Rows[dt_emp.Rows.Count - 1];

            String name = Convert.ToString(row_emp[1]);
            String eid = Convert.ToString(row_emp[0]);

            String dob = Convert.ToString(row_emp[2]);
            String email = Convert.ToString(row_emp[7]);
            String etypeid = Convert.ToString(row_emp[10]);
            String gen = Convert.ToString(row_emp[3]);
            String jdate = Convert.ToString(row_emp[8]);
            String cnt1 = Convert.ToString(row_emp[6]);
            String desigid = Convert.ToString(row_emp[11]);
            SqlCommand cmds1 = new SqlCommand("select * from Designation where desig_id='" + desigid + "'", c.con);
            SqlDataAdapter sds1 = new SqlDataAdapter(cmds1);
            DataTable dt_desig = new DataTable();
            sds1.Fill(dt_desig);

            int ks1 = cmds1.ExecuteNonQuery();

            if (dt_desig.Rows.Count > 0)
            {
                DataRow rowdesig = dt_desig.Rows[dt_desig.Rows.Count - 1];

                String designame = Convert.ToString(rowdesig[1]);
                txt_empname.Text = name;
                txt_empid.Text = eid;
                txt_dob.Text = dob;
                txt_email.Text = email;
                txt_gen.Text = gen;
                txt_joiningdate.Text = jdate;
                txt_con1.Text = cnt1;
                txt_desigid.Text = designame;
                txt_emptypeid.Text = etypeid;


            }
        }

        else
        {
            Response.Write("<script>alert('No Record Found');</script>");
            //  Response.Redirect("homepageadmin.aspx");
        }

    }
}