﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_ViewApprvdEmp : System.Web.UI.Page
{
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            c.getcon();
            gridviewemp();
            c.con.Close();

        }

    }
    public void gridviewemp()
    {
        string str_empview = "select * from Employee_details e inner join Designation  d on e.desig_id=d.desig_id where e.status='Approved'";

        SqlCommand cmd_empview = new SqlCommand(str_empview, c.con);
        SqlDataAdapter da_empview = new SqlDataAdapter(cmd_empview);
        DataTable dt_empview = new DataTable();
        da_empview.Fill(dt_empview);
        if (dt_empview.Rows.Count > 0)
        {
            GridView1.DataSource = dt_empview;
            GridView1.DataBind();
        }

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        c.getcon();
        GridView1.PageIndex = e.NewPageIndex;
        gridviewemp();
        c.con.Close();

    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
}