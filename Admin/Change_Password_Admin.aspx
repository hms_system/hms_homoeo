﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Change_Password_Admin.aspx.cs" Inherits="Admin_Change_Password_Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <center>
         
    <table>
            <tr>
                <td>
                    &nbsp;</td>
                <td colspan="3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td colspan="3" class="center">
                   CHANGE PASSWORD</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="label_small">Enter Old Password:</td>
                <td>
                    &nbsp;</td>
                <td class="text-left">
                    <asp:TextBox ID="txt_old" runat="server" TextMode="Password" CssClass="twitterStyleTextbox"></asp:TextBox>
                     <asp:Label ID="lbl_old" runat="server" CssClass="alert-warning"></asp:Label>
                    </td>
                <td>
                     &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="label_small">Enter New Password:</td>
                <td>
                    &nbsp;</td>
                <td class="text-left">
                    <asp:TextBox ID="txt_new" runat="server" TextMode="Password" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_new" ErrorMessage="*Please enter valid password" ForeColor="Red" ValidationExpression="[a-zA-Z0-9 ]{8,}" ValidationGroup="a" CssClass="auto-style41"></asp:RegularExpressionValidator>

                </td>
            </tr>
            <tr>
                <td></td>
                <td class="label_small">Retype New Password:</td>
                <td>
                    &nbsp;</td>
                <td class="text-left">
                    <asp:TextBox ID="txt_retype" runat="server" TextMode="Password" CssClass="twitterStyleTextbox"></asp:TextBox>              
                    <asp:Label ID="lbl_retype" runat="server" CssClass="alert-warning"></asp:Label>
                   
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btn_back" runat="server" OnClick="btn_back_Click" Text="Back" BorderStyle="None" CssClass="buttonyellow"/>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btn_chg" runat="server" OnClick="btn_chg_Click" Text="Change Password" BorderStyle="None" CssClass="buttonyellow" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
           
        </table>

    </center>
        </form>
</asp:Content>

