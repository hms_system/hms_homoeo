﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class Admin_DoctorSchedule : System.Web.UI.Page
{
    Connection c = new Connection();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            binddoc();
            bindroom();
            gridbind();

        }
    }

    //to bind the registered doctor employees

    public void binddoc()
    {
        c.getcon();
        string str_name = "select * from Employee_Details where empid not in (select empid from OP_Allocation where date in (Convert(datetime,'" + DateTime.Today + "',103)) ) and typeid='1' and status='Approved'";
        SqlCommand cmd_name = new SqlCommand(str_name, c.con);
        SqlDataAdapter da_name = new SqlDataAdapter(cmd_name);
        DataTable dt_name = new DataTable();
        da_name.Fill(dt_name);
        if (dt_name.Rows.Count > 0)
        {
            ddl_doc_name.DataSource = dt_name;
            ddl_doc_name.DataValueField = "empid";
            ddl_doc_name.DataTextField = "empname";
            ddl_doc_name.DataBind();
            ddl_doc_name.Items.Insert(0, "SELECT");
        }
        c.con.Close();
    }

    //to bind the oproom number for each doctors

    public void bindroom()
    {
        c.getcon();
        string str_oproom = "select * from OP_Room where oproom_id not in (select oproom_id from OP_Allocation where date in (Convert(datetime,'" + DateTime.Today + "',103)))";
        SqlCommand cmd_oproom = new SqlCommand(str_oproom, c.con);
        SqlDataAdapter da_oproom = new SqlDataAdapter(cmd_oproom);
        DataTable dt_oproom = new DataTable();
        da_oproom.Fill(dt_oproom);
        if (dt_oproom.Rows.Count > 0)
        {
            ddl_oproom_no.DataSource = dt_oproom;
            ddl_oproom_no.DataValueField = "oproom_id";
            ddl_oproom_no.DataTextField = "op_room";
            ddl_oproom_no.DataBind();
            ddl_oproom_no.Items.Insert(0, "SELECT");
        }
        c.con.Close();
    }

    //function to allocate each doctor to corresponding op rooms

    protected void btn_add_doc_schedule_Click(object sender, EventArgs e)
    {
        c.getcon();
        string in_schedule = "insert into OP_Allocation values('" + ddl_oproom_no.SelectedItem.Value + "','" + ddl_doc_name.SelectedItem.Value + "',Convert(datetime,'" + DateTime.Now + "',103))";
        SqlCommand cmd_in_schedule = new SqlCommand(in_schedule, c.con);
        cmd_in_schedule.ExecuteNonQuery();
        c.con.Close();
        gridbind();
        binddoc();
        bindroom();
   
    }
    public void gridbind()
    {
        c.getcon();
        string str_dr_schedule = " select e.empname,r.op_room ,op.op_id from Employee_details e inner join OP_Allocation op on e.empid=op.empid inner join OP_Room r on r.oproom_id=op.oproom_id where op.date  in (Convert(datetime,'" + DateTime.Today + "',103)) ";
        //where op.date='"+DateTime.Today+"'
        //where op.date=Convert(datetime,'" + DateTime.Now + "',103)";
        SqlCommand cmd_dr_schedule = new SqlCommand(str_dr_schedule, c.con);
        DataTable dt_dr_schedule = new DataTable();
        SqlDataAdapter da_test = new SqlDataAdapter(cmd_dr_schedule);
        da_test.Fill(dt_dr_schedule);
        if (dt_dr_schedule.Rows.Count > 0)
        {
            GridView1.DataSource = dt_dr_schedule;
            GridView1.DataBind();
        }
        c.con.Close();

    }
    //protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //     c.getcon();
    //    int sid = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0].ToString());
    //    string str_dr_del = "delete from OP_Allocation where op_id='" + sid + "'";
    //    //and date='"+DateTime.Today+"'";
    //    SqlCommand cmd_dr_del = new SqlCommand(str_dr_del, c.con);
    //    cmd_dr_del.ExecuteNonQuery();
    //    GridView1.EditIndex = -1;
    //    gridbind();

    //    string str_name1 = "select * from Employee_Details where empid not in (select empid from OP_Allocation ) and typeid='1'";
    //    SqlCommand cmd_name1 = new SqlCommand(str_name1, c.con);
    //    SqlDataAdapter da_name1 = new SqlDataAdapter(cmd_name1);
    //    DataTable dt_name1 = new DataTable();
    //    da_name1.Fill(dt_name1);
    //    if (dt_name1.Rows.Count > 0)
    //    {
    //        ddl_doc_name.DataSource = dt_name1;
    //        ddl_doc_name.DataValueField = "empid";
    //        ddl_doc_name.DataTextField = "empname";
    //        ddl_doc_name.DataBind();
    //        ddl_doc_name.Items.Insert(0, "SELECT");
    //    }


    //    string str_oproom1 = "select * from OP_Room where oproom_id not in (select oproom_id from OP_Allocation )";
    //    SqlCommand cmd_oproom1 = new SqlCommand(str_oproom1, c.con);
    //    SqlDataAdapter da_oproom1 = new SqlDataAdapter(cmd_oproom1);
    //    DataTable dt_oproom1 = new DataTable();
    //    da_oproom1.Fill(dt_oproom1);
    //    if (dt_oproom1.Rows.Count > 0)
    //    {
    //        ddl_oproom_no.DataSource = dt_oproom1;
    //        ddl_oproom_no.DataValueField = "oproom_id";
    //        ddl_oproom_no.DataTextField = "op_room";
    //        ddl_oproom_no.DataBind();
    //        ddl_oproom_no.Items.Insert(0, "SELECT");
    //    }


    //    c.con.Close();
    //}

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        c.getcon();
        ImageButton im = (ImageButton)sender;
        GridViewRow grv = im.NamingContainer as GridViewRow;
        int sid = Convert.ToInt32(GridView1.DataKeys[grv.RowIndex].Values[0].ToString());

        string str_dr_del = "delete from OP_Allocation where op_id='" + sid + "'";
        //and date='"+DateTime.Today+"'";
        SqlCommand cmd_dr_del = new SqlCommand(str_dr_del, c.con);
        cmd_dr_del.ExecuteNonQuery();
        GridView1.EditIndex = -1;
        c.con.Close();
        gridbind();
        binddoc();
        bindroom();
        
       
    }
}
