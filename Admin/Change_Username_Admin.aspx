﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Change_Username_Admin.aspx.cs" Inherits="Admin_Change_Username_Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
        <center>

    <table>
         <tr>
                <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             </tr>
            <tr>
                <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td colspan="4" class="center">CHANGE USERNAME</td>
                </tr>
        <tr>
                <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             </tr>
        <tr>
                <td>&nbsp;</td>
             <td>&nbsp;</td>
            <td>&nbsp;</td>
             <td class="label_small">Enter New UserName:</td>
            <td class="text-left">
                    <asp:TextBox ID="txt_newusername" runat="server"  CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_newusername" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
        <tr>
                <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             </tr>
        <tr>
                <td>&nbsp;</td>
             <td>&nbsp;</td>
            <td>&nbsp;</td>
             <td class="label_small">Enter Password:</td>
            <td class="text-left">
                   <asp:TextBox ID="txt_password" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    </td>
                       </tr>
         <tr>
                <td>&nbsp;</td>
             <td>&nbsp;</td>
            <td>&nbsp;</td>
             <td colspan="2">
                    <asp:Label ID="lbl_title" runat="server" CssClass="label_small"></asp:Label>
                </td>
             <td></td>
            </tr>
        <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btn_back" runat="server" OnClick="btn_back_Click" Text="Back" CssClass="buttonyellow" />
                </td>
                <td>
                    <asp:Button ID="btn_changeusername" runat="server" OnClick="btn_changeusername_Click" Text="Change Username" CssClass="buttonyellow"/>
                </td>
                <td>&nbsp;</td>
            </tr>            
    </table>

        </center>
        </form>
</asp:Content>

