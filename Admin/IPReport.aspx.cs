﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.DataVisualization.Charting;

//display the number of patients  who were admitted as in-patients

public partial class Admin_IPReport : System.Web.UI.Page
{
    Connection c= new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {

        if(!IsPostBack)
        {
            Label9.Visible = false;
        lbl_total.Visible = false;

        //lbl_stdate.Visible = false;
        //txt_stdate.Visible = false;
        //lbl_endate.Visible = false;
        //txt_endate.Visible = false;
        //btn_week.Visible = false;


        lbl_month.Visible = false;
        ddl_month.Visible = false;
        lbl_year.Visible = false;
        txt_year.Visible = false;
        btn_month.Visible = false;

        lbl_s_year.Visible = false;
        txt_search_year.Visible = false;
        btn_year.Visible = false;

        lbl_total.Visible = false;
        Label9.Visible = false;
        }

    }
   
    protected void btn_month_Click(object sender, EventArgs e)
    {
         c.getcon();
            Label9.Visible = true;
            lbl_total.Visible = true;

            int cnt_female = 0;
            int cnt_male = 0;
                 int cnt_ch = 0;
               int cnt_total = 0;
        

            int m = Convert.ToInt32(ddl_month.SelectedItem.Value);
            int y = Convert.ToInt32(txt_year.Text);
            String str_s1 = "select count(*) from Schedule s inner join Patient_details p on s.patient_id=p.cardno where p.gender=@gen and  YEAR(GETDATE())-YEAR(p.dob)>@age and  datepart(mm, schedule_date) =@date and datepart(yyyy,schedule_date)=@yr and s.status=@status  ";
            SqlCommand cmd_s1 = new SqlCommand(str_s1, c.con);
            cmd_s1.Parameters.AddWithValue("@date", m);
            cmd_s1.Parameters.AddWithValue("@yr", y);
            cmd_s1.Parameters.AddWithValue("@age","12");
            cmd_s1.Parameters.AddWithValue("@gen", "Male");
            cmd_s1.Parameters.AddWithValue("@status", "IP");
            SqlDataAdapter da_s1 = new SqlDataAdapter(cmd_s1);
            DataTable dt_s1 = new DataTable();
            da_s1.Fill(dt_s1);
            cmd_s1.ExecuteNonQuery();
            if (dt_s1.Rows.Count > 0)
            {
                // DataRow row_s1 = dt_s1.Rows[dt_s1.Rows.Count - 1];
                cnt_male = Convert.ToInt32(dt_s1.Rows[0][0]);
                //lbl_male.Text = Convert.ToString(cnt_male);
            }


            String str_s2 = "select count(*) from Schedule s inner join Patient_details p on s.patient_id=p.cardno where p.gender=@gen and  YEAR(GETDATE())-YEAR(p.dob)>@age and datepart(mm, schedule_date) =@date and datepart(yyyy,schedule_date)=@yr and s.status=@status ";
            SqlCommand cmd_s2 = new SqlCommand(str_s2, c.con);
            cmd_s2.Parameters.AddWithValue("@date", m);
            cmd_s2.Parameters.AddWithValue("@yr", y);    
        cmd_s2.Parameters.AddWithValue("@age", "12");
            cmd_s2.Parameters.AddWithValue("@gen", "Female");
            cmd_s2.Parameters.AddWithValue("@status", "IP");
            SqlDataAdapter da_s2 = new SqlDataAdapter(cmd_s2);
            DataTable dt_s2 = new DataTable();
            da_s2.Fill(dt_s2);
            cmd_s2.ExecuteNonQuery();
            if (dt_s2.Rows.Count > 0)
            {
                // DataRow row_s1 = dt_s1.Rows[dt_s1.Rows.Count - 1];
                cnt_female = Convert.ToInt32(dt_s2.Rows[0][0]);
                //lbl_female.Text = Convert.ToString(cnt_female);
            }

            String str_s3 = "select count(*) from Schedule s inner join Patient_details p on s.patient_id=p.cardno where  YEAR(GETDATE())-YEAR(p.dob)<=@age and datepart(mm, schedule_date) =@date and datepart(yyyy,schedule_date)=@yr and s.status=@status ";
            SqlCommand cmd_s3 = new SqlCommand(str_s3, c.con);
            cmd_s3.Parameters.AddWithValue("@date", m);
            cmd_s3.Parameters.AddWithValue("@yr", y);
            cmd_s3.Parameters.AddWithValue("@age", "12");
            cmd_s3.Parameters.AddWithValue("@status", "IP");
            SqlDataAdapter da_s3 = new SqlDataAdapter(cmd_s3);
            DataTable dt_s3 = new DataTable();
            da_s3.Fill(dt_s3);
            cmd_s3.ExecuteNonQuery();
            if (dt_s3.Rows.Count > 0)
            {
                // DataRow row_s1 = dt_s1.Rows[dt_s1.Rows.Count - 1];
                cnt_ch = Convert.ToInt32(dt_s3.Rows[0][0]);
               // lbl_children.Text = Convert.ToString(cnt_ch);
            }

            cnt_total = cnt_male + cnt_female + cnt_ch;
            lbl_total.Text = Convert.ToString(cnt_total);
            c.con.Close();

            ////BAR-CHART/////
            string[] x1 = new string[] { "Male", "Female", "Childern" };
            int[] y1= new int[] { cnt_male, cnt_female, cnt_ch };
            Chart1.Series[0].Points.DataBindXY(x1, y1);


    }
    protected void btn_year_Click(object sender, EventArgs e)
    {
        c.getcon();

       
        Label9.Visible = true;
        lbl_total.Visible = true;

        int cnt_female = 0;
        int cnt_male = 0;
        int cnt_ch = 0;
        int cnt_total = 0;


        // int year = Convert.ToInt32( ddl_month.SelectedItem.Value);
        int y = Convert.ToInt32(txt_search_year.Text);
        String str_s1 = "select count(*) from Schedule s inner join Patient_details p on s.patient_id=p.cardno where p.gender=@gen and  YEAR(GETDATE())-YEAR(p.dob)>@age and datepart(yyyy,schedule_date)=@yr and s.status=@status  ";
        SqlCommand cmd_s1 = new SqlCommand(str_s1, c.con);
        //cmd_s1.Parameters.AddWithValue("@date", m);
        cmd_s1.Parameters.AddWithValue("@yr", y);
        cmd_s1.Parameters.AddWithValue("@age", "12");
        cmd_s1.Parameters.AddWithValue("@gen", "Male");
        cmd_s1.Parameters.AddWithValue("@status", "IP");
        SqlDataAdapter da_s1 = new SqlDataAdapter(cmd_s1);
        DataTable dt_s1 = new DataTable();
        da_s1.Fill(dt_s1);
        cmd_s1.ExecuteNonQuery();
        if (dt_s1.Rows.Count > 0)
        {
            // DataRow row_s1 = dt_s1.Rows[dt_s1.Rows.Count - 1];
            cnt_male = Convert.ToInt32(dt_s1.Rows[0][0]);
            //lbl_male.Text = Convert.ToString(cnt_male);
        }


        String str_s2 = "select count(*) from Schedule s inner join Patient_details p on s.patient_id=p.cardno where p.gender=@gen and  YEAR(GETDATE())-YEAR(p.dob)>@age and datepart(yyyy,schedule_date)=@yr and s.status=@status";
        SqlCommand cmd_s2 = new SqlCommand(str_s2, c.con);
        //  cmd_s2.Parameters.AddWithValue("@date", m);
        cmd_s2.Parameters.AddWithValue("@yr", y);
        cmd_s2.Parameters.AddWithValue("@age", "12");
        cmd_s2.Parameters.AddWithValue("@gen", "Female");
        cmd_s2.Parameters.AddWithValue("@status", "IP");
        SqlDataAdapter da_s2 = new SqlDataAdapter(cmd_s2);
        DataTable dt_s2 = new DataTable();
        da_s2.Fill(dt_s2);
        cmd_s2.ExecuteNonQuery();
        if (dt_s2.Rows.Count > 0)
        {
            // DataRow row_s1 = dt_s1.Rows[dt_s1.Rows.Count - 1];
            cnt_female = Convert.ToInt32(dt_s2.Rows[0][0]);
            //lbl_female.Text = Convert.ToString(cnt_female);
        }

        String str_s3 = "select count(*) from Schedule s inner join Patient_details p on s.patient_id=p.cardno where  YEAR(GETDATE())-YEAR(p.dob)<=@age and  datepart(yyyy,schedule_date)=@yr  and s.status=@status";
        SqlCommand cmd_s3 = new SqlCommand(str_s3, c.con);
        // cmd_s3.Parameters.AddWithValue("@date", m);
        cmd_s3.Parameters.AddWithValue("@yr", y);
        cmd_s3.Parameters.AddWithValue("@age", "12");
        cmd_s3.Parameters.AddWithValue("@status", "IP");

        SqlDataAdapter da_s3 = new SqlDataAdapter(cmd_s3);
        DataTable dt_s3 = new DataTable();
        da_s3.Fill(dt_s3);
        cmd_s3.ExecuteNonQuery();
        if (dt_s3.Rows.Count > 0)
        {
            // DataRow row_s1 = dt_s1.Rows[dt_s1.Rows.Count - 1];
            cnt_ch = Convert.ToInt32(dt_s3.Rows[0][0]);
            //lbl_children.Text = Convert.ToString(cnt_ch);
        }

        cnt_total = cnt_male + cnt_female + cnt_ch;
        lbl_total.Text = Convert.ToString(cnt_total);
        c.con.Close();


        ////BAR-CHART/////
        string[] x2 = new string[] { "Male", "Female", "Childern" };
        int[] y2 = new int[] { cnt_male, cnt_female, cnt_ch };
        Chart1.Series[0].Points.DataBindXY(x2, y2);

    }
    protected void ddl_searchby_SelectedIndexChanged(object sender, EventArgs e)
    {

        Label9.Visible = true;
        lbl_total.Visible = true;


        if (ddl_searchby.SelectedItem.Text == "Today")
        {



            int cnt_female = 0;
            int cnt_male = 0;
            int cnt_ch = 0;
            int cnt_total = 0;
            c.getcon();

            String str_s1 = "select count(*) from Schedule s inner join Patient_details p on s.patient_id=p.cardno where p.gender=@gen and  YEAR(GETDATE())-YEAR(p.dob)>@age and s.schedule_date=@date and s.status=@status ";
            SqlCommand cmd_s1 = new SqlCommand(str_s1, c.con);
            cmd_s1.Parameters.AddWithValue("@date", DateTime.Today);
            cmd_s1.Parameters.AddWithValue("@age", "12");
            cmd_s1.Parameters.AddWithValue("@gen", "Male");
            cmd_s1.Parameters.AddWithValue("@status", "IP");
            SqlDataAdapter da_s1 = new SqlDataAdapter(cmd_s1);
            DataTable dt_s1 = new DataTable();
            da_s1.Fill(dt_s1);
            cmd_s1.ExecuteNonQuery();
            if (dt_s1.Rows.Count > 0)
            {
                // DataRow row_s1 = dt_s1.Rows[dt_s1.Rows.Count - 1];
                cnt_male = Convert.ToInt32(dt_s1.Rows[0][0]);
                //lbl_male.Text = Convert.ToString(cnt_male);
            }


            String str_s2 = "select count(*) from Schedule s inner join Patient_details p on s.patient_id=p.cardno where p.gender=@gen and  YEAR(GETDATE())-YEAR(p.dob)>@age and s.schedule_date=@date and s.status=@status ";
            SqlCommand cmd_s2 = new SqlCommand(str_s2, c.con);
            cmd_s2.Parameters.AddWithValue("@date", DateTime.Today);
            cmd_s2.Parameters.AddWithValue("@age", "12");
            cmd_s2.Parameters.AddWithValue("@gen", "Female");
            cmd_s2.Parameters.AddWithValue("@status", "IP");
            SqlDataAdapter da_s2 = new SqlDataAdapter(cmd_s2);
            DataTable dt_s2 = new DataTable();
            da_s2.Fill(dt_s2);
            cmd_s2.ExecuteNonQuery();
            if (dt_s2.Rows.Count > 0)
            {
                // DataRow row_s1 = dt_s1.Rows[dt_s1.Rows.Count - 1];
                cnt_female = Convert.ToInt32(dt_s2.Rows[0][0]);
                //lbl_female.Text = Convert.ToString(cnt_female);
            }

            String str_s3 = "select count(*) from Schedule s inner join Patient_details p on s.patient_id=p.cardno where  YEAR(GETDATE())-YEAR(p.dob)<=@age and s.schedule_date=@date and s.status=@status ";
            SqlCommand cmd_s3 = new SqlCommand(str_s3, c.con);
            cmd_s3.Parameters.AddWithValue("@date", DateTime.Today);
            cmd_s3.Parameters.AddWithValue("@age", "12");
            cmd_s3.Parameters.AddWithValue("@status", "IP");
            SqlDataAdapter da_s3 = new SqlDataAdapter(cmd_s3);
            DataTable dt_s3 = new DataTable();
            da_s3.Fill(dt_s3);
            cmd_s3.ExecuteNonQuery();
            if (dt_s3.Rows.Count > 0)
            {
                // DataRow row_s1 = dt_s1.Rows[dt_s1.Rows.Count - 1];
                cnt_ch = Convert.ToInt32(dt_s3.Rows[0][0]);
                //lbl_children.Text = Convert.ToString(cnt_ch);
            }

            cnt_total = cnt_male + cnt_female + cnt_ch;
            lbl_total.Text = Convert.ToString(cnt_total);
            c.con.Close();

            ////BAR-CHART///
            string[] x = new string[] { "Male", "Female", "Childern" };
            int[] y = new int[] { cnt_male, cnt_female, cnt_ch };
            Chart1.Series[0].Points.DataBindXY(x, y);




        }


        //else if (ddl_searchby.SelectedItem.Text == "WeekDay")
        //{
        //    lbl_stdate.Visible = true;
        //    txt_stdate.Visible = true;
        //    lbl_endate.Visible = true;
        //    txt_endate.Visible = true;
        //    btn_week.Visible = true;



        //}
        else if (ddl_searchby.SelectedItem.Text == "Month")
        {
            lbl_month.Visible = true;
            ddl_month.Visible = true;
            lbl_year.Visible = true;
            txt_year.Visible = true;
            btn_month.Visible = true;


        }

        else if (ddl_searchby.SelectedItem.Text == "Year")
        {
            lbl_s_year.Visible = true; ;
            txt_search_year.Visible = true;
            btn_year.Visible = true;


        }




    }
    protected void btn_ok_Click(object sender, EventArgs e)
    {
        Label9.Visible = true;
        lbl_total.Visible = true;


        if (ddl_searchby.SelectedItem.Text == "Today")
        {



            int cnt_female = 0;
            int cnt_male = 0;
            int cnt_ch = 0;
            int cnt_total = 0;
            c.getcon();

            String str_s1 = "select count(*) from Schedule s inner join Patient_details p on s.patient_id=p.cardno where p.gender=@gen and  YEAR(GETDATE())-YEAR(p.dob)>@age and s.schedule_date=@date and s.status=@status ";
            SqlCommand cmd_s1 = new SqlCommand(str_s1, c.con);
            cmd_s1.Parameters.AddWithValue("@date", DateTime.Today);
            cmd_s1.Parameters.AddWithValue("@age", "12");
            cmd_s1.Parameters.AddWithValue("@gen", "Male");
            cmd_s1.Parameters.AddWithValue("@status", "IP");
            SqlDataAdapter da_s1 = new SqlDataAdapter(cmd_s1);
            DataTable dt_s1 = new DataTable();
            da_s1.Fill(dt_s1);
            cmd_s1.ExecuteNonQuery();
            if (dt_s1.Rows.Count > 0)
            {
                // DataRow row_s1 = dt_s1.Rows[dt_s1.Rows.Count - 1];
                cnt_male = Convert.ToInt32(dt_s1.Rows[0][0]);
                //lbl_male.Text = Convert.ToString(cnt_male);
            }


            String str_s2 = "select count(*) from Schedule s inner join Patient_details p on s.patient_id=p.cardno where p.gender=@gen and  YEAR(GETDATE())-YEAR(p.dob)>@age and s.schedule_date=@date and s.status=@status ";
            SqlCommand cmd_s2 = new SqlCommand(str_s2, c.con);
            cmd_s2.Parameters.AddWithValue("@date", DateTime.Today);
            cmd_s2.Parameters.AddWithValue("@age", "12");
            cmd_s2.Parameters.AddWithValue("@gen", "Female");
            cmd_s2.Parameters.AddWithValue("@status", "IP");
            SqlDataAdapter da_s2 = new SqlDataAdapter(cmd_s2);
            DataTable dt_s2 = new DataTable();
            da_s2.Fill(dt_s2);
            cmd_s2.ExecuteNonQuery();
            if (dt_s2.Rows.Count > 0)
            {
                // DataRow row_s1 = dt_s1.Rows[dt_s1.Rows.Count - 1];
                cnt_female = Convert.ToInt32(dt_s2.Rows[0][0]);
                //lbl_female.Text = Convert.ToString(cnt_female);
            }

            String str_s3 = "select count(*) from Schedule s inner join Patient_details p on s.patient_id=p.cardno where  YEAR(GETDATE())-YEAR(p.dob)<=@age and s.schedule_date=@date and s.status=@status ";
            SqlCommand cmd_s3 = new SqlCommand(str_s3, c.con);
            cmd_s3.Parameters.AddWithValue("@date", DateTime.Today);
            cmd_s3.Parameters.AddWithValue("@age", "12");
            cmd_s3.Parameters.AddWithValue("@status", "IP");
            SqlDataAdapter da_s3 = new SqlDataAdapter(cmd_s3);
            DataTable dt_s3 = new DataTable();
            da_s3.Fill(dt_s3);
            cmd_s3.ExecuteNonQuery();
            if (dt_s3.Rows.Count > 0)
            {
                // DataRow row_s1 = dt_s1.Rows[dt_s1.Rows.Count - 1];
                cnt_ch = Convert.ToInt32(dt_s3.Rows[0][0]);
                //lbl_children.Text = Convert.ToString(cnt_ch);
            }

            cnt_total = cnt_male + cnt_female + cnt_ch;
            lbl_total.Text = Convert.ToString(cnt_total);
            c.con.Close();

            ////BAR-CHART///
            string[] x = new string[] { "Male", "Female", "Childern" };
            int[] y = new int[] { cnt_male, cnt_female, cnt_ch };
            Chart1.Series[0].Points.DataBindXY(x, y);




        }


        //else if (ddl_searchby.SelectedItem.Text == "WeekDay")
        //{
        //    lbl_stdate.Visible = true;
        //    txt_stdate.Visible = true;
        //    lbl_endate.Visible = true;
        //    txt_endate.Visible = true;
        //    btn_week.Visible = true;



        //}
        else if (ddl_searchby.SelectedItem.Text == "Month")
        {
            lbl_month.Visible = true;
            ddl_month.Visible = true;
            lbl_year.Visible = true;
            txt_year.Visible = true;
            btn_month.Visible = true;


        }

        else if (ddl_searchby.SelectedItem.Text == "Year")
        {
            lbl_s_year.Visible = true; ;
            txt_search_year.Visible = true;
            btn_year.Visible = true;


        }



    }
    protected void btn_month_Click1(object sender, EventArgs e)
    {

    }
    protected void btn_year_Click1(object sender, EventArgs e)
    {

    }
}

   