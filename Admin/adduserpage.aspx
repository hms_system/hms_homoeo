﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="adduserpage.aspx.cs" Inherits="Admin_adduserpage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title></title>
    <style type="text/css">
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <div>
            <br />
            <center>
                <table>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Label ID="Label3" runat="server" CssClass="label_small">Employee ID</asp:Label>

                        </td>
                        <td>
                            <asp:TextBox ID="txt_id" runat="server" Enabled="False" ReadOnly="True" CssClass="twitterStyleTextbox"></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Label ID="Label4" runat="server" CssClass="label_small">Employee Name</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_name" runat="server" Enabled="False" ReadOnly="True" CssClass="twitterStyleTextbox"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                             <asp:Label ID="Label5" runat="server" CssClass="label_small">Email</asp:Label>
                        
                        </td>
                        <td>
                            <asp:TextBox ID="txt_email" runat="server" Enabled="False" ReadOnly="true" CssClass="twitterStyleTextbox"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                             &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Label ID="Label6" runat="server" CssClass="label_small">User Type</asp:Label> </td>
                        <td>
                            <asp:TextBox ID="txt_type" runat="server" ReadOnly="true" Enabled="False" CssClass="twitterStyleTextbox"></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                             <asp:Label ID="Label7" runat="server" CssClass="label_small">Designation</asp:Label>

                        </td>
                        <td>
                            <asp:TextBox ID="txt_desig" runat="server" ReadOnly="true" Enabled="False" CssClass="twitterStyleTextbox"></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                             &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                                <asp:Label ID="Label8" runat="server" CssClass="label_small">Random Username</asp:Label></td>
                        <td>
                            <asp:Label ID="lbl_user" runat="server" CssClass="label-success"></asp:Label>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                                &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                                <asp:Label ID="Label9" runat="server" CssClass="label_small">Random Password</asp:Label></td>
                        <td>
                            <asp:Label ID="lbl_pass" runat="server" CssClass="label-success"></asp:Label>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                                &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Button ID="btn_submit" runat="server" Text="Submit" OnClick="btn_submit_Click" CssClass="buttonyellow" />
                            </td>

                        <td>&nbsp;</td>
                    </tr>

                </table>
            </center>
            <br />
        </div>

    </form>
</asp:Content>

