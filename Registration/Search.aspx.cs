﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Registration_Search : System.Web.UI.Page
{
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }

    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        bindGridView1();
    }
    public void bindGridView1()
    {

        c.getcon();
        string str_search = "select * from Patient_details where patient_name = '" + txt_name.Text + "'";
        SqlCommand cmd_search = new SqlCommand(str_search, c.con);
        DataTable dt_search = new DataTable();
        SqlDataAdapter da_search = new SqlDataAdapter(cmd_search);
        da_search.Fill(dt_search);
        if (dt_search.Rows.Count > 0)
        {
            GridView1.DataSource = dt_search;
            GridView1.DataBind();

        }

    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {

      
        if (e.CommandName == "SelectButton")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];
            Response.Redirect("PrintCard.aspx?PID=" + row.Cells[0].Text);
        }
    
    }
}