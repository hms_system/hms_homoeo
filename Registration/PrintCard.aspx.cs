﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;

using iTextSharp.text;
using iTextSharp.text.pdf;

public partial class Registration_PrintCard : System.Web.UI.Page
{
    Connection c = new Connection();
    int pid = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            //'" + Convert.ToInt32(ViewState["ipid"]) + "'

            pid = Convert.ToInt32(Request.QueryString["PID"].ToString());
            Session["id"] = pid;
            c.getcon();

            //int pid = 0;
            //ViewState["pid"] = Convert.ToInt32(Request.QueryString["PID"].ToString());


            string pat_disp = "select * from Patient_details where cardno='" + pid + "'";
            SqlCommand cmd_pat_disp = new SqlCommand(pat_disp, c.con);
            SqlDataAdapter da_pat_disp = new SqlDataAdapter(cmd_pat_disp);
            DataTable dt_pat_disp = new DataTable();
            da_pat_disp.Fill(dt_pat_disp);
            if (dt_pat_disp.Rows.Count > 0)
            {
                DataRow row1 = dt_pat_disp.Rows[dt_pat_disp.Rows.Count - 1];

                int id = Convert.ToInt32(row1[0]);
                lbl_disp_cardno.Text = Convert.ToString(pid);
                string name = Convert.ToString(row1[1]);
                lbl_disp_pname.Text = Convert.ToString(name);

                DateTime dob = Convert.ToDateTime(row1[2]);
                int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
                lbl_disp_age.Text = Convert.ToString(age) + "yrs";

                //DateTime today = DateTime.Today;

                //    string dob=Convert.ToString(row1[2]);
                // int age = today.Year-

                //int age_id = Convert.ToInt32(row1[12]);
                string gender = Convert.ToString(row1[3]);
                lbl_disp_gender.Text = Convert.ToString(gender);

                String addr = Convert.ToString(row1[4]);
                lbl_disp_addr.Text = Convert.ToString(addr);
                cmd_pat_disp.ExecuteNonQuery();

            }
        }

    }
    protected void btn_print_Click1(object sender, EventArgs e)
    {
    }


    private void showPdf(string strS)
    {
        Response.ClearContent();
        Response.ClearHeaders();
        Response.ContentType = "application/pdf";
        Response.AddHeader("Content-Dispostion", "attachment;filename=" + strS);
        Response.TransmitFile(strS);
        Response.End();
        Response.Flush();
        Response.Clear();
    }


    protected void btn_print_Click(object sender, EventArgs e)
    {
        c.getcon();
       // pid = Convert.ToInt32(Request.QueryString["PID"].ToString());
        pid = Convert.ToInt32(Session["id"]);

        string pat_disp = "select cardno,patient_name,gender, YEAR(GETDATE())- YEAR(dob) as age,house_name,street from Patient_details where cardno='" + pid + "'";
        SqlCommand cmd_pat_disp = new SqlCommand(pat_disp, c.con);
        SqlDataAdapter da_pat_disp = new SqlDataAdapter(cmd_pat_disp);
        DataTable dt_pat_disp = new DataTable();
        da_pat_disp.Fill(dt_pat_disp);
        cmd_pat_disp.ExecuteNonQuery();
        if (dt_pat_disp.Rows.Count > 0)
        {





            //////////////////////////////////////////////////


            //pdf
            //pdf generation

            //Create a Document object
            var document = new Document(PageSize.A4, 50, 50, 25, 25);

            // Create a new PdfWriter object, specifying the output stream
            var output = new MemoryStream();
            //var writer = PdfWriter.GetInstance(document, output);
            var writer = PdfWriter.GetInstance(document, new FileStream(HttpContext.Current.Server.MapPath("~/TestPrintCard.pdf"), FileMode.Create));
            writer.PageEvent = new ITextEvents();

            //Font set
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font times = new Font(bfTimes, 14, Font.BOLD);


            //Font set
            BaseFont bfTimesText = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font timesText = new Font(bfTimes, 12, Font.NORMAL);

            BaseFont bfTimes1 = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font times1 = new Font(bfTimes1, 14, Font.BOLD);


            // Open the Document for writing
            document.Open();




            //Image 


            string imageurl = Server.MapPath(".") + "/images/gov.png";
            iTextSharp.text.Image png = iTextSharp.text.Image.GetInstance(imageurl);

            png.Alignment = Element.ALIGN_CENTER;
            png.ScaleToFit(140f, 120f);
            png.SpacingBefore = 30f;
            png.SpacingAfter = 1f;
            document.Add(png);



            // Create a new Paragraph object with the text, "Hello, World!"
            var paragph1 = new Paragraph();
            paragph1.Alignment = Element.ALIGN_CENTER;
            paragph1.SetLeading(5.0f, 3.0f);
            //welcomeParagraph.FirstLineIndent = 50f;
            paragph1.SpacingBefore = 1f;
            paragph1.SpacingAfter = 28f;

            Phrase ph0 = new Phrase("HOMOEO MEDICAL COLLEGE GOVT.HOSPITAL,KURICHY,KOTTAYAM", times);
            paragph1.Add(ph0);





            //welcomeParagraph.FirstLineIndent = 50f;
            //var welcomeParagraph = new Paragraph(dtp.Rows[0][2].ToString(), timesText);
            //welcomeParagraph.FirstLineIndent = 50f;
            //welcomeParagrapha.SpacingAfter = 15f;
            // welcomeParagraphp.Font=Element.

            var paragph2 = new Paragraph("                   CARD NO        :           ", times1);
            paragph2.Alignment = Element.ALIGN_LEFT;
            paragph2.SpacingAfter = 15f;

            Phrase ph1 = new Phrase(dt_pat_disp.Rows[0][0].ToString(), timesText);
            paragph2.Add(ph1);

            var paragph3 = new Paragraph("                   NAME              :           ", times1);
            paragph3.Alignment = Element.ALIGN_LEFT;
            paragph3.SpacingAfter = 15f;

            Phrase ph2 = new Phrase(dt_pat_disp.Rows[0][1].ToString(), timesText);
            paragph3.Add(ph2);

            var paragph4 = new Paragraph("                   GENDER          :           ", times1);
            paragph4.Alignment = Element.ALIGN_LEFT;
            paragph4.SpacingAfter = 15f;

            Phrase ph3 = new Phrase(dt_pat_disp.Rows[0][2].ToString(), timesText);
            paragph4.Add(ph3);

            var paragph5 = new Paragraph("                   AGE                  :            ", times1);
            paragph5.Alignment = Element.ALIGN_LEFT;
            paragph5.SpacingAfter = 15f;

            Phrase ph4 = new Phrase(dt_pat_disp.Rows[0][3].ToString(), timesText);
            paragph5.Add(ph4);

            var paragph6 = new Paragraph("                   ADDRESS        :            ", times1);
            paragph6.Alignment = Element.ALIGN_LEFT;
            paragph6.SpacingAfter = 15f;

            Phrase ph5 = new Phrase(dt_pat_disp.Rows[0][4].ToString() + ',' + dt_pat_disp.Rows[0][5].ToString(), timesText);
            paragph6.Add(ph5);



            var paragph_line = new Paragraph();
            paragph_line.Alignment = Element.ALIGN_CENTER;
            paragph_line.SpacingBefore = 0.3f;
            Phrase ph_line = new Phrase("----------------------------------------------------------------------------------------------------------");
            paragph_line.Add(ph_line);

            //Phrase ph11 = new Phrase(dtfvb.Rows[0][11].ToString(), timesText);

            //welcomeParagraphl.Add(ph11);

            // Add the Paragraph object to the document
            document.Add(paragph1);
            document.Add(paragph_line);
            //document.Add(welcomeParagraph);

            document.Add(paragph2);
            // document.Add(welcomeParagraph1);
            document.Add(paragph3);
            //document.Add(welcomeParagraph2);
            document.Add(paragph4);

            document.Add(paragph5);

            document.Add(paragph6);
            document.Add(paragph_line);





            document.Close();

            string path = Server.MapPath("~/TestPrintCard.pdf");

            showPdf(path);


            Response.Redirect(Server.MapPath("~/TestPrintCard.pdf"));

        }
        c.con.Close();
   
    }
}