﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration/RegMaster.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Registration_Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
        <center>

            <table>
            <tr>
                <td >
                </td>
                <td colspan="2">
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3" class="center">
                   SEARCH BY NAME</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td colspan="2">
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_name" runat="server" Text="Enter Name" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_name" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>
                   <asp:Button ID="btn_search" runat="server" OnClick="btn_search_Click" Text="SEARCH" CssClass="buttonyellow" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cardno" ForeColor="#333333" GridLines="None" OnRowCommand="GridView1_RowCommand" Width="625px">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="cardno" HeaderText="Card No." />
                            <asp:BoundField DataField="patient_name" HeaderText="Patient Name" />
                            <asp:BoundField DataField="dob" HeaderText="Date Of Birth" />
                            <asp:BoundField DataField="house_name" HeaderText="Address" />
                           <asp:TemplateField>  
                            <ItemTemplate>  
                                <asp:Button ID="btn_view" runat="server" Width="90" Text="Print Card" CommandName="SelectButton"  CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />  
                            </ItemTemplate> 
                             
                        </asp:TemplateField> </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        </center>

    </form>

</asp:Content>

