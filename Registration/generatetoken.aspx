﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration/RegMaster.master" AutoEventWireup="true" CodeFile="generatetoken.aspx.cs" Inherits="Registration_generatetoken" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            font-size: 13px;
            border-radius: 6px;
            overflow: hidden;
            position: relative;
            left: -2px;
            top: 0px;
            border: 1px solid #ccc;
            padding-left: 14px;
            padding-right: 8px;
            padding-top: 10px;
            padding-bottom: 10px;
            background: #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <form id="Form1" runat="server">

        <div>
            <center>
              <table>
               
                
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td class="left">
                        &nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Card No." CssClass="label_small"></asp:Label>
                    </td>
                    <td class="text-left">
                        <asp:TextBox ID="txt_pid" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btn_search" runat="server" OnClick="btn_search_Click" Text="Search" CssClass="buttonyellow" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td class="left">
                        &nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Patient Name" CssClass="label_small"></asp:Label>
                    </td>
                    <td class="left">
                        <asp:TextBox ID="txt_pname" runat="server" ReadOnly="True" Enabled="False" CssClass="twitterStyleTextbox"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="DOB" CssClass="label_small"></asp:Label>
                    </td>
                    <td class="left">
                        <asp:TextBox ID="txt_dob" runat="server" Enabled="False" ReadOnly="True" CssClass="twitterStyleTextbox"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="Select Doctor" CssClass="label_small"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddl_doc" runat="server" AutoPostBack="True" CssClass="auto-style1">
                            <asp:ListItem Value="0">SELECT</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btn_generate_token" runat="server" OnClick="btn_generate_Click" Text="Generate Token" CssClass="buttonyellow" />
                    </td>
                </tr>
               
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
               
            </table>
                </center>




        </div>

    </form>
</asp:Content>

