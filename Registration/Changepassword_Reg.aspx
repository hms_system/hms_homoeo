﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration/RegMaster.master" AutoEventWireup="true" CodeFile="Changepassword_Reg.aspx.cs" Inherits="Registration_Changepassword_Reg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     
    <form id="form1" runat="server" >
        
        <table>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="label_small">Enter Old Password:</td>
                <td>
                    <asp:TextBox ID="txt_old" runat="server" TextMode="Password" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:Label ID="lbl_old" runat="server" CssClass="label_black" ></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="label_small">&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                <td class="label_small">Enter New Password:</td>
                <td>
                    <asp:TextBox ID="txt_new" runat="server" TextMode="Password" CssClass="twitterStyleTextbox" style="color: #0000FF"></asp:TextBox>
                </td>
                <td>
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_new" ErrorMessage="*Please enter valid password" ForeColor="Red" ValidationExpression="[a-zA-Z0-9 ]{8,}" ValidationGroup="a"></asp:RegularExpressionValidator>

                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="label_small">&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                     &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td class="label_small" >
                    Retype New Password:</td>
                <td>
                    <asp:TextBox ID="txt_retype" runat="server" TextMode="Password" CssClass="twitterStyleTextbox" ></asp:TextBox>
                    <asp:Label ID="lbl_retype" runat="server" CssClass="label_black" ></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
           
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                   &nbsp;
                </td>
                <td>
                    &nbsp;</td>
            </tr>
           
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btn_chg" runat="server" OnClick="btn_chg_Click" Text="Change Password" CssClass="buttonyellow"  ValidationGroup="a" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
           
        </table>
    </form>
</asp:Content>

