﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration/RegMaster.master" AutoEventWireup="true" CodeFile="PrintCard.aspx.cs" Inherits="Registration_PrintCard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <form id="form1" runat="server">
        <div>
            <center>
                <table>
                    <tr>
                        <td>
                            <br />
                            <asp:Image ID="Image3" runat="server" Height="150px" ImageUrl="~/Registration/images/gov.png" Width="200px" />
                        </td>
                        <td class="center">HOMOEOPATHIC MEDICAL COLLEGE GOVT. HOSPITAL<br />
                            KURICHY,KOTTAYAM</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Label ID="lbl_cardno" runat="server" Text="CARD NO." CssClass="label_small"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbl_disp_cardno" runat="server" CssClass="label_small"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Label ID="lbl_pname" runat="server" Text="NAME" CssClass="label_small"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbl_disp_pname" runat="server" CssClass="label_small"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>

                            <asp:Label ID="lbl_pgender" runat="server" Text="GENDER" CssClass="label_small"></asp:Label>

                        </td>
                        <td>

                            <asp:Label ID="lbl_disp_gender" runat="server" CssClass="label_small"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Label ID="lbl_age" runat="server" CssClass="label_small" Text="AGE"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbl_disp_age" runat="server" CssClass="label_small"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Label ID="Label1" runat="server" CssClass="label_small" Text="ADDRESS"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbl_disp_addr" runat="server" CssClass="label_small"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Button ID="btn_print" runat="server" Text="Print" OnClick="btn_print_Click" CssClass="buttonyellow" />
                        </td>
                    </tr>
                </table>
            </center>
        </div>
    </form>
</asp:Content>

