﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration/RegMaster.master" AutoEventWireup="true" CodeFile="OPRegistrationform.aspx.cs" Inherits="Registration_OPRegistrationform" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <form id="form1" runat="server">
     
    <div>
    
       <table>
            <tr>
                <td colspan="4">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="center" colspan="4">NEW PATIENT REGISTRATION</td>
            </tr>
            <tr>
                <td>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td class="label1">
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td >
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="label1">
              Patient Details :</td>
                <td></td>
                <td >
                    </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                   </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
            
                    <asp:Label ID="lbl_patient_name" runat="server" Text="Patient Name" CssClass="label_small"></asp:Label>
              
                </td>
                <td>
                    <asp:TextBox ID="txt_patient_name" runat="server" CssClass="twitterStyleTextbox" style="color: #0000FF"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_patient_name" ErrorMessage="*Please enter valid name" ForeColor="Red" ValidationExpression="[a-zA-Z ]{1,}" ValidationGroup="a"></asp:RegularExpressionValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_patient_name" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
            
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
             
                    <asp:Label ID="lbl_gender" runat="server" Text="Gender" CssClass="label_small"></asp:Label>
                
                </td>
                <td colspan="2">
                    <asp:RadioButtonList ID="rdb_gender" runat="server" AutoPostBack="True" CssClass="label_black" RepeatDirection="Horizontal"  >
                        <asp:ListItem Selected="True">Male</asp:ListItem>
                        <asp:ListItem>Female</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    </td>
                <td class="auto-style2">
             
                    </td>
                <td class="auto-style2">
                    </td>
            </tr>
            <tr>
                <td class="auto-style3">
                    </td>
                <td class="auto-style3">
              
                    <asp:Label ID="lbl_dob" runat="server" Text="Date Of Birth" CssClass="label_small"></asp:Label>
                 
                </td>
                <td class="auto-style3">
                    <asp:TextBox ID="txt_dob" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <cc1:MaskedEditExtender ID="txt_dob_MaskedEditExtender" runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txt_dob" UserDateFormat="DayMonthYear" InputDirection="RightToLeft" ClearTextOnInvalid="True">
                     </cc1:MaskedEditExtender>
                    <br />
                   (DD/MM/YYYY)</td>
                <td class="auto-style3">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_dob" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
              
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
 <tr>
                <td>
                    &nbsp;</td>
                <td>
                
                    <asp:Label ID="lbl_blood_grp" runat="server" Text="Blood Group" CssClass="label_small"></asp:Label>
                
                </td>
                <td>
                    <asp:DropDownList ID="ddl_blood_group" runat="server" AutoPostBack="True" CssClass="search_categories" >
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="1">Not Sure</asp:ListItem>
                        <asp:ListItem Value="2">A+</asp:ListItem>
                        <asp:ListItem Value="3">A-</asp:ListItem>
                        <asp:ListItem Value="4">B+</asp:ListItem>
                        <asp:ListItem Value="5">B-</asp:ListItem>
                        <asp:ListItem Value="6">AB+</asp:ListItem>
                        <asp:ListItem Value="7">AB-</asp:ListItem>
                        <asp:ListItem Value="8">O+</asp:ListItem>
                        <asp:ListItem Value="9">O-</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
 <tr>
                <td>
                    &nbsp;</td>
                <td>
                
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                </td>
                <td >
                   
                    <asp:Label ID="lbl_marital" runat="server" Text="Marital Status" CssClass="label_small"></asp:Label>
                   
                </td>
                <td>
                    <asp:DropDownList ID="ddl_marital" runat="server" AutoPostBack="True" CssClass="search_categories" >
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>Single</asp:ListItem>
                        <asp:ListItem>Married</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td >
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                 
                    <asp:Label ID="lbl_financial" runat="server" Text="Financial Status" CssClass="label_small"></asp:Label>
                
                </td>
                <td>
                    <asp:DropDownList ID="ddl_financial_status" runat="server" AutoPostBack="True" CssClass="search_categories" >
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>BPL</asp:ListItem>
                        <asp:ListItem>APL</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="3">
                
                    <asp:Label ID="lbl_address" runat="server" Text="Contact Details :" CssClass="label1" ></asp:Label>
                  
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td colspan="3">
                
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
               
                    <asp:Label ID="Label4" runat="server" Text="House Name" CssClass="label_small"></asp:Label>
                
                </td>
                <td>
                    <asp:TextBox ID="txt_housename" runat="server" CssClass="twitterStyleTextbox" ></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_housename" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
               
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
             
                    <asp:Label ID="lbl_street" runat="server" Text="Street/Place" CssClass="label_small"></asp:Label>
               
                </td>
                <td>
                    <asp:TextBox ID="txt_street" runat="server" CssClass="twitterStyleTextbox" style="color: #0000FF"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
             
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
            
                    <asp:Label ID="lbl_district" runat="server" Text="District" CssClass="label_small"></asp:Label>
                 
                </td>
                <td>
                    <asp:TextBox ID="txt_district" runat="server" CssClass="twitterStyleTextbox" ></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
            
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
            
                    <asp:Label ID="lbl_state" runat="server" Text="State" CssClass="label_small"></asp:Label>
                 
                </td>
                <td>
                    <asp:TextBox ID="txt_state" runat="server" CssClass="twitterStyleTextbox" ></asp:TextBox>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
            
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
               
                    <asp:Label ID="lbl_country" runat="server" Text="Country" CssClass="label_small"></asp:Label>
              
                </td>
                <td>
                    <asp:TextBox ID="txt_country" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
               
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
            
                    <asp:Label ID="lbl_pin" runat="server" Text="Pin Code" CssClass="label_small"></asp:Label>
                 
                </td>
                <td>
                    <asp:TextBox ID="txt_pin" runat="server"  CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txt_pin" ErrorMessage="*Please enter valid pin" ForeColor="Red" ValidationExpression="[0-9]{6,}" ValidationGroup="a"></asp:RegularExpressionValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txt_pin" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
            
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    </td>
                <td>
             
                    <asp:Label ID="lbl_contact_no" runat="server" Text="Contact No." CssClass="label_small"></asp:Label>
                
                </td>
                <td>
                    <asp:TextBox ID="txt_contact" runat="server" CssClass="twitterStyleTextbox" ></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txt_contact" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                    <br />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txt_contact" ErrorMessage="*Please enter valid phone no." ForeColor="Red" ValidationExpression="[0-9]{10}" ValidationGroup="a" Width="200px"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
             
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
               
                    <asp:Label ID="lbl_guardian" runat="server" Text="Name of guardian" CssClass="label_small"></asp:Label>
                
                </td>
                <td>
                    <asp:TextBox ID="txt_guardian" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txt_guardian" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                </td>
                <td>
                </td>
                <td>
                   </td>
            </tr>
            <tr>
                <td>               
                    </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                   </td>
                <td>          
                    
                    <asp:Button ID="btn_register" runat="server" Text="Register" OnClick="btn_register_Click" CssClass="buttonyellow"  ValidationGroup="a" />
                    
                    
                </td>
                <td>
                    &nbsp;</td>
                           </tr>
            </table>
    </div>
       
    </form>

</asp:Content>

