﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration/RegMaster.master" AutoEventWireup="true" CodeFile="Changeusername_Reg.aspx.cs" Inherits="Registration_Changeusername_Reg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <form id="form1" runat="server">
        
        <table>
            <tr>
                <td></td>
                <td class="center" colspan="2">CHANGE USERNAME</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="lbl_title" runat="server"></asp:Label>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td class="label_small">Enter New UserName</td>
                <td>
                    <asp:TextBox ID="txt_newusername" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_newusername" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td></td>
                <td class="label_small">Enter Password</td>
                <td>
                    <asp:TextBox ID="txt_password" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>
                    <asp:Button ID="btn_changeusername" runat="server" OnClick="btn_changeusername_Click" Text="Change Username" CssClass="buttonyellow" ValidationGroup="a" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td >&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>

</asp:Content>

