﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration/RegMaster.master" AutoEventWireup="true" CodeFile="Report.aspx.cs" Inherits="Registration_Report" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form runat="server">
        <center>

            <table>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" CssClass="label_small" Text="Search By"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddl_searchby" runat="server" AutoPostBack="True" CssClass="search_categories" OnSelectedIndexChanged="ddl_searchby_SelectedIndexChanged">
                    <asp:ListItem Value="0">SELECT</asp:ListItem>
                    <asp:ListItem Value="1">Today</asp:ListItem>
                    <asp:ListItem Value="2">Month</asp:ListItem>
                    <asp:ListItem Value="3">Year</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_ok" runat="server" Text="OK" OnClick="btn_ok_Click" CssClass="buttonyellow" />
            </td>
            <td>&nbsp;</td>
            <td></td>
        </tr>
        
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        
        <tr>
            <td>
                <asp:Label ID="lbl_month" runat="server" CssClass="label_small" Text="Month"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddl_month" runat="server" CssClass="search_categories" >
                    <asp:ListItem Value="0">SELECT</asp:ListItem>
                    <asp:ListItem Value="1">JAN</asp:ListItem>
                    <asp:ListItem Value="2">FEB</asp:ListItem>
                    <asp:ListItem Value="3">MARCH</asp:ListItem>
                    <asp:ListItem Value="4">APRIL</asp:ListItem>
                    <asp:ListItem Value="5">MAY</asp:ListItem>
                    <asp:ListItem Value="6">JUNE</asp:ListItem>
                    <asp:ListItem Value="7">JULY</asp:ListItem>
                    <asp:ListItem Value="8">AUG</asp:ListItem>
                    <asp:ListItem Value="9">SEPT</asp:ListItem>
                    <asp:ListItem Value="10">0CT</asp:ListItem>
                    <asp:ListItem Value="11">NOV</asp:ListItem>
                    <asp:ListItem Value="12">DEC</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;&nbsp;</td>
            <td>
                <asp:Label ID="lbl_year" runat="server" CssClass="label_small" Text="Year"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_year" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btn_month" runat="server" CssClass="buttonyellow" OnClick="btn_month_Click" Text="Search" />
            </td>
        </tr>
        
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_s_year" runat="server" CssClass="label_small" Text="Year"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_search_year" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_year" runat="server" CssClass="buttonyellow" Text="Search" OnClick=" btn_year_Click" />
                </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" CssClass="label_small" Text="Male"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_male" runat="server" CssClass="label_black"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" CssClass="label_small" Text="Female"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_female" runat="server" CssClass="label_black"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2"></td>
            <td class="auto-style2"></td>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style2"></td>
            <td class="auto-style2"></td>
            <td class="auto-style2"></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label7" runat="server" CssClass="label_small" Text="Children"></asp:Label>
            </td>
            <td class="auto-style9">
                <asp:Label ID="lbl_children" runat="server" CssClass="label_black"></asp:Label>
            </td>
            <td class="auto-style9">
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label9" runat="server" CssClass="label_small" Text="Total"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_total" runat="server" CssClass="label_black"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
            <td>
                <asp:Chart ID="Chart1" runat="server" Palette="Bright">
                    <Series>
                        <asp:Series Name="Series1" LabelForeColor="Red">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
        </center>

    </form>
</asp:Content>

