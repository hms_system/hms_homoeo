﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

public partial class Registration_generatetoken : System.Web.UI.Page
{
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            txt_dob.Visible = false;
            txt_pname.Visible = false;
            ddl_doc.Visible = false;
            btn_generate_token.Visible = false;


            Label2.Visible = false;
            Label3.Visible = false;
            Label4.Visible = false;

            try
            {
                c.getcon();
                SqlCommand cmd_empid = new SqlCommand("select e.empname,e.empid from Employee_details  e inner join OP_Allocation op on e.empid=op.empid where op.date=Convert(datetime,'" + DateTime.Today + "',103) and status='Approved' ", c.con);
                SqlDataAdapter da_empid = new SqlDataAdapter(cmd_empid);
                DataTable dt_empid = new DataTable();
                da_empid.Fill(dt_empid);

                int k = cmd_empid.ExecuteNonQuery();

                if (dt_empid.Rows.Count > 0)
                {

                    ddl_doc.DataSource = dt_empid;
                    ddl_doc.DataValueField = "empid";
                    ddl_doc.DataTextField = "empname";
                    ddl_doc.DataBind();
                    ddl_doc.Items.Insert(0, "SELECT");
                }


                c.con.Close();
            }
            catch (Exception exe )
            {
             
            }

        }
    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        c.getcon();
        btn_generate_token.Visible = true;
        Label2.Visible = true;
        Label3.Visible = true;
        Label4.Visible = true;
        txt_dob.Visible = true;
        txt_pname.Visible = true;
        ddl_doc.Visible = true;

        SqlCommand cmd_search = new SqlCommand("select * from Patient_details where cardno= '" + txt_pid.Text + "' ", c.con);
        SqlDataAdapter da_search = new SqlDataAdapter(cmd_search);
        DataTable dt_search = new DataTable();
        da_search.Fill(dt_search);

        int k = cmd_search.ExecuteNonQuery();

        if (dt_search.Rows.Count > 0)
        {
            DataRow row11 = dt_search.Rows[dt_search.Rows.Count - 1];


            // TO DISPLAY THE DATE WITHOUT TIME
            DateTime dtt = Convert.ToDateTime(row11[2]);
            String pdob = dtt.ToShortDateString();
            txt_dob.Text = pdob;

             string pname = Convert.ToString(row11[1]);
             txt_pname.Text = pname;
            

        }
        c.con.Close();


       

    }
    protected void btn_generate_Click(object sender, EventArgs e)
    {
        int count;

        try
        {

            c.getcon();

            SqlCommand cmd_status = new SqlCommand("update Patient_details set status='Pending' where cardno='" + txt_pid.Text + "'", c.con);
            cmd_status.ExecuteNonQuery();

            string str_opid = ddl_doc.SelectedItem.Value;
            SqlCommand cmd_opid = new SqlCommand("select op_id from OP_Allocation where empid='" + ddl_doc.SelectedItem.Value + "' and date=Convert(datetime,'" + DateTime.Today + "',103) ", c.con);
            SqlDataAdapter da_opid = new SqlDataAdapter(cmd_opid);
            DataTable dt_opid = new DataTable();
            da_opid.Fill(dt_opid);

            int k = cmd_opid.ExecuteNonQuery();

            if (dt_opid.Rows.Count > 0)
            {
                DataRow dr_opid = dt_opid.Rows[dt_opid.Rows.Count - 1];
                int opid = Convert.ToInt32(dr_opid[0]);

                //////
                String insert_token = "insert into Schedule values('" + txt_pid.Text + "', Convert(datetime,'" + DateTime.Now + "',103),'" + opid + "','0')";
                SqlCommand cmd_ins_token = new SqlCommand(insert_token, c.con);
                cmd_ins_token.ExecuteNonQuery();


                /////


                SqlCommand cmd_token = new SqlCommand("select count(*) from Schedule where schedule_date= Convert(datetime,'" + DateTime.Today + "',103) and op_id ='" + opid + "'", c.con);
                SqlDataAdapter da_token = new SqlDataAdapter(cmd_token);
                DataTable dt_token = new DataTable();
                da_token.Fill(dt_token);

                int r = cmd_token.ExecuteNonQuery();

                if (dt_token.Rows.Count > 0)
                {
                    DataRow row1 = dt_token.Rows[dt_token.Rows.Count - 1];

                    count = Convert.ToInt32(row1[0]);


                    SqlCommand cmd_gentok = new SqlCommand("  select op.oproom_id,pd.patient_name,pd.gender, YEAR(GETDATE())- YEAR( pd.dob) as age,pd.house_name from OP_Allocation op   inner join Schedule s on s.op_id=op.op_id inner join Patient_details pd on pd.cardno=s.patient_id where pd.cardno='" + txt_pid.Text + "' and op.date= Convert(datetime,'" + DateTime.Today + "',103)", c.con);
                    SqlDataAdapter da_gentok = new SqlDataAdapter(cmd_gentok);
                    DataTable dt_gentok = new DataTable();
                    da_gentok.Fill(dt_gentok);
                    cmd_gentok.ExecuteNonQuery();
                    if (dt_gentok.Rows.Count > 0)
                    {
                        DataRow row_gentok = dt_gentok.Rows[dt_gentok.Rows.Count - 1];


                        //////////////////////////////////////////////////


                        //pdf
                        //pdf generation

                        //Create a Document object
                        var document = new Document(PageSize.A4, 50, 50, 25, 25);

                        // Create a new PdfWriter object, specifying the output stream
                        var output = new MemoryStream();
                        //var writer = PdfWriter.GetInstance(document, output);
                        var writer = PdfWriter.GetInstance(document, new FileStream(HttpContext.Current.Server.MapPath("~/Test.pdf"), FileMode.Create));
                        writer.PageEvent = new ITextEvents();

                        //Font set
                        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                        Font times = new Font(bfTimes, 14, Font.BOLD);


                        //Font set
                        BaseFont bfTimesText = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                        Font timesText = new Font(bfTimes, 12, Font.NORMAL);

                        BaseFont bfTimes1 = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                        Font times1 = new Font(bfTimes1, 14, Font.BOLD);


                        // Open the Document for writing
                        document.Open();




                        //Image 


                        string imageurl = Server.MapPath(".") + "/images/gov.png";
                        iTextSharp.text.Image png = iTextSharp.text.Image.GetInstance(imageurl);

                        png.Alignment = Element.ALIGN_CENTER;
                        png.ScaleToFit(140f, 120f);
                        png.SpacingBefore = 30f;
                        png.SpacingAfter = 1f;
                        document.Add(png);



                        // Create a new Paragraph object with the text, "Hello, World!"
                        var welcomeParagrapha = new Paragraph();
                        welcomeParagrapha.Alignment = Element.ALIGN_LEFT;
                        welcomeParagrapha.SetLeading(5.0f, 3.0f);
                        //welcomeParagraph.FirstLineIndent = 50f;
                        welcomeParagrapha.SpacingBefore = 1f;
                        welcomeParagrapha.SpacingAfter = 28f;

                        Phrase ph0 = new Phrase("HOMOEO MEDICAL COLLEGE GOVT.HOSPITAL,KURICHY,KOTTAYAM", times);
                        welcomeParagrapha.Add(ph0);




                        DateTime t_date = DateTime.Now.Date;
                        String today = t_date.ToShortDateString();
                      


                        var date1 = new Paragraph();
                        date1.Alignment = Element.ALIGN_RIGHT;
                        date1.SpacingAfter = 15f;
                        Phrase phdate = new Phrase(today);
                        date1.Add(phdate);
                        //welcomeParagraph.FirstLineIndent = 50f;
                        //var welcomeParagraph = new Paragraph(dtp.Rows[0][2].ToString(), timesText);
                        //welcomeParagraph.FirstLineIndent = 50f;
                        //welcomeParagrapha.SpacingAfter = 15f;
                        // welcomeParagraphp.Font=Element.

                        var welcomeParagraphb = new Paragraph("                     Token No                :                               ", times1);
                        welcomeParagraphb.Alignment = Element.ALIGN_LEFT;
                        welcomeParagraphb.SpacingAfter = 15f;

                        Phrase ph1 = new Phrase(Convert.ToString(count + 1), timesText);
                        welcomeParagraphb.Add(ph1);

                        var welcomeParagraphc = new Paragraph("                     OP Room                 :                               ", times1);
                        welcomeParagraphc.Alignment = Element.ALIGN_LEFT;
                        welcomeParagraphc.SpacingAfter = 15f;

                        Phrase ph2 = new Phrase(dt_gentok.Rows[0][0].ToString(), timesText);
                        welcomeParagraphc.Add(ph2);

                        var welcomeParagraphd = new Paragraph("                     Patient Name            :                               ", times1);
                        welcomeParagraphd.Alignment = Element.ALIGN_LEFT;
                        welcomeParagraphd.SpacingAfter = 15f;

                        Phrase ph3 = new Phrase(dt_gentok.Rows[0][1].ToString(), timesText);
                        welcomeParagraphd.Add(ph3);

                        var welcomeParagraphe = new Paragraph("                     Age                            :                               ", times1);
                        welcomeParagraphe.Alignment = Element.ALIGN_LEFT;
                        welcomeParagraphe.SpacingAfter = 15f;

                        Phrase ph4 = new Phrase(dt_gentok.Rows[0][3].ToString(), timesText);
                        welcomeParagraphe.Add(ph4);

                        var welcomeParagraphf = new Paragraph("                     Gender                       :                               ", times1);
                        welcomeParagraphf.Alignment = Element.ALIGN_LEFT;
                        welcomeParagraphf.SpacingAfter = 15f;

                        Phrase ph5 = new Phrase(dt_gentok.Rows[0][2].ToString(), timesText);
                        welcomeParagraphf.Add(ph5);

                        var welcomeParagraphg = new Paragraph("                     House Name              :                               ", times1);
                        welcomeParagraphg.Alignment = Element.ALIGN_LEFT;
                        welcomeParagraphg.SpacingAfter = 15f;

                        Phrase ph6 = new Phrase(dt_gentok.Rows[0][4].ToString(), timesText);
                        welcomeParagraphg.Add(ph6);



                        var welcomeParagraphh = new Paragraph();
                        welcomeParagraphh.Alignment = Element.ALIGN_LEFT;
                        welcomeParagraphh.SpacingAfter = 15f;

                        Phrase ph7 = new Phrase("----------------------------------------------------------------------------------------------------------------------");
                        welcomeParagraphh.Add(ph7);


                        //Phrase ph11 = new Phrase(dtfvb.Rows[0][11].ToString(), timesText);

                        //welcomeParagraphl.Add(ph11);

                        // Add the Paragraph object to the document

                        document.Add(welcomeParagrapha);
                        
                        document.Add(welcomeParagraphh);
                        document.Add(date1);
                     
                        document.Add(welcomeParagraphb);
                        // document.Add(welcomeParagraph1);
                        document.Add(welcomeParagraphc);
                        document.Add(welcomeParagraphh);
                        
                        document.Add(welcomeParagraphd);
                        document.Add(welcomeParagraphe);

                        document.Add(welcomeParagraphf);

                        document.Add(welcomeParagraphg);



                        document.Close();

                        string path = Server.MapPath("~/Test.pdf");

                        showPdf(path);


                        Response.Redirect(Server.MapPath("~/Test.pdf"));


                    }
                }

            }

            c.con.Close();
        }
        catch (Exception exe )
        {
        }
       
    }

    private void showPdf(string strS)
    {
        Response.ClearContent();
        Response.ClearHeaders();
        Response.ContentType = "application/pdf";
        Response.AddHeader("Content-Dispostion", "attachment;filename=" + strS);
        Response.TransmitFile(strS);
        Response.End();
        Response.Flush();
        Response.Clear();
    }

}
