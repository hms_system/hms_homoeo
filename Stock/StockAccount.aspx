﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Stock/StockMaster.master" AutoEventWireup="true" CodeFile="StockAccount.aspx.cs" Inherits="Stock_StockAccount" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 4px;
        }
        .auto-style2 {
            height: 20px;
        }
        .auto-style3 {
            font-weight: bold;
            text-align: center;
            color: darkorchid;
            font-size: medium;
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form2" runat="server">
        <table class="auto-style32">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="auto-style1">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="Button5" runat="server" CssClass="btn-info" Text="Stock Entry" />
                    <asp:Button ID="Button6" runat="server" CssClass="btn-info" Text="Loan" />
                    <asp:Button ID="Button10" runat="server" CssClass="btn-info" Text="Add New Medicine" />
                </td>
                <td class="auto-style1">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="auto-style1">&nbsp;</td>
            </tr>
           
            <tr>
                <td>&nbsp;</td>
                
                <td>
                    <asp:MultiView ID="MultiView1" runat="server">
                        <asp:View ID="View1" runat="server">
                            <table class="auto-style2">
                                <tr>
                                    <td class="auto-style2"></td>
                                    <td class="auto-style2" colspan="5"><strong>STOCK ACCOUNT</strong></td>
                                </tr>
                                <tr>
                                    <td class="auto-style8"></td>
                                    <td class="auto-style5">&nbsp;</td>
                                    <td class="auto-style5">&nbsp;</td>
                                    <td class="auto-style5">&nbsp;</td>
                                    <td class="auto-style5">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style8">
                                        <asp:Label ID="lbl_search_med" runat="server" Text="Search by Medicine Name" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style5">
                                        <asp:DropDownList ID="ddl_sel_medicine" runat="server" CssClass="search_categories">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="auto-style5">
                                        <asp:Label ID="lbl_search_id" runat="server" Text="Search by Medicine ID" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style5">
                                        <asp:DropDownList ID="ddl_id0" runat="server" OnSelectedIndexChanged="ddl_id_SelectedIndexChanged" CssClass="search_categories">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="auto-style5">
                                        <asp:Button ID="btn_search0" runat="server" Text="Search" OnClick="btn_search0_Click" CssClass="buttonyellow" />
                                    </td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style8">
                                        &nbsp;</td>
                                    <td class="auto-style5">
                                        &nbsp;</td>
                                    <td class="auto-style5">&nbsp;</td>
                                    <td class="auto-style5">&nbsp;</td>
                                    <td class="auto-style5">&nbsp;</td>
                                    <td class="auto-style13">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style9">
                                        &nbsp;</td>
                                    <td class="auto-style6">
                                        &nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style14">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style9"></td>
                                    <td class="auto-style6">
                                        &nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style14">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style9"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style14"></td>
                                </tr>
                            </table>
                        </asp:View>
                        <br />
                        <asp:View ID="View2" runat="server">
                            <table class="auto-style16">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl_id" runat="server" CssClass="label_small" Text="ID"></asp:Label>
                                    </td>
                                    <td class="auto-style6">
                                        <asp:Label ID="lbl_disp_id" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl_med_name" runat="server"  Text="Name of Medicine" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_disp_med" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_med_code" runat="server"  Text="Code" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_disp_code" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lbtn_rate_stock" runat="server" CssClass="btn-info">Rate &amp; Stock Information</asp:LinkButton>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        <asp:LinkButton ID="lbtn_stockentry0" runat="server" CssClass="btn-info">Stock Entry</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbt_med_recived" runat="server" CssClass="btn-info">Recieved Medicine Information(yearly)</asp:LinkButton>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        <asp:LinkButton ID="lbn_grang_total0" runat="server" CssClass="btn-info">Grand Total</asp:LinkButton>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                        <br />
                        <asp:View ID="View3" runat="server">
                            <table class="auto-style16">
                                <tr>
                                    <td class="center" colspan="6"><strong>RATE &amp; STOCK INFORMATION</strong></td>
                                    <td class="auto-style17">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style29">
                                        <asp:Label ID="lbl_rate" runat="server" Text="Rate" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox1" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                    <td class="auto-style30">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td class="auto-style33">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td class="auto-style17">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style26"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style28"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style34"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style23"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style29">
                                        <asp:Label ID="lbl_stock" runat="server" Text="Stock" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox4" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                    <td class="auto-style30">
                                        <asp:Label ID="Label5" runat="server" Text="Stock Cost" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox5" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                    <td class="auto-style33">
                                        <asp:Label ID="Label12" runat="server" Text="Package Quantity" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox14" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                    <td class="auto-style17">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style29">
                                        <asp:Label ID="Label10" runat="server" Text="Medicine Issued" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox12" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                    <td class="auto-style30">
                                        <asp:Label ID="Label11" runat="server" Text="Cost of Issued Medicine" CssClass="label_small"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                                    <td>
                                        <asp:TextBox ID="TextBox13" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                    <td class="auto-style33">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                    <td>
                                        <asp:Button ID="Button1" runat="server" Text="Save" Width="125px" CssClass="buttonyellow" />
                                    </td>
                                    <td class="auto-style17">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="label_small">Damaged Medicines Information</asp:LinkButton>
                                    </td>
                                    <td class="auto-style30">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td class="auto-style33">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td class="auto-style17">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style29">
                                        <asp:Label ID="Label13" runat="server" Text="Quantity" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox15" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                    <td class="auto-style30">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td class="auto-style33">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td class="auto-style17">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style29">
                                        <asp:Label ID="Label16" runat="server" Text="Rate" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox18" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                    <td class="auto-style30">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td class="auto-style33">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td class="auto-style17">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style26">
                                        <asp:Label ID="Label17" runat="server" Text="Cost" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style6">
                                        <asp:TextBox ID="TextBox17" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                    <td class="auto-style28"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style34"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style23"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style26">&nbsp;</td>
                                    <td class="auto-style6">
                                        &nbsp;</td>
                                    <td class="auto-style28">&nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style34">&nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style23">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style26">&nbsp;</td>
                                    <td class="auto-style6">
                                        <asp:Button ID="Button9" runat="server" CssClass="buttonyellow" Text="Save" />
                                    </td>
                                    <td class="auto-style28">&nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style34">
                                        <asp:Button ID="Button3" runat="server" Text="&lt;&lt;Back" CssClass="buttonyellow" />
                                    </td>
                                    <td class="auto-style6">
                                        <asp:Button ID="Button4" runat="server" Text="Next&gt;&gt;" CssClass="buttonyellow" />
                                    </td>
                                    <td class="auto-style23">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style26">&nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style28">&nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style34">&nbsp;</td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style23">&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="View4" runat="server">
                            <table class="auto-style16">
                                <tr>
                                    <td class="center" colspan="5"><strong>STOCK ENTRY</strong></td>
                                    <td class="auto-style6"></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>(medicine recieved during current year)</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style5">
                                        <asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack="True" CssClass="search_categories">
                                            <asp:ListItem>Stock1_Entry</asp:ListItem>
                                            <asp:ListItem>Stock2_Entry</asp:ListItem>
                                            <asp:ListItem>Stock3_Entry</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="auto-style5">
                                        <asp:TextBox ID="TextBox19" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                    <td class="auto-style5">
                                        <asp:DropDownList ID="DropDownList4" runat="server" AutoPostBack="True" CssClass="search_categories">
                                            <asp:ListItem>Stock1_Rate</asp:ListItem>
                                            <asp:ListItem>Stock2_Rate</asp:ListItem>
                                            <asp:ListItem>Stock3_Rate</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="auto-style5">
                                        <asp:TextBox ID="TextBox20" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                    <td class="auto-style5">
                                        <asp:Button ID="btn_stockadd" runat="server" Text="Add" CssClass="buttonyellow" />
                                    </td>
                                    <td class="auto-style5"></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                        <br />
                        <asp:View ID="View5" runat="server">
                            <table class="auto-style16">
                                <tr>
                                    <td class="center" colspan="4"><strong>Yearly&nbsp; Stock&nbsp; Information</strong></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style35">
                                        <asp:Label ID="Label18" runat="server" Text="Year" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style35">
                                        <asp:TextBox ID="TextBox21" runat="server" ></asp:TextBox>
                                        <cc1:CalendarExtender ID="TextBox21_CalendarExtender" runat="server" Format="yyyy" Enabled="True" TargetControlID="TextBox21"></cc1:CalendarExtender>
                                    </td>
                                    <td class="auto-style35"></td>
                                    <td class="auto-style35"></td>
                                    <td class="auto-style35">
                                        <asp:ScriptManager ID="ScriptManager1" runat="server">
                                        </asp:ScriptManager>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style6">
                                        <asp:Label ID="Label19" runat="server" Text="Stock" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style6">
                                        <asp:Label ID="lbl_disp_stock" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label20" runat="server" Text="Stock Cost" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_disp_stockcost" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label21" runat="server" Text="Issue" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_issue" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label25" runat="server" Text="Issued  Cost" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_issued_cost" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label26" runat="server" Text="Packing Quantity" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_packing_quantity" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                        <br />
                        <asp:View ID="View6" runat="server">
                            <table class="auto-style16">
                                <tr>
                                    <td class="center" colspan="4"><strong>Grant Total</strong></td>
                                    <td class="auto-style6">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style6">
                                        <asp:Label ID="lbl_pgno0" runat="server" Text="Page No." CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style6">
                                        <asp:Label ID="lbl_disp_pgno" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style6">&nbsp;</td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl_gt_med" runat="server" Text="Total Medicines Received" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_disp_gt_med" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_gt_cost" runat="server" Text="Cost" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_disp_gt_cost" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style6">
                                        <asp:Label ID="lbl_gt_issued" runat="server" Text="Total Medicines Issued" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style6">
                                        <asp:Label ID="lbl_gt_disp_issued" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style6">
                                        <asp:Label ID="lbl_gt_issuedcost" runat="server" Text="Cost of issued" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style6">
                                        <asp:Label ID="lbl_disp_gt_issued_cost" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td class="auto-style6">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl_gt_bal" runat="server" Text="Balance Medicines" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_disp_gt_balmed" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_gt_bal_amt" runat="server" Text="Balance Amount" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl_gt_bal_cost" runat="server" CssClass="label_small"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                    <td class="auto-style6"></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                        <br />
                        <asp:View ID="View7" runat="server">
                            <table class="nav-justified">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="Button7" runat="server" CssClass="btn-info" Text="Loan Received" />
                                        <asp:Button ID="Button8" runat="server" CssClass="btn-info" Text="Loan Issued" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="View8" runat="server">
                               <table class="auto-style2">
            <tr>
                <td>&nbsp;</td>
                <td class="center" colspan="4"><strong>MEDICINE RECIEVED AS LOAN</strong></td>
            </tr>
            <tr>
                <td class="auto-style4"></td>
                <td class="auto-style4">
                    <asp:Label ID="Label1" runat="server" CssClass="label_small" Text="Name of Medicine"></asp:Label>
                </td>
                <td class="auto-style4">
                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" CssClass="search_categories">
                    </asp:DropDownList>
                </td>
                <td class="auto-style4"></td>
                <td class="auto-style4">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4"></td>
                <td class="auto-style4">
                    <asp:Label ID="lbl_rcvd_frm" runat="server" Text="Received From" CssClass="label_small"></asp:Label>
                </td>
                <td class="auto-style4">
                    <asp:TextBox ID="TextBox2" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td class="auto-style4"></td>
                <td class="auto-style4"></td>
            </tr>
            <tr>
                <td class="auto-style6"></td>
                <td class="auto-style6">
                    <asp:Label ID="Label2" runat="server" Text="Recevied Medicine" CssClass="label_small"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:TextBox ID="TextBox3" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td class="auto-style6"></td>
                <td class="auto-style6"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Rate" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox6" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style6"></td>
                <td class="auto-style6">
                    <asp:Label ID="Label3" runat="server" Text="Cost" CssClass="label_small"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:TextBox ID="TextBox7" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td class="auto-style6"></td>
                <td class="auto-style6">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="lbl__gt_medamt" runat="server" Text="Total Medicines Received" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_disp_gt_medamt" runat="server" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Total Cost" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label7" runat="server" CssClass="label_small"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
                        </asp:View>
                        <asp:View ID="View9" runat="server">
                             <table class="auto-style2">
            <tr>
                <td>&nbsp;</td>
                <td class="center" colspan="4"><strong>&nbsp;MEDICINE ISSUED AS LOAN</strong></td>
            </tr>
            <tr>
                <td class="auto-style4"></td>
                <td class="auto-style4">
                    <asp:Label ID="Label8" runat="server" CssClass="label_small" Text="Name of Medicine"></asp:Label>
                </td>
                <td class="auto-style4">
                    <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" CssClass="search_categories">
                    </asp:DropDownList>
                </td>
                <td class="auto-style4"></td>
                <td class="auto-style4">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4"></td>
                <td class="auto-style4">
                    <asp:Label ID="Label9" runat="server" Text="Received By" CssClass="label_small"></asp:Label>
                </td>
                <td class="auto-style4">
                    <asp:TextBox ID="TextBox8" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td class="auto-style4"></td>
                <td class="auto-style4"></td>
            </tr>
            <tr>
                <td class="auto-style6"></td>
                <td class="auto-style6">
                    <asp:Label ID="Label14" runat="server" Text="Issued Medicine(qty)" CssClass="label_small"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:TextBox ID="TextBox9" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td class="auto-style6"></td>
                <td class="auto-style6"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="Label15" runat="server" Text="Rate" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox10" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style6"></td>
                <td class="auto-style6">
                    <asp:Label ID="Label22" runat="server" Text="Cost" CssClass="label_small"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:TextBox ID="TextBox11" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td class="auto-style6"></td>
                <td class="auto-style6">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="Label23" runat="server" Text="Total Medicines Received" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label24" runat="server" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label27" runat="server" Text="Total Cost" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label28" runat="server" CssClass="label_small"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
                        </asp:View>
                        <asp:View ID="View10" runat="server">
                            <table class="nav-justified">
                                <tr>
                                    <td class="center">&nbsp;</td>
                                    <td class="center">ADD NEW MEDICINE</td>
                                    <td class="center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbl_newmed" runat="server" CssClass="label_small" Text="Name of Medicine"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox22" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style2"></td>
                                    <td class="auto-style2">
                                        <asp:Label ID="lbl_medcode" runat="server" CssClass="label_small" Text="Code"></asp:Label>
                                    </td>
                                    <td class="auto-style2">
                                        <asp:TextBox ID="TextBox23" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Label ID="lbl_pgno" runat="server" CssClass="label_small" Text="Page No."></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox24" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                    </asp:MultiView>
                </td>
                <td class="auto-style1">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="auto-style1">&nbsp;</td>
            </tr>
        </table>
</form>
</asp:Content>

