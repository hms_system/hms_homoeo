﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

//Functions used  to register a new employee 

public partial class EmployeeRegistration : System.Web.UI.Page
{
    int chk=1;
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
        if (!IsPostBack)
      
          {
            
              try
              {
                  c.getcon();
                  string str_job = "select * from Job_type";
                  SqlCommand cmd_job = new SqlCommand(str_job,c.con );
                  SqlDataAdapter da_job = new SqlDataAdapter(cmd_job);
                  DataTable dt_job = new DataTable();
                  da_job.Fill(dt_job);
                  if (dt_job.Rows.Count > 0)
                  {
                      ddl_job_type.DataSource = dt_job;
                      ddl_job_type.DataValueField = "typeid";
                      ddl_job_type.DataTextField = "job";
                      ddl_job_type.DataBind();
                      ddl_job_type.Items.Insert(0, "SELECT");



                  }
                  string str_desig = "select * from Designation";
                  SqlCommand cmd_desig = new SqlCommand(str_desig, c.con);
                  SqlDataAdapter da_desig = new SqlDataAdapter(cmd_desig);
                  DataTable dt_desig = new DataTable();
                  da_desig.Fill(dt_desig);
                  if (dt_desig.Rows.Count > 0)
                  {
                      ddl_designation.DataSource = dt_desig;
                      ddl_designation.DataValueField = "desig_id";
                      ddl_designation.DataTextField = "designation";
                      ddl_designation.DataBind();
                      ddl_designation.Items.Insert(0, "SELECT");

                  }
                  c.con.Close();
                 }
             catch(Exception exe )
              {

              }
        }
    }
   

    protected void btn_register_Click(object sender, EventArgs e)
    {
       SqlTransaction trans;
       c.getcon();
       trans = c.con.BeginTransaction();
        
        try
        {
            
            if (ddl_job_type.SelectedIndex > 0)
            {
                lbl_jobtype_error.Visible = false;
               
                if (chk == 0)
                {
                    string emp_insert = " insert into Employee_details values('" + txt_name.Text + "','" + txt_dob.Text + "','" + rbl_gender.SelectedItem.Text + "','" + txt_spouse_name.Text + "','" + chk + "','" + txt_mob_no.Text + "','" + txt_email.Text + "',Convert(datetime,'" + DateTime.Today + "',103),'" + txt_experience.Text + "','" + ddl_job_type.SelectedItem.Value + "','" + ddl_designation.SelectedItem.Value + "','Pending','','" + txt_qualifctn.Text + "','" + txt_spclzn.Text + "')select @@Identity";
                    SqlCommand cmd = new SqlCommand(emp_insert, c.con);
                   cmd.Transaction = trans;
                    object obj = new object();
                    obj = cmd.ExecuteScalar();

                    int empid = Convert.ToInt32(obj);
                    string address_insert = "insert into Permanent_address values('" + txt_per_hname.Text + "','" + txt_per_street.Text + "','" + txt_per_district.Text + "','" + txt_per_state.Text + "','" + txt_per_country.Text + "','" + txt_per_pin.Text + "','" + empid + "')";
                    SqlCommand cmd1 = new SqlCommand(address_insert, c.con);
                  cmd1.Transaction = trans;
                    cmd1.ExecuteNonQuery();

                }
                else
                {

                    string emp_insert = " insert into Employee_details values('" + txt_name.Text + "','" + txt_dob.Text + "','" + rbl_gender.SelectedItem.Text + "','" + txt_spouse_name.Text + "','" + chk + "','" + txt_mob_no.Text + "','" + txt_email.Text + "',Convert(datetime,'" + DateTime.Today + "',103) ,'" + txt_experience.Text + "','" + ddl_job_type.SelectedItem.Value + "','" + ddl_designation.SelectedItem.Value + "','pending','','" + txt_qualifctn.Text + "','" + txt_spclzn.Text + "')select @@Identity";
                    SqlCommand cmd = new SqlCommand(emp_insert, c.con);
                    cmd.Transaction = trans;
                    object obj = new object();
                    obj = cmd.ExecuteScalar();

                    int empid = Convert.ToInt32(obj);
                    string address_insert = "insert into Permanent_address values('" + txt_per_hname.Text + "','" + txt_per_street.Text + "','" + txt_per_district.Text + "','" + txt_per_state.Text + "','" + txt_per_country.Text + "','" + txt_per_pin.Text + "','" + empid + "')";
                    SqlCommand cmd1 = new SqlCommand(address_insert, c.con);
                    cmd1.Transaction = trans;
                    cmd1.ExecuteNonQuery();

                    string address_insert1 = "insert into Current_address values('" + txt_housename.Text + "','" + txt_street.Text + "','" + txt_per_district.Text + "','" + txt_state.Text + "','" + txt_country.Text + "','" + txt_pin.Text + "','" + empid + "')";
                    SqlCommand cmd2 = new SqlCommand(address_insert1, c.con);
                    cmd2.Transaction = trans;
                    cmd2.ExecuteNonQuery();
                }
               trans.Commit();
                c.con.Close();
                Response.Write("<script>alert('Registration Completed Successfully');</script>");

            }
            else
            {
                lbl_jobtype_error.Visible = true;
            }
    }
    
        catch(Exception exe)
        {
            trans.Rollback();
         
        }

          txt_name.Text = "";
            txt_email.Text = "";
            txt_country.Text = "";
            txt_district.Text = "";
            txt_dob.Text = "";
            txt_experience.Text = "";
            txt_housename.Text = "";
            txt_mob_no.Text = "";
            txt_per_country.Text = "";
            txt_per_district.Text = "";
            txt_per_hname.Text = "";
            txt_per_pin.Text = "";
            txt_per_state.Text = "";
            txt_per_street.Text = "";
            txt_pin.Text = "";
            txt_spouse_name.Text = "";
            txt_state.Text = "";
            txt_street.Text = "";
            CheckBoxList1.ClearSelection();
        

       
    }

    protected void CheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (CheckBoxList1.SelectedItem.Text == "Check if same as current address")
        {
            txt_per_hname.Text = txt_housename.Text;
            txt_per_street.Text = txt_street.Text;
            txt_per_district.Text = txt_district.Text;
            txt_per_state.Text = txt_state.Text;
            txt_per_country.Text = txt_country.Text;
            txt_per_pin.Text = txt_pin.Text;
          chk=0;

        }

    }
    

    protected void btn_back_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Home.aspx");
    }

    }