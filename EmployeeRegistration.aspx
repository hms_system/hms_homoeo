﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true" CodeFile="EmployeeRegistration.aspx.cs" Inherits="EmployeeRegistration"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
    
<link rel="shortcut icon" type="image/icon" href="images/favicon.ico"/>
<link href="css/bootstrap.min.css" rel="stylesheet">
 <link href="css/font-awesome.min.css" rel="stylesheet">
 <link id="switcher" href="css/themes/default-theme.css" rel="stylesheet">
<link href="css/slick.css" rel="stylesheet">
<link rel='stylesheet prefetch' href='css/photoswipe.css'>
 <link rel='stylesheet prefetch' href='css/default-skin.css'>    
<link href="style.css" rel="stylesheet">
    <link href="css/themes/default-theme.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>  
    <link href='http://fonts.googleapis.com/css?family=Habibi' rel='stylesheet' type='text/css'>   
    <link href='http://fonts.googleapis.com/css?family=Cinzel+Decorative:900' rel='stylesheet' type='text/css'>

    <style type="text/css">
        .auto-style1 {
            height: 40px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <form id="form1" runat="server">
      
         <table>
             <tr>
                <td>
                    &nbsp;</td>


                <td>&nbsp;</td>


                <td>&nbsp;</td>


            </tr>


            <tr>
                <td></td>


                <td class="center" colspan="5">EMPLOYEE REGISTRATION</td>


            </tr>


            <tr>
                <td>&nbsp;</td>


                <td class="labeltext">Personal Details:</td>


                <td></td>
                <td></td>

                <td>&nbsp;</td>

                <td>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>

            </tr>

            <tr>
                <td></td>
                <td>
                    <asp:Label ID="lbl_name" runat="server" Text="Name" CssClass="label_small"></asp:Label>

                </td>
                <td colspan="4">

                    <asp:TextBox ID="txt_name" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>

                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_name" ErrorMessage="*Name Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                    <br />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txt_name" ErrorMessage="*Please enter valid name" ForeColor="Red" ValidationExpression="[a-zA-Z ]{1,}"></asp:RegularExpressionValidator>

                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>

                    &nbsp;</td>
                <td colspan="3" >
                    &nbsp;</td>
            </tr>
            <tr>
                <td></td>

                <td>
                    <asp:Label ID="lbl_dob" runat="server" Text="Date of birth" CssClass="label_small"></asp:Label>
                </td>

                <td colspan="4">
                    <asp:TextBox ID="txt_dob" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <cc1:calendarextender ID="txt_dob_CalendarExtender" runat="server" TargetControlID="txt_dob">
                    </cc1:calendarextender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txt_dob" ErrorMessage="*Please enter date of birth" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    &nbsp;</td>

                <td>
                    &nbsp;</td>
                <td colspan="3">
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <td>
                    <asp:Label ID="lbl_gender" runat="server" Text="Gender" CssClass="label_small"></asp:Label>

                <td>
                    <asp:RadioButtonList ID="rbl_gender" runat="server" AutoPostBack="True" Width="187px" CssClass="label_small" RepeatDirection="Horizontal" ForeColor="#0000CC">
                        <asp:ListItem Value="0" Selected="True">Male</asp:ListItem>
                        <asp:ListItem Value="1">Female</asp:ListItem>
                    </asp:RadioButtonList>


                </td>
                <td></td>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>
                    &nbsp;<td>
                    &nbsp;<td>
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>

                <td>
                    <asp:Label ID="lbl_spouse_name" runat="server" Text="Spouse Name" CssClass="label_small"></asp:Label>
                </td>

                <td>
                    <asp:TextBox ID="txt_spouse_name" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td></td>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    &nbsp;</td>

                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td class="labeltext">Contact Details:</td>

                <td>
                    <br />
                </td>

                <td></td>

                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>

                <td>&nbsp;</td>

                <td></td>

            </tr>
            <tr>
                <td>&nbsp;</td>

                <td class="labeltext">Current Address:</td>

                <td>
                    <br />
                </td>
                <td></td>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td></td>

                <td>
                    <asp:Label ID="lbl_house_name" runat="server" Text="House Name" CssClass="label_small"></asp:Label>
                </td>

                <td>
                    <asp:TextBox ID="txt_housename" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                  </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_housename" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    &nbsp;</td>

                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>

                <td>
                    <asp:Label ID="lbl_street" runat="server" Text="Street/Place" CssClass="label_small"></asp:Label>
                </td>

                <td>

                    <asp:TextBox ID="txt_street" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_street" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    &nbsp;</td>

                <td>

                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>

                <td>
                    <asp:Label ID="lbl_district" runat="server" Text="District" CssClass="label_small"></asp:Label>
                </td>

                <td>
                    <asp:TextBox ID="txt_district" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                   
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txt_district" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
                <td></td>

            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    &nbsp;</td>

                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>

            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="lbl_state" runat="server" Text="State" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_state" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
          
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txt_state" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>

                <td>
                    <asp:Label ID="lbl_country" runat="server" Text="Country" CssClass="label_small"></asp:Label>
                </td>

                <td>
                    <asp:TextBox ID="txt_country" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>

                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txt_country" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td class="auto-style1"></td>

                <td class="auto-style1">
                </td>

                <td class="auto-style1">

                </td>
                <td class="auto-style1">
                </td>
                <td class="auto-style1">
                    </td>
                <td class="auto-style1"></td>
            </tr>
            <tr>
                <td></td>

                <td>
                    <asp:Label ID="lbl_pin" runat="server" Text="Pin Code" CssClass="label_small"></asp:Label>
                </td>

                <td colspan="4">
                    <asp:TextBox ID="txt_pin" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_pin" ErrorMessage="*Please enter valid pin" ForeColor="Red" ValidationExpression="[0-9]{6,}" ValidationGroup="a"></asp:RegularExpressionValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txt_pin" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                      </td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    &nbsp;</td>

                <td>
                    &nbsp;</td>
                <td colspan="3">
                    &nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td></td>

                <td></td>

                <td>
                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="CheckBoxList1_SelectedIndexChanged" ForeColor="#009900" CssClass="checkbox">
                        <asp:ListItem>Check if same as current address</asp:ListItem>
                    </asp:CheckBoxList>
                     </td>
                <td></td>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td></td>

                <td class="labeltext" colspan="5">Permanent Address:<br />
                </td>

            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    <asp:Label ID="lbl_per_hname" runat="server" Text="House Name" CssClass="label_small"></asp:Label>
                </td>

                <td>
                    <asp:TextBox ID="txt_per_hname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                 
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txt_per_hname" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    &nbsp;</td>

                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    <asp:Label ID="lbl_per_street" runat="server" Text="Street/Place" CssClass="label_small"></asp:Label>
                </td>

                <td>
                    <asp:TextBox ID="txt_per_street" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                 
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txt_per_street" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    &nbsp;</td>

                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    <asp:Label ID="lbl_per_district" runat="server" Text="District" CssClass="label_small"></asp:Label>
                </td>

                <td>
                    <asp:TextBox ID="txt_per_district" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
         
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txt_per_district" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    &nbsp;</td>

                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="lbl_per_state" runat="server" Text="State" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_per_state" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
           
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txt_per_state" ErrorMessage="*Required" Font-Size="Small" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    <asp:Label ID="lbl_per_country" runat="server" Text="Country" CssClass="label_small"></asp:Label>
                </td>

                <td>
                    <asp:TextBox ID="txt_per_country" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
           
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txt_per_country" ErrorMessage="*Required" Font-Size="Small" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    &nbsp;</td>

                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>
                    <asp:Label ID="lbl_per_pin" runat="server" Text="Pin Code" CssClass="label_small"></asp:Label>
                </td>

                <td colspan="4">
                    <asp:TextBox ID="txt_per_pin" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
             
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txt_per_pin" ErrorMessage="*Please enter valid pin" ForeColor="Red" ValidationExpression="[0-9]{6,}" ValidationGroup="a"></asp:RegularExpressionValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txt_per_pin" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
             
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td>&nbsp;</td>

                <td>
             
                </td>
                <td></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="lbl_mob_no" runat="server" Text="Mobile No." CssClass="label_small"></asp:Label>
                </td>
                <td colspan="4">
                    <asp:TextBox ID="txt_mob_no" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
      
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txt_mob_no" ErrorMessage="*Please enter valid phone no." ForeColor="Red" ValidationExpression="[0-9]{10}" ValidationGroup="a"></asp:RegularExpressionValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txt_mob_no" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
      
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td colspan="3">
                    &nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>

                <td>
                    <asp:Label ID="lbl_email" runat="server" Text="Email ID" CssClass="label_small"></asp:Label>
                </td>

                <td colspan="4">
                    <asp:TextBox ID="txt_email" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txt_email" ErrorMessage="*Please enter valid email id" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="a"></asp:RegularExpressionValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txt_email" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>

                <td class="labeltext">Employee Details:</td>

                <td></td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lbl_qualifctn" runat="server" CssClass="label_small" Text="Qualification"></asp:Label>
                    </td>
                <td>
                    <br />
                    <asp:TextBox ID="txt_qualifctn" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>

                <td>
                    <br />
                    
                </td>

            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="lbl_spclzn" runat="server" Text="Specilization" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_spclzn" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="lbl_job_type" runat="server" Text="Job Type" CssClass="label_small"></asp:Label>
                    </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddl_job_type" runat="server" AutoPostBack="True" CssClass="search_categories">
                    </asp:DropDownList>
                    <asp:Label ID="lbl_jobtype_error" runat="server" ForeColor="Red" Text="*Please select your jobtype"></asp:Label>
                    
                </td>
            </tr>

            <tr>
                <td></td>

                <td>
                    <asp:Label ID="lbl_experience" runat="server" Text="Experience(yrs)" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_experience" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
              
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="lbl_designation" runat="server" Text="Designation" CssClass="label_small"></asp:Label>
                    </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddl_designation" runat="server" AutoPostBack="True" CssClass="search_categories">
                    </asp:DropDownList>
                    
                    <asp:Label ID="lbl_desig_error" runat="server" ForeColor="Red" Text="Please select your designation"></asp:Label>
                    
                </td>
            </tr>
            <tr>
                <td></td>

                <td></td>

                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>

                <td></td>

                <td></td>
                <td></td>

            </tr>
            <tr>
                <td></td>

                <td></td>

                <td>
                    <asp:Button ID="btn_register" runat="server" OnClick="btn_register_Click" Text="Register" CssClass="buttonyellow" ValidationGroup="a" />
                </td>
                <td>
                    <asp:Button ID="btn_back" runat="server" OnClick="btn_back_Click" Text="Back" CssClass="buttonyellow" />
                </td>


            </tr>
            <tr>
                <td></td>

                <td></td>

                <td></td>
                <td></td>

            </tr>
        </table>
    </form>
</asp:Content>

