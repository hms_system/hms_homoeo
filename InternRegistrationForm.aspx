﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage2.master" AutoEventWireup="true" CodeFile="InternRegistrationForm.aspx.cs" Inherits="InternRegistrationForm" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   <title></title>
    
<link rel="shortcut icon" type="image/icon" href="images/favicon.ico"/>
<link href="css/bootstrap.min.css" rel="stylesheet">
 <link href="css/font-awesome.min.css" rel="stylesheet">
 <link id="switcher" href="css/themes/default-theme.css" rel="stylesheet">
<link href="css/slick.css" rel="stylesheet">
<link rel='stylesheet prefetch' href='css/photoswipe.css'>
 <link rel='stylesheet prefetch' href='css/default-skin.css'>    
<link href="style.css" rel="stylesheet">
    <link href="css/themes/default-theme.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>  
    <link href='http://fonts.googleapis.com/css?family=Habibi' rel='stylesheet' type='text/css'>   
    <link href='http://fonts.googleapis.com/css?family=Cinzel+Decorative:900' rel='stylesheet' type='text/css'>



    <style type="text/css">
        
        
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <form id="form1"  runat="server">
       
        <table>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="5" class="center">INTERNS REGISTRATION FORM</td>
            </tr>
            <tr>
                <td></td>
                <td >
                    
                </td>
                <td>
                  </td>
                <td>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label1" runat="server" CssClass="label_small" Text="Name"></asp:Label>
                </td>
                <td>
                    <br />
                    <asp:TextBox ID="txt_name" runat="server" Height="31px" Width="175px" CssClass="twitterStyleTextbox" style="color: #000099"></asp:TextBox>
                    <br />
                </td>
                <td>
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_name"  ErrorMessage="Please enter valid name" ValidationExpression="[a-zA-Z ]{1,}" ForeColor="Red" ValidationGroup="a"></asp:RegularExpressionValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_name" ErrorMessage="*Required" style="color: #FF0000" ValidationGroup="a"></asp:RequiredFieldValidator>
                    <br /></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label2" runat="server" CssClass="label_small" Text="DOB"></asp:Label>
                </td>
                <td>
                    <br />
                    <asp:TextBox ID="txt_dob" runat="server"  CssClass="twitterStyleTextbox"></asp:TextBox>
                    <cc1:CalendarExtender ID="txt_dob_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_dob">
                    </cc1:CalendarExtender>
                    <br />
                </td>
                <td> <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_dob" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label3" runat="server" CssClass="label_small" Text="Gender"></asp:Label>
                </td>
                <td>
                    <br />
                    <asp:RadioButtonList ID="rbl_gender" runat="server" AutoPostBack="True"  RepeatDirection="Horizontal"  CssClass="active" style="color: #000099">
                        <asp:ListItem Selected="True" Value="0">Male</asp:ListItem>
                        <asp:ListItem Value="1">Female</asp:ListItem>
                    </asp:RadioButtonList>
                    <br />
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td >
                    <asp:Label ID="Label4" runat="server" CssClass="label_small" Text="Name &amp; Address of Institute"></asp:Label>
                </td>
                <td>
                    <br />
                    <asp:TextBox ID="txt_institute" runat="server"  TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>                    
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_institute" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td >
                   </td>
                <td></td>
                <td ></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label5" runat="server" CssClass="label_small" Text="Address"></asp:Label>
                </td>
                <td>
                    <br />
                    <asp:TextBox ID="txt_address" runat="server" TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox>
                    <br />
                </td>
                <td> <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txt_address" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td >
                    <asp:Label ID="Label6" runat="server" CssClass="label_small" Text="Name of Gaurdian"></asp:Label>
                </td>
                <td>
                    <br />
                    <asp:TextBox ID="txt_gaurdian" runat="server"  CssClass="twitterStyleTextbox"></asp:TextBox>
                    <br />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txt_gaurdian" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>

                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td >
                    <asp:Label ID="Label7" runat="server" CssClass="label_small" Text="Nationality"></asp:Label>
                </td>
                <td >
                    <br />
                    <asp:TextBox ID="txt_nationality" runat="server"  CssClass="twitterStyleTextbox"></asp:TextBox>
                    <br />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txt_nationality" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td >
                    <asp:Label ID="Label8" runat="server" CssClass="label_small" Text="Date of Joining"></asp:Label>
                </td>
                <td>
                    <br />
                    <asp:TextBox ID="txt_joiningdate" runat="server"  CssClass="twitterStyleTextbox"></asp:TextBox>
                    <cc1:CalendarExtender ID="txt_joiningdate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_joiningdate">
                    </cc1:CalendarExtender>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txt_joiningdate" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label9" runat="server" CssClass="label_small" Text="Period of Internship"></asp:Label>
                </td>
                <td>
                    <br />
                    <asp:DropDownList ID="ddl_period" runat="server" AutoPostBack="True" CssClass="search_categories" >
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem Value="0">3 Months</asp:ListItem>
                        <asp:ListItem Value="1">6 Months</asp:ListItem>
                        <asp:ListItem Value="2">8 Months</asp:ListItem>
                    </asp:DropDownList>
                    <br/>
                    <asp:Label ID="lbl_perioderror" runat="server" ForeColor="Red" Text="*Please select your period of internship" ></asp:Label>
                    <br />
                </td>
                <td ></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="Label10" runat="server" CssClass="label_small" Text="Email Id"></asp:Label>
                </td>
                <td>
                    <br />
                    <asp:TextBox ID="txt_email" runat="server"  CssClass="twitterStyleTextbox"></asp:TextBox>
                    <br />
                </td>
                <td>
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txt_email" ErrorMessage="*Please enter valid email id" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="a"></asp:RegularExpressionValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txt_email" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
               </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="Label11" runat="server" CssClass="label_small" Text="Contact No"></asp:Label>
                </td>
                <td>
                    <br />
                    <asp:TextBox ID="txt_contact" runat="server"  CssClass="twitterStyleTextbox"></asp:TextBox>
                    <br />
                </td>
                <td> <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="*Please enter valid phone no." ForeColor="Red" ValidationExpression="[0-9]{10}" ValidationGroup="a" ControlToValidate="txt_contact"></asp:RegularExpressionValidator>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txt_contact" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td>
                   
                    <asp:Button ID="btn_back" runat="server" Text="BACK" OnClick="btn_back_Click"  CssClass="buttonyellow" />
                     </td>
                <td>
                    <asp:Button ID="btn_next" runat="server" Text="REGISTER" OnClick="btn_next_Click" ValidationGroup="a" CssClass="buttonyellow" />
                </td>
                <td>
                    <asp:Button ID="btn_clear" runat="server" Text="CLEAR" OnClick="btn_clear_Click" CssClass="buttonyellow" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td >&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        

    </form>
    
</asp:Content>

