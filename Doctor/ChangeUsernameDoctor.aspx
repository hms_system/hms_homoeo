﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Doctor/DoctorMaster.master" AutoEventWireup="true" CodeFile="ChangeUsernameDoctor.aspx.cs" Inherits="Doctor_ChangeUsernameDoctor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <table>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style35">&nbsp;</td>
                <td class="auto-style43">&nbsp;</td>
                <td class="auto-style15">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="center" colspan="2">CHANGE USERNAME</td>
                <td class="auto-style21">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="label_small">Enter New UserName:</td>
                <td class="auto-style44">
                    <asp:TextBox ID="txt_newusername" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_newusername" CssClass="auto-style42" ErrorMessage="*Required" ForeColor="#CC0000" ValidationGroup="a"></asp:RequiredFieldValidator>
                </td>
                <td rowspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style30"></td>
                <td class="auto-style30"></td>
                <td class="label_small">Enter Password:</td>
                <td class="auto-style45">
                    <asp:TextBox ID="txt_password" runat="server" CssClass="twitterStyleTextbox" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style35">
                    <asp:Label ID="lbl_title" runat="server" CssClass="label_small"></asp:Label>
                </td>
                <td class="auto-style43">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style40">
                    <asp:Button ID="btn_back" runat="server" OnClick="btn_back_Click" Text="<<BACK" CssClass="buttonyellow"  />
                    </td>
                <td class="auto-style46">
                    <asp:Button ID="btn_changeusername" runat="server" OnClick="btn_changeusername_Click" Text="CHANGE USERNAME" CssClass="buttonyellow" ValidationGroup="a" />
                    </td>
            </tr>
        </table>
        </form>
</asp:Content>

