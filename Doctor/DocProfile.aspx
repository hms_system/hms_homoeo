<%@ Page Title="" Language="C#" MasterPageFile="~/Doctor/DoctorMaster.master" AutoEventWireup="true" CodeFile="DocProfile.aspx.cs" Inherits="Doctor_DocProfile" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <form runat="server">
    <table>
        <tr>    
               <td  class="auto-style5">&nbsp;</td>    
               <td  class="auto-style13">    
                   <asp:Image ID="Image3" runat="server" Height="130px" Width="130px" />
                   
               </td>    
               <td  class="auto-style11">    
                   </td>    
               <td  class="auto-style11">    
                   &nbsp;</td>    
           </tr>    
        <tr>    
               <td  class="auto-style5">&nbsp;</td>    
               <td  class="label_small">Employee Id</td>    
               <td  class="auto-style17">    
                   <asp:TextBox ID="txt_empid" runat="server" CssClass="twitterStyleTextbox" />    
               </td>    
               <td  class="auto-style11">    
                   &nbsp;</td>    
           </tr>    
           <tr>    
               <td  class="auto-style6">&nbsp;</td>    
               <td  class="label_small">Employee Type</td>    
               <td >    
                   <asp:TextBox ID="txt_emptypeid" runat="server" CssClass="twitterStyleTextbox"  />    
               </td>    
               <td >    
                   &nbsp;</td>    
           </tr>
           <tr>    
               <td  class="auto-style6">&nbsp;</td>    
               <td  class="label_small">Employee Name</td>    
               <td >    
                   <asp:TextBox ID="txt_empname" runat="server" CssClass="twitterStyleTextbox"  />    
               </td>    
               <td >    
                   &nbsp;</td>    
           </tr>   
           <tr>    
               <td  class="auto-style6">&nbsp;</td>    
               <td  class="label_small">Date of Birth</td>    
               <td >    
                   <asp:TextBox ID="txt_dob" runat="server" CssClass="twitterStyleTextbox" /> </td>    
               <td >    
                   &nbsp;</td>    
           </tr>   
           <tr>    
               <td  class="auto-style6">&nbsp;</td>    
               <td  class="label_small">Address</td>    
               <td >    
                   <asp:TextBox ID="txt_hname" runat="server" CssClass="twitterStyleTextbox"  ></asp:TextBox>
                   <br />
                   <br />
                   <asp:TextBox ID="txt_street" runat="server" CssClass="twitterStyleTextbox"  ></asp:TextBox>
                   <br />
                   <br />
                   <asp:TextBox ID="txt_district" runat="server" CssClass="twitterStyleTextbox"  ></asp:TextBox>
                   <br />
                   <br />
                   <asp:TextBox ID="txt_pin" runat="server" CssClass="twitterStyleTextbox"  ></asp:TextBox>
               </td>    
               <td >    
                   &nbsp;</td>    
           </tr>   
           <tr>    
               <td  class="auto-style6">&nbsp;</td>    
               <td  class="label_small">Gender</td>    
               <td  class="auto-style4">    
                   <asp:TextBox ID="txt_gen" runat="server" CssClass="twitterStyleTextbox"  /> </td>    
               <td  class="auto-style4">    
                   </td>    
           </tr>   
           <tr>    
               <td  class="auto-style7"> </td>    
               <td  class="label_small">Joining date</td>    
               <td  class="auto-style8">    
                   <asp:TextBox ID="txt_joiningdate" runat="server" CssClass="twitterStyleTextbox"  />     
               </td>    
               <td  class="auto-style8">    
                   </td>    
           </tr>
           <tr>    
               <td  class="auto-style5">&nbsp;</td>    
               <td  class="label_small">Contact </td>    
               <td >    
                   <asp:TextBox ID="txt_con1" runat="server" CssClass="twitterStyleTextbox"  />    
               </td>    
               <td >    
                   &nbsp;</td>    
           </tr> 
           <tr>    
               <td  class="auto-style5">&nbsp;</td>    
               <td  class="label_small">Email Address</td>    
               <td >    
                   <asp:TextBox ID="txt_email" runat="server" CssClass="twitterStyleTextbox"  />       
               </td>    
               <td >    
                   &nbsp;</td>    
           </tr>    
           <tr>    
               <td  class="auto-style7"></td>    
               <td  class="label_small">Designation </td>    
               <td class="auto-style8">    
                   <asp:TextBox ID="txt_desigid" runat="server" CssClass="twitterStyleTextbox"  />    
               </td>    
               <td class="auto-style8">    
                   </td>    
           </tr>    
           <tr>    
               <td  class="auto-style9"></td>    
               <td  class="auto-style16">&nbsp;</td>    
               <td  class="label_small">Upload Photo:     
                   <asp:FileUpload ID="FileUpload1" runat="server" Height="16px" Width="109px" />
                   <asp:Button ID="btn_upload" runat="server" Text="Upload" OnClick="btn_upload_Click"  CssClass="buttonyellow" />
               </td>    
               <td  class="auto-style10">    
                   </td>    
           </tr>    
           <tr>    
               <td  class="auto-style5">&nbsp;</td>    
               <td  class="auto-style13">&nbsp;</td>    
               <td >    
                   &nbsp;</td>    
               <td >    
                   &nbsp;</td>    
           </tr>    
           <tr>    
               <td  class="auto-style5">&nbsp;</td>    
               <td  class="auto-style12">
                   <asp:Button ID="btn_edit" runat="server" Text="Edit" OnClick="btn_edit_Click" cssclass="buttonyellow" />
             
                   <asp:Button ID="btn_update" runat="server" Text="Update" OnClick="btn_update_Click" cssclass="buttonyellow"  />
               </td>    
               <td >    
                   &nbsp;</td>    
               <td >    
                   &nbsp;</td>    
           </tr>    
           <tr>    
               <td  class="auto-style5">&nbsp;</td>    
               <td  class="auto-style13">&nbsp;</td>    
               <td >    
                   &nbsp;</td>    
               <td >    
                   &nbsp;</td>    
           </tr>    
               </table>
       </form>
   
</asp:Content>

