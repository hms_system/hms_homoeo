﻿//
// Doctor consultation form in which previous records of each patients are available
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Doctor_DoctorConsultation : System.Web.UI.Page
{
    Connection c = new Connection();
    DataTable tb = new DataTable();
    DataRow dr;
    DataTable tb1 = new DataTable();
    DataRow dr1;


    int pid = 0;
    int did = 0;
    //int invst_id = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;


        //btn_addnewtest.Visible = false;
        //btn_back.Visible = false;
        //btn_recrd_back.Visible = false;

        did = Convert.ToInt32(Session["id"]);
        //  Session["id"]=pid;

        if (!IsPostBack)
        {
            MultiView1.ActiveViewIndex = 0;
            pid = Convert.ToInt32(Request.QueryString["PID"].ToString());
            Session["pid"] = pid;
            txt_addnewtest.Visible = false;
            btn_ok.Visible = false;
            GridView6.Visible = false;

            //Chart_vitals.Visible = false;

            c.getcon();

            string str_test = "select * from Add_Attributes ";
            SqlCommand cmd_test = new SqlCommand(str_test, c.con);
            SqlDataAdapter da_test = new SqlDataAdapter(cmd_test);
            DataTable dt_test = new DataTable();
            da_test.Fill(dt_test);
            if (dt_test.Rows.Count > 0)
            {
                ddl_test_attributes.DataSource = dt_test;
                ddl_test_attributes.DataValueField = "investigation_id";
                ddl_test_attributes.DataTextField = "investigation_attribute";
                ddl_test_attributes.DataBind();
                ddl_test_attributes.Items.Insert(0, "SELECT");
            }


            string pat_disp = "select * from Patient_details where cardno='" + pid + "'";
            SqlCommand cmd_pat_disp = new SqlCommand(pat_disp, c.con);
            SqlDataAdapter da_pat_disp = new SqlDataAdapter(cmd_pat_disp);
            DataTable dt_pat_disp = new DataTable();
            da_pat_disp.Fill(dt_pat_disp);
            if (dt_pat_disp.Rows.Count > 0)
            {
                DataRow row1 = dt_pat_disp.Rows[dt_pat_disp.Rows.Count - 1];

                int id = Convert.ToInt32(row1[0]);
                lbl_card.Text = Convert.ToString(pid);
                lbl_card1.Text = Convert.ToString(pid);
                lbl_card2.Text = Convert.ToString(pid);
                lbl_cardno3_disp.Text = Convert.ToString(pid);
                lbl_disp_card4.Text = Convert.ToString(pid);

                string name = Convert.ToString(row1[1]);
                lbl_dis_pname.Text = Convert.ToString(name);
                lbl_disp_pname1.Text = Convert.ToString(name);
                lbl_disp_pname2.Text = Convert.ToString(name);
                lbl_disp_pname3.Text = Convert.ToString(name);
                lbl_disp_pname4.Text = Convert.ToString(name);

                DateTime dob = Convert.ToDateTime(row1[2]);

                int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
                lbl_disp_age.Text = Convert.ToString(age) + "  yrs";
                lbl_disp_age1.Text = Convert.ToString(age) + "  yrs";
                lbl_disp_age2.Text = Convert.ToString(age) + "  yrs";
                lbl_disp_age3.Text = Convert.ToString(age) + "  yrs";
                lbl_disp_age4.Text = Convert.ToString(age) + "  yrs";

                string gender = Convert.ToString(row1[3]);
                lbl_disp_pgender.Text = Convert.ToString(gender);
                lbl_disp_gender1.Text = Convert.ToString(gender);
                lbl_disp_gender2.Text = Convert.ToString(gender);
                lbl_disp_gender3.Text = Convert.ToString(gender);
                lbl_disp_gender4.Text = Convert.ToString(gender);

                string bloodgrp = Convert.ToString(row1[12]);
                lbl_disp_bld_grp.Text = Convert.ToString(bloodgrp);
                lbl_disp_bld_grp1.Text = Convert.ToString(bloodgrp);
                lbl_disp_bld_grp2.Text = Convert.ToString(bloodgrp);
                lbl_disp_bldgrp3.Text = Convert.ToString(bloodgrp);
                lbl_disp_bldgrp4.Text = Convert.ToString(bloodgrp);

                cmd_pat_disp.ExecuteNonQuery();

            }

        }
    }



    protected void lnk_investigation_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getcon();
        //Chart_vitals.Visible = true;
        // pid = Convert.ToInt32(Request.QueryString["PID"].ToString());
        int patno = Convert.ToInt32(Session["pid"]);
        int id = Convert.ToInt32(Session["id"]);
        string str_invest = "insert into Investigation_details values(Convert(datetime,'" + DateTime.Now + "',103),'" + ddl_test_attributes.SelectedItem.Value + "','" + txt_test_values.Text + "','','" + patno + "')";
        SqlCommand cmd_invest = new SqlCommand(str_invest, c.con);
        cmd_invest.ExecuteNonQuery();

        //string testid=Convert.ToString(ddl_test_attributes.SelectedItem.Value);

        string str_test = "select * from Investigation_details  inner join Add_Attributes on Investigation_details.investigation_id=Add_Attributes.investigation_id where Investigation_details.patient_id='" + patno + "' and Add_Attributes.investigation_id='" + ddl_test_attributes.SelectedItem.Value + "' order by Investigation_details.date";
        SqlCommand cmd_test = new SqlCommand(str_test, c.con);
        DataTable dt_test = new DataTable();
        SqlDataAdapter da_test = new SqlDataAdapter(cmd_test);
        da_test.Fill(dt_test);
        if (dt_test.Rows.Count > 0)
        {

            GridView1.DataSource = dt_test;
            GridView1.DataBind();
        }
        //BindChart();
        c.con.Close();

    }

    //public void BindChart()
    //{
    //   // c.getcon();
    //    pid = Convert.ToInt32(Request.QueryString["PID"].ToString());
    //    SqlCommand cmd_chart = new SqlCommand("select i.test_value,a.investigation_attribute  from Investigation_details i inner join Add_Attributes a on a.investigation_id=i.investigation_id where patient_id='" + pid + "' ", c.con);
    //    SqlDataAdapter da_chart = new SqlDataAdapter(cmd_chart);
    //    DataTable dt_chart = new DataTable();
    //    da_chart.Fill(dt_chart);
    //    int k_pat = cmd_chart.ExecuteNonQuery();
    //    if (dt_chart.Rows.Count > 0)
    //    {
    //        int[] x = new int[dt_chart.Rows.Count];
    //        int[] y = new int[dt_chart.Rows.Count];
    //        for (int i = 0; i < dt_chart.Rows.Count; i++)
    //        {
    //             DataRow row1 = dt_chart.Rows[dt_chart.Rows.Count - i];

    //            int test = Convert.ToInt32(dt_chart.Rows[i][0]);
    //            int value = Convert.ToInt32(0);
    //            int attribute = Convert.ToInt32(1);
    //            //DateTime date = Convert.ToDateTime(dt_chart.Rows[i][1]);
    //            //int year = date.Year;
    //            //amt = amt + order;
    //            x[i] = value;
    //            y[i] = attribute;
    //        }

    //        Chart_vitals.Series[0].Points.DataBindXY(x, y);



    //    }


    //}


    protected void btn_back_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;

    }
    protected void lnk_diagnosis_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 3;

    }
    protected void lnk_prescription_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 4;
    }
    protected void lnk_previous_records_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;

    }


    protected void btn_addbtn_Click(object sender, EventArgs e)
    {
        c.getcon();
        int patno = Convert.ToInt32(Session["pid"]);
        String str_add = "insert into Diagnosis values('" + patno + "','" + did + "','" + txt_symptoms.Text + "','" + txt_diagnosis.Text + "','" + rbl_type.SelectedItem.Text + "',Convert(datetime,'" + DateTime.Now + "',103))";
        SqlCommand cmd_add = new SqlCommand(str_add, c.con);
        cmd_add.ExecuteNonQuery();
        MultiView1.ActiveViewIndex = 4;
    }
    protected void btn_pres_Click(object sender, EventArgs e)
    {


        c.getcon();
        int patno = Convert.ToInt32(Session["pid"]);
        SqlCommand cmd_daig_id = new SqlCommand("select Cons_Id from Diagnosis where cardno='" + patno+ "' ", c.con);
        //and cons_date=Convert(datetime,'" + DateTime.Today + "',103)
        SqlDataAdapter da_daig_id = new SqlDataAdapter(cmd_daig_id);
        DataTable dt_daig = new DataTable();
        da_daig_id.Fill(dt_daig);
        if (dt_daig.Rows.Count > 0)
        {
            int daigid = Convert.ToInt32(dt_daig.Rows[0][0]);



            String str_add = "insert into Prescription values('" + txt_drug.Text + "','" + txt_dose.Text + "','" + txt_freq.Text + "','" + did + "',Convert(datetime,'" + DateTime.Now + "',105),'" + daigid + "')";
            SqlCommand cmd_add = new SqlCommand(str_add, c.con);
            cmd_add.ExecuteNonQuery();


            gridbind5();
        }

        c.con.Close();
        txt_drug.Text = "";
        txt_freq.Text = "";
        txt_drug.Text = "";

        //SqlCommand cmd_daig_id = new SqlCommand("select Cons_Id from Diagnosis where cardno='"+pid+"'", c.con);
        //SqlDataAdapter da_daig_id = new SqlDataAdapter(cmd_daig_id);
        //DataTable dt_daig = new DataTable();
        //da_daig_id.Fill(dt_daig);
        //if (dt_daig.Rows.Count > 0)
        //{
        //    int daigid = Convert.ToInt32(dt_daig.Rows[0][0]);



        //    String str_add = "insert into Prescription values('" + txt_drug.Text + "','" + txt_dose.Text + "','" + txt_freq.Text + "','" + daigid + "','" + did + "',Convert(datetime,'" + DateTime.Today + "',103))";
        //    SqlCommand cmd_add = new SqlCommand(str_add, c.con);
        //    cmd_add.ExecuteNonQuery();

        //    ////QUERY TO BIND PRESCRIPTION TABLE//
        //    String medname = "select pr.drug_name from Prescription pr inner join Diagnosis d on d.Cons_Id=pr.diag_id where d.cardno='" + pid + "' and prescription_date=Convert(datetime,'" + DateTime.Today + "',103)";
        //    SqlCommand cmd_medname = new SqlCommand(medname, c.con);
        //    SqlDataAdapter da_medname = new SqlDataAdapter(cmd_medname);
        //    DataTable dt_medname = new DataTable();
        //    da_medname.Fill(dt_medname);
        //    if (dt_medname.Rows.Count > 0)
        //    {
        //        GridView5.DataSource = dt_medname;
        //        GridView5.DataBind();



        //    }
        //}

    }
    protected void btn_backpres_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    protected void GridView5_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
       
        //string str = "select * from  Prescription where patient_id='" + pid + "' and prescription_date=Convert(datetime,'" + DateTime.Today + "',103)";
        //SqlCommand cms_str = new SqlCommand(str, c.con);
        //SqlDataAdapter da_str = new SqlDataAdapter(cms_str);
        //DataTable dt_str = new DataTable();
        //da_str.Fill(dt_str);
        //if (dt_str.Rows.Count > 0)
        //{
        //    GridView5.DataSource = dt_str;
        //    GridView5.DataBind();



        //}
        txt_dose.Text = " ";
        txt_drug.Text = " ";
        txt_freq.Text = " ";

    }


    protected void btn_recrd_back_Click1(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    protected void lnk_patient_directed_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 5;
    }
    protected void btn_back_direct_Click(object sender, EventArgs e)
    {
        pid = Convert.ToInt32(Session["pid"]);
        c.getcon();
        MultiView1.ActiveViewIndex = 0;
        if (rbl_patient_directed.SelectedItem.Text == "Observation")
        {
            string str_status = "update  Schedule set status='" + rbl_patient_directed.SelectedItem.Text + "' where patient_id='" + pid + "'";
            SqlCommand cmd_status = new SqlCommand(str_status, c.con);
            cmd_status.ExecuteNonQuery();

            string str_insert_ip = "insert into Observation_details  values('" + did + "','" + pid + "','" + txt_remarks.Text + "',' ',' ',Convert(datetime,'" + DateTime.Now.Date + "',105) )";
            SqlCommand cmd_insert_ip = new SqlCommand(str_insert_ip, c.con);
            cmd_insert_ip.ExecuteNonQuery();
        }
        else
        {
            string str_status = "update  Schedule set status='" + rbl_patient_directed.SelectedItem.Text + "' where patient_id='" + pid + "'";
            SqlCommand cmd_status = new SqlCommand(str_status, c.con);
            cmd_status.ExecuteNonQuery();


            string str_insert_ip = "insert into IPAdmission_details  values('" + pid + "','" + did + "',' ',Convert(datetime,'" + DateTime.Now.Date + "',105) ,' ',' ',' ','Treatment Pending')";
            SqlCommand cmd_insert_ip = new SqlCommand(str_insert_ip, c.con);
            cmd_insert_ip.ExecuteNonQuery();

            c.con.Close();


        }
    }
    protected void btn_backsymptom_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    protected void btn_consultation_completed_Click(object sender, EventArgs e)
    {
        int patno = Convert.ToInt32(Session["pid"]);
        c.getcon();
        string str_complete = "update Patient_details set status='Completed' where cardno='" + patno + "'";
        SqlCommand cmd_complete = new SqlCommand(str_complete, c.con);
        cmd_complete.ExecuteNonQuery();
        c.con.Close();
        Response.Redirect("OPPatientList.aspx");
    }
    protected void btn_addnewtest_Click(object sender, EventArgs e)
    {
        txt_addnewtest.Visible = true;
        btn_ok.Visible = true;
        //btn_addnewtest.Visible = true;
    }
    protected void btn_ok_Click(object sender, EventArgs e)
    {
        c.getcon();
        string str_addnewtest = "insert into Add_Attributes values ('" + txt_addnewtest.Text + "')";
        SqlCommand cmd_addnewtest = new SqlCommand(str_addnewtest, c.con);
        cmd_addnewtest.ExecuteNonQuery();
        c.con.Close();
        txt_addnewtest.Text = "";
    }
    protected void btn_medical_history_Click(object sender, EventArgs e)
    {
        Response.Redirect("MedicalHistory.aspx?PID=" + lbl_card2.Text);
    }
    protected void btn_cnslt_history_Click(object sender, EventArgs e)
    {

        GridView6.Visible = true;
        c.getcon();
        gridbind1();
        c.con.Close();
    }
    public void gridbind1()
    {
        int patno = Convert.ToInt32(Session["pid"]);
        string str_bind1 = "select * from Employee_details e inner join OP_Allocation op on op.empid=e.empid inner join Schedule s on s.op_id=op.op_id where s.patient_id='" + patno + "' order by s.schedule_date";
        SqlCommand cms_str_bind1 = new SqlCommand(str_bind1, c.con);
        SqlDataAdapter da_str_bind1 = new SqlDataAdapter(cms_str_bind1);
        DataTable dt_str_bind1 = new DataTable();
        da_str_bind1.Fill(dt_str_bind1);
        if (dt_str_bind1.Rows.Count > 0)
        {
            GridView6.DataSource = dt_str_bind1;
            GridView6.DataBind();

        }
    }

    public void gridbind5()
    {

        int patno = Convert.ToInt32(Session["pid"]);
        ////QUERY TO BIND PRESCRIPTION TABLE//
        String medname = "select pr.prescription_id, pr.drug_name ,pr.dose,pr.frequency from Prescription pr inner join Diagnosis d on d.Cons_Id=pr.diag_id where d.cardno='" + patno + "' ";
        SqlCommand cmd_medname = new SqlCommand(medname, c.con);
        SqlDataAdapter da_medname = new SqlDataAdapter(cmd_medname);
        DataTable dt_medname = new DataTable();
        da_medname.Fill(dt_medname);
        if (dt_medname.Rows.Count > 0)
        {
            GridView5.DataSource = dt_medname;
            GridView5.DataBind();



        }


    }



    protected void Button2_Click(object sender, EventArgs e)
    {

    }

    protected void GridView6_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {

            int patno = Convert.ToInt32(Session["pid"]);
            int id = Convert.ToInt32(Session["id"]);
            c.getcon();
            if (e.CommandName == "SelectButton")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = GridView6.Rows[index];
                int dr_id = Convert.ToInt32(row.Cells[0].Text);

                int i1 = 1;

                tb1.Columns.Add("Symptoms", typeof(string));
                tb1.Columns.Add("Diagnosis", typeof(string));
                //tb1.Columns.Add("DiseaseType ", typeof(string));
                //tb2.Columns.Add("Drug Name ", typeof(string));
                //tb2.Columns.Add("Dosage", typeof(string));
                //tb2.Columns.Add("Frequency", typeof(string));


                string str_diag = "select * from Diagnosis where doc_id='" + dr_id + "'   and cardno='" + patno + "' ";
                SqlCommand cms_diag = new SqlCommand(str_diag, c.con);
                //cms_diag.Parameters.AddWithValue("@drid",dr_id);
                //cms_diag.Parameters.AddWithValue("@pno", patno);
                //cms_diag.Parameters.AddWithValue("@date",Convert.ToDateTime( row.Cells[3]));
                SqlDataAdapter da_diag = new SqlDataAdapter(cms_diag);
                DataTable dt_diag = new DataTable();
                da_diag.Fill(dt_diag);
                int c_diag = dt_diag.Rows.Count;
                int daigid = Convert.ToInt32(dt_diag.Rows[0][0]);
                while (c_diag > 0)
                {
                    DataRow row_table1 = dt_diag.Rows[dt_diag.Rows.Count - i1];
                    dr1 = tb1.NewRow();
                    dr1["Symptoms"] = Convert.ToString(row_table1[3]);
                    dr1["Diagnosis"] = Convert.ToString(row_table1[4]);
                    // dr2["DiseaseType"] = Convert.ToString(row_table1[5]);

                    tb1.Rows.Add(dr1);
                    Gv2.DataSource = tb1;
                    Gv2.DataBind();
                    ViewState["table2"] = tb1;
                    i1++;
                    c_diag--;
                }


                int i = 1;
                tb.Columns.Add("Vital Parameters", typeof(string));
                tb.Columns.Add("Values", typeof(string));

                string str_init = "select a.investigation_attribute,i.test_value from Investigation_details i inner join Add_Attributes a on i.investigation_id= a.investigation_id where i.patient_id='" + patno + "' ";
                SqlCommand cms_init = new SqlCommand(str_init, c.con);
                SqlDataAdapter da_init = new SqlDataAdapter(cms_init);
                DataTable dt_init = new DataTable();
                da_init.Fill(dt_init);
                int c_init = dt_init.Rows.Count;
                while (c_init > 0)
                {
                    DataRow row_table2 = dt_init.Rows[dt_init.Rows.Count - i];

                    dr = tb.NewRow();
                    dr["Vital Parameters"] = Convert.ToString(row_table2[0]);
                    dr["Values"] = Convert.ToString(row_table2[1]);

                    tb.Rows.Add(dr);
                    Gv1.DataSource = tb;
                    Gv1.DataBind();
                    ViewState["table1"] = tb;
                    i++;
                    c_init--;
                }




                lbl_futher1.Text = "Previous Vitals";
                lbl_diag.Text = "Previous Diagnosis";
            }

            btn_recrd_back.Visible = true;
            c.con.Close();



            ////

            //string str_diag = "select * from Diagnosis where doc_id='" + id + "'   and cardno='" + patno + "' ";
            //SqlCommand cms_diag = new SqlCommand(str_diag, c.con);
            //SqlDataAdapter da_diag = new SqlDataAdapter(cms_diag);
            //DataTable dt_diag = new DataTable();
            //da_diag.Fill(dt_diag);
            //int c_diag = dt_diag.Rows.Count;
            //int i = 1;
            //int daigid = Convert.ToInt32(dt_diag.Rows[0][0]);
            //while (c_diag > 0)
            //{
            //    DataRow row1 = dt_diag.Rows[dt_diag.Rows.Count - i];

            //    string diag = "Symptoms: (" + Convert.ToString(row1[3]) + ") Diagnosis: (" + Convert.ToString(row1[4]) + ") Type of disease: (" + Convert.ToString(row1[5]) + ") <br/>";

            //    lbl_diag.Text = lbl_diag.Text + diag;
            //    i++;
            //    c_diag--;
            //}
            //string str_pres = "select * from Prescription where empid='" + id + "'   and diag_id='" + daigid + "' ";
            //SqlCommand cms_pres = new SqlCommand(str_pres, c.con);
            //SqlDataAdapter da_pres = new SqlDataAdapter(cms_pres);
            //DataTable dt_pres = new DataTable();
            //da_pres.Fill(dt_pres);
            //int c_pres = dt_pres.Rows.Count;
            //int i2 = 1;
            //while (c_pres > 0)
            //{

            //    DataRow row1 = dt_pres.Rows[dt_pres.Rows.Count - i2];

            //    string pres = " Drug Name: (" + Convert.ToString(row1[1]) + ") Dosage: (" + Convert.ToString(row1[2]) + ") Frequency: (" + Convert.ToString(row1[3]) + ") <br/>";

            //    lbl_pres.Text = lbl_pres.Text + pres;
            //    c_pres--;
            //    i2++;
            //}

            //////

        }
        catch (Exception exe)
        {
        }
    }
    protected void GridView6_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        c.getcon();
        GridView6.PageIndex = e.NewPageIndex;
        gridbind1();
        c.con.Close();

    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void Button3_Click(object sender, EventArgs e)
    {

    }
    protected void GridView5_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        // int patno = Convert.ToInt32(Session["pid"]);
          //  int id = Convert.ToInt32(Session["id"]);
            c.getcon();
            if (e.CommandName == "SelectButton")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = GridView5.Rows[index];


                //int id = Convert.ToInt32(GridView5.DataKeys[e.RowIndex].Values[0]);
                //int dpid=Convert.ToInt32(GridView5.DataKeys[e.RowIndex].Values[]);
               
                String str_del = "delete from Prescription where drug_name='" + row.Cells[0].Text + "'";
                SqlCommand cmd_del = new SqlCommand(str_del, c.con);
                cmd_del.ExecuteNonQuery();
                gridbind5();        
            }
            
        c.con.Close();


    }
}