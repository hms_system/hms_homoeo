﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Doctor_ViewIPObservation : System.Web.UI.Page
{
    Connection c = new Connection();
    int ipid = 0;
    int did=0;
    protected void Page_Load(object sender, EventArgs e)
    {
        did = Convert.ToInt32(Session["id"]);

        
            
        if (!IsPostBack)
        {
            ipid = Convert.ToInt32(Request.QueryString["IPID"].ToString());
            Session["ip_id"] = ipid;
            c.getcon();
            Multilview_ipobv_dr.ActiveViewIndex = 0;
            //Session["ipid"] =ipid;
            SqlCommand cmd_pat = new SqlCommand("select p.cardno,p.patient_name,p.dob,p.gender,ip.status from Patient_details p inner join IPAdmission_details ip on ip.patient_id=p.cardno  where ip.dr_emp_id='" + did + "' and ip.status=@status and ip.admission_id=@ip_id ", c.con);
            cmd_pat.Parameters.AddWithValue("@dr_id", did);
            cmd_pat.Parameters.AddWithValue("@status","Treatment Pending");
            cmd_pat.Parameters.AddWithValue("@ip_id",ipid);
            SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
            DataTable dt_pat = new DataTable();
            sda_pat.Fill(dt_pat);


            int k_pat = cmd_pat.ExecuteNonQuery();

            if (dt_pat.Rows.Count > 0)
            {
                String name = Convert.ToString(dt_pat.Rows[0][1]);
                String pat_id = Convert.ToString(dt_pat.Rows[0][0]);
                
                string status = Convert.ToString(dt_pat.Rows[0][4]);
                DateTime dob = Convert.ToDateTime(dt_pat.Rows[0][2]);
                String gender = Convert.ToString(dt_pat.Rows[0][3]);
                int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
                lbl_age.Text = age.ToString();
                lbl_gen.Text = gender;
                lbl_patno.Text = pat_id;
                lbl_pname.Text = name;
                lbl_status.Text = status;
            }
            c.con.Close();
   
        }
    }
    protected void link_observ_Click(object sender, EventArgs e)
    {
        Multilview_ipobv_dr.ActiveViewIndex = 0;

        c.getcon();
         ipid = Convert.ToInt32(Session["ip_id"]);
        string bind1 = "select * from IPObservationDetails a  inner join IPAdmission_details b on b.patient_id=a.patient_id where b.admission_id='"+ipid+"' ";
        SqlCommand cmd_bind1 = new SqlCommand(bind1, c.con);
        SqlDataAdapter da_bind1 = new SqlDataAdapter(cmd_bind1);
        DataTable dt_bind1 = new DataTable();
        da_bind1.Fill(dt_bind1);
        if (dt_bind1.Rows.Count > 0)
        {
            GridView1.DataSource = dt_bind1;
            GridView1.DataBind();

        }
       }
    protected void link_vit_Click(object sender, EventArgs e)
    {
        Multilview_ipobv_dr.ActiveViewIndex = 2;
        BindGridView2_vitals();
    }
    public void BindGridView2_vitals()
    {
        c.getcon();
        string bind_vitals = "select * from Investigation_details n  inner join Add_Attributes a  on a.investigation_id=n.investigation_id inner join IPAdmission_details ip on ip.patient_id=n.patient_id where ip.admission_id= '" + ipid + "' ";
        SqlCommand cmd_bind_vitals = new SqlCommand(bind_vitals, c.con);
        SqlDataAdapter da_bind_vitals = new SqlDataAdapter(cmd_bind_vitals);
        DataTable dt_bind_vitals = new DataTable();
        da_bind_vitals.Fill(dt_bind_vitals);
        if (dt_bind_vitals.Rows.Count > 0)
        {
            GridView2_vitals.DataSource = dt_bind_vitals;
            GridView2_vitals.DataBind();

        }
   
    }
    protected void link_dis_Click(object sender, EventArgs e)
    {
        Multilview_ipobv_dr.ActiveViewIndex = 1;
    }
    protected void btn_dis_Click(object sender, EventArgs e)
    {
        c.getcon();
        int ipid = Convert.ToInt32(Session["ip_id"]);
        string update_ipstatus = "update IPAdmission_details set status='Treatment Completed' where admission_id='"+ipid+"' ";
        SqlCommand cmd_ipstatus = new SqlCommand(update_ipstatus,c.con);
        cmd_ipstatus.ExecuteNonQuery();
        c.con.Close();
        Response.Redirect("IPPatientList.aspx");

    }
   
    protected void GridView2_vitals_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2_vitals.PageIndex = e.NewPageIndex;
        BindGridView2_vitals();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void Button2_Click(object sender, EventArgs e)
    {

    }
}