﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Doctor/DoctorMaster.master" AutoEventWireup="true" CodeFile="MedicalCertificate.aspx.cs" Inherits="Doctor_MedicalCertificate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <center>
        <table>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <ajax:ToolkitScriptManager ID="toolkit1" runat="server"></ajax:ToolkitScriptManager>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Doctor Name" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_docname" runat="server" ReadOnly="True" CssClass="twitterStyleTextbox"></asp:TextBox>

                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td >
                    &nbsp;</td>
                <td >
                    <asp:Label ID="Label2" runat="server" CssClass="label_small" Text="Patient Name"></asp:Label>
                </td>
                <td >
                    <asp:TextBox ID="txt_patname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td >
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txt_patname" ErrorMessage="*Enter Patient Name" ForeColor="#CC0000"></asp:RequiredFieldValidator>

                </td>
                <td >
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="Label3" runat="server" CssClass="label_small" Text="Diagonosis"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_diag" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_diag" ErrorMessage="*Enter Diagnosis" ForeColor="#CC0000"></asp:RequiredFieldValidator>

                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="Label4" runat="server" CssClass="label_small" Text="No of days of leave"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_days" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_days" ErrorMessage="*Enter No of days" ForeColor="#CC0000"></asp:RequiredFieldValidator>

                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="Label5" runat="server" CssClass="label_small" Text="From Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_datefrom" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txt_datefrom" Format="dd/MM/yyyy" runat="server">
                    </ajax:CalendarExtender>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_datefrom" ErrorMessage="*Enter Date " ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="Label6" runat="server" CssClass="label_small" Text="To Date"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_dateto" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <ajax:CalendarExtender ID="CalendarExtender2" TargetControlID="txt_dateto" Format="dd/MM/yyyy" runat="server">
                    </ajax:CalendarExtender>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txt_dateto" ErrorMessage="*Enter date" ForeColor="#CC0000"></asp:RequiredFieldValidator>

                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnReport" runat="server" Text="Generate Report" OnClick="GenerateReport" CssClass="buttonyellow" /></td>
                <td></td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
            </center>
        </form>
     </asp:Content>

