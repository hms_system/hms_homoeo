﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Doctor/DoctorMaster.master" AutoEventWireup="true" CodeFile="MedicalHistory.aspx.cs" Inherits="Doctor_MedicalHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<form runat="server">
       <table>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2">
                <%--<div class="drop">
                    <ul class="drop_menu">
                        <li>
                            <a href="#">Take History</a>

                            <ul>
                                <li>
                                    <asp:LinkButton ID="lbtnphistory" runat="server" OnClick="lbtnphistory_Click">Personal History</asp:LinkButton>
                                </li>
                                <li></li>
                                <li>
                                    <asp:LinkButton ID="lbtnfhistory" runat="server" OnClick="lbtnfhistory_Click">Family History</asp:LinkButton>
                                </li>
                                <li>
                                    <asp:LinkButton ID="lbtnthistory" runat="server" OnClick="lbtnthistory_Click">Treatment History</asp:LinkButton>
                                </li>

                            </ul>
                        </li>

                        <li>
                            <a href="#">View History</a>

                            <ul>
                                <li>
                                    <asp:LinkButton ID="lbtn_viewphistory" runat="server" OnClick="lbtn_viewphistory_Click">Personal History</asp:LinkButton>
                                </li>
                                <li></li>
                                <li>
                                    <asp:LinkButton ID="lbtn_viewfhistory" runat="server" OnClick="lbtn_viewfhistory_Click">Family History</asp:LinkButton>
                                </li>
                                <li>
                                    <asp:LinkButton ID="lbtn_viewthistory" runat="server" OnClick="lbtn_viewthistory_Click">Treatment History</asp:LinkButton>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </div>--%>

                <asp:Button ID="btn_takehis" runat="server"  Text="Take History" CssClass="btn-info active" OnClick="btn_takehis_Click" />
                <asp:Button ID="btn_viewhis" runat="server"  Text="View History" CssClass="btn-info active" OnClick="btn_viewhis_Click" />
                                         
            </td>
        </tr>

        <tr>
            <td colspan="2">
                &nbsp;</td>
        </tr>

        <tr>
            <td>
                <asp:MultiView ID="MultiView1" runat="server" >
                    <table>
                        <tr>
                            <td>
                                <asp:View ID="View2_takehis" runat="server">
                                    <table>
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                           <asp:Button ID="btnphistory" runat="server" OnClick="lbtnphistory_Click" Text="Personal History" CssClass="btn-info active" />
                                           <asp:Button ID="btnfhistory" runat="server" OnClick="lbtnfhistory_Click" Text="Family History" CssClass="btn-info active" />
                                          <asp:Button ID="btnthistory" runat="server" OnClick="lbtnthistory_Click" Text="Treatment History" CssClass="btn-info active" />
                                                        
                                                   </td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                                   
                                             <tr>
                                                 <td colspan="2">&nbsp;</td>
                                                 <td>&nbsp;</td>
                                        </tr>
                                   
                                             </table>
                                </asp:View>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                         <tr>
                            <td>
                                <asp:View ID="View1_Viewhis" runat="server">
                                    <table>
                                    <tr>
                                        <td colspan="2">
                                            &nbsp;</td>
<td>&nbsp;</td>
                                    </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btn_viewphistory" runat="server" CssClass="btn-info active" OnClick="lbtn_viewphistory_Click" Text="View Personal History" />
                                                <asp:Button ID="btn_viewfhistory" runat="server" CssClass="btn-info active" OnClick="lbtn_viewfhistory_Click" Text="View Family History" />
                                                <asp:Button ID="btn_viewthistory" runat="server" CssClass="btn-info active" OnClick="lbtn_viewthistory_Click" Text="View Treatment History" />
                                            </td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                    <tr>
                                       <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                                   
                                             </table>
                                </asp:View>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>                                
                                <asp:View ID="View2_personalhis" runat="server">

                                    <table>
                                        <tr>
                                            <td class="center">&nbsp;</td>
                                            <td class="center" colspan="8">PERSONAL HISTORY</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>&nbsp;</td>
                                            <td></td>
                                            <td>&nbsp;</td>
                                            <td></td>
                                        </tr>
                                        <tr class="auto-style33">
                                            <td class="labeltext">&nbsp;</td>
                                            <td class="labeltext">Allergent</td>
                                            <td class="label" colspan="6">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td></td>
                                            <td colspan="3"></td>
                                            <td colspan="3">Describe Allergic Reaction and name of allergent where applicable </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblallergy1" runat="server" CssClass="label_small" Text="Iodine or X-ray contrast dye"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:RadioButtonList ID="rbl_allergent1" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txt_allergent9" runat="server" CssClass="twitterStyleTextbox" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblallergy2" runat="server" CssClass="label_small" Text="Latex or Rubber"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:RadioButtonList ID="rbl_allergent2" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txt_allergent10" runat="server" CssClass="twitterStyleTextbox" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblallergy3" runat="server" CssClass="label_small" Text="Bee or wasp stings"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:RadioButtonList ID="rbl_allergent3" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txt_allergent11" runat="server" CssClass="twitterStyleTextbox" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblallergy4" runat="server" CssClass="label_small" Text="Adhesive tape"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:RadioButtonList ID="rbl_allergent4" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txt_allergent12" runat="server" CssClass="twitterStyleTextbox" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblallergy5" runat="server" CssClass="label_small" Text="Sunlight"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:RadioButtonList ID="rbl_allergent5" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txt_allergent13" runat="server" CssClass="twitterStyleTextbox" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblallergy6" runat="server" CssClass="label_small" Text="Dust"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:RadioButtonList ID="rbl_allergent6" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txt_allergent14" runat="server" CssClass="twitterStyleTextbox" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblallergy7" runat="server" CssClass="label_small" Text="Medical Allergents"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:RadioButtonList ID="rbl_allergent7" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txt_allergent15" runat="server" CssClass="twitterStyleTextbox" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblallergy8" runat="server" CssClass="label_small" Text="Food Allergents"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:RadioButtonList ID="rbl_allergent8" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txt_allergent16" runat="server" CssClass="twitterStyleTextbox" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td colspan="3">&nbsp;</td>
                                            <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                <asp:Button ID="btn_add_allergent" runat="server" CssClass="buttonyellow" OnClick="btn_add_allergent_Click" Text="Add" Visible="False" />
                                                <asp:Button ID="Button1" runat="server" Text="Add" OnClick="btn_add_allergent_Click" />
                                                
                                            </td>
                                            <td colspan="6">&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td colspan="6">&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="center">&nbsp;</td>
                                            <td class="center" colspan="10">SOCIAL HISTORY</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td colspan="6">&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr><td class="labeltext">&nbsp;</td>
                                            <td class="label_black">Substance</td>
                                            <td class="label_black">Previous Usage?</td><td class="label_black">Current Usage?</td><td class="label_black">Frequency/Type/Amount</td>
                                            <td class="label_black">&nbsp;&nbsp;&nbsp; How Long?(years)</td>
                                            <td class="label_black">If Stopped, When?</td>
                                        </tr><tr><td>&nbsp;</td>
                                            <td></td>
                                            <td></td><td></td><td></td>
                                        <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr><tr><td>&nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblsub1" runat="server" CssClass="label_small" Text="Caffeine:coffe.tea,soda"></asp:Label>
                                            </td>
                                            <td><asp:RadioButtonList ID="rbl_pre_sub1" runat="server" RepeatDirection="Horizontal" CssClass="labelsmall" ><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:RadioButtonList ID="rbl_cur_sub1" runat="server" RepeatDirection="Horizontal" CssClass="labelsmall" ><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txt_freq_sub1" runat="server" CssClass="subnav"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txt_long_sub5" runat="server" CssClass="subnav"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txt_stop_sub1" runat="server" CssClass="subnav"></asp:TextBox>
                                            </td>
                                        </tr><tr><td>
                                            &nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblsub2" runat="server" CssClass="label_small" Text="Tobacco"></asp:Label>
                                            </td>
                                            <td><asp:RadioButtonList ID="rbl_pre_sub2" runat="server" RepeatDirection="Horizontal" CssClass="labelsmall" ><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:RadioButtonList ID="rbl_cur_sub2" runat="server" RepeatDirection="Horizontal" CssClass="labelsmall" ><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txt_freq_sub2" runat="server" CssClass="subnav"></asp:TextBox></td>
                                            <td>
                                                <asp:TextBox ID="txt_long_sub6" runat="server" CssClass="subnav"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txt_stop_sub2" runat="server" CssClass="subnav"></asp:TextBox>
                                            </td>
                                        </tr><tr><td >&nbsp;</td>
                                            <td >
                                                <asp:Label ID="lblsub3" runat="server" CssClass="label_small" Text="Alcohol-beer,wine,liquor"></asp:Label>
                                            </td>
                                            <td class="auto-style14">
           <asp:RadioButtonList ID="rbl_pre_sub3" runat="server" RepeatDirection="Horizontal" CssClass="labelsmall" ><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td class="auto-style15"><asp:RadioButtonList ID="rbl_cur_sub3" runat="server" RepeatDirection="Horizontal" CssClass="labelsmall" ><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txt_freq_sub3" runat="server" CssClass="subnav"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txt_long_sub7" runat="server" CssClass="subnav"></asp:TextBox>
                                            </td>
                                            <td >
                                                <asp:TextBox ID="txt_stop_sub3" runat="server" CssClass="subnav"></asp:TextBox>
                                            </td>
                                        </tr><tr><td>&nbsp;</td>
                                            <td>
                                                <asp:Label ID="lblsub4" runat="server" CssClass="label_small" Text="Recreational / Street drugs"></asp:Label>
                                            </td>
                                            <td><asp:RadioButtonList ID="rbl_pre_sub4" runat="server" RepeatDirection="Horizontal" CssClass="labelsmall"><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td>
           <asp:RadioButtonList ID="rbl_cur_sub4" runat="server" RepeatDirection="Horizontal" CssClass="labelsmall"><asp:ListItem>Yes</asp:ListItem><asp:ListItem Selected="True">No</asp:ListItem></asp:RadioButtonList></td><td><asp:TextBox ID="txt_freq_sub4" runat="server" CssClass="subnav"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txt_long_sub8" runat="server" CssClass="subnav"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txt_stop_sub4" runat="server" CssClass="subnav"></asp:TextBox>
                                            </td>
                                        </tr><tr><td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr><tr><td>&nbsp;</td>
                                            <td>
                                                <asp:Button ID="btn_add_sub" runat="server" CssClass="buttonyellow" OnClick="btn_add_sub_Click" Text="Add" />
                                            </td>
                                            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr><tr><td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                         </table>
                                </asp:View>
                                    <tr>
                            <td>
                                <asp:View ID="View3_familyhis" runat="server">
                                    <table>
                                        <tr>
                                            <td class="center" colspan="3">FAMILY HISTORY</td>

                                        </tr>
                                        <tr>
                                            <td class="center" colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="label" colspan="3">Illnesses/conditions which you know have occurred in your blood relatives.</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>&nbsp;</td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbfamily1" runat="server" CssClass="label_small" Style="font-size: small" Text="Cancer(describe the tpe of cancer for each pearson)"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="chkfam" runat="server" RepeatDirection="Horizontal" CssClass="tab-content">
                                                    <asp:ListItem>Grandparents</asp:ListItem>
                                                    <asp:ListItem>Father</asp:ListItem>
                                                    <asp:ListItem>Mother</asp:ListItem>
                                                    <asp:ListItem>Brothers</asp:ListItem>
                                                    <asp:ListItem>Sisters</asp:ListItem>
                                                    <asp:ListItem>Sons</asp:ListItem>
                                                    <asp:ListItem>Daughter</asp:ListItem>
                                                    <asp:ListItem>None</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="txtfam1" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbfamily2" runat="server" CssClass="label_small" Text="Heart Diseases"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="chkfam0" runat="server" RepeatDirection="Horizontal" CssClass="tab_content">
                                                    <asp:ListItem>Grandparents</asp:ListItem>
                                                    <asp:ListItem>Father</asp:ListItem>
                                                    <asp:ListItem>Mother</asp:ListItem>
                                                    <asp:ListItem>Brothers</asp:ListItem>
                                                    <asp:ListItem>Sisters</asp:ListItem>
                                                    <asp:ListItem>Sons</asp:ListItem>
                                                    <asp:ListItem>Daughter</asp:ListItem>
                                                    <asp:ListItem>None</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="txtfam2" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbfamily3" runat="server" CssClass="label_small" Text="Diabetes"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="chkfam1" runat="server" RepeatDirection="Horizontal" CssClass="tab_content">
                                                    <asp:ListItem>Grandparents</asp:ListItem>
                                                    <asp:ListItem>Father</asp:ListItem>
                                                    <asp:ListItem>Mother</asp:ListItem>
                                                    <asp:ListItem>Brothers</asp:ListItem>
                                                    <asp:ListItem>Sisters</asp:ListItem>
                                                    <asp:ListItem>Sons</asp:ListItem>
                                                    <asp:ListItem>Daughter</asp:ListItem>
                                                    <asp:ListItem Value="None">None</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="txtfam3" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbfamily4" runat="server" CssClass="label_small" Text="Stroke/TIA"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="chkfam2" runat="server" RepeatDirection="Horizontal" CssClass="tab_content">
                                                    <asp:ListItem>Grandparents</asp:ListItem>
                                                    <asp:ListItem>Father</asp:ListItem>
                                                    <asp:ListItem>Mother</asp:ListItem>
                                                    <asp:ListItem>Brothers</asp:ListItem>
                                                    <asp:ListItem>Sisters</asp:ListItem>
                                                    <asp:ListItem>Sons</asp:ListItem>
                                                    <asp:ListItem>Daughter</asp:ListItem>
                                                    <asp:ListItem>None</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="txtfam4" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbfamily5" runat="server" CssClass="label_small" Text="High Blood Pressure"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="chkfam3" runat="server" RepeatDirection="Horizontal" CssClass="tab_content" >
                                                    <asp:ListItem>Grandparents</asp:ListItem>
                                                    <asp:ListItem>Father</asp:ListItem>
                                                    <asp:ListItem>Mother</asp:ListItem>
                                                    <asp:ListItem>Brothers</asp:ListItem>
                                                    <asp:ListItem>Sisters</asp:ListItem>
                                                    <asp:ListItem>Sons</asp:ListItem>
                                                    <asp:ListItem>Daughter</asp:ListItem>
                                                    <asp:ListItem>None</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="txtfam5" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbfamily6" runat="server" CssClass="label_small" Text="High Cholesterol or Triglycerides"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="chkfam4" runat="server" RepeatDirection="Horizontal" CssClass="tab_content">
                                                    <asp:ListItem>Grandparents</asp:ListItem>
                                                    <asp:ListItem>Father</asp:ListItem>
                                                    <asp:ListItem>Mother</asp:ListItem>
                                                    <asp:ListItem>Brothers</asp:ListItem>
                                                    <asp:ListItem>Sisters</asp:ListItem>
                                                    <asp:ListItem>Sons</asp:ListItem>
                                                    <asp:ListItem>Daughter</asp:ListItem>
                                                    <asp:ListItem>None</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="txtfam6" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbfamily7" runat="server" CssClass="label_small" Text="Liver Disease"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="chkfam5" runat="server" RepeatDirection="Horizontal" CssClass="tab_content">
                                                    <asp:ListItem>Grandparents</asp:ListItem>
                                                    <asp:ListItem>Father</asp:ListItem>
                                                    <asp:ListItem>Mother</asp:ListItem>
                                                    <asp:ListItem>Brothers</asp:ListItem>
                                                    <asp:ListItem>Sisters</asp:ListItem>
                                                    <asp:ListItem>Sons</asp:ListItem>
                                                    <asp:ListItem>Daughter</asp:ListItem>
                                                    <asp:ListItem>None</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="txtfam7" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbfamily8" runat="server" CssClass="label_small" Text="Alcohol Or Drug Abuse"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="chkfam6" runat="server" RepeatDirection="Horizontal" CssClass="tab_content">
                                                    <asp:ListItem>Grandparents</asp:ListItem>
                                                    <asp:ListItem>Father</asp:ListItem>
                                                    <asp:ListItem>Mother</asp:ListItem>
                                                    <asp:ListItem>Brothers</asp:ListItem>
                                                    <asp:ListItem>Sisters</asp:ListItem>
                                                    <asp:ListItem>Sons</asp:ListItem>
                                                    <asp:ListItem>Daughter</asp:ListItem>
                                                    <asp:ListItem>None</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="txtfam8" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbfamily9" runat="server" CssClass="label_small" Text="Anxiety,Depression or Psychiatric Illness"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="chkfam7" runat="server" RepeatDirection="Horizontal" CssClass="tab_content">
                                                    <asp:ListItem>Grandparents</asp:ListItem>
                                                    <asp:ListItem>Father</asp:ListItem>
                                                    <asp:ListItem>Mother</asp:ListItem>
                                                    <asp:ListItem>Brothers</asp:ListItem>
                                                    <asp:ListItem>Sisters</asp:ListItem>
                                                    <asp:ListItem>Sons</asp:ListItem>
                                                    <asp:ListItem>Daughter</asp:ListItem>
                                                    <asp:ListItem>None</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="txtfam9" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbfamily10" runat="server" CssClass="label_small" Text="Tuberculosis"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="chkfam8" runat="server" RepeatDirection="Horizontal" CssClass="tab_content">
                                                    <asp:ListItem>Grandparents</asp:ListItem>
                                                    <asp:ListItem>Father</asp:ListItem>
                                                    <asp:ListItem>Mother</asp:ListItem>
                                                    <asp:ListItem>Brothers</asp:ListItem>
                                                    <asp:ListItem>Sisters</asp:ListItem>
                                                    <asp:ListItem>Sons</asp:ListItem>
                                                    <asp:ListItem>Daughter</asp:ListItem>
                                                    <asp:ListItem>None</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="txtfam10" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbfamily11" runat="server" CssClass="label_small" Text="Anesthesia Complications"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="chkfam9" runat="server" RepeatDirection="Horizontal" CssClass="tab_content">
                                                    <asp:ListItem>Grandparents</asp:ListItem>
                                                    <asp:ListItem>Father</asp:ListItem>
                                                    <asp:ListItem>Mother</asp:ListItem>
                                                    <asp:ListItem>Brothers</asp:ListItem>
                                                    <asp:ListItem>Sisters</asp:ListItem>
                                                    <asp:ListItem>Sons</asp:ListItem>
                                                    <asp:ListItem>Daughter</asp:ListItem>
                                                    <asp:ListItem>None</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="txtfam11" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbfamily12" runat="server" CssClass="label_small" Text="Genetic Disorders"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="chkfam10" runat="server" RepeatDirection="Horizontal" CssClass="tab_content">
                                                    <asp:ListItem>Grandparents</asp:ListItem>
                                                    <asp:ListItem>Father</asp:ListItem>
                                                    <asp:ListItem>Mother</asp:ListItem>
                                                    <asp:ListItem>Brothers</asp:ListItem>
                                                    <asp:ListItem>Sisters</asp:ListItem>
                                                    <asp:ListItem>Sons</asp:ListItem>
                                                    <asp:ListItem>Daughter</asp:ListItem>
                                                    <asp:ListItem>None</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="txtfam12" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style7">
                                                <asp:Button ID="btn_addfam" runat="server" CssClass="buttonyellow" OnClick="btn_addfam_Click" Text="Add" />
                                            </td>
                                            <td class="auto-style7"></td>
                                            <td class="auto-style7">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View4_treatmenthis" runat="server">

                                    <table>

                                        <tr>
                                            <td class="center" colspan="3">TREATMENT HISTORY</td>

                                        </tr>

                                        <tr>
                                            <td class="center" colspan="3">&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <asp:Label ID="Label1" runat="server" Text="Have you ever been hospitalized?" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList1" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td><span>Describe</span>:<asp:TextBox ID="textBox1" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="Have you  had any serious injuries and / or broken bones?" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList2" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>Describe:<asp:TextBox ID="textBox2" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label4" runat="server" Text="Have you ever received a blood transfusion ?" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList3" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>Describe:<asp:TextBox ID="textBox3" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="labeltext" colspan="2">Have you ever had any of the following?</td>
                                            <td class="labeltext">Describe Details:</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="Label5" runat="server" Text="Abnormal chest x-ray" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList4" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="textBox6" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="Label6" runat="server" Text="Anesthesia complications" CssClass="label_small"></asp:Label>&#160;&#160;</td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList5" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="textBox19" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="Label7" runat="server" Text="Anxiety,depression or mental illness" CssClass="label_small"></asp:Label>&#160;&#160;</td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList6" runat="server"  CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="textBox20" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="Label8" runat="server" Text="Blood problems (abnormal bleeding,anemia,high or low white count)" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList7" runat="server"  CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="textBox21" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="Label9" runat="server" Text="Diabetes" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList8" runat="server"  CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="textBox22" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="Label10" runat="server" Text="Growth removed from the colon or rectum (polyp or tumor)" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList9" runat="server" CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="textBox23" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="Label11" runat="server" Text="High blood pressure" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList10" runat="server"  CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="textBox24" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="Label12" runat="server" Text="High cholesterol or tryglycerides" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList11" runat="server"  CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="textBox25" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="Label13" runat="server" Text="Stroke or TIA" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList12" runat="server"  CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="textBox26" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="Label14" runat="server" Text="Treatment for alcohol and/ or drug abuse" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList13" runat="server"  CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="textBox27" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style17">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="Label15" runat="server" Text="Tuberculosis or positive tuberculin skin test" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td class="auto-style17">
                                                <asp:CheckBoxList ID="CheckBoxList14" runat="server"  CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td class="auto-style17">
                                                <asp:TextBox ID="textBox28" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:Label ID="Label16" runat="server" Text="Cosmetic or plastic surgery" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:CheckBoxList ID="CheckBoxList15" runat="server"  CssClass="labelsmall" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                </asp:CheckBoxList></td>
                                            <td>
                                                <asp:TextBox ID="textBox29" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr class="auto-style43">
                                            <td class="labeltext" colspan="2">Have you had a medical problem and/or surgery related to:</td>
                                            <td class="labeltext">Describe:(If surgery, specify Year of surgery)</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:Label ID="lbl1" runat="server" Text="Eyes (cataracts, glaucoma)" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt1" runat="server" TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl2" runat="server" Text="Ears,nose,sinuses,or tonsils" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList2" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt2" runat="server"  TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lbl3" runat="server" Text="Thyroid or parathyroid glands" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList4" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt3" runat="server"  TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl4" runat="server" Text="Heartvalves or abnormal heart rhythm" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList5" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt4" runat="server" TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lbl5" runat="server" Text="Coronary arteries" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList6" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt5" runat="server" TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl6" runat="server" Text="Arteries" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList13" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt6" runat="server" TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lbl7" runat="server" Text="Veins or blood clots in veins" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList14" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt7" runat="server"  TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl8" runat="server" Text="Lungs" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList15" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt8" runat="server"  TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lbl9" runat="server" Text="Esophagus or stomach (ulcer)" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList16" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt9" runat="server" TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl10" runat="server" Text="Bowel (small &amp; large intestine)" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList17" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt10" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lbl11" runat="server" Text="Appendix" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList18" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt11" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl12" runat="server" Text="Liver or gallbladder (including hepatitis)" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList19" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt12" runat="server"  TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lbl13" runat="server" Text="Hernia" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList20" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt13" runat="server"  TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl14" runat="server" Text="Kidneys or bladder" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList21" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt14" runat="server"  TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lbl15" runat="server" Text="Bones,joints or museles" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList22" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt15" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl16" runat="server" Text="Back,neck or spine" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList23" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt16" runat="server"  TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lbl17" runat="server" Text="Brain" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList24" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt17" runat="server"  TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl18" runat="server" Text="Skin" CssClass="label_small"></asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList25" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt18" runat="server" TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lbl19" runat="server" CssClass="label_small">Breasts</asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList26" runat="server" CssClass="label" RepeatDirection="Horizontal" >
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt19" runat="server"  TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
           <asp:Label ID="lbl20" runat="server" CssClass="label_small">Females:uterus,tubes,ovaries</asp:Label>&#160;</td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList27" runat="server" CssClass="label" RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txt20" runat="server"  TextMode="MultiLine"  CssClass="twitterStyleTextbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl21" runat="server" CssClass="label_small" Text="Males:prostate,penis,testes,vasectomy"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList28" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>No</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>Surgery</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txt21" runat="server" CssClass="twitterStyleTextbox" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btn_past" runat="server" OnClick="btn_past_Click" Text="Add" CssClass="btn" /></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td colspan="3" class="center">VACCINATION HISTORY</td>
                                        </tr>
                                        <tr>
                                            <td class="labeltext" colspan="3">Have you had these Vaccinations?</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>&nbsp;</td>
                                            <td class="labeltext">If Yes, Year when Vaccinated</td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac1" runat="server" Text="BCG (Bacillus Calmette Guerin) Vaccine" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac1" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>

                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txtvac1" runat="server" CssClass="subnav"></asp:TextBox></td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac2" runat="server" Text="DPT Vaccine" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac2" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>

                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txtvac2" runat="server" CssClass="subnav"></asp:TextBox></td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac3" runat="server" Text="OPV (Oral Polio) Vaccine" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac3" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>

                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txtvac3" runat="server" CssClass="subnav"></asp:TextBox>

                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac4" runat="server" Text="MMR (Measles Mumps Rubella) Vaccine " CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac4" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>

                                                </asp:RadioButtonList>

                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtvac4" runat="server" CssClass="subnav"></asp:TextBox>

                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac5" runat="server" Text="Hepatitis B Vaccine " CssClass="label_small"></asp:Label>

                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac5" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>

                                                </asp:RadioButtonList>

                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtvac5" runat="server" CssClass="subnav"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac6" runat="server" Text="Tetanus Toxoid (TT)" CssClass="label_small"></asp:Label>

                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac6" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>

                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txtvac6" runat="server" CssClass="subnav"></asp:TextBox></td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac7" runat="server" Text="Chicken Pox (Varicella) vaccine" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac7" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>

                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txtvac7" runat="server" CssClass="subnav"></asp:TextBox></td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac8" runat="server" Text="HPV (Human Papilloma Virus)" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac8" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>

                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txtvac8" runat="server" CssClass="subnav"></asp:TextBox></td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac9" runat="server" Text="HiB (Haemophilus Influenzae Type B) " CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac9" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>

                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txtvac9" runat="server" CssClass="subnav"></asp:TextBox></td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac10" runat="server" Text="Pneumococcal" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac10" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>

                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txtvac10" runat="server" CssClass="subnav"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac11" runat="server" Text="IPV (Inactivated Polio Vaccine)" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac11" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txtvac11" runat="server" CssClass="subnav"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac12" runat="server" Text="Rotavirus" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac12" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txtvac12" runat="server" CssClass="subnav"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac13" runat="server" Text="Influenza" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac13" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txtvac13" runat="server" CssClass="subnav"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac14" runat="server" Text="Pneumococcal Booster" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac14" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txtvac14" runat="server" CssClass="subnav"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="labvac15" runat="server" Text="Hepatitis A" CssClass="label_small"></asp:Label></td>
                                            <td>
                                                <asp:RadioButtonList ID="rblvac15" runat="server" RepeatDirection="Horizontal" CssClass="label">
                                                    <asp:ListItem>Unknown</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem Selected="True">No</asp:ListItem>
                                                </asp:RadioButtonList></td>
                                            <td>
                                                <asp:TextBox ID="txtvac15" runat="server" CssClass="subnav"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btn_addvac" runat="server" OnClick="btn_addvac_Click" Text="Add" CssClass="buttonyellow" /></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View5_viewpersonalhis" runat="server">
                                    <table>
                                        <tr>
                                            <td></td>

                                        </tr>
                                         <tr>
                                            <td class="center">ALLERGENT HISTORY</td>

                                        </tr>
                                         <tr>
                                            <td></td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_response6" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found" Visible="False"></asp:Label>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GridView_Allergy" runat="server" AutoGenerateColumns="False"
                                                    CellPadding="3"
                                                    Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerSettings LastPageText="Last" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                    <Columns>
                                                        <asp:BoundField DataField="allergent" HeaderText="Particulars" />
                                                        <asp:BoundField DataField="value" HeaderText="Yes/No" />
                                                        <asp:BoundField DataField="describe" HeaderText="Describe" />
                                                    </Columns>
                                                </asp:GridView>

                                            </td>

                                        </tr>
                                        <tr>
                                            <td></td>

                                        </tr>
                                         <tr>
                                            <td class="center">SOCIAL HISTORY</td>

                                        </tr>
                                         <tr>
                                            <td></td>

                                        </tr>


                                
    <tr>
        <td>
            <asp:Label ID="lbl_response5" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found" Visible="False"></asp:Label>
        </td>

    </tr>
    <tr><td>
<asp:GridView ID="GridView_Social" runat="server" AutoGenerateColumns="False" CellPadding="3" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
    <FooterStyle BackColor="White" ForeColor="#000066" />
    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
    <RowStyle ForeColor="#000066" />
    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
    <sortedascendingcellstyle backcolor="#F1F1F1" />
    <sortedascendingheaderstyle backcolor="#007DBB" />
    <sorteddescendingcellstyle backcolor="#CAC9C9" />
    <sorteddescendingheaderstyle backcolor="#00547E" />
    <Columns>
        <asp:BoundField DataField="Substance" HeaderText="Particulars" />
        <asp:BoundField DataField="currentusage" HeaderText="Currently Using? Yes/No" />
        <asp:BoundField DataField="previoususage" HeaderText="Previously Used? Yes/No" />
        <asp:BoundField DataField="frequency" HeaderText="Frequency/Type/Amount" />
        <asp:BoundField DataField="howlong" HeaderText="How Long?" />
        <asp:BoundField DataField="stoppedwhen" HeaderText="If stopped, When?" />
               </Columns>
               </asp:GridView>
                                                                                                                                                                   
    </td>

    </tr>


                                    </table>
                                </asp:View>
                                <asp:View ID="View3_viewfamilyhiatory" runat="server">
                                                                                            <table>
                       <tr>
                           <td></td>

                       </tr>
                       <tr>
                           <td class="center">FAMILY HISTORY</td>

                       </tr>
                                                                                                <tr>
                                                                                                    <td class="labeltext">Illnesses/conditions which you know have occurred in your blood relatives (Yes/No)</td>
                                                                                                </tr>
                       <tr>
                           <td>
                               <asp:Label ID="lbl_response3" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found" Visible="False"></asp:Label>
                           </td>

                       </tr>
                       <tr>
                           <td>
                               <asp:GridView ID="GridView_Family" runat="server" AutoGenerateColumns="False"
                                    CellPadding="3" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                   <FooterStyle BackColor="White" ForeColor="#000066" />
                                   <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                   <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                   <RowStyle ForeColor="#000066" />
                                   <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                   <sortedascendingcellstyle backcolor="#F1F1F1" />
                                   <sortedascendingheaderstyle backcolor="#007DBB" />
                                   <sorteddescendingcellstyle backcolor="#CAC9C9" />
                                   <sorteddescendingheaderstyle backcolor="#00547E" />
                                   <Columns>
                                       <asp:BoundField DataField="famillness" HeaderText="Particulars" />
                                       <asp:BoundField DataField="grandparents" HeaderText="Grand Parents" />
                                       <asp:BoundField DataField="father" HeaderText="Father" />
                                       <asp:BoundField DataField="mother" HeaderText="Mother" />
                                       <asp:BoundField DataField="brothers" HeaderText="Brothers" />
                                       <asp:BoundField DataField="sisters" HeaderText="Sisters" />
                                       <asp:BoundField DataField="sons" HeaderText="Sons" />
                                       <asp:BoundField DataField="daughters" HeaderText="Daughters" />
                                       <asp:BoundField DataField="others" HeaderText="None" />
                                       <asp:BoundField DataField="describe" HeaderText="Description" />
               </Columns>
               </asp:GridView>
                  </td>
               </tr>
               <tr>
                   <td></td>
                   </tr>
             </table>
                                                    </asp:View>
                                <asp:View ID="View4_treatmenthistory" runat="server">
                                    <table>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="center">TREATMENT HISTORY</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>     <asp:Label ID="lbl_response4" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found" Visible="False"></asp:Label></td>
                                            
                                        </tr>
                                       <tr><td>
                   <asp:GridView ID="gridview_surgery" runat="server" AutoGenerateColumns="False" 
                       CellPadding="3" Width="1000px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                       <FooterStyle BackColor="White" ForeColor="#000066" />
                       <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                       <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                       <RowStyle ForeColor="#000066" />
                       <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                       <sortedascendingcellstyle backcolor="#F1F1F1" />
                       <sortedascendingheaderstyle backcolor="#007DBB" />
                       <sorteddescendingcellstyle backcolor="#CAC9C9" />
                       <sorteddescendingheaderstyle backcolor="#00547E" />
                       <Columns>
                           <asp:BoundField DataField="MedicalProblem" HeaderText="Particulars" />
                           <asp:BoundField DataField="value" HeaderText="Surgery/Yes/No" />
                           <asp:BoundField DataField="describe" HeaderText="Description" />
               </Columns>
               </asp:GridView></td></tr>
                <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    <tr>
                                            <td class="center">VACCINATION HISTORY</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            
                                                    
                                            <td>
                                                <asp:Label ID="lbl_response7" runat="server" Font-Size="Large" ForeColor="#FFCCFF" Text="No records Found" Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Have you take immunizations for:</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GridView_Vaccination" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Width="1000px">
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <sortedascendingcellstyle backcolor="#F1F1F1" />
                                                    <sortedascendingheaderstyle backcolor="#007DBB" />
                                                    <sorteddescendingcellstyle backcolor="#CAC9C9" />
                                                    <sorteddescendingheaderstyle backcolor="#00547E" />
                                                    <Columns>
                                                        <asp:BoundField DataField="Immunization" HeaderText="Particulars" />
                                                        <asp:BoundField DataField="Value" HeaderText="Taken? Yes/No" />
                                                        <asp:BoundField DataField="Year" HeaderText="If taken, Year" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        </table>
                                </asp:View>
                                <asp:View ID="View5" runat="server">
                                    <table>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View6" runat="server">
                                    <table>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View7" runat="server">
                                    <table>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <br />
                            </td>
                            <td>
                                <br />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </asp:MultiView>
            </td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </form>
</asp:Content>

