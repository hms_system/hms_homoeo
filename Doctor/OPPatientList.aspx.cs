﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Doctor_OPPatientList : System.Web.UI.Page
{
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Button1.Visible = false;
            c.getcon();
            gridview();
            c.con.Close();
        }
    }

    public void gridview()
    {
        int id = Convert.ToInt32(Session["id"]);
        SqlCommand cmd_opid = new SqlCommand("select op_id from OP_Allocation where empid='" + id + "'", c.con);
        DataTable dt_opid = new DataTable();
        SqlDataAdapter da_opid = new SqlDataAdapter(cmd_opid);
        da_opid.Fill(dt_opid);
        if (dt_opid.Rows.Count > 0)
        {
            DataRow row_opid = dt_opid.Rows[dt_opid.Rows.Count - 1];
            int opid = Convert.ToInt32(row_opid[0]);


            // string str_date = "select schedule_date from Schedule ";


            string select_patient = "select * from patient_details p inner join Schedule s on p.cardno=s.patient_id where s.op_id='" + opid + "' and  s.schedule_date=Convert(datetime,'" + DateTime.Now.Date + "',105) and p.status='pending' ";
            SqlCommand cmd_select = new SqlCommand(select_patient, c.con);
            DataTable dt_select = new DataTable();
            SqlDataAdapter da_select = new SqlDataAdapter(cmd_select);
            da_select.Fill(dt_select);
            if (dt_select.Rows.Count > 0)
            {
                GridView1.DataSource = dt_select;
                GridView1.DataBind();

            }
            else
            {
                lbl_msg.Visible = true;
            }
        }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "SelectButton")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];
            Response.Redirect("DoctorConsultation.aspx?PID=" + row.Cells[0].Text);
        }
     
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
}