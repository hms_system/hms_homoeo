﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Doctor_MedicalHistory : System.Web.UI.Page
{
    Connection c = new Connection();
    int pno = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            pno = Convert.ToInt32(Request.QueryString["PID"].ToString());
            Session["PID"] = pno;
            MultiView1.ActiveViewIndex = 3;
            
            Gridview_Allergy_Bind();
            GridView_Social_Bind();
            GridView_Family_Bind();
            gridview_surgery_Bind();
            GridView_Vaccination_Bind();
            lbl_response6.Visible = false;
        }

    }
  
    
    protected void lbtnphistory_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;

    }
    protected void lbtnfhistory_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 3;

    }
    protected void lbtnthistory_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 4;

    }
    protected void lbtn_viewphistory_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 5;

    }
    protected void lbtn_viewfhistory_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 6;

    }
    protected void lbtn_viewthistory_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 7;

    }
    protected void btn_add_allergent_Click(object sender, EventArgs e)
    {
        c.getcon();
        pno = Convert.ToInt32(Session["PID"]);
        Label[] lbl = { lblallergy1, lblallergy2, lblallergy3, lblallergy4, lblallergy5, lblallergy6, lblallergy7, lblallergy8 };
        TextBox[] tbs = { txt_allergent9, txt_allergent10, txt_allergent11, txt_allergent12, txt_allergent13, txt_allergent14, txt_allergent15, txt_allergent16 };
        RadioButtonList[] rbl = { rbl_allergent1, rbl_allergent2, rbl_allergent3, rbl_allergent4, rbl_allergent5, rbl_allergent6, rbl_allergent7, rbl_allergent8 };

        for (int i = 0; i <= tbs.Length - 1; i++)
        {
            string txt = lbl[i].Text;

            SqlCommand cmd_aller = new SqlCommand("select * from AllergentMaster where allergent='" + txt + "' ", c.con);
            SqlDataAdapter sda_aller = new SqlDataAdapter(cmd_aller);
            DataTable dt_aller = new DataTable();
            sda_aller.Fill(dt_aller);


            int k_aller = cmd_aller.ExecuteNonQuery();

            if (dt_aller.Rows.Count > 0)
            {
                DataRow row_aller = dt_aller.Rows[dt_aller.Rows.Count - 1];

                int allerid = Convert.ToInt32(row_aller[0]);
                String s = "insert into AllergyHistory values('" + allerid + "','" + rbl[i].SelectedItem.Text + "','" + tbs[i].Text + "' ,'" + pno + "',Convert(datetime,'" + DateTime.Today + "',103))";
                SqlCommand cmds = new SqlCommand(s, c.con);
                cmds.ExecuteNonQuery();

            }
        }
        MultiView1.ActiveViewIndex = 0;

        c.con.Close();

    }
    public void Gridview_Allergy_Bind()
    {
        c.getcon();

        pno = Convert.ToInt32(Session["PID"]);
        SqlCommand cmd = new SqlCommand("select * from AllergyHistory p inner join AllergentMaster h on h.allergentid=p.allergentid where patientid=@pno ", c.con);
        cmd.Parameters.AddWithValue("@pno", pno);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Allergy.DataSource = dt;
            GridView_Allergy.DataBind();
        }
        else
            lbl_response6.Visible = true;
        c.con.Close();

    }


    protected void btn_add_sub_Click(object sender, EventArgs e)
    {
        c.getcon();
        pno = Convert.ToInt32(Session["PID"]);
        Label[] lbl = { lblsub1, lblsub2, lblsub3, lblsub4 };
        TextBox[] tbsfreq = { txt_freq_sub1, txt_freq_sub2, txt_freq_sub3, txt_freq_sub1 };
        TextBox[] tbslong = { txt_long_sub5, txt_long_sub6, txt_long_sub7, txt_long_sub8 };

        TextBox[] tbsstop = { txt_stop_sub1, txt_stop_sub2, txt_stop_sub3, txt_stop_sub1 };

        RadioButtonList[] rblcur = { rbl_cur_sub1, rbl_cur_sub2, rbl_cur_sub3, rbl_cur_sub4 };
        RadioButtonList[] rblpre = { rbl_pre_sub1, rbl_pre_sub2, rbl_pre_sub3, rbl_pre_sub4 };

        for (int i = 0; i <= tbsfreq.Length - 1; i++)
        {
            string txt = lbl[i].Text;

            SqlCommand cmd_sub = new SqlCommand("select * from SubstanceMaster where Substance='" + txt + "' ", c.con);
            SqlDataAdapter sda_sub = new SqlDataAdapter(cmd_sub);
            DataTable dt_sub = new DataTable();
            sda_sub.Fill(dt_sub);


            int k_sub = cmd_sub.ExecuteNonQuery();

            if (dt_sub.Rows.Count > 0)
            {
                DataRow row_sub = dt_sub.Rows[dt_sub.Rows.Count - 1];

                int subid = Convert.ToInt32(row_sub[0]);
                String s = "insert into SubstanceUsageHistory values('" + subid + "','" + rblcur[i].SelectedItem.Text + "','" + rblpre[i].SelectedItem.Text + "','" + tbsfreq[i].Text + "' ,'" + tbslong[i].Text + "' ,'" + tbsstop[i].Text + "' ,'" + pno + "',Convert(datetime,'" + DateTime.Today + "',103))";
                SqlCommand cmds = new SqlCommand(s, c.con);
                cmds.ExecuteNonQuery();

            }
        }
        MultiView1.ActiveViewIndex = 0;

        c.con.Close();

    }
    public void GridView_Social_Bind()
    {
        c.getcon();
        pno = Convert.ToInt32(Session["PID"]);
        
        SqlCommand cmd = new SqlCommand("select * from SubstanceUsageHistory p inner join SubstanceMaster h on h.SubId=p.subid where patientid=@pno ", c.con);
        cmd.Parameters.AddWithValue("@pno", pno);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Social.DataSource = dt;
            GridView_Social.DataBind();
        }
        else
            lbl_response5.Visible = true;
        c.con.Close();
    }

    protected void btn_addfam_Click(object sender, EventArgs e)
    {
        c.getcon();
        pno = Convert.ToInt32(Session["PID"]);
        Label[] lbl = { lbfamily1, lbfamily2, lbfamily3, lbfamily4, lbfamily5, lbfamily6, lbfamily7, lbfamily8, lbfamily9, lbfamily10, lbfamily11, lbfamily12 };
        TextBox[] tbs = { txtfam1, txtfam2, txtfam3, txtfam4, txtfam5, txtfam6, txtfam7, txtfam8, txtfam9, txtfam10, txtfam11, txtfam12 };
        CheckBoxList[] chk = { chkfam, chkfam0, chkfam1, chkfam2, chkfam3, chkfam4, chkfam5, chkfam6, chkfam7, chkfam8, chkfam9, chkfam10 };
        for (int i = 0; i <= tbs.Length - 1; i++)
        {
            string txt = lbl[i].Text;

            SqlCommand cmd_fam = new SqlCommand("select * from FamilyHistoryMaster where famillness='" + txt + "' ", c.con);
            SqlDataAdapter sda_fam = new SqlDataAdapter(cmd_fam);
            DataTable dt_fam = new DataTable();
            sda_fam.Fill(dt_fam);


            int k_fam = cmd_fam.ExecuteNonQuery();

            if (dt_fam.Rows.Count > 0)
            {
                DataRow row_fam = dt_fam.Rows[dt_fam.Rows.Count - 1];

                int famid = Convert.ToInt32(row_fam[0]);
                string gp = "No", fat = "No", mot = "No", bro = "No", sis = "No", son = "No", dau = "No", oth = "No";

                if (chk[i].Items[0].Selected == true)
                    gp = "yes";
                if (chk[i].Items[1].Selected == true)
                    fat = "yes";
                if (chk[i].Items[2].Selected == true)
                    mot = "yes";
                if (chk[i].Items[3].Selected == true)
                    bro = "yes";
                if (chk[i].Items[4].Selected == true)
                    sis = "yes";
                if (chk[i].Items[5].Selected == true)
                    son = "yes";
                if (chk[i].Items[6].Selected == true)
                    dau = "yes";
                if (chk[i].Items[7].Selected == true)
                    oth = "yes";



                String s = "insert into FamilyHistory values('" + famid + "','" + gp + "','" + fat + "','" + mot + "','" + bro + "','" + sis + "','" + son + "','" + dau + "','" + oth + "','" + tbs[i].Text+ "' ,'" + pno + "',Convert(datetime,'" + DateTime.Today + "',103))";
                SqlCommand cmds = new SqlCommand(s, c.con);
                cmds.ExecuteNonQuery();

            }
        }
        MultiView1.ActiveViewIndex = 0;

        c.con.Close();
   
    }
    public void GridView_Family_Bind()
    {
        c.getcon();
        pno = Convert.ToInt32(Session["PID"]);
       
        SqlCommand cmd = new SqlCommand("select * from FamilyHistory p inner join FamilyHistoryMaster h on h.famid=p.famid where patientid=@pno ", c.con);
        cmd.Parameters.AddWithValue("@pno", pno);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Family.DataSource = dt;
            GridView_Family.DataBind();
        }
        else
            lbl_response3.Visible = true;
        c.con.Close();
    }
   

    protected void btn_addvac_Click(object sender, EventArgs e)
    {
        c.getcon();
        pno = Convert.ToInt32(Session["PID"]);
        Label[] lbl = { labvac1, labvac2, labvac3, labvac4, labvac5, labvac6, labvac7, labvac8, labvac9, labvac10, labvac11, labvac12, labvac13, labvac14, labvac15 };
        TextBox[] tbs = { txtvac1, txtvac2, txtvac3, txtvac4, txtvac5, txtvac6, txtvac7, txtvac8, txtvac9, txtvac10, txtvac11, txtvac12, txtvac13, txtvac14, txtvac15 };
        RadioButtonList[] rbl = { rblvac1, rblvac2, rblvac3, rblvac4, rblvac5, rblvac6, rblvac7, rblvac8, rblvac9, rblvac10, rblvac11, rblvac12, rblvac13, rblvac14, rblvac15 };
        for (int i = 0; i <= tbs.Length - 1; i++)
        {
            string txt = lbl[i].Text;

            SqlCommand cmd_vac = new SqlCommand("select * from ImmunizationMaster where Immunization='" + txt + "' ", c.con);
            SqlDataAdapter sda_vac = new SqlDataAdapter(cmd_vac);
            DataTable dt_vac = new DataTable();
            sda_vac.Fill(dt_vac);


            int k_vac = cmd_vac.ExecuteNonQuery();

            if (dt_vac.Rows.Count > 0)
            {
                DataRow row_vac = dt_vac.Rows[dt_vac.Rows.Count - 1];

                int vacid = Convert.ToInt32(row_vac[0]);
                String s = "insert into ImmunizationHistory values('" + vacid + "','" + rbl[i].SelectedItem.Text + "','" + tbs[i].Text + "' ,'" + pno + "',Convert(datetime,'" + DateTime.Today + "',103))";
                SqlCommand cmds = new SqlCommand(s, c.con);
                cmds.ExecuteNonQuery();

            }
        }
        MultiView1.ActiveViewIndex = 0;

        c.con.Close();
    
    }
    public void GridView_Vaccination_Bind()
    {
        c.getcon();
        pno = Convert.ToInt32(Session["PID"]);
        SqlCommand cmd = new SqlCommand("select * from ImmunizationHistory p inner join ImmunizationMaster h on h.ImmuId=p.ImmuId where patientid=@pno ", c.con);
        cmd.Parameters.AddWithValue("@pno", pno);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Vaccination.DataSource = dt;
            GridView_Vaccination.DataBind();
        }
        else
            lbl_response7.Visible = true;
        c.con.Close();
    }

    protected void btn_past_Click(object sender, EventArgs e)
    {
        c.getcon();
        pno = Convert.ToInt32(Session["PID"]);
        Label[] lbl = { Label1,  Label3, Label4, Label5, Label6, Label7, Label8, Label9, Label10, Label11, Label12, Label13, Label14, Label15,Label16 };
        TextBox[] tbs = { textBox1, textBox2, textBox3, textBox6, textBox19, textBox20, textBox21, textBox22, textBox23, textBox24, textBox25, textBox26, textBox27, textBox28, textBox29 }; //put all into this array
        CheckBoxList[] chk = { CheckBoxList1, CheckBoxList2, CheckBoxList3, CheckBoxList4, CheckBoxList5, CheckBoxList6, CheckBoxList7, CheckBoxList8, CheckBoxList9, CheckBoxList10, CheckBoxList11, CheckBoxList12, CheckBoxList13, CheckBoxList14, CheckBoxList15 };
        for (int i = 0; i <= tbs.Length - 1; i++)
        {
            string txt = lbl[i].Text;

            SqlCommand cmd_past = new SqlCommand("select * from PastHistoryMaster where history='" + txt + "' ", c.con);
            SqlDataAdapter sda_past = new SqlDataAdapter(cmd_past);
            DataTable dt_past = new DataTable();
            sda_past.Fill(dt_past);


            int k_past = cmd_past.ExecuteNonQuery();

            if (dt_past.Rows.Count > 0)
            {
                DataRow row_past = dt_past.Rows[dt_past.Rows.Count - 1];

                int pastid = Convert.ToInt32(row_past[0]);

                String s = "insert into PastHistory values('" + pastid + "','" + chk[i].SelectedItem.Text + "','" + tbs[i].Text + "', '" + pno + "',Convert(datetime,'" + DateTime.Today + "',103))";
                SqlCommand cmds = new SqlCommand(s, c.con);
                cmds.ExecuteNonQuery();
            }
        }
        Label[] labmed = { lbl1, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8, lbl9, lbl10, lbl11, lbl12, lbl13, lbl14, lbl15, lbl16, lbl17, lbl18, lbl19, lbl20,lbl21 };
        TextBox[] txtmed = { txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9, txt10, txt11, txt12, txt13, txt14, txt15, txt16, txt17, txt18, txt19, txt20,txt21 };
        RadioButtonList[] rblmed = { RadioButtonList1, RadioButtonList2, RadioButtonList4, RadioButtonList5, RadioButtonList6, RadioButtonList13, RadioButtonList14, RadioButtonList15, RadioButtonList16, RadioButtonList17, RadioButtonList18, RadioButtonList19, RadioButtonList20,RadioButtonList21,RadioButtonList22,RadioButtonList23,RadioButtonList24,RadioButtonList25,RadioButtonList26,RadioButtonList27,RadioButtonList28 };
        for (int i = 0; i <= txtmed.Length - 1; i++)
        {
            string txt = labmed[i].Text;


            SqlCommand cmd_med = new SqlCommand("select * from MedicalProblemMaster where MedicalProblem='" + txt + "' ", c.con);
            SqlDataAdapter sda_med = new SqlDataAdapter(cmd_med);
            DataTable dt_med = new DataTable();
            sda_med.Fill(dt_med);


            int k_med = cmd_med.ExecuteNonQuery();

            if (dt_med.Rows.Count > 0)
            {
                DataRow row_med = dt_med.Rows[dt_med.Rows.Count - 1];

                int medid = Convert.ToInt32(row_med[0]);

                String s = "insert into MedicalProblemHistory values('" + medid + "','" + rblmed[i].SelectedItem.Text + "','" + txtmed[i].Text + "', '" + pno + "',Convert(datetime,'" + DateTime.Today + "',103))";
                SqlCommand cmds = new SqlCommand(s, c.con);
                cmds.ExecuteNonQuery();
            }
        }
        MultiView1.ActiveViewIndex = 0;

        c.con.Close();
    }

    public void gridview_surgery_Bind()
    {
        c.getcon();
        pno = Convert.ToInt32(Session["PID"]);

        SqlCommand cmd = new SqlCommand("select * from MedicalProblemMaster p inner join MedicalProblemHistory h on h.medicalid=p.MedicalId where patientid=@pno ", c.con);
        cmd.Parameters.AddWithValue("@pno", pno);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_surgery.DataSource = dt;
            gridview_surgery.DataBind();
        }
        else
            lbl_response4.Visible = true;
        c.con.Close();
    }



    protected void btn_takehis_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    protected void btn_viewhis_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
}