﻿//
//to view the IP patients observations

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Doctor_IPPatientList : System.Web.UI.Page
{
    Connection c = new Connection();
    int did = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            did = Convert.ToInt32(Session["id"]);
       
            c.getcon();
            gridbind();
            c.con.Close();
        }
    }
    public void gridbind()
    {
        //pid = Convert.ToInt32(Request.QueryString["PID"].ToString());
        did = Convert.ToInt32(Session["id"]);


        string str_ip_patient = "select * from Patient_details p inner join IPAdmission_details ip on p.cardno=ip.patient_id where ip.dr_emp_id='"+did+"' and ip.status='Treatment Pending'  ";

        SqlCommand cmd_ip_patient = new SqlCommand(str_ip_patient, c.con);

        //cmd_ip_patient.Parameters.AddWithValue("@ipstatus", "Treatment Pending");
        DataTable dt_ip_patient = new DataTable();
        SqlDataAdapter da_ip_patient = new SqlDataAdapter(cmd_ip_patient);
        da_ip_patient.Fill(dt_ip_patient);
        if (dt_ip_patient.Rows.Count > 0)
        {

            GridView1.DataSource = dt_ip_patient;
            GridView1.DataBind();
        }



    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "SelectButton")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];
            Response.Redirect("ViewIPObservation.aspx?IPID=" + row.Cells[0].Text);
        }

    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }
}