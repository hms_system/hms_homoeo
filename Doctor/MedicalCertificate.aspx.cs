﻿//
// generate Medical certificate of each patients
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

public partial class Doctor_MedicalCertificate : System.Web.UI.Page
{
    Connection c = new Connection();

        protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
       
        if (!IsPostBack)
        {
            c.getcon();
            SqlCommand cmd_pat = new SqlCommand("select empname from Employee_details where empid= '" + Session["id"] + "' ", c.con);
            SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
            DataTable dt_pat = new DataTable();
            sda_pat.Fill(dt_pat);


            int k_pat = cmd_pat.ExecuteNonQuery();

            if (dt_pat.Rows.Count > 0)
            {
                DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

                string docname = Convert.ToString(row_pat[0]);
                txt_docname.Text = docname;

                c.con.Close();
            }
        }
    }
        private void showPdf(string strS)
        {
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Dispostion", "attachment;filename=" + strS);
            Response.TransmitFile(strS);
            Response.End();
            Response.Flush();
            Response.Clear();
        }
    protected void GenerateReport(object sender, EventArgs e)
    {

         //pdf
                    //pdf generation

                    //Create a Document object
                    var document = new Document(PageSize.A4, 50, 50, 25, 25);

                    // Create a new PdfWriter object, specifying the output stream
                    var output = new MemoryStream();
                    //var writer = PdfWriter.GetInstance(document, output);
                    var writer = PdfWriter.GetInstance(document, new FileStream(HttpContext.Current.Server.MapPath("~/MedicalCer.pdf"), FileMode.Create));
                    writer.PageEvent = new ITextEvents();

                    //Font set
                    BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                    Font times = new Font(bfTimes, 14, Font.BOLD);


                    //Font set
                    BaseFont bfTimesText = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                    Font timesText = new Font(bfTimes, 12, Font.NORMAL);

                    BaseFont bfTimes1 = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                    Font times1 = new Font(bfTimes1, 14, Font.BOLD);


                    // Open the Document for writing
                    document.Open();
                        
                        
                        

                    //Image 

                    string imageurl = Server.MapPath(".") + "/images/gov.png";
                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(imageurl);

                    img.Alignment = Element.ALIGN_CENTER;
                    img.ScaleToFit(140f, 120f);
                    img.SpacingBefore = 30f;
                    img.SpacingAfter = 1f;
                    document.Add(img);



                    // Create a new Paragraph object with the text, "Hello, World!"
                    var welcomeParagrapha = new Paragraph();
                    welcomeParagrapha.Alignment = Element.ALIGN_LEFT;
                    welcomeParagrapha.SetLeading(5.0f, 3.0f);
                    //welcomeParagraph.FirstLineIndent = 50f;
                    welcomeParagrapha.SpacingBefore = 1f;
                    welcomeParagrapha.SpacingAfter = 28f;

                    Phrase ph0 = new Phrase("HOMOEO MEDICAL COLLEGE GOVT.HOSPITAL,KURICHY,KOTTAYAM", times);
                    welcomeParagrapha.Add(ph0);

                    //datetime to date
                    DateTime dtt = Convert.ToDateTime(DateTime.Now.Date);
                    String shrtdate = dtt.ToShortDateString();



                    var date1 = new Paragraph();
                    date1.Alignment = Element.ALIGN_RIGHT;
                    // date1.SetLeading(5.0f, 3.0f);
                    Phrase phdate = new Phrase(shrtdate);
                    date1.Add(phdate);
                   

                    var welcomeParagraphb = new Paragraph("MEDICAL CERTIFICATE", times1);
                    welcomeParagraphb.Alignment = Element.ALIGN_CENTER;
                    welcomeParagraphb.SpacingAfter = 15f;


                    var welcomeParagraphc = new Paragraph("         This is to certify that I have examined Mr./Miss.", timesText);
                    welcomeParagraphc.Alignment = Element.ALIGN_JUSTIFIED;
                    welcomeParagraphc.SpacingAfter = 15f;
                    welcomeParagraphc.SpacingBefore = 15f;
                    
                    Phrase ph1 = new Phrase(txt_patname.Text, times1);
                    welcomeParagraphc.Add(ph1);
       

                    Phrase ph2 = new Phrase("  was examined and treated from ", timesText);
                    welcomeParagraphc.Add(ph2);

                    Phrase ph3 = new Phrase(txt_datefrom.Text, times1);
                    welcomeParagraphc.Add(ph3);

                    Phrase ph4 = new Phrase(" to ", timesText);
                    welcomeParagraphc.Add(ph4);

                    Phrase ph5 = new Phrase(txt_dateto.Text, times1);
                    welcomeParagraphc.Add(ph5);


                    Phrase ph6 = new Phrase(" with the following findings and/or diagnosis/diagnoses :  ", timesText);
                    welcomeParagraphc.Add(ph6);

                    Phrase ph7 = new Phrase(txt_diag.Text, timesText);
                    welcomeParagraphc.Add(ph7);


                    var welcomeParagraphd = new Paragraph("REMARKS : ", times1);
                    welcomeParagraphd.Alignment = Element.ALIGN_LEFT;
                    welcomeParagraphd.SpacingAfter = 15f;
                    welcomeParagraphd.SpacingBefore = 15f;



                    var welcomeParagraphe = new Paragraph("Issuing Doctor's Name and Signature :  ", timesText);
                    welcomeParagraphe.Alignment = Element.ALIGN_RIGHT;
                    welcomeParagraphe.SpacingAfter = 5f;
                    welcomeParagraphe.SpacingBefore = 15f;


                    var welcomeParagraphf = new Paragraph("Dr . ", times1);
                    welcomeParagraphf.Alignment = Element.ALIGN_RIGHT;
                    welcomeParagraphf.SpacingAfter = 5f;
                    welcomeParagraphf.SpacingBefore = 15f;
                    Phrase ph8 = new Phrase(txt_docname.Text, times1);
                    welcomeParagraphf.Add(ph8);







                    //Phrase ph11 = new Phrase(dtfvb.Rows[0][11].ToString(), timesText);

                    //welcomeParagraphl.Add(ph11);

                    // Add the Paragraph object to the document
                    document.Add(welcomeParagrapha);
                    //document.Add(welcomeParagraph);
                    document.Add(date1);
                    document.Add(welcomeParagraphb);
                    // document.Add(welcomeParagraph1);
                    document.Add(welcomeParagraphc);
                    //document.Add(welcomeParagraph2);
                    document.Add(welcomeParagraphd);

                    document.Add(welcomeParagraphe);
                    document.Add(welcomeParagraphf);
                    document.Close();

                    string path = Server.MapPath("~/MedicalCer.pdf");

                    showPdf(path);


                  Response.Redirect(Server.MapPath("~/MedicalCer.pdf"));


                }


                //Response.Redirect(Server.MapPath("~/Test.pdf"));



            }

         




  
