﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Doctor/DoctorMaster.master" AutoEventWireup="true" CodeFile="DoctorConsultation.aspx.cs" Inherits="Doctor_DoctorConsultation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title></title>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <div>
            <table>
                <tr>
                    <td>
                        <asp:MultiView ID="MultiView1" runat="server">

                            <table>
                                <tr>
                                    <td>
                                        <asp:View ID="View1" runat="server">
                                            <table>
                                                <tr>
                                                    <td colspan="3" class="center">PATIENT DETAILS</td>
                                                    <td class="center">&nbsp;</td>
                                                    <td class="center">&nbsp;</td>
                                                    <td class="center">&nbsp;</td>
                                                    <td class="center">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>

                                                        <asp:Label ID="lbl_cardno" runat="server" Text="Card No" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_card" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:Label ID="lbl_pname" runat="server" Text="Patient Name" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_dis_pname" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="lbl_age" runat="server" Text="Age" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_age" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="lbl_pgender" runat="server" Text="Gender" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_pgender" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="lbl_bld_grp" runat="server" Text="Blood Group" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_bld_grp" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td colspan="6">
                                                        <%--<div class="drop">
                                                        <ul class="drop_menu">
  <li><asp:LinkButton ID="lnk_previous_records" runat="server" OnClick="lnk_previous_records_Click">Previous Records</asp:LinkButton></li>
  <li><asp:LinkButton ID="lnk_investigation" runat="server" OnClick="lnk_investigation_Click">Further Investigation</asp:LinkButton></li>
  <li><asp:LinkButton ID="lnk_diagnosis" runat="server" OnClick="lnk_diagnosis_Click">Diagnosis</asp:LinkButton></li>
  <li><asp:LinkButton ID="lnk_prescription" runat="server" OnClick="lnk_prescription_Click">Prescription</asp:LinkButton></li>
  <li><asp:LinkButton ID="lnk_patient_directed" runat="server" OnClick="lnk_patient_directed_Click">Patient Directed To</asp:LinkButton></li>
  <li></li>
</ul>
</div--%>
                                                        <asp:Button ID="btn_pre_cons" runat="server" OnClick="lnk_previous_records_Click" Text="Previous consultation" CssClass="btn-info active" />
                                                        <asp:Button ID="btn_invstigatn" runat="server" OnClick="lnk_investigation_Click" Text="Further Investigation" CssClass="btn-info active" />
                                                        <asp:Button ID="btn_diagnosis" runat="server" OnClick="lnk_diagnosis_Click" Text="Diagnosis" CssClass="btn-info active" />
                                                        <asp:Button ID="btn_prescription" runat="server" OnClick="lnk_prescription_Click" Text="Prescription" CssClass="btn-info active" />
                                                        <asp:Button ID="btn_patient_directed" runat="server" OnClick="lnk_patient_directed_Click" Text="Patient Refered " CssClass="btn-info active" />
                                                        
                                                        <asp:Button ID="btn_consultation_completed" runat="server" OnClick="btn_consultation_completed_Click" Text="Consultation Completed" CssClass="btn-info active" />


                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:View ID="View2" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label1" runat="server" CssClass="label_small" Text="Card No"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_card1" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label2" runat="server" Text="Patient Name" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td class="auto-style95" colspan="2">
                                                        <asp:Label ID="lbl_disp_pname1" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label3" runat="server" Text="Age" CssClass="label_small"></asp:Label>

                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_age1" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label4" runat="server" Text="Gender" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_gender1" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>

                                                        <asp:Label ID="Label5" runat="server" CssClass="label_small" Text="Blood Group"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_bld_grp1" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" class="center">FURTHER INVESTIGATION</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>

                                                        <asp:Label ID="lbl_test" runat="server" CssClass="label_small" Text="Name of Test"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddl_test_attributes" runat="server" AutoPostBack="True" CssClass="search_categories">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbl_value0" runat="server" CssClass="label_small" Text="Value"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txt_test_values" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                        <asp:Button ID="btn_add0" runat="server" BorderStyle="None" CssClass="buttonyellow" ForeColor="White" OnClick="btn_add_Click" Text="ADD" />
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" Visible="False" />
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3" GridLines="Vertical" DataKeyNames="invs_detail_id" Height="134px" Width="423px" CssClass="auto-style54" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px">
                                                            <AlternatingRowStyle BackColor="#DCDCDC" />
                                                            <Columns>
                                                                <asp:BoundField DataField="investigation_attribute" HeaderText="Name of Test" />
                                                                <asp:BoundField DataField="test_value" HeaderText="Value" />
                                                                <asp:BoundField DataField="date" HeaderText="Date" />
                                                            </Columns>
                                                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                            <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                            <SortedDescendingHeaderStyle BackColor="#000065" />
                                                        </asp:GridView>
                                                    </td>
                                                    <td>

                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btn_addnewtest" runat="server" OnClick="btn_addnewtest_Click" Text="Add New Test" CssClass="buttonyellow
                                                        " />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txt_addnewtest" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                        <asp:Button ID="btn_ok" runat="server" OnClick="btn_ok_Click" Text="OK" CssClass="buttonyellow" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btn_back0" runat="server" CssClass="buttonyellow" OnClick="btn_back_Click" Text="BACK" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:HMS_TESTConnectionString %>" SelectCommand="SELECT * FROM [Investigation_details]"></asp:SqlDataSource>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:View ID="View3" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label6" runat="server" Text="Card No" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_card2" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label7" runat="server" Text="Patient Name" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_pname2" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label8" runat="server" Text="Age" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_age2" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style100">
                                                        <asp:Label ID="Label9" runat="server" Text="Gender" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_gender2" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label10" runat="server" Text="Blood Group" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_bld_grp2" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" class="center">PREVIOUS RECORDS</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btn_cnslt_history0" runat="server" OnClick="btn_cnslt_history_Click" Text="Consultation History" CssClass="buttonyellow" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btn_medical_history0" runat="server" OnClick="btn_medical_history_Click" Text="Medical History" CssClass="buttonyellow" />
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Button ID="Button2" runat="server" Text="Button" OnClick="Button2_Click" Visible="False" />
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridView6" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="2" DataKeyNames="empid" GridLines="None" OnPageIndexChanging="GridView6_PageIndexChanging" OnRowCommand="GridView6_RowCommand" Width="600px">
                                                            <AlternatingRowStyle BackColor="PaleGoldenrod" />
                                                            <Columns>
                                                                <asp:BoundField DataField="empid" HeaderText="Doctors Id" />
                                                                <asp:BoundField DataField="empname" HeaderText="Consulted  Doctor" />
                                                                <asp:BoundField DataField="oproom_id" HeaderText="OP Room" />
                                                                <asp:BoundField DataField="schedule_date" HeaderText="Date of Consultations" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Button ID="btn_select" runat="server" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" CommandName="SelectButton" Text="View Records" Width="90px" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle BackColor="Tan" />
                                                            <HeaderStyle BackColor="Tan" Font-Bold="True" />
                                                            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                                                            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                                                            <SortedAscendingCellStyle BackColor="#FAFAE7" />
                                                            <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                                                            <SortedDescendingCellStyle BackColor="#E1DB9C" />
                                                            <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                                                        </asp:GridView>
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="lbl_futher1" runat="server" CssClass="label_small"></asp:Label>
                                                        <asp:GridView ID="Gv1" runat="server">
                                                        </asp:GridView>
                                                        <br />
                                                        <asp:Label ID="lbl_diag" runat="server" CssClass="label_small"></asp:Label>
                                                        <br />
                                                        <asp:GridView ID="Gv2" runat="server">
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>

                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>&nbsp;</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="lbl_pres" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td></td>
                                                    <td>
                                                        <asp:Button ID="btn_recrd_back" runat="server" CssClass="buttonyellow" OnClick="btn_recrd_back_Click1" Text="BACK" />
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:View ID="View4" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbl_cardno3" runat="server" Text="Card No" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_cardno3_disp" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbl_pname3" runat="server" Text="Patient Name" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_pname3" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbl_age3" runat="server" Text="Age" CssClass="label_small"></asp:Label>

                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_age3" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style2">
                                                        <asp:Label ID="lbl_gender3" runat="server" Text="Gender" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td class="auto-style2">
                                                        <asp:Label ID="lbl_disp_gender3" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td class="auto-style2"></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbl_bldgrp3" runat="server" Text="Blood Group" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_bldgrp3" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style2"></td>
                                                    <td class="auto-style2"></td>
                                                    <td class="auto-style2"></td>
                                                    <td class="auto-style2"></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td style="font-family: 'Times New Roman', Times, serif; font-size: medium; font-weight: normal; font-style: normal; color: #0000CC">Symptoms:</td>
                                                    <td>
                                                        <asp:TextBox ID="txt_symptoms" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_symptoms" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a" CssClass="auto-style37"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="label_small">Provisional Diagnosis:</td>
                                                    <td>
                                                        <asp:TextBox ID="txt_diagnosis" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_diagnosis" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a" CssClass="auto-style37"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td class="label_small">Disease Type:</td>
                                                    <td>

                                                        <div>
                                                            <asp:RadioButtonList ID="rbl_type" runat="server" AutoPostBack="True" CssClass="input" ForeColor="#0000CC">
                                                                <asp:ListItem Selected="True">Life Style Disease</asp:ListItem>
                                                                <asp:ListItem>Communicable Disease</asp:ListItem>
                                                                <asp:ListItem>Non-communicable Disease</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Button ID="btn_addbtn" runat="server" CssClass="buttonyellow" OnClick="btn_addbtn_Click" Text="ADD" ValidationGroup="a" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btn_backsymptom" runat="server" OnClick="btn_backsymptom_Click" Text="BACK" CssClass="buttonyellow" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:View ID="View5" runat="server">
                                            <table>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="lbl_card4" runat="server" CssClass="label_small" Text="Card No"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_card4" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:Label ID="lbl_pname4" runat="server" CssClass="label_small" Text="Patient Name"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_pname4" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:Label ID="lbl_age4" runat="server" CssClass="label_small" Text="Age"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_age4" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="lbl_gender4" runat="server" CssClass="label_small" Text="Gender"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_gender4" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="lbl_bldgrp4" runat="server" CssClass="label_small" Text="Blood Group"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_disp_bldgrp4" runat="server" CssClass="label_small"></asp:Label>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>

                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>

                                                <tr>
                                                    <td colspan="4" class="center">PRISCRIPTION</td>
                                                </tr>
                                                <tr>
                                                    <td class="center" colspan="4">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="label_small">Drug Name</td>
                                                    <td>
                                                        <asp:TextBox ID="txt_drug" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_drug" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="b" CssClass="auto-style37"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="label_small">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td class="label_small">Dose(No of Medicines per intake)</td>
                                                    <td>
                                                        <asp:TextBox ID="txt_dose" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txt_dose" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="b" CssClass="auto-style37"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="label_small">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td class="label_small">Frequency of Intake(hours)</td>
                                                    <td>
                                                        <asp:TextBox ID="txt_freq" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style5"></td>
                                                    <td class="auto-style5"></td>
                                                    <td class="auto-style5">
                                                        <asp:Button ID="btn_pres" runat="server" OnClick="btn_pres_Click" Text="Add" CssClass="buttonyellow" ValidationGroup="b" />
                                                    </td>
                                                    <td class="auto-style5"></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:Button ID="Button3" runat="server" Text="Button" OnClick="Button3_Click" Visible="False" />
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:GridView ID="GridView5" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="prescription_id" ForeColor="#333333" GridLines="None" OnRowDeleting="GridView5_RowDeleting" OnRowCommand="GridView5_RowCommand">
                                                            <EditRowStyle BackColor="#2461BF" />
                                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                            <RowStyle BackColor="#EFF3FB" />
                                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                            <AlternatingRowStyle BackColor="White" />
                                                            <Columns>
                                                                <asp:BoundField DataField="drug_name" HeaderText="Medicine" />
                                                                <asp:BoundField DataField="dose" HeaderText="Dossage" />
                                                                <asp:BoundField DataField="frequency" HeaderText="Frequency" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Button ID="btn_delete" runat="server" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" CommandName="SelectButton" Text="Delete" Width="90px" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td></td>
                                                    <td><strong>
                                                        <asp:Button ID="btn_backpres" runat="server" CssClass="buttonyellow" OnClick="btn_backpres_Click" Text="BACK" />
                                                    </strong></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:View ID="View6" runat="server">
                                            <table>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="label_small">Patient Directed To:</td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rbl_patient_directed" runat="server" AutoPostBack="True">
                                                            <asp:ListItem Value="0" Selected="True">Observation</asp:ListItem>
                                                            <asp:ListItem Value="1">IP</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="label_small">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="label_small">Remarks</td>
                                                    <td>
                                                        <asp:TextBox ID="txt_remarks" runat="server" TextMode="MultiLine" CssClass="twittertextarea"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <asp:Button ID="btn_back_direct" runat="server" OnClick="btn_back_direct_Click" Text="BACK" CssClass="buttonyellow" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="White">&nbsp;</td>
                                </tr>
                            </table>
                        </asp:MultiView>
                    </td>
                    <td>&nbsp; </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td></td>
                </tr>
            </table>

        </div>
    </form>


</asp:Content>

