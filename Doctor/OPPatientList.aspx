﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Doctor/DoctorMaster.master" AutoEventWireup="true" CodeFile="OPPatientList.aspx.cs" Inherits="Doctor_OPPatientList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></ti>   
</title></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
            <table>
        <tr>
            <td></td>
            <td>
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
            </td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cardno" GridLines="None" OnRowCommand="GridView1_RowCommand"  Width="800px" ForeColor="#333333" >
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="cardno" HeaderText="No" />
                            <asp:BoundField DataField="patient_name" HeaderText="Patient Name" />
                                         
                        <asp:TemplateField>  
                            <ItemTemplate>  
                                <asp:Button ID="btn_select" runat="server" Width="60" Text="SELECT" CommandName="SelectButton"  CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />  
                            </ItemTemplate> 
                             
                        </asp:TemplateField>  
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
           </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Label ID="lbl_msg" runat="server" CssClass="center" ForeColor="#FF6699" Text="No New Patients!!!!" Visible="False"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
        </form>

</asp:Content>

