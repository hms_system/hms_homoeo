﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Doctor/DoctorMaster.master" AutoEventWireup="true" CodeFile="ViewIPObservation.aspx.cs" Inherits="Doctor_ViewIPObservation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
   </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <form id="form1" runat="server">
        <table >
            <tr>
                <td ></td>
                <td ></td>
            </tr>
            <tr>
                <td class="label_small">Patient Id:</td>
                <td class="label_small">
                    <asp:Label ID="lbl_patno" runat="server" CssClass="label_small"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label_small">Patient Name:</td>
                <td>
                    <asp:Label ID="lbl_pname" runat="server" CssClass="label_small"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label_small">Gender:</td>
                <td class="auto-style4">
                    <asp:Label ID="lbl_gen" runat="server" CssClass="label_small"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label_small">Age:</td>
                <td>
                    <asp:Label ID="lbl_age" runat="server" CssClass="label_small"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label_small">Treatment Status:</td>
                <td>
                    <asp:Label ID="lbl_status" runat="server" CssClass="label_small"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="link_observ" runat="server" OnClick="link_observ_Click" Text="Observations" CssClass="btn-info" />
                    <asp:Button ID="link_vitals" runat="server" OnClick="link_vit_Click" Text="Vitals" CssClass="btn-info" />
                    <asp:Button ID="link_dis" runat="server" OnClick="link_dis_Click" Text="Discharge" CssClass="btn-info" />
                    
                     <%--<div class="drop">
<ul class="drop_menu">

     <li>
         <asp:LinkButton ID="link_observ" runat="server" OnClick="link_observ_Click">Observations</asp:LinkButton>
     </li>
     <li>
     </li>
    <li>
         <asp:LinkButton ID="link_vitals" runat="server" OnClick="link_vit_Click">Vitals</asp:LinkButton>
     </li>
    <li>
        <asp:LinkButton ID="link_dis" runat="server" OnClick="link_dis_Click">Discharge</asp:LinkButton>
     </li>
</ul>
</div>--%>
            </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:MultiView ID="Multilview_ipobv_dr" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <asp:View ID="view_observations" runat="server">
                                        <table>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" Visible="False" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BorderStyle="None" CellPadding="3" CellSpacing="2" DataKeyNames="ipobv_id"  >                                                        <Columns>
                                                            <asp:BoundField DataField="particulars" HeaderText="Particulars" />
                                                            <asp:BoundField DataField="observation" HeaderText="Observations" />
                                                            <asp:BoundField DataField="remarks" HeaderText="Remarks" />
                                                        </Columns>
                                                        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                                        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                                                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#FFF1D4" />
                                                        <SortedAscendingHeaderStyle BackColor="#B95C30" />
                                                        <SortedDescendingCellStyle BackColor="#F1E5CE" />
                                                        <SortedDescendingHeaderStyle BackColor="#93451F" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td class="auto-style2">
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:View ID="view_discharge" runat="server">
                                        <table>
                                            <tr>
                                                <td >&nbsp;</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td class="label-success">Are you sure you want to discharge this patient?</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:Button ID="btn_dis" runat="server" CssClass="buttonyellow" OnClick="btn_dis_Click" Text="Discharge" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:View ID="view_vitals" runat="server">
                                        <table>
                                             <tr>
            <td class="label">&nbsp;</td>
                                                 <td class="label">
                                                     <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Button" Visible="False" />
                                                 </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style2">
                &nbsp;</td>
            <td class="auto-style3">
                &nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td >
                <asp:GridView ID="GridView2_vitals" runat="server" AutoGenerateColumns="False"  width="500px" BackColor ="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None" OnPageIndexChanging="GridView2_vitals_PageIndexChanging">
                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    <Columns>
                        <asp:BoundField DataField="investigation_attribute" HeaderText="PARTICULARS" />
                        <asp:BoundField DataField="test_value" HeaderText="VALUES" />
                    </Columns>
                    <FooterStyle BackColor="Tan" />
                    <HeaderStyle BackColor="Tan" Font-Bold="True" />
                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                    <SortedAscendingCellStyle BackColor="#FAFAE7" />
                    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                    <SortedDescendingCellStyle BackColor="#E1DB9C" />
                    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                </asp:GridView>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                        </table>
                    </asp:MultiView>
                </td>
            </tr>
            </table>
    </form>
</asp:Content>

