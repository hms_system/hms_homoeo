﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Collections.Generic;

//to display doctor profile

public partial class Doctor_DocProfile : System.Web.UI.Page
{
    Connection c = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {

        int id = Convert.ToInt32(Session["id"]);
        if (!IsPostBack)
        {

            btn_update.Visible = false;
            txt_empname.Enabled = false;
            txt_empname.ReadOnly = true;
            txt_empid.Enabled = false;
            txt_empid.ReadOnly = true;
            txt_emptypeid.Enabled = false;
            txt_emptypeid.ReadOnly = true;
            txt_dob.Enabled = false;
            txt_dob.ReadOnly = true;
            txt_con1.Enabled = false;
            txt_con1.ReadOnly = true;
            txt_desigid.Enabled = false;
            txt_desigid.ReadOnly = true;
            txt_district.Enabled = false;
            txt_district.ReadOnly = true;
            txt_email.Enabled = false;
            txt_email.ReadOnly = true;
            txt_gen.Enabled = false;
            txt_gen.ReadOnly = true;
            txt_hname.Enabled = false;
            txt_hname.ReadOnly = true;
            txt_joiningdate.Enabled = false;
            txt_joiningdate.ReadOnly = true;
            txt_pin.Enabled = false;
            txt_pin.ReadOnly = true;
            txt_street.Enabled = false;
            txt_street.ReadOnly = true;
            
            BindTextBoxvalues();
            string str_img = "select photo from Employee_details where empid='"+id+"'";
            SqlCommand cmd_img = new SqlCommand(str_img, c.con);
            SqlDataAdapter da_img = new SqlDataAdapter(cmd_img);
            DataTable dt_img = new DataTable();
            da_img.Fill(dt_img);
            if (dt_img.Rows.Count > 0)
            {
                DataRow row_img = dt_img.Rows[dt_img.Rows.Count - 1];

                String img_path = Convert.ToString(row_img[0]);
                Image3.ImageUrl = "~/Uploads/" +img_path;
            }
            
    
                      
        }
    }

    //function to bind the data of employee to the textbox
    private void BindTextBoxvalues()
    {
       c.getcon();
        int id = Convert.ToInt32(Session["id"]);
        string str_emp_profile="select * from Employee_details where empid='" + id + "'";
        SqlCommand cmd_emp_profile = new SqlCommand(str_emp_profile, c.con);
        DataTable dt_emp_profile = new DataTable();
        SqlDataAdapter da_emp_profile = new SqlDataAdapter(cmd_emp_profile);
        da_emp_profile.Fill(dt_emp_profile);
        if (dt_emp_profile.Rows.Count > 0)
        {
            DataRow row_emp = dt_emp_profile.Rows[dt_emp_profile.Rows.Count - 1];

            String name = Convert.ToString(row_emp[1]);
            String eid = Convert.ToString(row_emp[0]);
            String addresstype = Convert.ToString(row_emp[5]);
            String dob = Convert.ToString(row_emp[2]);
            String email = Convert.ToString(row_emp[7]);
            String etypeid = Convert.ToString(row_emp[10]);
            

            String gen = Convert.ToString(row_emp[3]);
            String jdate = Convert.ToString(row_emp[8]);
            String cnt1 = Convert.ToString(row_emp[6]);
            String desigid = Convert.ToString(row_emp[11]);
            SqlCommand cmds1 = new SqlCommand("select * from Designation where desig_id='" + desigid + "'", c.con);
            SqlDataAdapter sds1 = new SqlDataAdapter(cmds1);
            DataTable dt_desig = new DataTable();
            sds1.Fill(dt_desig);

            int ks1 = cmds1.ExecuteNonQuery();

            if (dt_desig.Rows.Count > 0)
            {
                DataRow rowdesig = dt_desig.Rows[dt_desig.Rows.Count - 1];

                String designame = Convert.ToString(rowdesig[1]);
                txt_empname.Text = name;
                txt_empid.Text = eid;
                txt_dob.Text = dob;
                txt_email.Text = email;
                txt_gen.Text = gen;
                txt_joiningdate.Text = jdate;
                txt_con1.Text = cnt1;
                txt_desigid.Text = designame;
                txt_emptypeid.Text = etypeid;


            }
           
            //if (addresstype=1)
            //{
            SqlCommand cmd_address = new SqlCommand("select * from Permanent_address where empid='" + id + "'", c.con);
            SqlDataAdapter da_address = new SqlDataAdapter(cmd_address);
            DataTable dt_address = new DataTable();
            da_address.Fill(dt_address);
            int ad1 = cmd_address.ExecuteNonQuery();
            if (dt_address.Rows.Count > 0)
            {
                DataRow row_address = dt_address.Rows[dt_address.Rows.Count - 1];
                string hname = Convert.ToString(row_address[1]);
                string street = Convert.ToString(row_address[2]);
                string district = Convert.ToString(row_address[3]);
                string pin = Convert.ToString(row_address[6]);

                txt_empname.Text = name;
                txt_empid.Text = eid;
                txt_dob.Text = dob;
                txt_email.Text = email;
                txt_gen.Text = gen;
                txt_joiningdate.Text = jdate;
                txt_con1.Text = cnt1;
                //txt_desigid.Text = designame;
                txt_emptypeid.Text = etypeid;
                txt_hname.Text = hname;
                txt_street.Text = street;
                txt_district.Text = district;
                txt_pin.Text = pin;
            }
            String str_type = "select * from Job_type where typeid='" + etypeid + "'";
            SqlCommand cmd_type = new SqlCommand(str_type, c.con);
            SqlDataAdapter da_type = new SqlDataAdapter(cmd_type);
            DataTable dt_type = new DataTable();
            da_type.Fill(dt_type);
            int ad2 = cmd_type.ExecuteNonQuery();
            if (dt_type.Rows.Count > 0)
            {
                DataRow row_type = dt_type.Rows[dt_type.Rows.Count - 1];
                string type = Convert.ToString(row_type[1]);
                
                txt_empname.Text = name;
                txt_empid.Text = eid;
                txt_dob.Text = dob;
                txt_email.Text = email;
                txt_gen.Text = gen;
                txt_joiningdate.Text = jdate;
                txt_con1.Text = cnt1;
               txt_emptypeid.Text = type;
               }
        }
    }

    //function to upload the photo to their own profile
    protected void btn_upload_Click(object sender, EventArgs e)
    {
        c.getcon();
        int id = Convert.ToInt32(Session["id"]);
        //if (FileUpload1.HasFile)
        //{
        //    string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
        //    FileUpload1.PostedFile.SaveAs(Server.MapPath("~/Uploads/") + fileName);
        //    Response.Redirect(Request.Url.AbsoluteUri);
        //    string str_uploadfile = "update Employee_details set photo='" + fileName + "' where empid='"+id+"'";
        //    SqlCommand cmd_uploadfile = new SqlCommand(str_uploadfile, c.con);
        //    cmd_uploadfile.ExecuteNonQuery();
        //}

        
        //if (FileUploadControl.HasFile)
        //{
        //    try
        //    {
        //        string filename = Path.GetFileName(FileUploadControl.FileName);
        //        FileUploadControl.SaveAs(Server.MapPath("~/") + filename);
        //        StatusLabel.Text = "Upload status: File uploaded!";
        //    }
        //    catch (Exception ex)
        //    {
        //        StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
        //    }
        //}


        string image;
        string path = "";
        if (FileUpload1.HasFile)
        {
            image = FileUpload1.FileName;
            //check whether the img with same name doesnt exist


            if (Directory.Exists(Server.MapPath("~/Uploads")))
            {
                FileUpload1.SaveAs(Server.MapPath("~/Uploads/" + image));
                string str_uploadfile = "update Employee_details set photo='" +image + "' where empid='" + id + "'";
                SqlCommand cmd_uploadfile = new SqlCommand(str_uploadfile, c.con);
                cmd_uploadfile.ExecuteNonQuery();
                
            }
            else
            {
                Directory.CreateDirectory(Server.MapPath("~/Uploads"));
                FileUpload1.SaveAs(Server.MapPath("~/Uploads/" + image));
                string str_uploadfile = "update Employee_details set photo='" + image + "' where empid='" + id + "'";
                SqlCommand cmd_uploadfile = new SqlCommand(str_uploadfile, c.con);
                cmd_uploadfile.ExecuteNonQuery();
            
            }
            path = "~/Uploads/" + image;
            Image3.ImageUrl = path;
        }
        c.con.Close();
    }

    protected void btn_edit_Click(object sender, EventArgs e)
    {
        txt_empname.Enabled = true;
        txt_empname.ReadOnly = false;
        //txt_hname.Enabled = true;
        //txt_street.Enabled = true;
        //txt_district.Enabled = true;
        //txt_pin.Enabled = true;
        txt_con1.Enabled = true;
        txt_con1.ReadOnly = false;
        txt_email.Enabled = true;
        txt_email.ReadOnly = false;
        btn_update.Visible = true;
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        int id=Convert.ToInt32(Session["id"]);
        c.getcon();
       // txt_empname.Text='"++"'
        string str_edit = "update Employee_details set empname='" + txt_empname.Text + "',mobno='" + txt_con1.Text + "',email='" + txt_email.Text + "' where empid='"+id+"'";
        SqlCommand cmd_edit = new SqlCommand(str_edit, c.con);
        cmd_edit.ExecuteNonQuery();

        txt_empname.Enabled = false;
        txt_empname.ReadOnly = true;
        txt_con1.Enabled = false;
        txt_con1.ReadOnly = true;
        txt_email.Enabled = false;
        txt_email.ReadOnly = true;
        btn_update.Visible = false;


        c.con.Close();
        Response.Write("<script>alert('Updated Successfully');</script>");
    }
} 