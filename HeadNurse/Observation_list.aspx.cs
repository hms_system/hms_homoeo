﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class HeadNurse_Observation_list : System.Web.UI.Page
{
    Connection c = new Connection();
   
    //int pid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            c.getcon();
            bindgrid();
            c.con.Close();
        }
    }
     
    public void bindgrid()
    {
    //pid = Convert.ToInt32(Request.QueryString["PID"].ToString());

        string str_patient = "select * from patient_details p inner join Observation_details o on p.cardno=o.obv_patientid where date_of_obsv=Convert(datetime,'" + DateTime.Now.Date + "',105) ";

        SqlCommand cmd_patient = new SqlCommand(str_patient, c.con);
        DataTable dt_patient = new DataTable();
        SqlDataAdapter da_patient = new SqlDataAdapter(cmd_patient);
        da_patient.Fill(dt_patient);
        if (dt_patient.Rows.Count > 0)
        {

            GridView1.DataSource = dt_patient;
            GridView1.DataBind();
        }

  
   
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
      
       
        if (e.CommandName == "SelectButton")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];
            Response.Redirect("View_observation.aspx?OID=" + row.Cells[0].Text);
        }
   
           
    }


    protected void Button1_Click(object sender, EventArgs e)
    {

    }
}