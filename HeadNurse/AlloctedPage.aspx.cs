﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class HeadNurse_AlloctedPage : System.Web.UI.Page
{
    Connection c = new Connection();
    int bid = 0;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            int bid = Convert.ToInt32(Request.QueryString["Bedid"].ToString());
         Session["bid"]=bid;
            bindlabel();

            if (lbl_status.Text == "Treatment Completed")
               mulview_headnurse.ActiveViewIndex = 1;
                 
           else
                mulview_headnurse.ActiveViewIndex = 0;
           


        }
    }
    public void bindlabel()
    {
        c.getcon();
         bid = Convert.ToInt32(Session["bid"]);

        SqlCommand cmd_pat = new SqlCommand("select p.cardno,p.patient_name,p.dob,p.gender,c.bed_id from Patient_details p inner join Bed_allocation c on p.cardno=c.patient_id where c.bed_id ='" + bid + "' and c.timeout=' ' ", c.con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            String name = Convert.ToString(dt_pat.Rows[0][1]);
            String pid = Convert.ToString(dt_pat.Rows[0][0]);
            String bedid = Convert.ToString(dt_pat.Rows[0][4]);
            //string status = Convert.ToString(dt_pat.Rows[0][4]);
            DateTime dob = Convert.ToDateTime(dt_pat.Rows[0][2]);
            String gender = Convert.ToString(dt_pat.Rows[0][3]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
            lbl_age.Text = age.ToString();
            lbl_gen.Text = gender;
            lbl_patno.Text = pid;
            lbl_pname.Text = name;
            //lbl_status.Text = status;
        }
        c.con.Close();
    }
   
    protected void link_observ_Click(object sender, EventArgs e)
    {
        mulview_headnurse.ActiveViewIndex = 0;
    }
    protected void link_dis_Click(object sender, EventArgs e)
    {
        mulview_headnurse.ActiveViewIndex = 1;
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getcon();
        String pno = lbl_patno.Text;
       // int bid = Convert.ToInt32(Request.QueryString["Bedid"].ToString());
        bid = Convert.ToInt32(Session["bid"]);
        String s = "insert into IPObservationDetails values('" + pno + "','" + bid + "','" + txt_obspart.Text + "','" + txt_observ.Text + "','" + txt_rem.Text + "','" + Session["id"] + "',Convert(datetime,'" + DateTime.Now+ "',103))";
        SqlCommand cmds = new SqlCommand(s, c.con);
        cmds.ExecuteNonQuery();

        c.con.Close();
        txt_obspart.Text = " ";
        txt_observ.Text = " ";
        txt_rem.Text = " ";

    }
    protected void btn_dis_Click(object sender, EventArgs e)
    {
        c.getcon();
        btn_dis.Visible = true;
       // int bid = Convert.ToInt32(Request.QueryString["Bedid"].ToString());
        bid = Convert.ToInt32(Session["bid"]);
        String s = "update  Bed_allocation set timeout='" + DateTime.Today + "' where bed_id=@bid and patient_id=@pid and timeout=@date";
        SqlCommand cmds = new SqlCommand(s, c.con);
        cmds.Parameters.AddWithValue("@pid", lbl_patno.Text);
        cmds.Parameters.AddWithValue("@bid", bid);
        cmds.Parameters.AddWithValue("@date", ' ');
        cmds.ExecuteNonQuery();
        c.con.Close();
        Response.Redirect("HeadNurse_home.aspx");
    }
    protected void link_vit_Click(object sender, EventArgs e)
    {
        mulview_headnurse.ActiveViewIndex = 2;
        c.getcon();
        string str_vtest = "select * from Add_Attributes ";
        SqlCommand cmd_vtest = new SqlCommand(str_vtest, c.con);
        SqlDataAdapter da_vtest = new SqlDataAdapter(cmd_vtest);
        DataTable dt_vtest = new DataTable();
        da_vtest.Fill(dt_vtest);
        if (dt_vtest.Rows.Count > 0)
        {
            ddl_vitals.DataSource = dt_vtest;
            ddl_vitals.DataValueField = "investigation_id";
            ddl_vitals.DataTextField = "investigation_attribute";
            ddl_vitals.DataBind();
            ddl_vitals.Items.Insert(0, "SELECT");
        }
        c.con.Close();

    }
    protected void btn_addvit_Click(object sender, EventArgs e)
    {
        c.getcon();
        int pno = Convert.ToInt32(lbl_patno.Text);
        SqlCommand cmd_vid = new SqlCommand("select * from Add_Attributes where investigation_attribute='" + ddl_vitals.SelectedItem.Value + "' ", c.con);
        SqlDataAdapter sda_vid = new SqlDataAdapter(cmd_vid);
        DataTable dt_vid = new DataTable();
        sda_vid.Fill(dt_vid);


        int k_vid = cmd_vid.ExecuteNonQuery();

        if (dt_vid.Rows.Count > 0)
        {
            DataRow row_vid = dt_vid.Rows[dt_vid.Rows.Count - 1];

            int vid = Convert.ToInt32(row_vid[0]);
            String s = "insert into Investigation_details values('" + DateTime.Today + "','" + vid + "','" + txt_value.Text + "','','" + lbl_patno.Text + "')";
            SqlCommand cmds = new SqlCommand(s, c.con);
            cmds.ExecuteNonQuery();
        }
        c.con.Close();
        
        txt_value.Text = " ";
        BindGridView_Vitals();
    }
    public void BindGridView_Vitals()
    {
        c.getcon();
        int pno = Convert.ToInt32(lbl_patno.Text);

        SqlCommand cmd = new SqlCommand("select * from  Add_Attributes vm inner join Investigation_details v on vm.investigation_id=v.investigation_id  where patient_id=@pno  and  CAST(date AS DATE)=Convert(datetime,'" + DateTime.Today + "',105) order by invs_detail_id desc", c.con);
        cmd.Parameters.AddWithValue("@pno", pno);
       
        //cmd.Parameters.AddWithValue("@date",Convert.ToString( DateTime.Today);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Vitals.DataSource = dt;
            GridView_Vitals.DataBind();
        }
        c.con.Close();

    }
    protected void GridView_Vitals_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_Vitals.PageIndex = e.NewPageIndex;
        BindGridView_Vitals();
    }

}