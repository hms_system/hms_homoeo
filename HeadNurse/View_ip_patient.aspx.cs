﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;

public partial class HeadNurse_View_ip_patient : System.Web.UI.Page
{
    Connection c = new Connection();

    int nid = 0;
    int ipid = 0;
    protected void Page_Load(object sender, EventArgs e)
    {


        //this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;

        if (!IsPostBack)
        {
            ipid = Convert.ToInt32(Request.QueryString["IPID"].ToString());
            Session["ip"] = ipid;
            MultiView1.ActiveViewIndex = 0;
            c.getcon();

            // ViewState["ipid"] = Convert.ToInt32(Request.QueryString["IPID"].ToString());
            nid = Convert.ToInt32(Session["id"].ToString());

            //lbl_specify.Visible = false;
            //txt_specify.Visible = false;

           // btn_routineback2.Visible = false;
            //btn_back.Visible = false;




            string view_ip_patient = "select ip.admission_id, p.cardno, p.patient_name,p.dob,p.gender,p.house_name,p.street,p.contact_no,ip.date_of_admission from IPAdmission_details ip inner join  Patient_details p on ip.patient_id=p.cardno where ip.admission_id='" + ipid + "'";
            SqlCommand cmd_view_ip_patient = new SqlCommand(view_ip_patient, c.con);
            SqlDataAdapter da_view_ip_patient = new SqlDataAdapter(cmd_view_ip_patient);
            DataTable dt_view_ip_patient = new DataTable();
            da_view_ip_patient.Fill(dt_view_ip_patient);
            if (dt_view_ip_patient.Rows.Count > 0)
            {
                DataRow row1 = dt_view_ip_patient.Rows[dt_view_ip_patient.Rows.Count - 1];

                int id = Convert.ToInt32(row1[0]);
                lbl_ip_admsn_no.Text = Convert.ToString(id);
                int cardno = Convert.ToInt32(row1[1]);
                lbl_cardno.Text = Convert.ToString(row1[1]);


                string name = Convert.ToString(row1[2]);
                lbl_ip_patient_name.Text = Convert.ToString(name);

                DateTime dob = Convert.ToDateTime(row1[3]);

                int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
                lbl_age.Text = Convert.ToString(age) + "  yrs";


                string gender = Convert.ToString(row1[4]);
                lbl_gender.Text = Convert.ToString(gender);

                string hname = Convert.ToString(row1[5]);
                string place = Convert.ToString(row1[6]);
                string contact = Convert.ToString(row1[7]);

                lbl_hname.Text = Convert.ToString(hname);
                lbl_place.Text = Convert.ToString(place);
                lbl_contact.Text = Convert.ToString(contact);





                c.con.Close();
            }
        }
    }
    protected void btn_add_income_Click(object sender, EventArgs e)
    {
        c.getcon();
        //ipid = Convert.ToInt32(Request.QueryString["IPID"].ToString());
        ipid = Convert.ToInt32(Session["ip"]);
        string str_income = "update IPAdmission_details set annual_income='" + txt_annualincome.Text + "'where admission_id='" + ipid + "'";
        SqlCommand cmd_income = new SqlCommand(str_income, c.con);
        cmd_income.ExecuteNonQuery();
        c.con.Close();

    }

    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getcon();
        //ipid = Convert.ToInt32(Request.QueryString["IPID"].ToString());
        ipid = Convert.ToInt32(Session["ip"]);

        string str_select = "select patient_id from IPAdmission_details where admission_id='" + ipid + "'";
        SqlCommand cmd_select = new SqlCommand(str_select, c.con);
        SqlDataAdapter da_select = new SqlDataAdapter(cmd_select);
        DataTable dt_select = new DataTable();
        da_select.Fill(dt_select);
        if (dt_select.Rows.Count > 0)
        {
            DataRow row1 = dt_select.Rows[dt_select.Rows.Count - 1];

            int pid = Convert.ToInt32(row1[0]);

            if (ddl_relation.SelectedItem.Text == "Family")
            {
                lbl_specify.Visible = true;
                txt_specify.Visible = true;
            }

            string str_bystander = "insert into Bystander_details values('" + txt_bystander_name.Text + "','" + txt_bystander_hname.Text + "','" + txt_bystander_place.Text + "','" + txt_bystander_district.Text + "','" + txt_bystander_state.Text + "','" + txt_bystander_pin.Text + "','" + txt_bystander_contact.Text + "','" + ddl_relation.SelectedItem.Text + "','" + txt_specify.Text + "','" + pid + "')";
            SqlCommand cmd_bystander = new SqlCommand(str_bystander, c.con);
            cmd_bystander.ExecuteNonQuery();




        }
        c.con.Close();
    }

    protected void ddl_relation_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddl_relation.SelectedItem.Text == "Family")
        {
            lbl_specify.Visible = true;
            txt_specify.Visible = true;
        }
    }

    //protected void lbtn_bystander_Click(object sender, EventArgs e)
    //{
    //    MultiView1.ActiveViewIndex = 1;
    //}
    public void gridbind()
    {
        // c.getcon();
        //ipid = Convert.ToInt32(Request.QueryString["IPID"].ToString());
        ipid = Convert.ToInt32(Session["ip"]);

        string str_diag = "select d.Cons_Id, d.diagnosis from Diagnosis d  inner join IPAdmission_details ip on ip.patient_id=d.cardno where ip.admission_id='" + ipid + "' ";
        SqlCommand cmd_diag = new SqlCommand(str_diag, c.con);
        SqlDataAdapter da_diag = new SqlDataAdapter(cmd_diag);
        DataTable dt_diag = new DataTable();
        da_diag.Fill(dt_diag);
        if (dt_diag.Rows.Count > 0)
        {
            // DataRow row_med = dt_med.Rows[dt_med.Rows.Count - 1];

            String diag = Convert.ToString(dt_diag.Rows[0][1]);
            lbl_disp_dis_diagnosis.Text = Convert.ToString(diag);

            int d_id = Convert.ToInt32(dt_diag.Rows[0][0]);
            string str_med = "select * from  Prescription pr   where pr.diag_id='" + d_id + "' ";
            SqlCommand cmd_med = new SqlCommand(str_med, c.con);
            SqlDataAdapter da_med = new SqlDataAdapter(cmd_med);
            DataTable dt_med = new DataTable();
            da_med.Fill(dt_med);
            if (dt_med.Rows.Count > 0)
            {

                GridView2.DataSource = dt_med;
                GridView2.DataBind();
            }
        }

    }



    protected void btn_nurse_add_Click(object sender, EventArgs e)
    {
        c.getcon();
        //int id = Convert.ToInt32(Session["id"]);
        //ipid = Convert.ToInt32(Request.QueryString["IPID"].ToString());
        ipid = Convert.ToInt32(Session["ip"]);

        string str_ip_med = "insert into Medicine_log values('" + txt_ip_medicin_name.Text + "','" + txt_ip_dosage.Text + "','" + nid + "',Convert(datetime,'" + DateTime.Now + "',103),'" + ipid + "')";
        SqlCommand cmd_ip_med = new SqlCommand(str_ip_med, c.con);
        cmd_ip_med.ExecuteNonQuery();

        string str_ip_disp_medicine = "select medicin_name,dose,time from Medicine_log where patient_id='" + ipid + "' order by medicin_id desc ";
        SqlCommand cmd_disp_medicine = new SqlCommand(str_ip_disp_medicine, c.con);
        DataTable dt_disp_medicine = new DataTable();
        SqlDataAdapter da_disp_parameter = new SqlDataAdapter(cmd_disp_medicine);
        da_disp_parameter.Fill(dt_disp_medicine);
        if (dt_disp_medicine.Rows.Count > 0)
        {
            GridView3.DataSource = dt_disp_medicine;
            GridView3.DataBind();
        }
        c.con.Close();
        //btn_back.Visible = true;

    }

    //protected void btn_back1_Click(object sender, EventArgs e)
    //{
    //    MultiView1.ActiveViewIndex = 0;
    //}
    //protected void btn_medback_Click(object sender, EventArgs e)
    //{
    //    MultiView1.ActiveViewIndex = 0;

    //}
    //protected void btn_chkback_Click(object sender, EventArgs e)
    //{
    //    MultiView1.ActiveViewIndex = 0;

    //}
    //protected void btn_discharge_Click(object sender, EventArgs e)
    //{
    //    MultiView1.ActiveViewIndex = 0;

    //}
    protected void btn_routinetest_Click(object sender, EventArgs e)
    {
        c.getcon();
        //ipid = Convert.ToInt32(Request.QueryString["IPID"].ToString());
        ipid = Convert.ToInt32(Session["ip"]);

        //int ipid = 0;
        // ipid = Convert.ToInt32(ViewState["ipid"]);
        int id = Convert.ToInt32(Session["id"]);
        string str_invest = "insert into Routine_checkup values('" + txt_routinetest.Text + "','" + txt_routinevalue.Text + "','" + ipid + "',Convert(datetime,'" + DateTime.Now + "',103))";
        SqlCommand cmd_invest = new SqlCommand(str_invest, c.con);
        cmd_invest.ExecuteNonQuery();


        //string testid=Convert.ToString(ddl_test_attributes.SelectedItem.Value);
        string str_rtest = "select * from Routine_checkup  where ipid='" + ipid + "' order by routinedate";
        SqlCommand cmd_rtest = new SqlCommand(str_rtest, c.con);
        DataTable dt_rtest = new DataTable();
        SqlDataAdapter da_test = new SqlDataAdapter(cmd_rtest);
        da_test.Fill(dt_rtest);
        if (dt_rtest.Rows.Count > 0)
        {

            GridView4.DataSource = dt_rtest;

            GridView4.DataBind();
        }

        c.con.Close();
       // btn_routineback2.Visible = true;
    }
    protected void btn_vitaladd_Click(object sender, EventArgs e)
    {
        c.getcon();
        //ipid = Convert.ToInt32(ViewState["ipid"]);
        //ipid = Convert.ToInt32(Request.QueryString["IPID"].ToString());
        ipid = Convert.ToInt32(Session["ip"]);

        int id = Convert.ToInt32(Session["id"]);
        string str_invest = "insert into Investigation_details values(Convert(datetime,'" + DateTime.Now + "',103),'" + ddl_test_attributes.SelectedItem.Value + "','" + txt_test_values.Text + "','','" + ipid + "')";
        SqlCommand cmd_invest = new SqlCommand(str_invest, c.con);
        cmd_invest.ExecuteNonQuery();


        //string testid=Convert.ToString(ddl_test_attributes.SelectedItem.Value);
        string str_test = "select * from Investigation_details  inner join Add_Attributes on Investigation_details.investigation_id=Add_Attributes.investigation_id where Investigation_details.patient_id='" + ipid + "' and Add_Attributes.investigation_id='" + ddl_test_attributes.SelectedItem.Value + "' order by Investigation_details.date";
        SqlCommand cmd_test = new SqlCommand(str_test, c.con);
        DataTable dt_test = new DataTable();
        SqlDataAdapter da_test = new SqlDataAdapter(cmd_test);
        da_test.Fill(dt_test);
        if (dt_test.Rows.Count > 0)
        {

            GridView1.DataSource = dt_test;
            GridView1.DataBind();
        }

        c.con.Close();
        //btn_back.Visible = true;
    }
    //protected void btn_back_Click(object sender, EventArgs e)
    //{
    //    MultiView1.ActiveViewIndex = 0;
    //}

    protected void Button1_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 3;
        c.getcon();
        string str_vtest = "select * from Add_Attributes ";
        SqlCommand cmd_vtest = new SqlCommand(str_vtest, c.con);
        SqlDataAdapter da_vtest = new SqlDataAdapter(cmd_vtest);
        DataTable dt_vtest = new DataTable();
        da_vtest.Fill(dt_vtest);
        if (dt_vtest.Rows.Count > 0)
        {
            ddl_test_attributes.DataSource = dt_vtest;
            ddl_test_attributes.DataValueField = "investigation_id";
            ddl_test_attributes.DataTextField = "investigation_attribute";
            ddl_test_attributes.DataBind();
            ddl_test_attributes.Items.Insert(0, "SELECT");
        }
        c.con.Close();

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("IP_Admission.aspx");

    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 5;

    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
        c.getcon();
        gridbind();
        c.con.Close();

    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 4;
        //ipid = Convert.ToInt32(Request.QueryString["IPID"].ToString());
        c.getcon();

        ipid = Convert.ToInt32(Session["ip"]);

        string view_ip_patient1 = "select ip.admission_id, p.cardno, p.patient_name,p.dob,p.gender,p.house_name,p.street,p.contact_no,ip.date_of_admission from IPAdmission_details ip inner join  Patient_details p on ip.patient_id=p.cardno where ip.admission_id='" + ipid + "'";
        SqlCommand cmd_view_ip_patient1 = new SqlCommand(view_ip_patient1, c.con);
        SqlDataAdapter da_view_ip_patient1 = new SqlDataAdapter(cmd_view_ip_patient1);
        DataTable dt_view_ip_patient1 = new DataTable();
        da_view_ip_patient1.Fill(dt_view_ip_patient1);
        if (dt_view_ip_patient1.Rows.Count > 0)
        {
            DataRow row1 = dt_view_ip_patient1.Rows[dt_view_ip_patient1.Rows.Count - 1];
            {
                lbl_disp_dis_cardno.Text = Convert.ToString(row1[1]);

                string name = Convert.ToString(row1[2]);
                lbl_disp_dis_pname.Text = Convert.ToString(name);

                DateTime dob = Convert.ToDateTime(row1[3]);
                int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
                lbl_disp_dis_age.Text = Convert.ToString(age) + "  yrs";

                string gender = Convert.ToString(row1[4]);
                lbl_disp_dis_gender.Text = Convert.ToString(gender);

                string hname = Convert.ToString(row1[5]);
                string place = Convert.ToString(row1[6]);
                string contact = Convert.ToString(row1[7]);
                lbl_disp_dis_addr.Text = Convert.ToString(hname) + Convert.ToString(place);

                DateTime date_adm = Convert.ToDateTime(row1[8]);
                lbl_disp_dis_admsn_date.Text = Convert.ToString(date_adm);



                string str_diag = "select d.Cons_Id, d.diagnosis from Diagnosis d  inner join IPAdmission_details ip on ip.patient_id=d.cardno where ip.admission_id='" + ipid + "' ";
                SqlCommand cmd_diag = new SqlCommand(str_diag, c.con);
                SqlDataAdapter da_diag = new SqlDataAdapter(cmd_diag);
                DataTable dt_diag = new DataTable();
                da_diag.Fill(dt_diag);
                if (dt_diag.Rows.Count > 0)
                {
                    // DataRow row_med = dt_med.Rows[dt_med.Rows.Count - 1];

                    String diag = Convert.ToString(dt_diag.Rows[0][1]);
                    lbl_disp_dis_diagnosis.Text = Convert.ToString(diag);


                    int d_id = Convert.ToInt32(dt_diag.Rows[0][0]);
                    string str_med = "select  pr.drug_name from  Prescription pr   where pr.diag_id='" + d_id + "' ";
                    SqlCommand cmd_med = new SqlCommand(str_med, c.con);
                    SqlDataAdapter da_med = new SqlDataAdapter(cmd_med);
                    DataTable dt_med = new DataTable();
                    da_med.Fill(dt_med);
                    if (dt_med.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt_med.Rows.Count; i++)
                        {
                            string medname = Convert.ToString(dt_med.Rows[i][0]);
                            lbl_disp_dis_medcines.Text = lbl_disp_dis_medcines.Text + medname + ",";

                        }

                    }


                }

            }
        }
        c.con.Close();

    }
    protected void btn_dischrgesummary_print_Click(object sender, EventArgs e)
    {
        c.getcon();
        //ipid = Convert.ToInt32(Request.QueryString["IPID"].ToString());
        ipid = Convert.ToInt32(Session["ip"]);

        //DateTime dis_date = Convert.ToDateTime(txt_date_of_dischrge.Text);
        String str_date_dicharge = "update IPAdmission_details set date_of_discharge ='" + txt_date_of_dischrge.Text + "'  where admission_id='" + ipid + "'";
        SqlCommand cmd_date_dicharge = new SqlCommand(str_date_dicharge, c.con);
        cmd_date_dicharge.ExecuteNonQuery();

        //pdf
        //pdf generation

        //Create a Document object
        var document = new Document(PageSize.A4, 50, 50, 25, 25);

        // Create a new PdfWriter object, specifying the output stream
        var output = new MemoryStream();
        //var writer = PdfWriter.GetInstance(document, output);
        var writer = PdfWriter.GetInstance(document, new FileStream(HttpContext.Current.Server.MapPath("~/Discharge.pdf"), FileMode.Create));
        writer.PageEvent = new ITextEvents();

        //Font set
        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 14, Font.BOLD);


        //Font set
        BaseFont bfTimesText = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font timesText = new Font(bfTimes, 12, Font.NORMAL);

        BaseFont bfTimes1 = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times1 = new Font(bfTimes1, 14, Font.BOLD);


        // Open the Document for writing
        document.Open();

        // Create a new Paragraph object with the text, "Hello, World!"
        var paragph1 = new Paragraph();
        paragph1.Alignment = Element.ALIGN_CENTER;
        paragph1.SetLeading(5.0f, 3.0f);
        //welcomeParagraph.FirstLineIndent = 50f;
        paragph1.SpacingBefore = 1f;
        paragph1.SpacingAfter = 28f;

        Phrase ph0 = new Phrase("HOMOEO MEDICAL COLLEGE GOVT.HOSPITAL,KURICHY,KOTTAYAM", times);
        paragph1.Add(ph0);


        // TO DISPLAY THE DATE WITHOUT TIME
        DateTime dtt = Convert.ToDateTime(DateTime.Today);
        String c_date = dtt.ToShortDateString();




        //welcomeParagraph.FirstLineIndent = 50f;
        //var welcomeParagraph = new Paragraph(dtp.Rows[0][2].ToString(), timesText);
        //welcomeParagraph.FirstLineIndent = 50f;
        //welcomeParagrapha.SpacingAfter = 15f;
        // welcomeParagraphp.Font=Element.

        var paragph2 = new Paragraph("               CARD NO        :           ", times1);
        paragph2.Alignment = Element.ALIGN_LEFT;
        paragph2.SpacingAfter = 15f;

        Phrase ph1 = new Phrase(lbl_disp_dis_cardno.Text, timesText);
        paragph2.Add(ph1);

        var paragph3 = new Paragraph("               ADMISSION DATE               :           ", times1);
        paragph3.Alignment = Element.ALIGN_LEFT;
        paragph3.SpacingAfter = 15f;

        Phrase ph2 = new Phrase(lbl_disp_dis_admsn_date.Text, timesText);
        paragph3.Add(ph2);

        var paragph4 = new Paragraph("               NAME          :           ", times1);
        paragph4.Alignment = Element.ALIGN_LEFT;
        paragph4.SpacingAfter = 15f;

        Phrase ph3 = new Phrase(lbl_disp_dis_pname.Text, timesText);
        paragph4.Add(ph3);

        var paragph5 = new Paragraph("               AGE                  :            ", times1);
        paragph5.Alignment = Element.ALIGN_LEFT;
        paragph5.SpacingAfter = 15f;

        Phrase ph4 = new Phrase(lbl_disp_dis_age.Text, timesText);
        paragph5.Add(ph4);

        var paragph6 = new Paragraph("               ADDRESS        :            ", times1);
        paragph6.Alignment = Element.ALIGN_LEFT;
        paragph6.SpacingAfter = 15f;

        Phrase ph5 = new Phrase(lbl_disp_dis_addr.Text, timesText);
        paragph6.Add(ph5);


        var paragph7 = new Paragraph("                    DATE OF DISCHARGE   :            ", times1);
        paragph7.Alignment = Element.ALIGN_LEFT;
        paragph7.SpacingAfter = 15f;

        Phrase ph6 = new Phrase(txt_date_of_dischrge.Text, timesText);
        paragph7.Add(ph6);


        var paragph8 = new Paragraph("                  DIAGNOSIS     :            ", times1);
        paragph8.Alignment = Element.ALIGN_LEFT;
        paragph8.SpacingAfter = 15f;

        Phrase ph7 = new Phrase(lbl_disp_dis_diagnosis.Text, timesText);
        paragph8.Add(ph7);



        var paragph9 = new Paragraph("                   MEDICINES      :            ", times1);
        paragph9.Alignment = Element.ALIGN_LEFT;
        paragph9.SpacingAfter = 15f;

        Phrase ph8 = new Phrase(lbl_disp_dis_medcines.Text, timesText);
        paragph9.Add(ph8);





        var paragph10 = new Paragraph("                 ADVICE         :            ", times1);
        paragph10.Alignment = Element.ALIGN_LEFT;
        paragph10.SpacingAfter = 15f;

        Phrase ph9 = new Phrase(ddl_dis_advice.SelectedItem.Text, timesText);
        paragph10.Add(ph9);



        var paragph11 = new Paragraph("             REASON FOR DISCHARGE     :            ", times1);
        paragph11.Alignment = Element.ALIGN_LEFT;
        paragph11.SpacingAfter = 15f;

        Phrase ph10 = new Phrase(ddl_dischrge_reason.SelectedItem.Text, timesText);
        paragph11.Add(ph10);


        var paragph12 = new Paragraph("                     NAME OF DISCHARGING DOCTOR    :            ", times1);
        paragph12.Alignment = Element.ALIGN_LEFT;
        paragph12.SpacingAfter = 15f;

        Phrase ph11 = new Phrase(txt_dis_dr.Text, timesText);
        paragph12.Add(ph10);



        var paragph_line = new Paragraph();
        paragph_line.Alignment = Element.ALIGN_CENTER;
        paragph_line.SpacingBefore = 0.3f;
        Phrase ph_line = new Phrase("----------------------------------------------------------------------------------------------------------");
        paragph_line.Add(ph_line);

        //Phrase ph11 = new Phrase(dtfvb.Rows[0][11].ToString(), timesText);

        //welcomeParagraphl.Add(ph11);

        // Add the Paragraph object to the document
        document.Add(paragph1);
        document.Add(paragph_line);
        //document.Add(welcomeParagraph);

        document.Add(paragph2);
        // document.Add(welcomeParagraph1);
        document.Add(paragph3);
        //document.Add(welcomeParagraph2);
        document.Add(paragph4);

        document.Add(paragph5);

        document.Add(paragph6);
        document.Add(paragph7);
        document.Add(paragph8);
        document.Add(paragph9);
        document.Add(paragph10);
        document.Add(paragph11);

        document.Add(paragph_line);





        document.Close();

        string path = Server.MapPath("~/Discharge.pdf");

        showPdf(path);


        Response.Redirect(Server.MapPath("~/Discharge.pdf"));


        c.con.Close();
    }

    private void showPdf(string strS)
    {
        Response.ClearContent();
        Response.ClearHeaders();
        Response.ContentType = "application/pdf";
        Response.AddHeader("Content-Dispostion", "attachment;filename=" + strS);
        Response.TransmitFile(strS);
        Response.End();
        Response.Flush();
        Response.Clear();
    }


    protected void lbtn_bystander_Click(object sender, EventArgs e)
    {

    }
    protected void lbtn_routine_ckup_Click(object sender, EventArgs e)
    {

    }
    protected void lbtn_ward_details_Click(object sender, EventArgs e)
    {

    }
    protected void lbn_dis_sumry_Click(object sender, EventArgs e)
    {

    }
    protected void lbtn_vital_parmtr_Click(object sender, EventArgs e)
    {

    }
}