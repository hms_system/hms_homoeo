﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HeadNurse/HeadNurseMaster.master" AutoEventWireup="true" CodeFile="ChangePassword_Hnurse.aspx.cs" Inherits="HeadNurse_ChangePassword_Hnurse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     
     <link href="default.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1"  runat="server">
     <form id="Form1" runat="server">
      
     <table bgcolor="White">
            <tr>
                <td class="auto-style21">
                    <strong></strong>
                </td>
                <td class="auto-style21">
                    &nbsp;</td>
                <td class="auto-style21">
                    &nbsp;</td>
                <td class="center" colspan="2">
                    CHANGE YOUR PASSWORD</td>
                <td class="auto-style22">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style32">&nbsp;</td>
                <td class="auto-style32">&nbsp;&nbsp;&nbsp; &nbsp;</td>
                <td class="auto-style32">&nbsp;</td>
                <td class="auto-style37">
                    <span class="auto-style36">
                    <asp:Label ID="lbl_title" runat="server" CssClass="label_small"></asp:Label>
                </td>
                <td class="auto-style30"></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style32">&nbsp;</td>
                <td class="auto-style32">&nbsp;</td>
                <td class="auto-style32">&nbsp;</td>
                <td class="label_small">
                    Enter Old Password:</td>
                <td class="auto-style34"><asp:TextBox ID="txt_old" runat="server" TextMode="Password" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:Label ID="lbl_old" runat="server" CssClass="label_small"></asp:Label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style18">&nbsp;</td>
                <td class="auto-style18">&nbsp;</td>
                <td class="label_small">&nbsp;</td>
                <td class="label_small">Enter New Password:</td>
                <td class="auto-style30">
                    <asp:TextBox ID="txt_new" runat="server" TextMode="Password" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
                <td>
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_new" ErrorMessage="*Please enter valid password" ForeColor="Red" ValidationExpression="[a-zA-Z0-9 ]{8,}" ValidationGroup="a" CssClass="auto-style38"></asp:RegularExpressionValidator>

                </td>
            </tr>
            <tr>
                <td class="auto-style18">&nbsp;</td>
                <td class="auto-style18">&nbsp;</td>
                <td class="label_small">&nbsp;</td>
                <td class="label_small">Retype New Password:</td>
                <td class="auto-style30">
                    <asp:TextBox ID="txt_retype" runat="server" TextMode="Password" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:Label ID="lbl_retype" runat="server" CssClass="label_small"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style32">&nbsp;</td>
                <td class="auto-style32"></td>
                <td class="auto-style32"></td>
                <td class="auto-style27"></td>
                <td class="auto-style30"></td>
                <td></td>
            </tr>
            <tr>
                <td class="auto-style32">
                    &nbsp;</td>
                <td class="auto-style32">
                    &nbsp;</td>
                <td class="auto-style32">
                    &nbsp;</td>
                <td class="auto-style29">
                    <asp:Button ID="btn_back" runat="server" OnClick="btn_back_Click" Text="BACK" CssClass="buttonyellow" />
                </td>
                <td class="auto-style31">
                    <asp:Button ID="btn_chg" runat="server" OnClick="btn_chg_Click" Text="CHANGE PASSWORD" CssClass="buttonyellow" ValidationGroup="a" />
                </td>
                <td class="auto-style19">
                    &nbsp;</td>
            </tr>
           
        </table>
         </form>
 </asp:Content>

