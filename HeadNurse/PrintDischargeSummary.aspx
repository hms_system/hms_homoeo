﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Doctor/DoctorMaster.master" AutoEventWireup="true" CodeFile="PrintDischargeSummary.aspx.cs" Inherits="HeadNurse_PrintDischargeSummary" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
        <table>
                        <tr>
                            <td class="center" colspan="4">DISCHARGE SUMMARY</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="lbl_dis_cardno" runat="server" CssClass="label_small" Text="Card No."></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_cardno" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                <asp:Button ID="btn_search" runat="server" Text="SEARCH" OnClick="btn_search_Click" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="lbl_dispatname" runat="server" CssClass="label_small" Text="Name of Patient"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_pname" runat="server" CssClass="label_small"></asp:Label>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_disage" runat="server" Text="Age" CssClass="label_small"></asp:Label>

                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_age" runat="server" CssClass="label_small"></asp:Label>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_disgender" runat="server" Text="Gender" CssClass="label_small"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_gender" runat="server" CssClass="label_small"></asp:Label>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_dis_address" runat="server" Text="Address" CssClass="label_small"></asp:Label>

                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_addr" runat="server" CssClass="label_small"></asp:Label>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Label ID="Label5" runat="server" Text="Date of Admission" CssClass="label_small"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_admsn_date" runat="server" CssClass="label_small"></asp:Label>
                                <br />

                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <%--<cc1:MaskedEditExtender ID="txt_date_of_dischrge_MaskedEditExtender" runat="server" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txt_date_of_dischrge" UserDateFormat="DayMonthYear" InputDirection="RightToLeft" ClearTextOnInvalid="True">
                                                    </cc1:MaskedEditExtender>--%>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Label ID="Label7" runat="server" Text="Provisional Diagonosis" CssClass="label_small"></asp:Label>

                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_diagnosis" runat="server" CssClass="label_small"></asp:Label>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="Label8" runat="server" Text="Medicines" CssClass="label_small"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbl_disp_dis_medcines" runat="server" CssClass="label_small"></asp:Label>
                                <br />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Label ID="Label9" runat="server" CssClass="label_small" Text="Advice"></asp:Label>

                            </td>
                            <td>
                                <asp:DropDownList ID="ddl_dis_advice" runat="server" AutoPostBack="True" CssClass="search_categories">
                                    <asp:ListItem Value="0">SELECT</asp:ListItem>
                                    <asp:ListItem Value="1">abc</asp:ListItem>
                                    <asp:ListItem Value="2">xyz</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="Label10" runat="server" CssClass="label_small" Text="Reason For Discharge"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddl_dischrge_reason" runat="server" AutoPostBack="True" CssClass="search_categories">
                                    <asp:ListItem>select</asp:ListItem>
                                    <asp:ListItem>Improved</asp:ListItem>
                                    <asp:ListItem>Worst</asp:ListItem>
                                    <asp:ListItem>Discharge at request</asp:ListItem>
                                    <asp:ListItem>Discharge against advice</asp:ListItem>
                                    <asp:ListItem>Abscond</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>

                                <asp:Label ID="lbl_dis_dr" runat="server" Text="Name of Discharging Doctor" CssClass="label_small"></asp:Label>

                            </td>
                            <td>
                                <asp:TextBox ID="txt_dis_dr" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="Label6" runat="server" CssClass="label_small" Text="Date of Discharge"></asp:Label>
                            </td>
                            <td>

                                <asp:TextBox ID="txt_date_of_dischrge" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                <%--<cc1:calendarextender ID="txt_date_of_dischrge_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_date_of_dischrge">
                                </calendarextender>--%>
                                    <cc1:calendarextender ID="txt_date_of_dischrge_CalendarExtender" runat="server" format="dd/MM/yyyy" TargetControlID="txt_date_of_dischrge">
                    </cc1:calendarextender>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                </asp:ScriptManager>
                            </td>
                            <td>
                                <asp:Button ID="btn_discharge" runat="server" OnClick="btn_discharge_Click" Text="<<BACK" CssClass="buttonyellow" />
                            </td>
                            <td>
                                <asp:Button ID="btn_dischrgesummary_print" runat="server" CssClass="buttonyellow" OnClick="btn_dischrgesummary_print_Click" Text="PRINT" />
                            </td>
                        </tr>
                    </table>
        </form>
            </asp:Content>

