﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class HeadNurse_NursesSchedule : System.Web.UI.Page
{
    Connection c = new Connection();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            c.getcon();
            SqlCommand cmd_dateout = new SqlCommand("delete from Nurse_Schedule where dateout<Convert(datetime,'" + DateTime.Today + "',103)", c.con);
            cmd_dateout.ExecuteNonQuery();

            bindnurse();

            bindtimeslot();
            gridbind();
            c.con.Close();    
        }
    }

    public void bindnurse()
    {
        
        string str_nurse_name = "select * from Employee_details where  typeid='7' and empid not in (select nurse_empid from Nurse_Schedule where datein  in (Convert(datetime,'" + DateTime.Today + "',103) ))";
        SqlCommand cmd_nurse_name = new SqlCommand(str_nurse_name, c.con);
        SqlDataAdapter da_nurse_name = new SqlDataAdapter(cmd_nurse_name);
        DataTable dt_nurse_name = new DataTable();
        da_nurse_name.Fill(dt_nurse_name);
        if (dt_nurse_name.Rows.Count > 0)
        {
            ddl_nurse_name.DataSource = dt_nurse_name;
            ddl_nurse_name.DataValueField = "empid";
            ddl_nurse_name.DataTextField = "empname";
            ddl_nurse_name.DataBind();
            ddl_nurse_name.Items.Insert(0, "SELECT");
        }
        
    }


    public void bindtimeslot()
    {
         string str_time = "select * from Time_SlotMaster";
        SqlCommand cmd_time = new SqlCommand(str_time, c.con);
        SqlDataAdapter da_time = new SqlDataAdapter(cmd_time);
        DataTable dt_time = new DataTable();
        da_time.Fill(dt_time);
        if (dt_time.Rows.Count > 0)
        {
            ddl_timeslot.DataSource = dt_time;
            ddl_timeslot.DataValueField = "time_id";
            ddl_timeslot.DataTextField = "timeslot";
            ddl_timeslot.DataBind();
            ddl_timeslot.Items.Insert(0, "SELECT");
        }
        
    }

    public void gridbind()
    {

        string str_nurse_bind = "select n.nurse_schedule_id, e.empid,e.empname,n.datein,n.dateout,t.timeslot  from Employee_details e inner join    Nurse_Schedule n   on n.nurse_empid=e.empid inner join  Time_SlotMaster t on t.time_id=n.time_id where e.typeid='7'";
        SqlCommand cmd_nurse_bind= new SqlCommand(str_nurse_bind, c.con);
        DataTable dt_nurse_bind = new DataTable();
        SqlDataAdapter da_nurse_bind = new SqlDataAdapter(cmd_nurse_bind);
        da_nurse_bind.Fill(dt_nurse_bind);
        if (dt_nurse_bind.Rows.Count > 0)
        {
            lbl_no.Visible = false; 
            GridView2.Visible = true;
            GridView2.DataSource = dt_nurse_bind;
            GridView2.DataBind();
        }
        else
        {
            lbl_no.Visible = true;//TO REMOVE REMAING ONE VALE IN THE GRID
            GridView2.Visible = false;
        }

    
    }

    protected void btn_add_nurse_schedule_Click(object sender, EventArgs e)
    {
        c.getcon();
           //dateout1=Convert(datetime,'" +  + "',103);
        DateTime dateout = Convert.ToDateTime(txt_date.Text).AddDays(7);
   
        string nurse_schedule = "insert into Nurse_Schedule values(Convert(datetime,'" + dateout + "',103),Convert(datetime,'" + txt_date.Text + "',103),'" + ddl_timeslot.SelectedItem.Value + "','" + ddl_nurse_name.SelectedItem.Value + "')";
        SqlCommand cmd_nurse_schedule = new SqlCommand(nurse_schedule, c.con);
        cmd_nurse_schedule.ExecuteNonQuery();
        
        gridbind();
        bindnurse();
        bindtimeslot();
        c.con.Close();
    }
    
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        c.getcon();
        ImageButton im = (ImageButton)sender;
        GridViewRow grv = im.NamingContainer as GridViewRow;

        int id = Convert.ToInt32(GridView2.DataKeys[grv.RowIndex].Values[0].ToString());
        
        string str_nurse_delete = "delete from Nurse_Schedule where nurse_schedule_id='" + id + "'";
        SqlCommand cmd_nurse_delete = new SqlCommand(str_nurse_delete, c.con);
        cmd_nurse_delete.ExecuteNonQuery();
        GridView2.EditIndex = -1;
        
        gridbind();
        bindnurse();
        bindtimeslot();
        c.con.Close();
    }
}