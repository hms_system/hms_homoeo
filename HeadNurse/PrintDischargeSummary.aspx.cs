﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;

public partial class HeadNurse_PrintDischargeSummary : System.Web.UI.Page
{
    Connection c = new Connection();
    int ipid = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
        }
    }
    protected void btn_dischrgesummary_print_Click(object sender, EventArgs e)
    {
        //pdf
        //pdf generation

        //Create a Document object
        var document = new Document(PageSize.A4, 50, 50, 25, 25);

        // Create a new PdfWriter object, specifying the output stream
        var output = new MemoryStream();
        //var writer = PdfWriter.GetInstance(document, output);
        var writer = PdfWriter.GetInstance(document, new FileStream(HttpContext.Current.Server.MapPath("~/PrintDischarge.pdf"), FileMode.Create));
        writer.PageEvent = new ITextEvents();

        //Font set
        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 14, Font.BOLD);


        //Font set
        BaseFont bfTimesText = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font timesText = new Font(bfTimes, 12, Font.NORMAL);

        BaseFont bfTimes1 = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times1 = new Font(bfTimes1, 14, Font.BOLD);


        // Open the Document for writing
        document.Open();

        //Image 


        string imageurl = Server.MapPath(".") + "/images/gov.png";
        iTextSharp.text.Image png = iTextSharp.text.Image.GetInstance(imageurl);

        png.Alignment = Element.ALIGN_CENTER;
        png.ScaleToFit(140f, 120f);
        png.SpacingBefore = 30f;
        png.SpacingAfter = 1f;
        document.Add(png);



           


        // Create a new Paragraph object with the text, "Hello, World!"
        var paragph1 = new Paragraph();
        paragph1.Alignment = Element.ALIGN_CENTER;
        paragph1.SetLeading(5.0f, 3.0f);
        //welcomeParagraph.FirstLineIndent = 50f;
        paragph1.SpacingBefore = 1f;
        paragph1.SpacingAfter = 28f;

        Phrase ph0 = new Phrase("HOMOEO MEDICAL COLLEGE GOVT.HOSPITAL,KURICHY,KOTTAYAM", times);
        paragph1.Add(ph0);


        // TO DISPLAY THE DATE WITHOUT TIME
        DateTime dtt = Convert.ToDateTime(lbl_disp_dis_admsn_date.Text);
        String c_date = dtt.ToShortDateString();


        var paragph_line = new Paragraph();
        paragph_line.Alignment = Element.ALIGN_CENTER;
        paragph_line.SpacingBefore = 0.3f;
        Phrase ph_line = new Phrase("----------------------------------------------------------------------------------------------------------");
        paragph_line.Add(ph_line);
     


        //welcomeParagraph.FirstLineIndent = 50f;
        //var welcomeParagraph = new Paragraph(dtp.Rows[0][2].ToString(), timesText);
        //welcomeParagraph.FirstLineIndent = 50f;
        //welcomeParagrapha.SpacingAfter = 15f;
        // welcomeParagraphp.Font=Element.

        var paragph2 = new Paragraph("                      CARD NO                                      :           ", times1);
        paragph2.Alignment = Element.ALIGN_LEFT;
        paragph2.SpacingAfter = 15f;

        Phrase ph1 = new Phrase(txt_cardno.Text, timesText);
        paragph2.Add(ph1);

        var paragph3 = new Paragraph("                      ADMISSION DATE                     :           ", times1);
        paragph3.Alignment = Element.ALIGN_LEFT;
        paragph3.SpacingAfter = 15f;    

        Phrase ph2 = new Phrase(c_date, timesText);
        paragph3.Add(ph2);

        var paragph4 = new Paragraph("                      NAME                                           :           ", times1);
        paragph4.Alignment = Element.ALIGN_LEFT;
        paragph4.SpacingAfter = 15f;

        Phrase ph3 = new Phrase(lbl_disp_dis_pname.Text, timesText);
        paragph4.Add(ph3);

        var paragph5 = new Paragraph("                      AGE                                             :            ", times1);
        paragph5.Alignment = Element.ALIGN_LEFT;
        paragph5.SpacingAfter = 15f;

        Phrase ph4 = new Phrase(lbl_disp_dis_age.Text, timesText);
        paragph5.Add(ph4);

        var paragph6 = new Paragraph("                      ADDRESS                                    :            ", times1);
        paragph6.Alignment = Element.ALIGN_LEFT;
        paragph6.SpacingAfter = 15f;

        Phrase ph5 = new Phrase(lbl_disp_dis_addr.Text, timesText);
        paragph6.Add(ph5);


        var paragph7 = new Paragraph("                      DATE OF DISCHARGE             :            ", times1);
        paragph7.Alignment = Element.ALIGN_LEFT;
        paragph7.SpacingAfter = 15f;

        Phrase ph6 = new Phrase(txt_date_of_dischrge.Text, timesText);
        paragph7.Add(ph6);


        var paragph8 = new Paragraph("                      DIAGNOSIS                                   :            ", times1);
        paragph8.Alignment = Element.ALIGN_LEFT;
        paragph8.SpacingAfter = 15f;

        Phrase ph7 = new Phrase(lbl_disp_dis_diagnosis.Text, timesText);
        paragph8.Add(ph7);



        var paragph9 = new Paragraph("                      MEDICINES                                  :            ", times1);
        paragph9.Alignment = Element.ALIGN_LEFT;
        paragph9.SpacingAfter = 15f;

        Phrase ph8 = new Phrase(lbl_disp_dis_medcines.Text, timesText);
        paragph9.Add(ph8);





        var paragph10 = new Paragraph("                     ADVICE                                         :            ", times1);
        paragph10.Alignment = Element.ALIGN_LEFT;
        paragph10.SpacingAfter = 15f;

        Phrase ph9 = new Phrase(ddl_dis_advice.SelectedItem.Text, timesText);
        paragph10.Add(ph9);



        var paragph11 = new Paragraph("                     REASON FOR DISCHARGE                         :            ", times1);
        paragph11.Alignment = Element.ALIGN_LEFT;
        paragph11.SpacingAfter = 15f;

        Phrase ph10 = new Phrase(ddl_dischrge_reason.SelectedItem.Text, timesText);
        paragph11.Add(ph10);


        var paragph12 = new Paragraph("                     NAME OF DISCHARGING DOCTOR                  :            ", times1);
        paragph12.Alignment = Element.ALIGN_LEFT;
        paragph12.SpacingAfter = 15f;

        Phrase ph11 = new Phrase(txt_dis_dr.Text, timesText);
        paragph12.Add(ph11);



        //Phrase ph11 = new Phrase(dtfvb.Rows[0][11].ToString(), timesText);

        //welcomeParagraphl.Add(ph11);

        // Add the Paragraph object to the document
        document.Add(paragph1);
        document.Add(paragph_line);
        //document.Add(welcomeParagraph);

        document.Add(paragph2);
        // document.Add(welcomeParagraph1);
        document.Add(paragph3);
        //document.Add(welcomeParagraph2);
        document.Add(paragph4);

        document.Add(paragph5);

        document.Add(paragph6);
        document.Add(paragph7);
        document.Add(paragph8);
        document.Add(paragph9);
        document.Add(paragph10);
        document.Add(paragph11);
        document.Add(paragph12);

        document.Add(paragph_line);





        document.Close();

        string path = Server.MapPath("~/PrintDischarge.pdf");

        showPdf(path);


        Response.Redirect(Server.MapPath("~/PrintDischarge.pdf"));


        c.con.Close();

    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        c.getcon();
        int pid = Convert.ToInt32(txt_cardno.Text);

        string str_lblbind = "select p.patient_name,p.dob,p.gender,p.house_name,p.street,p.contact_no ,ip.date_of_admission,ip.admission_id from Patient_details p inner join IPAdmission_details ip on ip.patient_id=p.cardno   where ip.status='Treatment Completed' and p.cardno='" + pid + "' ";
        SqlCommand cmd_lblbind = new SqlCommand(str_lblbind, c.con);
        SqlDataAdapter da_lblbind = new SqlDataAdapter(cmd_lblbind);
        DataTable dt_lblbind = new DataTable();
        da_lblbind.Fill(dt_lblbind);
        cmd_lblbind.ExecuteNonQuery();
       if(dt_lblbind.Rows.Count>0)
        {
            DataRow row1 = dt_lblbind.Rows[dt_lblbind.Rows.Count - 1];
                {
                    
                    string name = Convert.ToString(row1[0]);
                    lbl_disp_dis_pname.Text = Convert.ToString(name);

                    DateTime dob = Convert.ToDateTime(row1[1]);
                    int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
                    lbl_disp_dis_age.Text = Convert.ToString(age) + "  yrs";

                    string gender = Convert.ToString(row1[2]);
                    lbl_disp_dis_gender.Text = Convert.ToString(gender);

                    string hname = Convert.ToString(row1[3]);
                    string place = Convert.ToString(row1[4]);
                    string contact = Convert.ToString(row1[5]);
                    lbl_disp_dis_addr.Text = Convert.ToString(hname) + Convert.ToString(place);


                    
                    

                    DateTime date_adm = Convert.ToDateTime(row1[6]);
                    String adm_date = date_adm.ToShortDateString();
                    lbl_disp_dis_admsn_date.Text = Convert.ToString(adm_date);

                    int admsnid = Convert.ToInt32(row1[7]);

                     string str_diag = "select d.Cons_Id, d.diagnosis from Diagnosis d  inner join IPAdmission_details ip on ip.patient_id=d.cardno where ip.admission_id='" + admsnid + "' ";
                    SqlCommand cmd_diag = new SqlCommand(str_diag, c.con);
                    SqlDataAdapter da_diag = new SqlDataAdapter(cmd_diag);
                    DataTable dt_diag = new DataTable();
                    da_diag.Fill(dt_diag);
                    if (dt_diag.Rows.Count > 0)
                    {
                        // DataRow row_med = dt_med.Rows[dt_med.Rows.Count - 1];

                        String diag = Convert.ToString(dt_diag.Rows[0][1]);
                        lbl_disp_dis_diagnosis.Text = Convert.ToString(diag);

                        int d_id = Convert.ToInt32(dt_diag.Rows[0][0]);
                        string str_med = "select  pr.drug_name from  Prescription pr   where pr.diag_id='" + d_id + "' ";
                        SqlCommand cmd_med = new SqlCommand(str_med, c.con);
                        SqlDataAdapter da_med = new SqlDataAdapter(cmd_med);
                        DataTable dt_med = new DataTable();
                        da_med.Fill(dt_med);
                        if (dt_med.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt_med.Rows.Count; i++)
                            {
                                string medname = Convert.ToString(dt_med.Rows[i][0]);
                                lbl_disp_dis_medcines.Text = lbl_disp_dis_medcines.Text + medname + ",";

                            }

                        }


                    }

                }
            }

            c.con.Close();


        }


    protected void btn_discharge_Click(object sender, EventArgs e)
    {
        Response.Redirect("IPAdmission_list.aspx");

            }

   
 private void showPdf(string strS)
    {
        Response.ClearContent();
        Response.ClearHeaders();
        Response.ContentType = "application/pdf";
        Response.AddHeader("Content-Dispostion", "attachment;filename=" + strS);
        Response.TransmitFile(strS);
        Response.End();
        Response.Flush();
        Response.Clear();
    }

}
