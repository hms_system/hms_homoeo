﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HeadNurse/HeadNurseMaster.master" AutoEventWireup="true" CodeFile="NursesSchedule.aspx.cs" Inherits="HeadNurse_NursesSchedule" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        
         
         
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <form runat="server">
        <center>
    <table>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="8">&nbsp;</td>
            <td>&nbsp;</td>
            <td class="auto-style4">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <asp:Label ID="lbl_nurse_name" runat="server" Text="Name:" CssClass="label_small" ></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddl_nurse_name" runat="server" AutoPostBack="True"  CssClass="search_categories">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                               <asp:Label ID="lbl_timeslot" runat="server" Text="Time Slot:" CssClass="label_small"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddl_timeslot" runat="server" AutoPostBack="True"  CssClass="search_categories">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Date" CssClass="label_small" ></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_date" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                <cc1:CalendarExtender ID="txt_date_CalendarExtender" Format="dd/MM/yyyy" runat ="server" Enabled="True" TargetControlID="txt_date">
                
                </cc1:CalendarExtender>
            </td>
            <td>
                &nbsp;</td>
            <td>
                               <asp:Button ID="btn_add_nurse_schedule" runat="server" OnClick="btn_add_nurse_schedule_Click" Text="ADD"  CssClass="buttonyellow" />
                           </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                               &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="auto-style4">
                               &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1"></td>
            <td class="auto-style1"></td>
            <td colspan="8" class="auto-style1">
                <asp:Label ID="lbl_no" runat="server" CssClass="label_danger" ForeColor="Red" Text="ADD NEW SCHEDULE!!!" Visible="False" Width="300px"></asp:Label>
            </td>
            <td class="auto-style1">
                </td>
            <td class="auto-style1"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="8">
                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" BackColor="White" Width="500px" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" DataKeyNames="nurse_schedule_id" CssClass="auto-style2" BorderStyle="None">
                    <Columns>
                        <asp:BoundField DataField="empid" HeaderText="NO." />
                        <asp:BoundField DataField="empname" HeaderText="Name" />
                        <asp:BoundField DataField="datein" HeaderText="Date From" />
                        <asp:BoundField DataField="dateout" HeaderText="Date To" />
                        <asp:BoundField DataField="timeslot" HeaderText="Time Slot" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" OnClick="ImageButton1_Click" ImageUrl="~/HeadNurse/images/delete.png" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" ForeColor="White" Font-Bold="True" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                </asp:GridView>
            </td>
            <td>
                            </td>
            <td class="auto-style4">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>s</td>
            <td colspan="8">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>&nbsp;</td>
            <td class="auto-style4">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
        </center>
</form>
</asp:Content>


