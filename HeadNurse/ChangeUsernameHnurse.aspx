﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HeadNurse/HeadNurseMaster.master" AutoEventWireup="true" CodeFile="ChangeUsernameHnurse.aspx.cs" Inherits="HeadNurse_ChangeUsernameHnurse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="Form1" runat="server">
     
    <table class="auto-style1">
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style35">&nbsp;</td>
                <td class="auto-style29">&nbsp;</td>
                <td class="auto-style15">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style36" colspan="2"><strong>CHANGE USERNAME</strong></td>
                <td class="auto-style21">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style37"><strong>Enter New UserName:</strong></td>
                <td class="auto-style28">
                    <asp:TextBox ID="txt_newusername" runat="server" Height="22px" Width="174px" CssClass="auto-style44"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_newusername" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="a"></asp:RequiredFieldValidator>

                    <br />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_newusername" ErrorMessage="*Please enter valid name" ForeColor="Red" ValidationExpression="[a-zA-Z]{1,}" ValidationGroup="a"></asp:RegularExpressionValidator>

                </td>
                <td rowspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style30"></td>
                <td class="auto-style30"></td>
                <td class="auto-style38"><strong>Enter Password:</strong></td>
                <td class="auto-style31">
                    <asp:TextBox ID="txt_password" runat="server" Height="22px" Width="176px" CssClass="auto-style44"></asp:TextBox>
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style43">
                    <strong>
                    <asp:Label ID="lbl_title" runat="server" CssClass="auto-style22"></asp:Label>
                    </strong>
                </td>
                <td class="auto-style45">&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style20">&nbsp;</td>
                <td class="auto-style40"><strong>
                    <asp:Button ID="btn_back" runat="server" OnClick="btn_back_Click" Text="&lt;&lt;BACK" Width="87px" CssClass="auto-style39" Height="21px" />
                    </strong></td>
                <td class="auto-style41"><strong>
                    <asp:Button ID="btn_changeusername" runat="server" OnClick="btn_changeusername_Click" Text="CHANGE USERNAME" CssClass="auto-style39" Height="30px" Width="150px" ValidationGroup="a" />
                    </strong></td>
                <td></td>
            </tr>
        </table>
        </form>
</asp:Content>

